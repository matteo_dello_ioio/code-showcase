﻿app.controller('homeController', ['$scope', '$state', 'applicationGlobals', 'resourceMapper', function($scope, $state, applicationGlobals, resourceMapper) {
    applicationGlobals.breadcrumb.init([
        ['Home']
    ]);
    $scope.resourceMapper = resourceMapper;
    
}]);