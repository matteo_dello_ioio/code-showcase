﻿
app.factory('GeoDataLookupDataService', ['$http', '$q', 'appDataHelper', function ($http, $q, appDataHelper) {

    // interface
    var service = {
        serviceBaseUrl: '/api/lookupapi/',
        getCityById: getCityById,
        getCitiesByName: getCitiesByName,
        getZonesByCityID: getZonesByCityID
    };
    return service;


    // implementation
    function getCityById(callId, id) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetTownFromCode', { code: id })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getCitiesByName(callId, name) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetCitiesByName', { name: name })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getZonesByCityID(callId, city_id) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetZonesByCityID', { cityId: city_id })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }
}]);

/* HINT: this service can be used to share Estate data between controllers */
app.service('GeoDataEditingDataService', ['$rootScope', function ($rootScope) {

    return {
        data: {
            city: null,
            zone: null,
            address: null,
            postalCode: null,
            latitude: null,
            longitude: null
        },

        setData: function (data) {
            if (!data)
                throw new Error("geo data cannot be null");
            this.data = data;
            $rootScope.$broadcast('GeoDataUpdated');
        },

        getData: function(){
            return this.data;
        }
    };
}]);