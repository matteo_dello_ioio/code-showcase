﻿
app.controller('GeoDataController', ['$rootScope', '$scope', '$timeout', 'GeoDataLookupDataService', 'GeoDataEditingDataService', 'accountManagementService', 'canEditCity',
function ($rootScope, $scope, $timeout, GeoDataLookupDataService, GeoDataEditingDataService, accountManagementService, canEditCity) {

    $scope.Lookup = {
        Cities: [],
        Zones: []
    }

    $scope.canEditCity = canEditCity;

    $scope.GeoData = GeoDataEditingDataService.getData();

    $rootScope.$on('GeoDataUpdated', function () {
        $scope.GeoData = GeoDataEditingDataService.getData();
        console.log("loaded geodata", $scope.GeoData);

        //$scope.UpdateImages();
    });

    /* write city name into the input leaving city id as value */
    $scope.formatCityLabel = function (model) {
        for (var i = 0; i < $scope.Lookup.Cities.length; i++) {
            if (model === $scope.Lookup.Cities[i].id) {
                return $scope.Lookup.Cities[i].description;
            }
        }
    };

    $scope.GetCitiesByName = function (name) {
        //console.log('executing GetCities ' + val);
        return GeoDataLookupDataService.getCitiesByName('noLoader', name)
            .then(function (resp) {
                if (!resp)
                    $scope.Lookup.Cities = [];
                else
                    $scope.Lookup.Cities = resp;
                return $scope.Lookup.Cities;
            }, function (reason) {
                console.log("Unable to retrieve cities.", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('...catch');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetCityById = function (id) {
        //console.log('executing GetCities ' + val);
        return GeoDataLookupDataService.getCityById('main', id)
            .then(function (resp) {
                return resp;
            })
            .catch(function () {
                console.log('...catch');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetZonesByCityID = function (cityID) {
        //console.log('executing GetZones cityID ', cityID);
        GeoDataLookupDataService.getZonesByCityID('main', cityID)
            .then(function (resp) {
                $scope.setZoneCollection(resp);
                return resp;
            }, function (reason) {
                console.log("Unable to retrieve zones.", reason);
                $scope.Message = "Unable to retrieve zones.";
            })
            .catch(function () {
                console.log('...catch');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.setZoneCollection = function (collection) {
        $scope.Lookup.Zones = collection;
        $timeout(function () {
            RUI.Select.replaceSelect();
            RUI.Select.refresh();
        }, 400);
    };

    $scope.InitializeMap = function () {
        if (!$scope.GeoData.latitude || !$scope.GeoData.longitude) {
            $scope.map = { center: { latitude: 41, longitude: 11.0 }, zoom: 5 };
            $scope.marker = { id: 0 };
        } else {
            $scope.map = { center: { latitude: $scope.GeoData.latitude, longitude: $scope.GeoData.longitude }, zoom: 15 };
            $scope.marker = {
                id: 0,
                coords: {
                    latitude: $scope.GeoData.latitude,
                    longitude: $scope.GeoData.longitude,
                },
                options: { draggable: true }
            };
        }

        //$scope.marker = {
        //    id: 0,
        //    coords: {
        //        latitude: 41,
        //        longitude: 11
        //    }
        //};
    };

    $scope.ToggleMarker = function () {
        if (HideAddressOnline.checked) {
            $scope.marker = {};
        } else {
            $scope.marker = {
                id: 0,
                coords: {
                    latitude: $scope.map.center.latitude,
                    longitude: $scope.map.center.longitude
                }
            };
        }
    };

    $scope.GeoCodeAddress = function () {

        //var _address = /\d{1,3}/.test(Address.value) == true ? Address.value : Address.value + " 1";
        $scope.Alerts = [];
        var city = $scope.Listing.geoData.city.townName;
        var address = $scope.Listing.geoData.address;

        if (city && address) {
            var geocodeAddress = address + " " + city;

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': geocodeAddress, 'region': 'IT' }, function (results, status) {
                switch (status) {
                    case google.maps.GeocoderStatus.OK:
                        {
                            if (results.length > 0) {
                                var addr_type = results[0].types[0];	// type of address inputted that was geocoded

                                var addressObj = null;
                                var resultIdx = -1;
                                var zones =[];
                                _.each($scope.Lookup.Zones, function (element, index, list) {
                                    zones.push(element.zone);
                                });

                                _.each(results, function (element, index, list) {
                                    var currentAddressObj = {};
                                    _.each(element.address_components, function (element, index, list) {
                                        currentAddressObj[element.types[0]] = element;
                                    });
                                    if (currentAddressObj.route && currentAddressObj.postal_code && ((currentAddressObj.locality && currentAddressObj.locality.long_name == city) || (currentAddressObj.administrative_area_level_3 && currentAddressObj.administrative_area_level_3.long_name == city)))
                                    {
                                        addressObj = currentAddressObj;
                                        resultIdx = index;
                                    }
                                    else if(currentAddressObj.route && currentAddressObj.postal_code && (currentAddressObj.locality && zones.indexOf(currentAddressObj.locality.long_name) > -1))
                                    {
                                        addressObj = currentAddressObj;
                                        resultIdx = index;
                                        $scope.Listing.geoData.zone = $scope.Lookup.Zones[zones.indexOf(currentAddressObj.locality.long_name)].id;
                                    }
                                });

                                if (addressObj == null || resultIdx == -1) {
                                    $scope.Alerts = [{ type: 'danger', msg: "Indirizzo non trovato" }];
                                }
                                else
                                {
                                    switch (addr_type) {
                                        case "street_address":
                                        case "route":
                                            {
                                                $scope.Listing.geoData.address = addressObj.route.long_name + ((addressObj.street_number) ? (' ' + addressObj.street_number.long_name) : '');
                                                $scope.Listing.geoData.postalCode = addressObj.postal_code.long_name;
                                                $scope.Listing.geoData.latitude = $scope.map.center.latitude = results[resultIdx].geometry.location.lat();
                                                $scope.Listing.geoData.longitude = $scope.map.center.longitude = results[resultIdx].geometry.location.lng();
                                                $scope.map.zoom = 15;
                                                break;
                                            }
                                        case "country":
                                        case "locality":
                                        case "political":
                                        case "postal_code":
                                        case "administrative_area_level_1":
                                        case "neighborhood":
                                            {
                                                $scope.Alerts = [{ type: 'danger', msg: "Indirizzo non trovato" }];
                                                // for these types geolocalization is not valid
                                                break;
                                            }
                                        default: $scope.map.zoom = 15;
                                    }

                                    $scope.marker = {
                                        id: 0,
                                        coords: {
                                            latitude: $scope.map.center.latitude,
                                            longitude: $scope.map.center.longitude
                                        }
                                    };
                                }
                                $scope.$apply();
                            } else {
                                $scope.Alerts = [{ type: 'danger', msg: "Inserisci un indirizzo più preciso" }];
                            }
                            break;
                        }
                    case google.maps.GeocoderStatus.ZERO_RESULTS:
                    case google.maps.GeocoderStatus.INVALID_REQUEST:
                        {
                            $scope.Alerts = [{ type: 'danger', msg: "Inserisci un indirizzo esistente" }];
                            break;
                        }
                }
            });
        }
        else
        {
            $scope.Alerts.push({ type: 'danger', msg: "Specificare città e indirizzo" });
        }
        return;
    };

    $scope.CityName = null;

    $scope.enableMapControls = function () {
        $scope.mapOptions = {
            scrollwheel: true,
            draggable: true
        };

        $scope.mapControlEnabled = true;
    };

    $scope.disableMapControls = function () {
        $scope.mapOptions = {
            scrollwheel: false,
            draggable: false
        };

        $scope.mapControlEnabled = false;
    };

    $scope.init = function () {

        $scope.$watch('Listing.geoData.city', function (newCity, oldCity) {
            if (!newCity) return;

            if ((typeof newCity) === "string") {
                $scope.setZoneCollection([]);
            }
            else {
                $scope.GetZonesByCityID(newCity.id);
            }

            if (newCity != oldCity)
                $scope.Listing.geoData.zone = null;
        }, true);


        if ($scope.Listing.geoData && $scope.Listing.geoData.city) {
            $scope.GetZonesByCityID($scope.Listing.geoData.city.id);
        }

        $scope.mapOptions = {
            scrollwheel: false,
            draggable: false
        };

        $scope.mapControlEnabled = false;

        $scope.InitializeMap();
    }

    $scope.init();

}]);

