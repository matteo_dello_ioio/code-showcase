﻿

angular.module('CasaNet.ListingManager')
.controller('upgradeListingController', ['$scope', '$modalInstance', 'listing', 'listingIdToUpgrade', 'catalog', 'cartDataService',
function ($scope, $modalInstance, listing, listingIdToUpgrade, catalog, cartDataService) {

    $scope.catalog = catalog;
        
    $scope.SelectedProduct = null;

    $scope.ContainsUnavailableProducts = false;

    $scope.listing = listing;

    $scope.ok = function () {

        if (!$scope.SelectedProduct)
            return;

        var productId = $scope.SelectedProduct.productSiteId;
        var description = $scope.SelectedProduct.name;
        var priceVAT = $scope.SelectedProduct.priceInclVAT;
        var price = $scope.SelectedProduct.priceExcVAT;
        var quantity = 1;
        var listingId = listingIdToUpgrade;
        
        cartDataService.addItem('main', null, productId, price, priceVAT, quantity, listingId, description)
        .then(function () {
            $modalInstance.close(/*$scope.selected.item*/);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.init = function () {
        $scope.ContainsUnavailableProducts = false;
        var products = catalog.depthProducts;
        angular.forEach(products, function (value, key) {
            if (!value.isAvailable) {
                $scope.ContainsUnavailableProducts = true;
            }
        });
    }

    $scope.init();
}]);