﻿
angular.module('CasaNet.ListingManager').controller('activateProductForListingController', ['$rootScope', '$scope', '$modalInstance', 'listingManagerDataService', 'activables', 'listingId', 'analytics',
function ($rootScope, $scope, $modalInstance, listingManagerDataService, activables, listingId, analytics) {

    $scope.activables = activables;

    $scope.SelectedProduct = null;
    $scope.listingId = listingId;

    $scope.ok = function () {

        if (!$scope.SelectedProduct)
            return;

        var activableProductId = $scope.SelectedProduct.id;
        var description = $scope.SelectedProduct.name;
        var listingId = $scope.listingId;

        listingManagerDataService.activateProduct('main', 1, listingId, activableProductId)
        .then(function () {
            $modalInstance.close(/*$scope.selected.item*/);

            //analytics.push({ 'event': 'attivazione-prodotti', 'id-annuncio': listingId, 'prodotto': description });
            $rootScope.$emit('ProductActivation', { 'id-annuncio': listingId, 'prodotto': description });
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);
