
angular.module('CasaNet.ListingManager')
.controller('listingManagementController', ['$scope', '$rootScope', '$state', 'listingManagerDataService', 'propertyListSource', 'applicationGlobals', '$modal', '$filter', 'accountManagementService', 'config', '$window',
function ($scope, $rootScope, $state, listingManagerDataService, propertyListSource, applicationGlobals, $modal, $filter, accountManagementService, config, $window) {

    $scope.elementsToGet = 10;
    $scope.mainLoadingFlagName = 'main';

    $scope.HasSubscriptionActive = function (listing) {
        return accountManagementService.hasSubscriptionActive(listing);
    }

    $scope.HasTopActive = function (listing) {
        return accountManagementService.hasTopActive(listing);
    }

    $scope.HasSuperTopActive = function (listing) {
        return accountManagementService.hasSuperTopActive(listing);
    }

    $scope.HasPremiereActive = function (listing) {
        return accountManagementService.hasPremiereActive(listing);
    }

    $scope.HasDepthActive = function (listing) {
        return accountManagementService.hasDepthActive(listing);
    }

    $scope.setData = function (dataCollection) {
        if (dataCollection) {
            $scope.collectionOthersExists = dataCollection.length > $scope.elementsToGet;
            $scope.propertyListCollection = $scope.setGroupedProducts($scope.collectionOthersExists ? dataCollection.slice(0, -1) : dataCollection);
        }
    }

    $scope.hideToggle = function (idx) {
        var id = $scope.propertyListCollection[idx].id;
        var currentListingStateIsVisible = $scope.propertyListCollection[idx].isVisible;
        listingManagerDataService.hideListing($scope.mainLoadingFlagName, id, currentListingStateIsVisible).then(function (resp) {
            if (resp.succeed) {
                $scope.propertyListCollection[idx].isVisible = resp.data.isVisible;
            }
        });
    };

    $scope.clickedDeleteId = 0;

    $scope.delete = function () {
        //console.log("DELETING ID:", listingId);
        //var id = $scope.propertyListCollection[idx].id;
        listingManagerDataService.deleteListing($scope.mainLoadingFlagName, $scope.clickedDeleteId).then(function (resp) {
            if (resp.succeed) {
                //$scope.propertyListCollection.splice(idx, 1);

                //---
                listingManagerDataService.get('main', 10, 0, '', '', $scope.clickedDeleteId).then(function (resp) {
                    if (resp.succeed) {
                        $scope.propertyListCollection = resp.data;
                        $state.reload();
                        $('#deleteListing').modal('hide');
                    }
                });
                //---
            }
        });
    }

    $scope.openDeleteListing = function (id) {
        $scope.clickedDeleteId = id;
    }

    $scope.loadMore = function () {
        if ($scope.collectionOthersExists) {
            var collectionLength = $scope.propertyListCollection.length;
            listingManagerDataService.get($scope.mainLoadingFlagName, $scope.elementsToGet, collectionLength, '', '').then(function (resp) {
                if (resp.succeed) {
                    $scope.collectionOthersExists = resp.data.length > $scope.elementsToGet;

                    if (resp.data.length > 0) {
                        var newCollection = $scope.setGroupedProducts($scope.collectionOthersExists ? resp.data.slice(0, -1) : resp.data);
                        $scope.propertyListCollection = $scope.propertyListCollection.concat(newCollection);
                    }
                }
            });
        }
    }

    $scope.load = function () {
        listingManagerDataService.get($scope.mainLoadingFlagName, $scope.elementsToGet, 0, '', '').then(function (resp) {
            if (resp.succeed) {
                $scope.setData(resp.data);
            }
        });
    }

    $scope.openUpgradeModal = function (listing) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'App/ListingManagement/upgrade.html',
            controller: 'upgradeListingController',
            resolve: {
                catalog: function (catalogDataService) {
                    return catalogDataService.get('main', 1, listing.id, '').then(function (resp) {
                        resp.subscriptionProducts = $filter('orderBy')(resp.subscriptionProducts, '+displayOrder');
                        resp.depthProducts = $filter('orderBy')(resp.depthProducts, '-displayOrder');
                        return resp;
                    });
                },
                listing: function () { return listing; },
                listingIdToUpgrade: function () { return listing.id; }
            }
        });
        modalInstance.result.then(function (/*selectedItem*/) {
            //$scope.selected = selectedItem;
            $state.go('Root.Cart');
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openActivateProductModal = function (listing) {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'App/ListingManagement/activate-products.html',
            controller: 'activateProductForListingController',
            resolve: {
                activables: function (productDataService) {
                    return productDataService.getActivableDepthProducts('main', 1, listing.id).then(function (resp) {
                        resp.subscriptionProducts = $filter('orderBy')(resp.subscriptionProducts, '+displayOrder');
                        resp.depthProducts = $filter('orderBy')(resp.depthProducts, '+displayOrder');
                        return resp;
                    });
                },
                listingId: function () {
                    return listing.id;
                }
            }
        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.setGroupedProducts = function (listingCollection) {
        angular.forEach(listingCollection, function (element, index) {
            element._products = {};

            angular.forEach(element.activeProducts, function (product, index) {
                if (applicationGlobals.productConfigurations[product.productSiteId]) {
                    var category = applicationGlobals.productConfigurations[product.productSiteId].category;
                    if (!element._products[category]) {
                        element._products[category] = [];
                    }
                    element._products[category].push(product);
                }
            });
            angular.forEach(element.images, function (image, index) {

                if (image.imageTypeId == 1 && image.isMainImage) {
                    element.mainImage = image;
                }
            });
        });
        return listingCollection;
    }

    $scope.goToFinalizeListing = function (listingId) {
        $state.go($scope.config.finalizeListingStateName, { listingId: listingId });
    };

    $scope.goToCart = function () {
        $state.go($scope.config.cartStateName);
    };

    $scope.init = function () {
        $scope.propertyListCollection = [];
        $scope.setData(propertyListSource);
        $scope.config = config;
    };

    $scope.init();
}])
.filter('addressFormat', function () {
    return function (listingItem) {
        if (listingItem === undefined || listingItem === null)
            return '';

        return ((listingItem.city) ? listingItem.city + ', ' : '')
            + ((listingItem.address) ? listingItem.address + ' - ' : '')
            //+ ((listingItem['addressNr']) ? listingItem.addressNr + ' - ' : '')
            + ((listingItem.postalCode) ? listingItem.postalCode + ' ' : '')
            + ((listingItem['city'] && listingItem['regionName']) ? listingItem.regionName : '');
    };
})
.filter('lookupDescription', ['$filter', 'applicationGlobals', function ($filter, applicationGlobals) {
    return function (object) {
        if (object === undefined || object === null)
            return '';

        if (!applicationGlobals['lookups'] || !applicationGlobals.lookups[object.lookupName])
            return '';

        var item = $filter('filter')(applicationGlobals.lookups[object.lookupName], { id: object.lookupId });

        if (item.length == 0)
            return '';

        return item[0].description;
    };
}]);