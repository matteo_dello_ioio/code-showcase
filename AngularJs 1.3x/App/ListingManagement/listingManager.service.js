﻿
angular.module('CasaNet.ListingManager')
.factory('listingManagerDataService', ['$http', '$q', function ($http, $q) {

    // interface
    var service = {
        serviceBaseUrl: '/api/ListingManagement/',
        get: get,
        hideListing: hideListing,
        deleteListing: deleteListing,
        activateProduct: activateProduct
    };
    return service;

    // implementation
    function get(callId, qty, lastIndex, sortField, sortDirection) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            headers: { _callId: callId },
            url: this.serviceBaseUrl + 'Get',
            params: {
                qty: qty,
                idx: lastIndex
            }
        }).success(function (ajaxRespone, status, headers, config) {
            /*if (ajaxRespone.succeed == false && ajaxRespone.errCode == 1410) {
                $rootScope.$broadcast("SessionExpired");
            }*/
            deferred.resolve(ajaxRespone);
        }).error(function (ajaxRespone, status, headers, config) {
            deferred.reject(ajaxRespone);
        });
        return deferred.promise;
    }

    function activateProduct(callId, siteId, listingId, activableProductId) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            headers: { _callId: callId },
            url: this.serviceBaseUrl + 'ActivateProduct',
            data: {
                siteId: siteId,
                listingId: listingId,
                activableProductId: activableProductId
            }
        }).success(function (ajaxRespone, status, headers, config) {
            deferred.resolve(ajaxRespone);
        }).error(function (ajaxRespone, status, headers, config) {
            deferred.reject(ajaxRespone);
        });
        return deferred.promise;
    }

    function hideListing(callId, id, hide) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            headers: { _callId: callId },
            url: this.serviceBaseUrl + 'HideListing',
            data: { id: id, hide: hide }
        }).success(function (ajaxRespone, status, headers, config) {
            deferred.resolve(ajaxRespone);
        }).error(function (ajaxRespone, status, headers, config) {
            deferred.reject(ajaxRespone);
        });
        return deferred.promise;
    }

    function deleteListing(callId, id) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            headers: { _callId: callId },
            url: this.serviceBaseUrl + 'Delete',
            data: { id: id }
        }).success(function (ajaxRespone, status, headers, config) {
            deferred.resolve(ajaxRespone);
        }).error(function (ajaxRespone, status, headers, config) {
            deferred.reject(ajaxRespone);
        });
        return deferred.promise;
    }
}]);