﻿
app.factory('userProfileDataService', ['appDataHelper', function (appDataHelper) {
    return {
        serviceBaseUrl: '/api/UserProfile/',
        get: function (callId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'Get', {});
        },
        update: function(callId, object) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'Update', object);
        },
        updatebillinginfo: function (callId, object) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'UpdateBillingInfo', object);
        },
        updateEmail: function (callId, object) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'UpdateEmail', object);
        },
        cancelverifymail: function (callId, object) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'CancelVerifyMail', object);
        },
        changepassword: function (callId, object) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'ChangePassword', object);
        },
        phoneIsAvailable: function (callId, phoneNr) {
            return appDataHelper.httpPost(callId, '/api/CustomerApi/PhoneIsAvailable', { phoneNumber: phoneNr });
        },
        validatePhone: function (callId, phonePrefix, phoneNr, phoneIsVisible) {
            return appDataHelper.httpPost(callId, '/api/CustomerApi/ValidatePhone', { phoneNumber: phoneNr, phonePrefix: phonePrefix, IsVisible: phoneIsVisible });
        }
    };
}]);

