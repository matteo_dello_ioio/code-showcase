﻿app.controller('userProfileController', ['$scope', '$rootScope', '$state', 'userProfileDataService', 'userProfileSource', '$modal', 'appDataService', '$window',
function ($scope, $rootScope, $state, userProfileDataService, userProfileSource, $modal, appDataService, $window) {
    $scope.email_validator = '(^\\d{6}$)|(^[^@\\"\']*?[^@]*?@[^@\\"\']*?\\.[^@.\\"\']{2,6}$)';
    $scope.password_validator = '((?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})$)';
    $scope.editEmail = false;
    $scope.editNameLastName = true;
    $scope.editPhoneNumber = true;
    $scope.editPassword = true;


    $scope.Password = "abcdefgh"; // act like a placeholder

    $scope.PhoneValStatus = 0;
    $scope.HideCustomerPhone = false;
    

    $scope.init = function () {
        $scope.userProfile = userProfileSource;
        $scope.editEmail = false;

        if ($scope.userProfile.isPhoneVerified) {
            $scope.PhoneValStatus = 1;
            $scope.editPhoneNumber = true;
        }
        else {
            $scope.editPhoneNumber = false;
        }

        $window.callbackPhoneValidatorCompact = function (statusValue, obj) {
            $scope.callbackPhoneValidatorCompact(statusValue, obj);
            $scope.$apply();
        };

        $scope.precPhoneNr = $scope.userProfile.phoneMainNr;
        $scope.HideCustomerPhone = !$scope.userProfile.hasVisiblePhone;
        $scope.precIsVisible = !$scope.HideCustomerPhone;

        casaPhoneValidator.dispose();
        casaPhoneValidator.init();
    };

    $scope.get = function () {
        userProfileDataService.get('main').then(function (resp) {
            if (resp.succeed === true) {
                $scope.userProfile = resp.data;
            } else {
                $scope.userProfileFormMessage = resp.errMsg;
            }
        })
        .catch(function () {
            $scope.userProfileFormMessage = 'Errore di comunicazione con il server';
        });
    };

    $scope.update = function () {
        if ($scope.editNameLastName === false) {
            if ($scope.userProfileForm.$invalid === false) {
                userProfileDataService.update('main', $scope.userProfile).then(function (resp) {
                    $scope.userProfileSucceed = resp.succeed;
                    if (resp.succeed === true) {
                        $scope.userProfileFormMessage = "I tuoi dati sono stati salvati con successo.";
                        $scope.editNameLastName = true;

                        appDataService.loadCurrentUser('main');
                    } else {
                        $scope.userProfileFormMessage = resp.errMsg;
                    }
                })
                .catch(function () {
                    $scope.userProfileFormMessage = 'Errore di comunicazione con il server';
                });
            } else {
                $scope.userProfileSucceed = false;
                $scope.userProfileFormMessage = "Compila tutti i dati";
            }
        } else {
            $scope.editNameLastName = false;
        }
    };

    $scope.updatePhoneNumber = function () {
        $scope.PhoneValStatus = 0;
        $scope.editPhoneNumber = false;
    };

    $scope.VerifyPhone = function () {

        if ($scope.emailForm.phoneMainNr.$error.pattern) {
            $scope.MessageTel = "Inserire un numero di cellulare valido";
            return;
        }

        userProfileDataService.phoneIsAvailable('main', $scope.userProfile.phoneMainNr).then(function (ret) {

            if (ret.data === true) {
                $scope.PhoneValStatus = 2;

                //if ($scope.userProfile.hasVisiblePhone === undefined || $scope.userProfile.hasVisiblePhone === false) {
                //    isVisible = true;
                //}                

                casaPhoneValidator.setPhoneNumber($scope.userProfile.phoneMainNr);
                casaPhoneValidator.setPhoneVisible($scope.HideCustomerPhone);
                $scope.editPhoneNumber = true;
                $scope.MessageTel = null;
            }
            else {
                $scope.MessageTel = "Il numero inserito risulta già utilizzato";
            }

        });
    }

    $scope.cancelModifyPhone = function () {
        if ($scope.userProfile.isPhoneVerified) {
            $scope.editPhoneNumber = true;
            $scope.PhoneValStatus = 1;
        } else {
            $scope.PhoneValStatus = 0
            $scope.editPhoneNumber = false;
        }
        $scope.userProfile.phoneMainNr = $scope.precPhoneNr;
        $scope.HideCustomerPhone = !$scope.precIsVisible;
    }

    $scope.callbackPhoneValidatorCompact = function (statusValue, obj) {

        switch (statusValue) {
            case casaPhoneValidator.status.properties['RESTART'].value:
                //Annulla
                $scope.cancelModifyPhone();
                break;
            case casaPhoneValidator.status.properties['CAPTCHAVALID'].value:
                $scope.PhoneValStatus = 3;
                break;
            case casaPhoneValidator.status.properties['PHONEVALID'].value:
                var phoneIsVisible = false;
                if ($scope.HideCustomerPhone === undefined || $scope.HideCustomerPhone === false) {
                    phoneIsVisible = true;
                }

                userProfileDataService.validatePhone('main', obj.phoneIntPrefix, obj.phoneNumber, phoneIsVisible).then(function (ret) {
                    if (ret != null && ret.data === true) {
                        $state.reload();
                    }
                });
                break;
            case casaPhoneValidator.status.properties['RESTART'].value:
                casaPhoneValidator.dispose();
                casaPhoneValidator.init();
                break;
            case casaPhoneValidator.status.properties['ENDVERIF'].value:
                break;
            default:
                break;
        }
    }

    $scope.verifyMail = function () {
        if ($scope.editEmail == true || $scope.userProfile.tempNewEmailToReplace != null) {
            userProfileDataService.updateEmail('main', $scope.userProfile).then(function (resp) {
                $scope.verifyMailSucceed = resp.succeed;
                if (resp.succeed === true) {
                    $scope.userProfile.tempNewEmailToReplace = resp.data.email;
                    $scope.Message = "";
                    $modal.open({
                        animation: false,
                        templateUrl: 'modalTemplate.html',
                        controller: 'ModalInstanceCtrl',
                        windowClass: 'mobile-modal',
                        size: 'lg',
                        resolve: {
                            email: function () {
                                return resp.data.email;
                            }
                        }
                    });

                    $scope.editEmail = false;
                } else {
                    $scope.Message = "L'indirizzo email da te indicato è già utilizzato, inseriscine un altro";
                }
            }, function (reason) {

            })
            .catch(function () {
                $scope.Message = 'Errore di comunicazione con il server';
            });
        } else {
            $scope.editEmail = true;
        }
    }

    $scope.cancelVerifyMail = function () {
        userProfileDataService.cancelverifymail('main').then(function (resp) {
            if (resp.succeed === true) {
                $scope.editEmail = false;
                $scope.userProfile.emailField = resp.data.email;
                $scope.userProfile.isEmailVerified = resp.data.isEmailVerified;
                $scope.userProfile.tempNewEmailToReplace = resp.data.tempNewEmailToReplace;
            } else {
                $scope.Message = resp.errMsg;
            }
        })
        .catch(function () {
            $scope.Message = 'Errore di comunicazione con il server';
        });
    }

    $scope.changePassword = function () {
        if ($scope.editPassword == false) {
            if ($scope.changePasswordForm.$invalid === false) {
                var postData = {
                    password: $scope.Password,
                    newpassword: $scope.NewPassword,
                    passwordconfirm: $scope.PasswordConfirm
                };

                userProfileDataService.changepassword('main', postData)
                    .then(function (resp) {
                        $scope.changePasswordSucceed = resp.succeed;
                        if (resp.succeed === true) {
                            $scope.changePasswordFormMessage = "La password è stata aggiornata con successo";
                            $scope.editPassword = true;
                            $scope.Password = "abcdefgh"; // act like a placeholder
                            $scope.NewPassword = "";
                            $scope.PasswordConfirm = "";
                            $scope.changePasswordFormErrorCode = 0;
                        } else {
                            $scope.changePasswordFormErrorCode = resp.errCode;
                            $scope.changePasswordFormMessage = resp.errMsg;
                        }
                    })
                    .catch(function () {
                        $scope.changePasswordFormMessage = "Some error has occurred. Try again";
                    });

            } else {
                $scope.changePasswordSucceed = false;
                $scope.changePasswordFormMessage = "";
                $scope.changePasswordFormErrorCode = 0;
                if ($scope.changePasswordForm.password.$error.pattern) {
                    $scope.changePasswordFormMessage = "Inserisci una password di almeno 8 caratteri contenente lettere e numeri nel campo Password.";
                }
                if (($scope.changePasswordForm.newpassword.$error.pattern) || ($scope.changePasswordForm.passwordconfirm.$error.pattern)) {
                    $scope.changePasswordFormMessage = "Inserisci una password di almeno 8 caratteri contenente lettere e numeri nel campo Nuova Password.";
                }
                if ($scope.changePasswordForm.passwordconfirm.$error.passwordConfirm) {
                    $scope.changePasswordFormMessage = "Le password non coincidono.";
                }
                if ($scope.changePasswordForm.passwordconfirm.$pristine || $scope.changePasswordForm.password.$pristine || $scope.changePasswordForm.newpassword.$pristine) {
                    $scope.changePasswordFormMessage = "Compila tutti i campi.";
                }
            }
        } else {
            $scope.Password = "";
            $scope.editPassword = false;
        }
    }

    $scope.cancelchangePassword = function () {
        $scope.Password = "abcdefgh"; // act like a placeholder
        $scope.editPassword = true;
        $scope.changePasswordFormMessage = '';
        $scope.changePasswordFormErrorCode = 0;
        $scope.changePasswordSucceed = false;
        $scope.NewPassword = "";
        $scope.PasswordConfirm = "";
        $scope.changePasswordForm.$setPristine();
    }

    $scope.init();

}]);

app.directive("passwordConfirm", function () {
    return {
        require: "ngModel",
        scope: {
            passwordConfirm: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;
                if (scope.passwordConfirm || ctrl.$viewValue) {
                    combined = scope.passwordConfirm + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordConfirm;
                        if (scope.passwordConfirm != scope.newpassword) {
                            if (origin !== viewValue) {
                                ctrl.$setValidity("passwordConfirm", false);
                                return undefined;
                            } else {
                                ctrl.$setValidity("passwordConfirm", true);
                                return viewValue;
                            }
                        };
                    });
                }
            });
        }
    };
});

app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, email) {
    $scope.email = email;

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});