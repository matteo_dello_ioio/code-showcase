﻿var IsAjaxCallInProgress = false;
var iOS = /iPad|iPhone|iPod/.test(navigator.platform);

// BROWSER DETECT
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) !== -1) {
                return data[i].identity;
            }
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) {
            return;
        }

        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },

    dataBrowser: [
        { string: navigator.userAgent, subString: "Edge", identity: "MS Edge" },
        { string: navigator.userAgent, subString: "Chrome", identity: "Chrome" },
        { string: navigator.userAgent, subString: "MSIE", identity: "Explorer" },
        { string: navigator.userAgent, subString: "Trident", identity: "Explorer" },
        { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
        { string: navigator.userAgent, subString: "Safari", identity: "Safari" },
        { string: navigator.userAgent, subString: "Opera", identity: "Opera" }
    ]

};


BrowserDetect.init();

var app = angular.module('CasaNet', [
    'CasaNet.ListingManager',
    'ngCookies',
    'ngSanitize',
    'ui.bootstrap',
    'ngAnimate',
    'ngTouch',
    'uiGmapgoogle-maps',
    'ui.router',
    'angularFileUpload',
    'angulartics',
    'angulartics.google.tagmanager',
    'CasaNet.AccountManagement'
])
.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthorized: 'auth-not-authorized'
})
.run(['$rootScope', 'applicationGlobals', '$window', '$location', 'AUTH_EVENTS', 'analytics',
function ($rootScope, applicationGlobals, $window, $location, AUTH_EVENTS, analytics) {
    $rootScope.applicationGlobals = applicationGlobals;
    $rootScope.wind = $window;
    $rootScope.applicationGlobals.useRuiSelect = BrowserDetect.browser != 'Explorer' || BrowserDetect.version > 11;
    $rootScope.applicationGlobals.loginHref = '/Home/Login';
    // *****
    $rootScope.$on(AUTH_EVENTS.notAuthorized, function () {
        if ($window.location.pathname != $rootScope.applicationGlobals.loginHref)
            $window.location.href = $rootScope.applicationGlobals.loginHref;
    });

    $rootScope.$on(AUTH_EVENTS.sessionTimeout, function () {
        if ($window.location.pathname != $rootScope.applicationGlobals.loginHref)
            $window.location.href = $rootScope.applicationGlobals.loginHref;
    });
    // *****

    // *** applicatione events ***
    $rootScope.$on("$stateChangeSuccess", function (event, next, current) {
        var path = $location.path();
        var absUrl = $location.absUrl();
        var virtualUrl = absUrl.substring(absUrl.indexOf(path));

        analytics.push({ 'event': 'virtualPageView', 'virtualUrl': virtualUrl });
    });

    $rootScope.$on("UserLoggedIn", function (event, data) {
        console.log("LOGIN EVENT");

        analytics.push({ 'event': 'login' });
    });

    // - listing editor events -
    $rootScope.$on("StepCompleted", function (event, data) {
        console.log("STEP COMPLETE EVENT");

        analytics.push({ 'event': 'creazione-annuncio', 'step-name': data['step-name'] });
    });

    $rootScope.$on("ItemAddedInCart", function (event, data) {
        console.log("ITEM ADDED IN CART EVENT");

        var analytics_data = {
            'event': 'creazione-annuncio',
            'step-name': 'carrello'
        };

        if (data.subscription) {

            var duration = null;
            if (data.subscription.duration)
                duration = data.subscription.duration.replace(' ', '-');

            var subscription = null;
            if (duration != null)
                subscription = data.subscription.name.toLowerCase() + '-' + duration;
            else
                subscription = data.subscription.name.toLowerCase();

            analytics_data['abbonamento'] = subscription;
        }

        if (data.product) {
            var duration = null;
            if (data.product.duration)
                duration = data.product.duration.replace(' ', '-');

            var product = null;
            if (duration != null)
                product = data.product.name.toLowerCase() + '-' + duration;
            else
                product = data.product.name.toLowerCase();

            analytics_data['tipo-annuncio'] = product;
        }

        analytics.push(analytics_data);
    });

    $rootScope.$on("ECommerceTransactionCompleted", function (event, data) {
        console.log("ECOMMERCE TRANSACTION COMPLETED EVENT");

        analytics.push(data);
    });

    //
    $rootScope.$on("ProductActivation", function (event, data) {
        console.log("PRODUCT ACTIVATION EVENT");

        analytics.push({ 'event': 'attivazione-prodotti', 'id-annuncio': data.listingId, 'prodotto': data.description });

        $window.location.reload();
    });

}]);