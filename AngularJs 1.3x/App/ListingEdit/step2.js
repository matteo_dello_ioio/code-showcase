﻿app.controller('Step2Controller', ['$filter', '$rootScope', '$scope', '$state', 'listingEditLookupDataService', 'listingEditDataService', 'listing', 'FileUploader', 'photoUploaderDataService', 'appHelper', 'applicationGlobals',
function ($filter, $rootScope, $scope, $state, listingEditLookupDataService, listingEditDataService, listing, FileUploader, photoUploaderDataService, appHelper, applicationGlobals) {

    // main model
    $scope.Listing = listing;
    $scope.original = angular.copy(listing);

    $scope.Alerts = [];

    $scope.CloseUserAlert = function () {
        Alerts = [];
    };


    //rotate Image
    $scope.rotateImg = function (el) {
        var elem = $('#img-' + el.guid)[0];
        var degrees = el.rotation + 90;
        if (degrees == 270) {
            degrees = 0;
        }

        if (navigator.userAgent.match("Chrome")) {
            elem.style.WebkitTransform = "rotate(" + degrees + "deg)";
        } else if (navigator.userAgent.match("Firefox")) {
            elem.style.MozTransform = "rotate(" + degrees + "deg)";
        } else if (navigator.userAgent.match("MSIE")) {
            elem.style.msTransform = "rotate(" + degrees + "deg)";
        } else if (navigator.userAgent.match("Opera")) {
            elem.style.OTransform = "rotate(" + degrees + "deg)";
        } else {
            elem.style.transform = "rotate(" + degrees + "deg)";
        }
        el.rotation = degrees;
    }

    // event handlers
    $rootScope.GoToPrevState = function () {
        console.log("GoToPrevState");
        $rootScope.$broadcast('GoPrevious');
    }
    $rootScope.GoToNextState = function () {
        console.log("GoToNextState");
        if (listingEditDataService.step2IsComplete($scope.Listing)) {
            if ($rootScope.HasChanges()) {
                $rootScope.Save().then(function (resp) {
                    if (!listing.status || listing.status == 0) {
                        $rootScope.$emit('StepCompleted', { 'step-name': 'posizione-foto' });
                        $rootScope.$broadcast('GoNext');
                    }
                    else
                    {
                        listingEditDataService.publishChanges('main', $scope.Listing.id)
                       .then(function () {
                           $rootScope.$emit('StepCompleted', { 'step-name': 'posizione-foto' });
                           window.location = $state.href('Root.Home');
                       });
                        
                    }

                }, function (reason) {
                    $scope.Alerts.push({ type: 'danger', msg: "Some error has occurred. Try again." });
                })
                .catch(function () {
                    console.log('...catch');
                    $scope.Alerts.push({ type: 'danger', msg: "Some error has occurred. Try again." });
                });
            }
            else
            {
                if (!listing.status || listing.status == 0) {
                    $rootScope.$broadcast('GoNext');
                }
                else
                {
                    window.location = $state.href('Root.Home');
                }
                
            }
        } else {
            $scope.showValidationErrors = true;
            $rootScope.$broadcast('scroll', { scroll: 'top' });
        }
    }

    $scope.GetPictures = function () {
        return $filter('filter')($scope.Listing.images, { imageType: 'Photo', isDeleted: 'false' });
    }

    $scope.GetPlanimetries = function () {
        return $filter('filter')($scope.Listing.images, { imageType: 'Planimetry', isDeleted: 'false' });
    }

    $scope.uploadKey = (new Date()).getTime().toString();

    $scope.imageFormatFilterFactory = function (uploader) {
        return function (item) {
            if (item.type == 'image/jpeg' || item.type == 'image/png' || item.type == 'image/tiff' || item.type == 'image/gif' || item.type == 'image/jpg') {
                return true;
            }
            else {
                $scope.uploaderAlert = "Inserisci un formato immagine valido";
                uploader._queueError = true;
                return false;
            }
        }
    };

    $scope.imageSizeFilterFactory = function(uploader) {
       return function (item) {
           if (item.size <= 3145728) {
               return true;
           }
           else {
               $scope.uploaderAlert = "Le immagini non devono superare i 3 MB";
               uploader._queueError = true;
               return false;
           }
        }
    };


    $scope.uploaderPhoto = new FileUploader({
        autoUpload: true,
        removeAfterUpload: true,
        scope: $scope,
        url: '/api/listingeditapi/UploadImages',
        filters: [],
        headers: {
            //'X-CSRF-TOKEN': csrf_token // X-CSRF-TOKEN is used for Ruby on Rails Tokens
            'imageTypeId': 1,
            'appListingId': $scope.Listing.id,
            'listingKey': $scope.uploadKey
        }
    });

    $scope.uploaderPhoto.filters.push({
        name: 'imageFilter',
        fn: $scope.imageFormatFilterFactory($scope.uploaderPhoto)
    });

    $scope.uploaderPhoto.filters.push({
        name: 'sizeFilter',
        fn: $scope.imageSizeFilterFactory($scope.uploaderPhoto)
    });

    $scope.uploaderPhoto.onCompleteItem = function (item, response, status, headers) {
        console.log("onCompleteItem", item, response, status, headers);
        if (response.succeed) {
            photoUploaderDataService.addImage({
                id: response.data[0].id,
                guid: response.data[0].guid,
                bitmapHash: response.data[0].bitmapHash,
                url: response.data[0].url,
                uploadKey: response.data[0].uploadKey,
                uploadReference: response.data[0].uploadReference,
                name: item.file.name,
                size: item.file.size,
                imageType: 'Photo',
                width: response.data[0].width,
                height: response.data[0].height,
                isFavourite: false
            });
        }
        else {
            $scope.uploaderAlert = response.errMsg;
        }
    }

    $scope.uploaderPhoto.onAfterAddingAll = function () {
        var maxPics = 10;
        if ($scope.uploaderPhoto._queueError || (($scope.uploaderPhoto.queue.length + $scope.GetPictures().length) > maxPics))
        {
            if (($scope.uploaderPhoto.queue.length + $scope.GetPictures().length) > maxPics) {
                $scope.uploaderAlert = ((maxPics - $scope.GetPictures().length) == 0 || (maxPics - $scope.GetPictures().length) == maxPics) ? 'Non puoi caricare più di ' + maxPics + ' foto' : 'Puoi caricare ancora solo ' + (maxPics - $scope.GetPictures().length) + ' foto';
            }
            $scope.uploaderPhoto.clearQueue();
        }
        else
        {
            $scope.uploaderAlert = null;
        }
        $scope.uploaderPhoto._queueError = false;
    }

    $scope.uploaderPhoto.onErrorItem = function (item, response, status, headers) {
        $scope.uploaderAlert = "Errore durante il caricamento dell'immagine";
        console.log("onErrorItem", item, response, status, headers);
    }

    $scope.uploaderPlan = new FileUploader({
        autoUpload: true,
        removeAfterUpload: true,
        scope: $scope,
        url: '/api/listingeditapi/UploadImages',
        filters: [],
        headers: {
            'imageTypeId': 2,
            'appListingId': $scope.Listing.id,
            'listingKey': $scope.uploadKey
        }
    });

    $scope.uploaderPlan.onWhenAddingFileFailed = function (item, filter, options) {
        var i = 1;
    };

    $scope.uploaderPlan.filters.push({
        name: 'imageFilter',
        fn: $scope.imageFormatFilterFactory($scope.uploaderPlan)
    });

    $scope.uploaderPlan.filters.push({
        name: 'sizeFilter',
        fn: $scope.imageSizeFilterFactory($scope.uploaderPlan)
    });

    $scope.uploaderPlan.onAfterAddingAll = function (addedItems) {
        var maxPics = 4;
        if ($scope.uploaderPlan._queueError || (($scope.uploaderPlan.queue.length + $scope.GetPlanimetries().length) > maxPics)) {
            if (($scope.uploaderPlan.queue.length + $scope.GetPlanimetries().length) > maxPics) {
                $scope.uploaderAlert = ((maxPics - $scope.GetPlanimetries().length) == 0 || (maxPics - $scope.GetPlanimetries().length) == maxPics) ? 'Non puoi caricare più di ' + maxPics + ' planimetrie' : 'Puoi caricare ancora solo ' + (maxPics - $scope.GetPlanimetries().length) + ' planimetrie';
            }
            $scope.uploaderPlan.clearQueue();
        }
        else {
            $scope.uploaderAlert = null;
        }
        $scope.uploaderPlan._queueError = false;
    };

    $scope.uploaderPlan.onCompleteItem = function (item, response, status, headers) {
        console.log("onCompleteItem", item, response, status, headers);
        if (response.succeed) {
            photoUploaderDataService.addImage({
                id: response.data[0].id,
                guid: response.data[0].guid,
                bitmapHash: response.data[0].bitmapHash,
                url: response.data[0].url,
                uploadKey: response.data[0].uploadKey,
                uploadReference: response.data[0].uploadReference,
                name: item.file.name,
                size: item.file.size,
                imageType: 'planimetry',
                width: response.data[0].width,
                height: response.data[0].height,
                isFavourite: false
            });
        }
        else {
            $scope.uploaderAlert = response.errMsg;
        }
    }

    $scope.uploaderPlan.onErrorItem = function (item, response, status, headers) {
        $scope.uploaderAlert = "Errore durante il caricamento dell'immagine";
        console.log("onErrorItem", item, response, status, headers);
    }

    $scope.SetPhotoAsFavourite = function (photo) {
        $scope.uploaderAlert = null;
        var images = photoUploaderDataService.getImages()
        for (var i = 0; i < images.length; ++i) {
            images[i].isFavourite = false;
        }
        photo.isFavourite = true;
    }

    $scope.DeleteImage = function (image) {
        var callId = (image.imageType == 'Photo') ? 'photoUploaderSpin' : 'planUploaderSpin';
        $scope.uploaderAlert = null;
        if (image.imageType == 'Photo' && $scope.GetPictures().length <= 1 && $scope.listingHasActiveDepth) {
            $scope.uploaderAlert = "E' richiesta l'esistenza di almeno una foto per l'annuncio";
        }
        else {
            photoUploaderDataService.deleteImage(callId, image);
        }
    }

    $scope.init = function () {
        $scope.appHelper = appHelper;
        $scope.applicationGlobals = applicationGlobals;

        appHelper.setLoadingData(false, 'photoUploaderSpin');
        appHelper.setLoadingData(false, 'planUploaderSpin');

        $scope.listingHasActiveDepth = false;
        if ($scope.Listing.activeProductIds && applicationGlobals.productConfigurations) {
            $scope.listingHasActiveDepth = $filter('filter')(_.toArray(applicationGlobals.productConfigurations), function (value, index, array) {
                var prodIdx = $scope.Listing.activeProductIds.indexOf(value.id);
                return (prodIdx != -1 && value.category == 2) //2 = Depth
            }, true).length > 0;
        }

        //UPLOADERS OVERRIDES################################
        //Overriding original function addToQueueTarget, to let initialize queue error flag
        $scope.uploaderPlan.addToQueueTarget = $scope.uploaderPlan.addToQueue;
        $scope.uploaderPlan.addToQueue = function (files, options, filters) {
            $scope.uploaderPlan._queueError = false;
            $scope.uploaderPlan.addToQueueTarget(files, options, filters);
        };

        $scope.uploaderPhoto.addToQueueTarget = $scope.uploaderPhoto.addToQueue;
        $scope.uploaderPhoto.addToQueue = function (files, options, filters) {
            $scope.uploaderPhoto._queueError = false;
            $scope.uploaderPhoto.addToQueueTarget(files, options, filters);
        };
        //##########################################
    };

    $scope.init();
}]);