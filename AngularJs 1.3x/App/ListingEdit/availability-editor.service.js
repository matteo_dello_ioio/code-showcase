﻿
app.factory('AvailabilityLookupDataService', ['$http', '$q', function ($http, $q) {
    return {

        getFeeTypes: function (val) {
            return $q(function (resolve, reject) {
                resolve([{ code: 1, description: 'Giorno' }, { code: 7, description: 'Settimana' }, { code: 30, description: 'Mese' }]);
            });
        },

        lookupFee: function (feeId){
        	var fees = [{ code: 1, description: 'Giorno' }, { code: 7, description: 'Settimana' }, { code: 30, description: 'Mese' }];
        	for(var i = 0; i < fees.length; ++i){
        		if(fees[i].code == feeId)
        			return fees[i];
        	}
        }
    }
}]);

/* HINT: this service can be used to share Estate data between controllers */
app.factory('AvailabilityEditingDataService',  ['$rootScope', '$filter', function ($rootScope, $filter) {

    return {

        uuid: 0,

        timetable: [],

        GetTimetable: function(){
            return this.timetable;
        },
        SetTimetable: function(value) {
            this.timetable = [];

            for (var i = 0; i < value.length; ++i) {
                value[i].uuid = ++this.uuid;
                //value[i].startDate = value[i].startDate;
                //value[i].endDate = value[i].endDate;
                //value[i].startDate = new Date(value[i].startDate);
                //value[i].endDate = new Date(value[i].endDate);
                this.timetable.push(value[i]);
            }

            this.timetable = value;
            $rootScope.$broadcast('AvailabilityUpdated');
        },
        getNewAvailability: function(){
            return {
                startDate: null,
                endDate: null,
                feeType: null,
                price: null,
                notes: null,
                isNotAvailable: null,
                isDeleted: false
            };
        },
        addAvailability: function (entry) {
            this.timetable.push({
                uuid: ++this.uuid,
                //startDate: $filter('date')(entry.startDate, 'yyyy-MM-dd HH:mm:ss Z'),
                //endDate: $filter('date')(entry.endDate, 'yyyy-MM-dd HH:mm:ss Z'),
                startDate: entry.startDate,
                endDate: entry.endDate,
                feeType: entry.feeType,
                price: entry.price,
                notes: entry.notes,
                isNotAvailable: entry.isNotAvailable
            });

            $rootScope.$broadcast('AvailabilityUpdated');
        },
        updateAvailability: function (index, entry) {
            this.timetable[index] = angular.copy(entry);

            $rootScope.$broadcast('AvailabilityUpdated');
        },
        removeAvailability: function(index)
        {
            //this.timetable.splice(index, 1);
            this.timetable[index].isDeleted = true;

            $rootScope.$broadcast('AvailabilityUpdated');
        },
        setTimetable: function (timetable) {
            this.timetable = timetable;

            $rootScope.$broadcast('AvailabilityUpdated');
        },

        hasRequiredData: function (entry) {
            return !(!entry.startDate || !entry.endDate || !entry.price || !entry.feeType);
        }
    }
    /*
    return [
            //{
            //    startDate: null,
            //    endDate: null,
            //    feeType: null,
            //    price: null,
            //    notes: null
            //}
    ];
    */
}]);