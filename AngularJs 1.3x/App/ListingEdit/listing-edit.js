﻿
app.controller('ListingEditController', ['listing', '$q', '$filter', '$rootScope', '$scope', '$state', '$stateParams', '$window', '$location', '$anchorScroll', '$modal', 'listingEditDataService', 'listingEditLookupDataService', 'GeoDataLookupDataService', 'GeoDataEditingDataService', 'AvailabilityEditingDataService', 'photoUploaderDataService', 'catalogDataService', 'cartDataService', 'MEDIA_SERVER',
function (listing, $q, $filter, $rootScope, $scope, $state, $stateParams, $window, $location, $anchorScroll, $modal, listingEditDataService, listingEditLookupDataService, GeoDataLookupDataService, GeoDataEditingDataService, AvailabilityEditingDataService, photoUploaderDataService, catalogDataService, cartDataService, MEDIA_SERVER) {


    $scope.MEDIA_SERVER = MEDIA_SERVER;
    $scope.LISTING_DESCRIPTION_MIN_LENGTH = listingEditDataService.LISTING_DESCRIPTION_MIN_LENGTH;
    $scope.LISTING_DESCRIPTION_MAX_LENGTH = listingEditDataService.LISTING_DESCRIPTION_MAX_LENGTH;

    $scope.$state = $state;
    $scope.$stateParams = $stateParams;

    $scope.Alerts = [];
    $scope.hasErrors = false; //TODO: check if it is still used


    $scope.tabs = [
        {
            id: '1',
            stateName: 'Root.ListingEdit.EstateData',
            title: 'DATI IMMOBILE',
            linkDisable: function () {
                return (!$scope.Step1IsComplete() || $state.current.name == this.stateName);
            },
            tabVisible: true
        },
        {
            id: '2',
            stateName: 'Root.ListingEdit.GeoAndPicture',
            title: 'POSIZIONE E FOTO',
            linkDisable: function () {
                return (!$scope.Step2IsComplete() || $state.current.name == this.stateName);
            },
            tabVisible: true
        },
        {
            id: '3',
            stateName: 'Root.ListingEdit.ProductsCatalog',
            title: 'ACQUISTO PRODOTTI',
            linkDisable: function () {
                return (!$scope.Step2IsComplete() || $state.current.name == this.stateName);
            },
            tabVisible: (listing.status == 0 || listing == null || !listing.status)
        }
    ];

    if (BrowserDetect.browser == 'Explorer') {
        $scope.userAgentIsIE = true;
        if (BrowserDetect.version <= 9) {
            $scope.isIE9 = true;
        }
    } else {
        $scope.userAgentIsIE = false;
    }

    if (listing.geoData)
        GeoDataEditingDataService.setData(listing.geoData);

    if (listing.rentalAvailability)
        AvailabilityEditingDataService.SetTimetable(listing.rentalAvailability);

    if (listing.images)
        photoUploaderDataService.setImages(listing.images);

    // main model
    $scope.Listing = listing;
    $scope.original = angular.copy(listing);

    if (!$scope.Listing.id)
        $scope.Listing.contractType = 2;

    $rootScope.HasChanges = function () {
        return (angular.equals($scope.original, $scope.Listing) == false);
    }


    var exit = false;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        console.log('event', event);
        console.log('toState', toState);
        console.log('toParams', toParams);
        console.log('fromState', fromState);
        console.log('fromParams', fromParams);


        if (exit == false) {
            if (toState.name && !toState.name.startsWith('Root.ListingEdit')) {
                if (!fromState.name.startsWith('Root.ListingEdit.ProductsCatalog')) {
                    if (fromParams.listingId != null && !toParams.listingId) {
                        if ($scope.ListingEditForm.$dirty === true) {
                            console.log($scope.ListingEditForm);
                            event.preventDefault();
                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: 'App/ListingEdit/confirmDialog.html',
                                controller: 'confirmDialogController'
                            });

                            modalInstance.result.then(function (/*selectedItem*/) {
                                exit = true;
                                $state.go(toState.name, toParams);
                            }, function () {

                            });
                        }
                    }
                }
            }
        }
    });

    $scope.$on('AvailabilityUpdated', function () {
        $scope.Listing.rentalAvailability = AvailabilityEditingDataService.GetTimetable();
    });

    $scope.$on('ImagesUpdated', function () {
        $scope.Listing.images = photoUploaderDataService.getImages();
        console.log("loaded images", $scope.Listing.images);
    });


    // navigation events
    $scope.$on('GoPrevious', function () {
        $scope.GoTo($state.current.data.prevState);
    });
    $scope.$on('GoNext', function (event, myargs) {        
        $scope.GoTo($state.current.data.nextState);
    });


    // helpers for step menu UI
    $scope.Step1IsComplete = function () {
        return listingEditDataService.step1IsComplete($scope.Listing);
    }
    $scope.Step2IsComplete = function () {
        return listingEditDataService.step2IsComplete($scope.Listing);
    }
    $scope.Step3IsComplete = function () {
        return catalogDataService.selectedSubscription != null;
    }

    $scope.CloseUserAlert = function () {
        Alerts = [];
    };

    $scope.init = function () {
        
    }



    // navigation
    $scope.GoTo = function (state) {
        //console.log("GOTO");
        if (!listing.id) {
            $state.go(state);
        } else {
            $state.go(state, { listingId: listing.id });
        }
        return;
    }


    // 

    $rootScope.Save = function () {

        console.log("saving listing ", $scope.Listing);

        var deferred = $q.defer();

        listingEditDataService.update('main', $scope.Listing)
        .then(function (resp) {

            if (resp.succeed == false) {
                console.log('...unable to save listing', resp);
                deferred.reject("Some error has occurred. Try again");
            }
            else {
                console.log('...listing data saved successfully', resp);

                listing = resp.data;
                $scope.original = angular.copy(resp.data);
                $scope.ListingEditForm.$setPristine();
                if (listing.images)
                    photoUploaderDataService.setImages(listing.images);
                AvailabilityEditingDataService.setTimetable(listing.rentalAvailability);

                deferred.resolve(resp);
            }
        }, function (reason) {
            deferred.reject(reason);
        })
        .catch(function () {
            console.log('...catch');
            $scope.Message = "Some error has occurred. Try again";
        });

        return deferred.promise;
    }

    $scope.init();

}]);


app.controller('confirmDialogController', ['$scope', '$modalInstance',
    function ($scope, $modalInstance) {
        $scope.ok = function () {
            $modalInstance.close('ok');
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
    }]);
