﻿app.controller('EstateDataController', ['$rootScope', '$scope', '$filter', 'listingEditLookupDataService', 'listingEditDataService', 'listing', 'energyEfficiencyValuesSource', 'applicationGlobals', 'accountManagementService',
function ($rootScope, $scope, $filter, listingEditLookupDataService, listingEditDataService, listing, energyEfficiencyValuesSource, applicationGlobals, accountManagementService) {

    // main model
    $scope.Listing = listing;
    $scope.original = angular.copy(listing);

    $scope.canEditContractType = !accountManagementService.hasDepthActive(listing);

    $scope.Lookup = {
        Categories: [],
        Typologies: [],
        RoomNumbers: [],
        EnergyEfficiencyRatings: [],
        Floors: [],
        HeatingTypes: [],
        PhysicalConditions: [],
        OccupationStates: [],
        ParkingTypes: [],
        GardenTypes: []
    }

    $scope.ListingRequireRoomNumbersInfo = false;

    $scope.GetBuildingCategories = function () {
        //console.log('executing GetBuildingCategories');
        listingEditLookupDataService.getBuildingCategories('main')
            .then(function (resp) {
                $scope.Lookup.Categories = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetBuildingTipologiesByCategoryID = function (categoryID) {
        console.log('DEBUG:GET-BUILDING-TIPOLOGIES-BY-CATEGORY-ID');

        if (categoryID === undefined) return;
        //console.log('executing GetBuildingTipologiesByCategoryID');
        listingEditLookupDataService.getBuildingTipologiesByCategoryID('main', categoryID)
            .then(function (resp) {
                $scope.Lookup.Typologies = resp;
                RUI.Select.refresh('#Tipology');
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetRoomNumbers = function () {
        //console.log('executing GetFloors');
        listingEditLookupDataService.getRoomNumbers('main')
            .then(function (resp) {
                $scope.Lookup.RoomNumbers = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetFloors = function () {
        //console.log('executing GetFloors');
        listingEditLookupDataService.getFloors('main')
            .then(function (resp) {
                $scope.Lookup.Floors = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetHeatingTypes = function () {
        //console.log('executing GetHeatingTypes');
        listingEditLookupDataService.getHeatingTypes('main')
            .then(function (resp) {
                $scope.Lookup.HeatingTypes = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetPhysicalConditions = function () {
        //console.log('executing GetPhysicalConditions');
        listingEditLookupDataService.getPhysicalConditions('main')
            .then(function (resp) {
                $scope.Lookup.PhysicalConditions = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetOccupationStates = function () {
        //console.log('executing GetOccupationStates');
        listingEditLookupDataService.getOccupationStates('main')
            .then(function (resp) {
                $scope.Lookup.OccupationStates = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetParkingTypes = function () {
        //console.log('executing GetParkingTypes');
        listingEditLookupDataService.getParkingTypes('main')
            .then(function (resp) {
                $scope.Lookup.ParkingTypes = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.GetGardenTypes = function () {
        //console.log('executing GetGardenTypes');
        listingEditLookupDataService.getGardenTypes('main')
            .then(function (resp) {
                $scope.Lookup.GardenTypes = resp;
            }, function (reason) {
                console.log("some error has occurred", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('unexpected error');
                $scope.Message = "Some error has occurred. Try again";
            });
    }


    // event handlers
    $rootScope.GoToPrevState = function () {
        console.log("GoToPrevState");
        $rootScope.$broadcast('GoPrevious');
    }
    $rootScope.GoToNextState = function () {

        $scope.EnergyValidationError = ($scope.Listing.energyEfficiency.ratingId == null);

        if (listingEditDataService.step1IsComplete($scope.Listing)) {

            $scope.showValidationErrors = false;

            if ($scope.ListingEditForm.$invalid) {
                $scope.showValidationErrors = true;
                return;
            }


            if ($scope.EnergyValidationError) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
                return;
            }

            /* if listing is new, must be saved at the end of step 2 */
            if (listingEditDataService.isNew($scope.Listing)) {

                $rootScope.$emit('StepCompleted', { 'step-name': 'dati-immobile' });

                $rootScope.$broadcast('GoNext');
            } else {
                if ($rootScope.HasChanges()) {

                    var ar = $scope.Lookup.EnergyEfficiencyRatings;
                    for (var i = 0; i < ar.length ; i++) {

                        if (ar[i].id == $scope.Listing.energyEfficiency.ratingId) {
                            $scope.statusEER = ar[i].classificationEnum;

                            if (ar[i].classificationEnum == null) {
                                $scope.Listing.energyEfficiency.valueRenew = null;
                                $scope.Listing.energyEfficiency.indexWinter = null;
                                $scope.Listing.energyEfficiency.indexSummer = null;
                                $scope.Listing.energyEfficiency.cunsumptionAlmostZero = null;
                                $scope.Listing.energyEfficiency.value = null;
                                $scope.Listing.energyEfficiency.isCubeMeters = null;
                            }
                            if (ar[i].classificationEnum == 1) {
                                $scope.Listing.energyEfficiency.valueRenew = null;
                                $scope.Listing.energyEfficiency.indexWinter = null;
                                $scope.Listing.energyEfficiency.indexSummer = null;
                                $scope.Listing.energyEfficiency.cunsumptionAlmostZero = null;
                            }
                        }
                    }

                    $rootScope.Save().then(function (resp) {
                        $rootScope.$broadcast('GoNext');
                    }, function (reason) {
                        $scope.Alerts.push({ type: 'danger', msg: "Some error has occurred. Try again." });
                    })
                    .catch(function () {
                        console.log('...catch');
                        $scope.Alerts.push({ type: 'danger', msg: "Some error has occurred. Try again." });
                    });
                } else {
                    $rootScope.$broadcast('GoNext');
                }
            }
        } else {
            $scope.showValidationErrors = true;
            $rootScope.$broadcast('scroll', { scroll: 'top' });
        }
    }

    $scope.EnergyValidationError = false;


    $scope.init = function () {
        $scope.applicationGlobals = applicationGlobals;

        $scope.$watch('Listing.category', function (newValue, oldValue) {
            console.log('DEBUG: WATCH LISTING CATEGORY');
            if (!newValue) {
                $scope.ListingRequireRoomNumbersInfo = false;
            } else {
                $scope.ListingRequireRoomNumbersInfo = listingEditDataService.listingRequireRoomNumbersInfo(newValue, $scope.Listing.tipology);
                $scope.GetBuildingTipologiesByCategoryID(newValue);
            }

            if (newValue != oldValue)
                $scope.Listing.tipology = null;

            if (!$scope.ListingRequireRoomNumbersInfo)
                $scope.Listing.roomsNumber = null;
        }, true);

        $scope.$watch('Listing.tipology', function (newValue, oldValue) {
            console.log('DEBUG: WATCH LISTING TIPOLOGY');
            var conf = $scope.applicationGlobals.lookups.buildingTypeConfiguration;
            if (newValue != oldValue) {
                if (!newValue || (conf[newValue].showField && (!oldValue || !conf[oldValue].showField))) {
                        $scope.Listing.roomsNumber = null;
                } else if (!conf[newValue].showField) {
                        var defaultValue = conf[newValue].roomsNrValue;
                        var filtered = $filter('filter')($scope.Lookup.RoomNumbers, { roomDesc: defaultValue }, true);
                        if (!filtered || filtered.length == 0)
                            $scope.Listing.roomsNumber = null;
                        else
                            $scope.Listing.roomsNumber = filtered[0].id;
                    }
            }
        }, true);

        $scope.$watch('Listing.energyEfficiency.ratingId', function (newValue, oldValue) {
            if (newValue != null || oldValue != null) {

                $scope.EnergyValidationError = newValue == null;
                var ar = $scope.Lookup.EnergyEfficiencyRatings;
                for (var i = 0; i < ar.length ; i++) {

                    if (ar[i].id == newValue) {
                        $scope.statusEER = ar[i].classificationEnum;
                    }
                }
                EnergyValidationError = false;
            }
        }, true);


        $scope.GetBuildingCategories();
        //$scope.GetBuildingTipologiesByCategoryID();

        $scope.Lookup.EnergyEfficiencyRatings = energyEfficiencyValuesSource;

        $scope.GetRoomNumbers();
        $scope.GetFloors();
        $scope.GetHeatingTypes();
        $scope.GetPhysicalConditions();
        $scope.GetOccupationStates();
        $scope.GetParkingTypes();
        $scope.GetGardenTypes();

        $scope.EnergyEfficiencyUoM = [{ value: 0, name: 'kWh\m2' }, { value: 1, name: 'kWh\m3' }];
    }

    $scope.descriptionBoxFocus = function () {
        $scope.applicationGlobals.desc_focus = true;
    };

    $scope.descriptionBoxBlur = function () {
        $scope.applicationGlobals.desc_focus = false;
        if ($scope.Listing.description.en != null && $scope.Listing.description.en.length == 0) {
            $scope.ListingEditForm.ListingDescriptionEN.$setPristine();
            $scope.Listing.description.en = null;
        }
        if ($scope.Listing.description.fr != null && $scope.Listing.description.fr.length == 0) {
            $scope.ListingEditForm.ListingDescriptionFR.$setPristine();
            $scope.Listing.description.fr = null;
        }
        if ($scope.Listing.description.es != null && $scope.Listing.description.es.length == 0) {
            $scope.ListingEditForm.ListingDescriptionES.$setPristine();
            $scope.Listing.description.es = null;
        }
        if ($scope.Listing.description.de != null && $scope.Listing.description.de.length == 0) {
            $scope.ListingEditForm.ListingDescriptionDE.$setPristine();
            $scope.Listing.description.de = null;
        }
    };

    /* logic */
    $scope.IsRentalForVacation = function () {
        return ($scope.Listing.contractType == 1 && $scope.Listing.category == 2);
    }

    $scope.init();
}]);