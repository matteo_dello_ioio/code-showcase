﻿

app.controller('AvailabilityEditorController', ['$filter', '$rootScope', '$scope', 'AvailabilityLookupDataService', 'AvailabilityEditingDataService',
    function ($filter, $rootScope, $scope, AvailabilityLookupDataService, AvailabilityEditingDataService) {

        $scope.AvailabilityDataRequired = false;

        $scope.Availability = AvailabilityEditingDataService.getNewAvailability();

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.AvailabilityTimeTable = AvailabilityEditingDataService.GetTimetable();

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.startDateOpened = true;
            $scope.endDateOpened = false;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.startDateOpened = false;
            $scope.endDateOpened = true;
        };

        $scope.Alerts = [];

        //todo: Creare servizio per gestire in una sola chiamata
        $scope.CloseAlert = function () {
            $scope.Alerts = [];
        };

        $scope.Lookup = {
            FeeTypes: null,
            LookupFee: function(feeId){
                return AvailabilityLookupDataService.lookupFee(feeId);
            }
        }

        $scope.GetFeeTypes = function () {
            AvailabilityLookupDataService.getFeeTypes()
                .then(function(resp) {
                    if (resp !== undefined)
                        $scope.Lookup.FeeTypes = resp;
                })
                .catch(function () {
                    console.log('...catch');
                    $scope.Message = "Some error has occurred. Try again";
                });
        }

        $scope.AddNewAvailability = function () {
            //$scope.Availability = AvailabilityEditingDataService.getNewAvailability();
            $scope.showEditor = true;
        }
        $scope.Cancel = function () {
            $scope.showEditor = false;
        }

        $scope.AddAvailability = function () {

            //$scope.AvailabilityDataRequired = !$scope.hasRequiredData($scope.Availability);
            $scope.AvailabilityDataRequired = !AvailabilityEditingDataService.hasRequiredData($scope.Availability);

            if ($scope.AvailabilityDataRequired) {
                return;
            }
            AvailabilityEditingDataService.addAvailability($scope.Availability);

            $scope.Availability = AvailabilityEditingDataService.getNewAvailability();

            $scope.showEditor = false;
        }

        $scope.EditAvailability = function (index) {
            //RUI.Select.refresh('#availability_inline_editor_feetype');
            //AvailabilityEditingDataService.GetTimetable()[index].edit = true;

            var entries = AvailabilityEditingDataService.GetTimetable();
            for(var i = 0; i < entries.length; ++i)
            {
                if(i != index)
                    entries[i].edit = undefined;
                else
                    entries[i].edit = true;
            }
        }

        $scope.RemoveAvailability = function (index) {
            AvailabilityEditingDataService.removeAvailability(index);
        }

        $scope.init = function () {

            $scope.GetFeeTypes();

            $rootScope.$on("AvailabilityUpdated", function (event, data) {
                $scope.AvailabilityTimeTable = AvailabilityEditingDataService.GetTimetable();
            });
        }

        $scope.init();

        /* events */

    }]);

app.controller('InlineAvailabilityEditorController', ['$filter', '$scope', 'AvailabilityEditingDataService',
    function ($filter, $scope, AvailabilityEditingDataService) {

        $scope.AvailabilityDataRequired = false;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        //console.log("InlineAvailabilityEditorController", $scope.availability);

        //$scope.ID = $scope.$index;
        $scope.original = angular.copy($scope.availability);
        $scope.InlineAvailability = angular.copy($scope.availability);

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.startDateEditOpened = true;
            $scope.endDateEditOpened = false;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.startDateEditOpened = false;
            $scope.endDateEditOpened = true;
        };

        $scope.Save = function () {

            //$scope.AvailabilityDataRequired = !$scope.hasRequiredData($scope.InlineAvailability);
            $scope.AvailabilityDataRequired = !AvailabilityEditingDataService.hasRequiredData($scope.InlineAvailability);
            
            if ($scope.AvailabilityDataRequired) {
                return;
            }
            //$scope.availability.edit = undefined;
            AvailabilityEditingDataService.updateAvailability($scope.$index, $scope.InlineAvailability);
        }

        $scope.Cancel = function () {
            $scope.AvailabilityDataRequired = false;
            $scope.availability.edit = undefined;
            $scope.InlineAvailability = $scope.original;
        }

    }]);