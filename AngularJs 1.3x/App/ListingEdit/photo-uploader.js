﻿
app.controller('PhotoUploaderController', ['$scope', 'FileUploader', '$http', 'photoUploaderDataService',
    function ($rootScope, $scope, FileUploader, $http, photoUploaderDataService) {

        $scope.imgAlerts = [];
        $scope.toPos = 0;
        $scope.fromPos = 0;


        $scope.uploaderPhoto = new FileUploader({
            autoUpload: true,
            queueLimit: 10,
            scope: $scope,
            url: '/api/listingeditapi/UploadImages',
            filters: [],
            headers : {
                //'X-CSRF-TOKEN': csrf_token // X-CSRF-TOKEN is used for Ruby on Rails Tokens
                'imageTypeId': 1,
                'appListingId': $scope.Listing.id
            }
        });
        $scope.uploaderPhoto.filters.push({
            name: 'imageFilter',
            fn: function (item) {
                if (item.type == 'image/jpeg' || item.type == 'image/png' || item.type == 'image/tiff' || item.type == 'image/gif' || item.type == 'image/jpg') {
                    return true;
                }
                else {
                    $scope.imgAlerts = [];
                    $scope.imgAlerts.push({ type: 'danger', msg: "Inserisci un formato immagine valido" });
                    return false;
                }
            }
        });
        $scope.uploaderPhoto.filters.push({
            name: 'sizeFilter',
            fn: function (item) {
                if (item.size <= 3145728)
                    return true;
                else {
                    $scope.imgAlerts = [];
                    $scope.imgAlerts.push({ type: 'danger', msg: "Le immagini non devono superare i 3 MB" });
                    return false;
                }
            }
        });

        //$scope.uploaderPhoto.onAfterAddingFile = function (fileItem) {
        //    console.info('onAfterAddingFile', fileItem);
        //    console.log('uploaderPhoto.queue', $scope.uploaderPhoto.queue);

        //    photoUploaderDataService.addImage({ name: fileItem.file.name, size: fileItem.file.size, imageType: 'photo' });
        //};
        $scope.uploaderPhoto.onCompleteItem = function (item, response, status, headers) {
            console.log("onCompleteItem", item, response, status, headers);
            $scope.Listing.images.push(response.data);
            photoUploaderDataService.addImage({ guid: response.data[0], name: item.file.name, size: item.file.size, imageType: 'photo' });
        }
        $scope.uploaderPhoto.onErrorItem = function (item, response, status, headers) {
            alert("error uploading image");
            console.log("onErrorItem", item, response, status, headers);
        }
        


        $scope.uploaderPlan = new FileUploader({
            autoUpload: true,
            queueLimit: 4,
            scope: $scope,
            url: '/api/listingeditapi/UploadImages',
            filters: [],
            headers : {
                //'X-CSRF-TOKEN': csrf_token // X-CSRF-TOKEN is used for Ruby on Rails Tokens
                'imageTypeId': 2,
                'appListingId': $scope.Listing.id
            }
        });
        $scope.uploaderPlan.filters.push({
            name: 'imageFilter',
            fn: function (item) {
                if (item.type == 'image/jpeg' || item.type == 'image/png' || item.type == 'image/tiff' || item.type == 'image/gif' || item.type == 'image/jpg')
                    return true;
                else {
                    $scope.imgAlerts = [];
                    $scope.imgAlerts.push({ type: 'danger', msg: "Inserisci un formato immagine valido" });
                    return false;
                }
            }
        });
        $scope.uploaderPlan.filters.push({
            name: 'sizeFilter',
            fn: function (item) {
                if (item.size <= 3145728)
                    return true;
                else {
                    $scope.imgAlerts = [];
                    $scope.imgAlerts.push({ type: 'danger', msg: "Le immagini non devono superare i 3 MB" });
                    return false;
                }
            }
        });
        //$scope.uploaderPlan.onAfterAddingFile = function (fileItem) {
        //    console.info('onAfterAddingFile', fileItem);
        //    console.log('uploaderPhoto.queue', $scope.uploaderPlan.queue);

        //    photoUploaderDataService.addImage({ name: fileItem.file.name, size: fileItem.file.size, imageType: 'planimetry' });
        //};
        $scope.uploaderPlan.onCompleteItem = function (item, response, status, headers) {
            console.log("onCompleteItem", item, response, status, headers);
            $scope.Listing.images.push(response.data);
            photoUploaderDataService.addImage({ guid: response.data[0], name: fileItem.file.name, size: fileItem.file.size, imageType: 'planimetry' });
        }
        $scope.uploaderPhoto.onErrorItem = function (item, response, status, headers) {
            alert("error uploading image");
            console.log("onErrorItem", item, response, status, headers);
        }


        function arrayMove(arrayVar, from, to) {
            arrayVar.splice(to, 0, arrayVar.splice(from, 1)[0]);
        }

        $scope.SAVEIMAGETEST = function () {

            $scope.uploaderPhoto.uploadAll();
        }

        $scope.DeleteImage = function (item) {
            console.log("item", item);

            //$http({
            //    withCredentials: false,
            //    method: 'POST',
            //    url: '/api/ListingEditApi/DeleteImage',
            //    params: { guid: "acd8424f4ec1449897034945e1c59116" }
            //}).success(function (ajaxResponse, status, headers, config) {
            //    if (ajaxResponse.succeed) {

            //        item.remove();

            //    }
            //}).error(function (ajaxResponse, status, headers, config) {
            //    console.log("error");
            //});


            photoUploaderDataService.removeImage("main", index)
                .then(function (resp) {
                    //retrieve item from queue and call remove() method
                })
                .catch(function () {
                    console.log('...catch');
                    $scope.Message = "Some error has occurred. Try again";
                });
        }


        $scope.SetPrefer = function (item) {
            $scope.fromPos = $scope.uploaderPhoto.queue.indexOf(item);
            arrayMove($scope.uploaderPhoto.queue, $scope.fromPos, $scope.toPos);
        };

        $scope.showBtnPhoto = function (i) {
            var _elem = angular.element('div#btnPanelPhoto')[i];
            _elem.className = 'showBtnPanel';
        };

        $scope.hideBtnPhoto = function (i) {
            var _elem = angular.element('div#btnPanelPhoto')[i];
            _elem.className = 'hideBtnPanel';
        };

        $scope.showBtnPlan = function (i) {
            var _elem = angular.element('div#btnPanelPlan')[i];
            _elem.className = 'showBtnPanel';
        };

        $scope.hideBtnPlan = function (i) {
            var _elem = angular.element('div#btnPanelPlan')[i];
            _elem.className = 'hideBtnPanel';
        };


        $scope.changeColorPhoto = function () {
            $scope.bordColorPhoto = { borderColor: 'blue' };
        };

        $scope.initialColorPhoto = function () {
            $scope.bordColorPhoto = { borderColor: 'black' };
        };

        $scope.changeColorPlan = function () {
            $scope.bordColorPlan = { borderColor: 'blue' };
        };

        $scope.initialColorPlan = function () {
            $scope.bordColorPlan = { borderColor: 'black' };

        };

        $scope.setIniPhoto = function () {

        };

        $scope.Images = [];
        $rootScope.$on('ImagesUpdated', function () {
            $scope.Images = photoUploaderDataService.getImages();
        });

        $scope.init = function () {

            $scope.$watchCollection('uploaderPhoto.queue', function () {
                var _el = angular.element('div#photoUpd');
                if ($scope.uploaderPhoto.queue.length > 0) {
                    _el[0].className = 'photoUpd_notEmpty';
                } else {
                    _el[0].className = 'photoUpd';
                }
            });
        }
    }]);
