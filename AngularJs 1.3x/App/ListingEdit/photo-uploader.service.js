﻿

/* HINT: this service can be used to share Estate data between controllers */
app.factory('photoUploaderDataService', ['$q', '$rootScope', '$filter', 'appDataHelper', function ($q, $rootScope, $filter, appDataHelper) {

    // interface
    var service = {
        serviceBaseUrl: '/api/ListingEditApi/',
        uuid: 0,
        images: [],
        getImages: getImages,
        setImages: setImages,
        addImage: addImage,
        deleteImage: deleteImage
    };
    return service;


    // implementation
    function getImages() {
        return service.images;
    }

    function setImages(value) {
        service.images = [];

        for (var i = 0; i < value.length; ++i) {
            value[i].uuid = ++this.uuid;
            service.images.push(value[i]);
        }

        this.images = value;
        $rootScope.$broadcast('ImagesUpdated');
    }

    function addImage(entry) {

        if (service.images.length == 0 || _.where(service.images, { imageType: 'Photo', isDeleted: false }).length == 0) {

            // this is the case when a planimetry became the favourite image
            var favPlanimetries = _.where(service.images, { imageType: 'planimetry', isDeleted: false, isFavourite: true });
            if (favPlanimetries.length > 0)
            {
                _.each(favPlanimetries, function (image) {
                    image.isFavourite = false;
                });
            }

            entry.isFavourite = true;
        }

        var imageIndex = _.where(service.images, { imageType: entry.imageType, isDeleted: false }).length + 1;

        service.images.push({
            uuid: ++this.uuid,
            id: entry.id,
            guid: entry.guid,
            bitmapHash: entry.bitmapHash,
            url: entry.url,
            uploadKey: entry.uploadKey,
            uploadReference: entry.uploadReference,
            orderIndex: imageIndex,
            isFavourite: entry.isFavourite,
            imageType: entry.imageType,
            name: entry.name,
            size: entry.size,
            width: entry.width,
            height: entry.height,
            isDeleted: false,
            rotation:0
        });

        $rootScope.$broadcast('ImagesUpdated');
    }

    function deleteImage(callId, image) {
        image.isDeleted = true;
        
        if (image.isFavourite) {
            var pictures = $filter('filter')(service.images, { imageType: 'Photo', isDeleted: 'false' });
            if (pictures.length > 0) {
                for (var i = 0; i < service.images.length; i++) {
                    if (pictures[i].orderIndex != image.orderIndex) {
                        pictures[i].isFavourite = true;
                        break;
                    }
                }
            }
        }
        // set the right call to delete image
        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'DeleteImage', image)
        .then(function (resp) {
            $rootScope.$broadcast('ImagesUpdated');
        }, function (reason) {
            console.log("delete image fail", reason);
        });
    }

}]);