﻿
app.factory('listingEditLookupDataService', ['$http', '$q', 'appDataHelper', 'applicationGlobals', function ($http, $q, appDataHelper, applicationGlobals) {

    // interface
    var service = {
        serviceBaseUrl: '/api/lookupapi/',
        loadLookups: loadLookups,
        getContractTypes: getContractTypes,
        getBuildingCategories: getBuildingCategories,
        getBuildingTipologiesByCategoryID: getBuildingTipologiesByCategoryID,
        getRoomNumbers: getRoomNumbers,
        getFloors: getFloors,
        getHeatingTypes: getHeatingTypes,
        getPhysicalConditions: getPhysicalConditions,
        getOccupationStates: getOccupationStates,
        getParkingTypes: getParkingTypes,
        getGardenTypes: getGardenTypes,
        getEnergyEfficiencyRatings: getEnergyEfficiencyRatings
    };
    return service;


    // implementation
    function loadLookups(callId) {
        return appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetLookups', {}).then(function(resp) {
            if (resp.succeed) {
                applicationGlobals.lookups = {
                    contractType: resp.data.contractType,
                    buildingCategory: resp.data.buildingCategory,
                    buildingType: resp.data.buildingType,
                    buildingTypeConfiguration: resp.data.buildingTypeConfiguration
                };
            }
        });
    }

    function getContractTypes(callId) {
        return appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetContractTypes', {});
    }

    function getBuildingCategories(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetBuildingCategories', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getBuildingTipologiesByCategoryID(callId, categoryID) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetBuildingTipologies', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {

                var buildingTypes = [];

                angular.forEach(ajaxResponse.data, function (buildingType, key) {
                    if (buildingType.destinationTypeIds.indexOf('|') == -1) {
                        if (buildingType.destinationTypeIds == categoryID)
                            buildingTypes.push(buildingType);
                    }
                    else {
                        var tmpCatIDs = buildingType.destinationTypeIds.split('|');

                        angular.forEach(tmpCatIDs, function (catID) {
                            if (catID == categoryID) {
                                buildingTypes.push(buildingType);
                            }
                        });
                    }

                });
                deferred.resolve(buildingTypes);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getRoomNumbers(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetRoomNumberTypes', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getFloors(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetFloors', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getHeatingTypes(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetHeatingTypes', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getPhysicalConditions(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetPhysicalConditions', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getOccupationStates(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetOccupationStates', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getParkingTypes(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetParkingTypes', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getGardenTypes(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetGardenTypes', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

    function getEnergyEfficiencyRatings(callId) {
        var deferred = $q.defer();
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetEnergyEfficiencyRatings', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    }

}]);

app.service('listingEditDataService', ['$http', '$q', 'appDataHelper', 'AvailabilityEditingDataService', function ($http, $q, appDataHelper, AvailabilityEditingDataService) {

    // interface
    var service = {
        LISTING_DESCRIPTION_MIN_LENGTH: 100,
        LISTING_DESCRIPTION_MAX_LENGTH: 4000,
        serviceBaseUrl: '/api/ListingEditApi/',
        listing: {},
        getNew: getNew,
        update: update,
        getByKey: getByKey,
        publishChanges: publishChanges,

        isNew: isNew,
        listingRequireRoomNumbersInfo: listingRequireRoomNumbersInfo,
        step1IsComplete: step1IsComplete,
        step2IsComplete: step2IsComplete,
        step3IsComplete: step3IsComplete
    }
    return service;

    // implementation
    function getNew () {
        return {
            id: null,
            uploadKey : (new Date()).getTime().toString(),
            contractType: null,
            category: null,
            tipology: null,
            surface: null,
            roomsNumber: null,
            bathroomsNumber: null,
            price: null,
            isPrivateNegotiation: false,
            hideAddressOnline: false,
            description: {
                it: null,
                en: null,
                fr: null,
                es: null,
                de: null
            },
            energyEfficiency: {
                //rating: null,
                //index: null,
                //indexUnitOfMeasure: null
                valueRenew: null,
                indexWinter: null,
                indexSummer: null,
                consumptionAlmostZero: null,
                value: null,
                ratingId: null,
                isCubeMeters: null
            },
            constructionYear: null,
            currentPhysicalCondition: null,
            floor: null,
            totalFloors: null,
            heatingType: null,
            occupationState: null,
            units: null,
            monthlyExpenses: null,
            hasBalcony: null,
            hasTerrace: null,
            hasElevator: null,
            parking: {
                type: null,
                surface: null,
            },
            garden: {
                type: null,
                surface: null,
            },
            geoData: {
                country: null,
                city: null,
                zone: null,
                address: null,
                postalCode: null,
                latitude: null,
                longitude: null
            },
            images: [],
            rentalAvailability: [],
            guid: null
        };
    }

    function update (callId, listing) {
        return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'Update', listing);
    }

    function publishChanges(callId, listingId) {
        return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'PublishChanges', listingId);
    }

    function getByKey(callId, id) {
        return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetByKey', { id: id });
    }

    function isNew(listing) {
        return ((!listing.id) || (listing.id == 0));
    }

    function listingRequireRoomNumbersInfo (categoryId, typologyId) {
        if ((categoryId == 1) || (categoryId == 2)) {
            if 
        (
                (typologyId == 1)
                || (typologyId == 3)
                || (typologyId == 4)
                || (typologyId == 5)
                || (typologyId == 6)
                || (typologyId == 7)
                || (typologyId == 8)
                || (typologyId == 9)
                || (typologyId == 10)
                || (typologyId == 11)
                || (typologyId == 12)
                || (typologyId == 13)
                || (typologyId == 14)
                //|| (typologyId == 15)
                //|| (typologyId == 17)
                || (typologyId == 35)
                //|| (typologyId == 37)
                || (typologyId == 38)
                //|| (typologyId == 39)
                //|| (typologyId == 41)
            ) {
                return true;
            }
            else {
                return false;
            }
        }
        if ((categoryId == 4)) {
            if
        (
                (typologyId == 1)
                //|| (typologyId == 2)
                || (typologyId == 3)
                || (typologyId == 4)
                || (typologyId == 5)
                || (typologyId == 6)
                || (typologyId == 7)
                || (typologyId == 8)
                || (typologyId == 9)
                || (typologyId == 10)
                //|| (typologyId == 11)
                || (typologyId == 12)
                || (typologyId == 13)
                || (typologyId == 14)
                //|| (typologyId == 15)
                //|| (typologyId == 16)
                //|| (typologyId == 17)
                || (typologyId == 18)
                || (typologyId == 19)
                || (typologyId == 20)
                || (typologyId == 21)
                || (typologyId == 22)
                || (typologyId == 23)
                || (typologyId == 24)
                || (typologyId == 26)
                || (typologyId == 27)
                || (typologyId == 28)
                || (typologyId == 29)
                || (typologyId == 30)
                || (typologyId == 31)
                || (typologyId == 32)
                || (typologyId == 33)
                || (typologyId == 34)
                || (typologyId == 35)
                || (typologyId == 36)
                //|| (typologyId == 37)
                || (typologyId == 38)
                //|| (typologyId == 39)
                || (typologyId == 40)
                //|| (typologyId == 41)
                || (typologyId == 42)
                //|| (typologyId == 43)
                //|| (typologyId == 44)
                || (typologyId == 45)
                || (typologyId == 46)
            ) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    function step1IsComplete(listing) {
        return (
            (listing != null)
            &&
            (listing.contractType != null)
            &&
            (listing.category != null)
            &&
            (listing.tipology != null)
            &&
            (
                (listing.price != null) && ((listing.price > 999) && (listing.contractType == 2))
                ||
                (listing.price != null) && ((listing.price > 9) && (listing.contractType == 1))
            )
            &&
            (listing.surface != null)
            &&
            (
                (listingRequireRoomNumbersInfo(listing.category, listing.tipology) == false)
                ||
                ((listingRequireRoomNumbersInfo(listing.category, listing.tipology) == true))
            )
            //&&
            //(listing.energyEfficiency.ratingId != null)
            &&
            (
                ((listing.description != null) && (listing.description.it != null) && (listing.description.it.length >= this.LISTING_DESCRIPTION_MIN_LENGTH) && (listing.description.it.length <= this.LISTING_DESCRIPTION_MAX_LENGTH))
                &&
                (
                    ((listing.description.en == null) || (listing.description.en != null) && (listing.description.en.length >= this.LISTING_DESCRIPTION_MIN_LENGTH) && (listing.description.en.length <= this.LISTING_DESCRIPTION_MAX_LENGTH))
                    &&
                    ((listing.description.fr == null) || (listing.description.fr != null) && (listing.description.fr.length >= this.LISTING_DESCRIPTION_MIN_LENGTH) && (listing.description.fr.length <= this.LISTING_DESCRIPTION_MAX_LENGTH))
                    &&
                    ((listing.description.es == null) || (listing.description.es != null) && (listing.description.es.length >= this.LISTING_DESCRIPTION_MIN_LENGTH) && (listing.description.es.length <= this.LISTING_DESCRIPTION_MAX_LENGTH))
                    &&
                    ((listing.description.de == null) || (listing.description.de != null) && (listing.description.de.length >= this.LISTING_DESCRIPTION_MIN_LENGTH) && (listing.description.de.length <= this.LISTING_DESCRIPTION_MAX_LENGTH))
                )
            )
        );
    }

    function step2IsComplete (listing) {
        return (
            (listing.geoData !== undefined)
            &&
            ((listing.geoData.city !== undefined) && (listing.geoData.city !== null) && (typeof listing.geoData.city) !== "string")
            //&&
            //(listing.geoData.zone != null)
            &&
            (listing.geoData.address != null)
            );
    }

    function step3IsComplete(listing) {
        return (listing.subscription !== null);
    }
}]);