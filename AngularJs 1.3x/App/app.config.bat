﻿app.constant('MEDIA_SERVER', {
    baseUrl: 'http://images.bat1.ams.casa.it/',
    url: 'http://images.bat1.ams.casa.it/external//images.bat1.ams.casa.it/images/casanet_temp/'
});

app.config(['$locationProvider', 'uiGmapGoogleMapApiProvider', function ($locationProvider, uiGmapGoogleMapApiProvider) {
    $locationProvider.html5Mode(true);
    uiGmapGoogleMapApiProvider.configure({
        china: false,
        key: '',
        //    key: 'your api key',
        v: '3.17',
        libraries: 'geometry'
    });
}]);