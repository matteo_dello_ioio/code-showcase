
angular.module('CasaNet.AccountManagement', []);

angular.module('CasaNet.AccountManagement')
.factory('accountManagementService', [function () {

    // interface
    var service = {
        //serviceBaseUrl: '/api/ListingManagement/',
        hasSubscriptionActive: hasSubscriptionActive,
        hasTopActive: hasTopActive,
        hasSuperTopActive: hasSuperTopActive,
        hasPremiereActive: hasPremiereActive,
        hasDepthActive: hasDepthActive
    };
    return service;

    function hasSubscriptionActive(listing) {
        var result = false;
        for (var i = 0; listing.activeProducts && i < listing.activeProducts.length; i++) {
            if (listing.activeProducts[i].name.toLowerCase() === "smart" || listing.activeProducts[i].name.toLowerCase() === "gold" || listing.activeProducts[i].name.toLowerCase() === "gratis") {
                result = true;
                break;
            }
        }
        return result;
    }

    function hasTopActive(listing) {
        var result = false;
        for (var i = 0; listing.activeProducts && i < listing.activeProducts.length; i++) {
            //if (listing.activeDepthProducts[i].id == 488 || listing.activeDepthProducts[i].id == 489 || listing.activeDepthProducts[i].id == 490 || listing.activeDepthProducts[i].id == 491) {
            if (listing.activeProducts[i].name.toLowerCase().indexOf("annuncio top") != -1) {
                result = true;
                break;
            }
        }
        return result;
    }

    function hasSuperTopActive(listing) {
        var result = false;
        for (var i = 0; listing.activeProducts && i < listing.activeProducts.length; i++) {
            if (listing.activeProducts[i].name.toLowerCase() === "annuncio supertop") {
                result = true;
                break;
            }
        }
        return result;
    }

    function hasPremiereActive(listing) {
        var result = false;
        for (var i = 0; listing.activeProducts && i < listing.activeProducts.length; i++) {
            if (listing.activeProducts[i].name.toLowerCase() === "annuncio premiere") {
                result = true;
                break;
            }
        }
        return result;
    }

    function hasDepthActive(listing) {
        return (
            (this.hasTopActive(listing))
            ||
            (this.hasSuperTopActive(listing))
            ||
            (this.hasPremiereActive(listing))
        );
    }

}]);