﻿
angular.module('CasaNet')
.config(['$httpProvider', 'AUTH_EVENTS', function ($httpProvider, AUTH_EVENTS) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.xsrfHeaderName = window.headerName;
    $httpProvider.defaults.xsrfCookieName = window.cookieName;
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $httpProvider.interceptors.push(['$q', '$rootScope', '$location', 'appHelper', function ($q, $rootScope, $location, appHelper) {
        return {
            'request': function (config) {
                appHelper.setLoadingData(true, ((config.headers._callId) ? config.headers._callId : ''));
                config.headers.requestResource = $location.$$url; //TODO: if there is some problem with API comment this!

                return config;
            },
            'response': function (response) {
                appHelper.setLoadingData(false, ((response && response.config.headers._callId) ? response.config.headers._callId : ''));
                return response;
            },
            'responseError': function (response) {
                appHelper.setLoadingData(false, ((response.config.headers._callId) ? response.config.headers._callId : ''));

                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthorized,
                    419: AUTH_EVENTS.sessionTimeout,
                    440: AUTH_EVENTS.sessionTimeout
                }[response.status], response);

                return $q.reject(response);
            }
        };
    }]);
}]);