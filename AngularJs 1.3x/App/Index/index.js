﻿app.controller('indexController', ['$rootScope', '$scope', '$window', '$state', 'applicationGlobals', 'authDataService', 'listingEditLookupDataService', '$templateCache', '$modal', 'appDataService', '$analytics', 'appHelper',
function ($rootScope, $scope, $window, $state, applicationGlobals, authDataService, listingEditLookupDataService, $templateCache, $modal, appDataService, $analytics, appHelper) {


    $scope.appHelper = appHelper;

    $analytics.pageTrack('/');

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        //Clean the cache for page templates
        var states = toState.name.split('.');
        var stateName = '';
        _.each(states, function (name, index, list) {
            if (stateName != '')
                stateName = stateName + '.';
            stateName = stateName + name;
            var state = $state.get(stateName);
            if (state.views) {
                _.each(state.views, function (element, index, list) {
                    if (element.templateUrl) {
                        $templateCache.remove(element.templateUrl);
                    }
                });
            }
        });

        //Reload counters
        if (states && states.length > 0 && states[0] == 'Root') {
            appDataService.loadCurrentUser();
            appDataService.loadCounters();
        }
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (applicationGlobals.xsScreenMode === true) {
            $scope.leftSidebarHidden = true;
        }
    });

    $rootScope.$on('SessionExpired', function () {
        $window.location.href = "/Home/Login";
    });

    $scope.init = function () {
        $scope.applicationGlobals = applicationGlobals;
        applicationGlobals.startTutorial = $scope.showTutorial;

        //if (applicationGlobals.showTutorial) {
        //    applicationGlobals.tutorialStepNumber = 0;
        //    $scope.showTutorial();
        //} else {
        //    applicationGlobals.tutorialStepNumber = 5;
        //}

 
    };


    $scope.menuitems = [
        { Text: 'Annunci', Url: $state.href('Root.Home', {}, { absolute: false }), IconClass: 'icon-annuncio', trackingId: 'listings_sidemenu' },
        { Text: 'Nuovo Annuncio', Url: $state.href('Root.ListingEdit.EstateData', {}, { absolute: false }), IconClass: 'icon-nuovo_annuncio', shouldTrack: true, trackingId: 'new_listing_sidemenu' },
        { Text: 'Prodotti', Url: $state.href('Root.Products', {}, { absolute: false }), IconClass: 'icon-prodotti', trackingId: 'products_sidemenu' },
        { Text: 'Richieste', Url: $state.href('Root.SpecificRequests', {}, { absolute: false }), OccursContainer: 'specificRequestsCount', IconClass: 'icon-notifica', OccursColor: 'bg-red', trackingId: 'requests_sidemenu' },
        { Text: 'Servizi Extra', Url: $state.href('Root.Extra', {}, { absolute: false }), IconClass: 'icon-extra', trackingId: 'extras_sidemenu' }
    ];

    $scope.leftSidebarHidden = false;

    $scope.xsWidthCheck = function (xs) {
        if (xs === true && !$scope.leftSidebarHidden) {
            $scope.leftSidebarHidden = true;
        } else if (xs === false && $scope.leftSidebarHidden === true) {
            $scope.leftSidebarHidden = false;
            $rootScope.$broadcast('scroll');
        }
        applicationGlobals.xsScreenMode = xs;
    };

    $scope.leftSidebarToggle = function () {
        $scope.leftSidebarHidden = !$scope.leftSidebarHidden;
        if (!$scope.leftSidebarHidden)
            $rootScope.$broadcast('scroll', null, 200);
    };

    $scope.signOut = function () {

        authDataService.signout()
            .then(function (resp) {
                if (resp.succeed) {
                    $window.location.href = '/Home/Login';
                }
            })
            .catch(function () {
                //TODO: Correggere queste istruzioni, NON PERMESSE IN QUESTA FASE!!!
                $('#errormessage').show();
                $('#errormessage').text("Some error has occurred. Try again");
            });
    };

    $scope.showTutorial = function () {
        if (applicationGlobals.windowWidth >= 1200) {
            $rootScope.$broadcast('scroll', { scroll: 'top' });
            applicationGlobals.tutorialStart = $modal.open({
                animation: true,
                templateUrl: 'App/Tutorial/tutorialSteps.html',
                controller: 'tutorialController',
            });
        }
    };

    $scope.$state = $state;

    $scope.init();

}]);

app.controller('tutorialController', ['$scope', '$modalInstance', 'applicationGlobals',
function ($scope, $modalInstance, applicationGlobals) {

    $scope.ok = function () {
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        applicationGlobals.tutorialStepNumber = 5;
    };

    $scope.init = function () {

    }

    applicationGlobals.tutorialStepNumber = 0;

    $scope.init();
}]);

app.directive('scrollbarDisable', [function () {
    return {
        link: function (scope, element, attrs) {
            scope.$watch(attrs.scrollbarDisable, function (newValue, oldValue) {
                if (newValue === true) {
                    $(attrs.scrollbarTarget).attr('style', 'overflow:hidden !important;');
                } else {
                    $(attrs.scrollbarTarget).attr('style', '');
                }
            });
        }
    };
}]);

app.directive('widthWatch', ['appHelper', 'applicationGlobals', function (appHelper, applicationGlobals) {
    return {
        scope: {
            widthWatch: '&widthWatch'
        },
        link: function (scope, element, attrs) {
            scope.appHelper = appHelper;
            scope.applicationGlobals = applicationGlobals;
            scope.__appWidth = -1;
            scope.widthLimit = 1024;

            scope.widthWatchDo = function () {
                var newWidth = scope.applicationGlobals.windowWidth;

                var targetMethod = scope.widthWatch();
                if ((scope.__appWidth >= scope.widthLimit && newWidth < scope.widthLimit) || (scope.__appWidth == -1 && newWidth < scope.widthLimit)) {
                    targetMethod(true);
                } else if (scope.__appWidth < scope.widthLimit && newWidth >= scope.widthLimit) {
                    targetMethod(false);
                }

                scope.__appWidth = newWidth;
            };

            $(window).resize(function () {
                scope.appHelper.storeWindowWidth($(window).width());
                scope.widthWatchDo();
                scope.$apply();
            });

            scope.appHelper.storeWindowWidth($(window).width());
            scope.widthWatchDo(); //Execute check function for the first time
        }
    };
}]);