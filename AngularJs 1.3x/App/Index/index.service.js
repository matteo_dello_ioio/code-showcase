﻿app.factory('indexControllerDataService', ['$http', '$q', function ($http, $q) {
    return {
        getidentity: function () {
            var deferred = $q.defer();
            $http({ method: 'GET', url: '/api/Home/GetIdentity'}).success(function (ajaxRespone, status, headers, config) {
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        }
    }
}]);