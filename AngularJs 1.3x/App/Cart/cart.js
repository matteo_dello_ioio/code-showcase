
app.controller('CartController', ['$filter', '$rootScope', '$scope', 'catalogDataService', 'applicationGlobals', 'userProfileDataService', 'GeoDataLookupDataService', 'MEDIA_SERVER', 'cartDataService', 'appHelper', '$window',
function ($filter, $rootScope, $scope, catalogDataService, applicationGlobals, userProfileDataService, GeoDataLookupDataService, MEDIA_SERVER, cartDataService, appHelper, $window) {

    $scope.email_validator = '(^\\d{6}$)|(^[^@\\"\']*?[^@]*?@[^@\\"\']*?\\.[^@.\\"\']{2,6}$)';
    $scope.MEDIA_SERVER = MEDIA_SERVER;

    $scope.PaymentMethod = null;

    $scope.Cart = null;

    $scope.$watch('Cart', function () {
        if ($scope.Cart) {
            $scope.BillingDataMandatory = $scope.Cart.total > 0;
        }
    });

    var updateBillingInfo = function () {
        return userProfileDataService.updatebillinginfo('main', $scope.BillingInfo)
                .then(function (response) {
                    if (response.succeed) {
                        $scope.BillingInfo = response.data.billingInfo;
                    }
                });
    }

    var checkoutCart = function () {
        return cartDataService.checkout('main', $scope.PaymentMethod).then(function (resp) {
            window.location = resp;
        }, function (reason) {
            $scope.Message = "Unable to checkout the cart. Try again";
            console.log($scope.Message, reason);
        })
        .catch(function () {
            console.log('...catch');
            $scope.Message = "Some error has occurred. Try again";
        });
    }

    $scope.PhoneValStatus = 0;
    $scope.showPlvErrors = false;
    $scope.showPlvErrorsNr = 0;

    $scope.PhoneIsAvailable = function () {

        if ($scope.PLVform.plvPhoneNumber.$error.pattern) {
            $scope.showPlvErrors = true;
            return;
        }

        cartDataService.phoneIsAvailable('main', $scope.plvPhoneNumber).then(function (ret) {

            if (ret.data === true) {
                $scope.PhoneValStatus = 2;

                var isVisible = false;

                if ($scope.plvPhoneHidden === undefined || $scope.plvPhoneHidden === false) {
                    isVisible = true;
                }

                casaPhoneValidator.setPhoneNumber($scope.plvPhoneNumber);
                casaPhoneValidator.setPhoneVisible(isVisible);
                $scope.phoneNr = $scope.plvPhoneNumber;
            } else {
                $scope.PLVform.$setPristine();
                $scope.showPlvErrors = true;
                $scope.showPlvErrorsNr = 11;
            }
        });

    }

    $scope.PreCheckout = function () {

        if ($scope.Cart.total > 0) {
            if (!$scope.PaymentMethod) {
                $scope.paymentMethodNotSelected = true;
                return false;
            }
        }

        if ($scope.Customer.isPhoneVerified == true) {
            $scope.Checkout();
        }
        else {
            $("#PLVcontrol").modal("show");
            $scope.PhoneValStatus = 1;
        }
    }

    $scope.Checkout = function () {

        if ($scope.Cart.total > 0) {

            if ($scope.BillingDataBlocked == false) {
                var taxCodeInvalid = ($scope.BillingInfoForm.$invalid || $scope.taxCodeIsPersonal === null); // taxCodeIsPersonal == null indicate that a tax code is invalid
                if (taxCodeInvalid) {
                    $scope.showBillingInfoValidationErrors = taxCodeInvalid;
                    return false;
                } else {
                    if ($scope.taxCodeIsPersonal)
                        $scope.BillingInfo.fiscalCode = $scope.taxCode;
                    else
                        $scope.BillingInfo.vatCode = $scope.taxCode;

                    updateBillingInfo()
                        .then(checkoutCart)
                        .catch(function () {
                            console.log('...catch');
                            $scope.Message = "Some error has occurred. Try again";
                        });
                }
            } else {
                checkoutCart();
            }
        } else {
            checkoutCart();
        }
    }

    $scope.GetCart = function () {
        cartDataService.get('main', $scope.PromoCode).then(function (resp) {
            if (!resp)
                $scope.Cart = {};
            else
                $scope.Cart = resp;
        }, function (reason) {
            console.log("Unable to retrieve the cart.", reason);
            $scope.Message = "Some error has occurred. Try again";
        })
        .catch(function () {
            console.log('...catch');
            $scope.Message = "Some error has occurred. Try again";
        });
    }

    $scope.removeItemFromCart = function (productId, listingId) {
        //alert("not yet implemented");
        cartDataService.removeItem('main', null, productId, listingId)
            .then(function (response) {
                $scope.GetCart();
            });
    }

    $scope.emptyCart = function () {
        cartDataService.empty('main')
            .then(function (response) {
                $scope.GetCart();
            });
    }

    $scope.applyDiscount = function () {

        cartDataService.applyPromoCode('main', $scope.PromoCode)
        .then(function (response) {
            if (response.items == null || response.items.length == 0) {
                alert("Codice promo non valido.")
            } else {
                $scope.GetCart();
            }

        }, function (reason) {

        });
    }

    // *** BILLING ***
    var PersonalTaxCodeRegEx = /^([A-Za-z]{6}[0-9lmnpqrstuvLMNPQRSTUV]{2}[abcdehlmprstABCDEHLMPRST]{1}[0-9lmnpqrstuvLMNPQRSTUV]{2}[A-Za-z]{1}[0-9lmnpqrstuvLMNPQRSTUV]{3}[A-Za-z]{1})$/;
    var VATCodeRegEx = /^[0-9]{11}$/;

    userProfileDataService.get('main')
    .then(function (response) {
        if (response.succeed) {
            $scope.Customer = response.data;
            $scope.plvPhoneNumber = $scope.Customer.phoneMainNr;
            $scope.plvPhoneHidden = !$scope.Customer.hasVisiblePhone;
            $scope.BillingInfo = response.data.billingInfo;
            if ($scope.BillingInfo)
                $scope.taxCode = (!$scope.BillingInfo.fiscalCode) ? $scope.BillingInfo.vatCode : $scope.BillingInfo.fiscalCode;
            $scope.showHide = ($scope.BillingInfo == null);

            if (!$scope.BillingInfo)
                $scope.BillingDataBlocked = false;
            else
                $scope.BillingDataBlocked = true;
        }
    });

    $scope.showBillingInfoValidationErrors = false;
    //$scope.BillingDataBlocked = true;
    //$scope.BillingDataMandatory = $scope.Cart.total > 0;
    $scope.taxCode = null;
    $scope.taxCodeIsPersonal = null; // null -> unknown code | true -> codice fiscale | false -> VAT

    $scope.Lookup = {
        Cities: []
    }

    $scope.GetCitiesByName = function (name) {
        //console.log('executing GetCities ' + val);
        return GeoDataLookupDataService.getCitiesByName('main', name)
            .then(function (resp) {
                if (!resp)
                    $scope.Lookup.Cities = [];
                else
                    $scope.Lookup.Cities = resp;
                return $scope.Lookup.Cities;
            }, function (reason) {
                console.log("Unable to retrieve cities.", reason);
                $scope.Message = "Some error has occurred. Try again";
            })
            .catch(function () {
                console.log('...catch');
                $scope.Message = "Some error has occurred. Try again";
            });
    }

    $scope.$watch('taxCode', function () {
        if ($scope.taxCode != null) {
            if (PersonalTaxCodeRegEx.test($scope.taxCode))
                $scope.taxCodeIsPersonal = true;
            else if (VATCodeRegEx.test($scope.taxCode)) {
                $scope.taxCodeIsPersonal = false;
            }
            else {
                $scope.taxCodeIsPersonal = null;
            }
        } else {
            $scope.taxCodeIsPersonal = null;
        }

        console.log($scope.taxCodeIsPersonal);
    });
    // *** /BILLING ***


    $scope.init = function () {
        $scope.GetCart();
        $scope.appHelper = appHelper;
        $window.callbackPhoneValidatorCompact = function (statusValue, obj) {
            $scope.callbackPhoneValidatorCompact(statusValue, obj);
            $scope.$apply();
        };

        $scope.$watch('plvPhoneNumber', function (newValue, oldValue) {
            if (newValue != null || oldValue != null) {
                $scope.showPlvErrorsNr = 0;
                $scope.showPlvErrors = false;
                if (!$scope.PLVform.plvPhoneNumber.$error.pattern) {
                    $scope.showPlvErrors = false;
                    return;
                }
            }
        }, true);
        casaPhoneValidator.dispose();
        casaPhoneValidator.init();
    }

    $scope.callbackPhoneValidatorCompact = function (statusValue, obj) {

        switch (statusValue) {
            case casaPhoneValidator.status.properties['RESTART'].value:
                //Annulla
                $scope.PhoneValStatus = 0;
                $scope.showPlvErrors = false;
                $scope.showPlvErrorsNr = 0;
                break;
            case casaPhoneValidator.status.properties['CAPTCHAVALID'].value:
                $scope.PhoneValStatus = 3;
                break;
            case casaPhoneValidator.status.properties['PHONEVALID'].value:

                var isVisible = false;

                if ($scope.plvPhoneHidden === undefined || $scope.plvPhoneHidden === false) {
                    isVisible = true;
                }

                cartDataService.validatePhone('main', obj.phoneIntPrefix, obj.phoneNumber, isVisible).then(function (ret) {

                    if (ret != null && ret.data === true) {
                        $scope.Customer.isPhoneVerified = true;
                        $scope.PhoneValStatus = 4;
                        $scope.Checkout();
                    }
                });
                break;
            case casaPhoneValidator.status.properties['RESTART'].value:
                casaPhoneValidator.dispose();
                casaPhoneValidator.init();
                break;
            case casaPhoneValidator.status.properties['ENDVERIF'].value:
                break;
            default:
                break;
        }
    }



    $scope.init();
}]);
