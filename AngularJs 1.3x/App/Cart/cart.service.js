﻿


/* HINT: this service can be used to share Estate data between controllers */
app.factory('cartDataService', ['$q', 'appDataHelper', function ($q, appDataHelper) {

    // interface
    var service = {
        serviceBaseUrl: '/api/Cart/',
        empty: empty,
        applyPromoCode: applyPromoCode,
        get: get,
        addItem: addItem,
        removeItem: removeItem,
        checkout: checkout,
        phoneIsAvailable: phoneIsAvailable,
        validatePhone: validatePhone,
        getPurchaseSummary: getPurchaseSummary
    };
    return service;


    // implementation
    function checkout(callId, paymentMethod) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'CheckoutCart', '"' + paymentMethod + '"')
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function empty(callId) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'Empty', {})
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function applyPromoCode(callId, promoCode) {

        //if (!siteId)
        //    siteId = '';// workaround to avoid problem with asp.net routing

        if (!promoCode)
            promoCode = '';// workaround to avoid problem with asp.net routing

        var deferred = $q.defer();

        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'ApplyPromoCode', '"'+promoCode+'"')
        .then(function (ajaxResponse) {
            deferred.resolve(ajaxResponse.data);
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function get(callId, promoCode) {

        if (!promoCode)
            promoCode = '';// workaround to avoid problem with asp.net routing

        var deferred = $q.defer();

        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'Get', { promoCode: promoCode })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getPurchaseSummary(callId, transactionId, token) {

        var deferred = $q.defer();

        if (!transactionId)
            deferred.reject("transactionId cannot be null");

        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetPurchaseSummary', { transactionId: transactionId, token: token })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function addItem(callId, siteId, productId, price, priceVAT, quantity, listingId, description) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'AddToCart', { PriceWithVAT: priceVAT, PriceNet: price, quantity: quantity, productID: productId, linkedObjectType: 'listing', linkedObjectID: listingId, description: description })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function removeItem(callId, siteId, productId, listingId) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpPost(callId, service.serviceBaseUrl + 'RemoveFromCart', { productID: productId, linkedObjectType: 'listing', linkedObjectID: listingId })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function phoneIsAvailable(callId, phoneNr) {
        return appDataHelper.httpPost(callId, '/api/CustomerApi/PhoneIsAvailable', { phoneNumber: phoneNr });
    }

    function validatePhone(callId, phonePrefix, phoneNr, phoneIsVisible) {
        
        return appDataHelper.httpPost(callId, '/api/CustomerApi/ValidatePhone', { phoneNumber: phoneNr, phonePrefix: phonePrefix, IsVisible: phoneIsVisible });

    }

}]);