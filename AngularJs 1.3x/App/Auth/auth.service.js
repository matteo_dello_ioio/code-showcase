﻿app.factory('authDataService', ['$http', '$q', 'authHelper', 'appDataHelper', function ($http, $q, authHelper, appDataHelper) {
    return {
        serviceBaseUrl: '/api/Auth/',
        signin: function(callId, authData) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: '/api/Auth/SignIn', headers:{_callId: callId}, data: authData }).success(function(ajaxRespone, status, headers, config) {
                if (ajaxRespone && ajaxRespone.succeed) {
                    if (authData.remember) {
                        authHelper.setAuthToken(ajaxRespone.data.token, false);
                    } else {
                        authHelper.setAuthToken(ajaxRespone.data.token, true);
                    }
                }
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        },
        externalSignIn: function(callId, extToken) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: '/api/Auth/ExternalSignIn', headers: { _callId: callId }, data: { extToken: extToken } }).success(function (ajaxRespone, status, headers, config) {
                if (ajaxRespone && ajaxRespone.succeed) {
                    authHelper.setAuthToken(ajaxRespone.data.token, false);
                    window.location.href = "/home/index";
                }
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        },
        signout: function (callId) {
            var token = { token: authHelper.getAuthToken() }

            var deferred = $q.defer();
            $http({ method: 'POST', url: '/api/Auth/SignOut', headers:{_callId: callId}, data: token }).success(function (ajaxRespone, status, headers, config) {
                if (ajaxRespone && ajaxRespone.succeed)
                    authHelper.deleteAuthToken();

                deferred.resolve(ajaxRespone);
            })
            .error(function(ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        },
        getgeneralinfo: function (callId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetGeneralInfo', {}).then(function (resp) {
                if (resp.succeed) {
                    authHelper.setCurrentUser(resp.data.user);
                }
                return resp;
            });
        },
        emailon: function (callId, authData) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: '/api/Auth/EmailOn', headers:{_callId: callId}, data: authData }).success(function (ajaxRespone, status, headers, config) {
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        }
    }
}]);

app.factory('authHelper', ['$q', 'appHelper', 'applicationGlobals', function ($q, appHelper, applicationGlobals) {
    return {
        setAuthToken: function (value, cancelOnClose) {
            appHelper.setCookie(window.cookieName, value, '/', ((cancelOnClose) ? null : 30));
        },
        getAuthToken: function () {
            return appHelper.getCookie(window.cookieName);
        },
        deleteAuthToken: function () {
            appHelper.deleteCookie(window.cookieName, '/');
        },
        setCurrentUser: function(user) {
            applicationGlobals.currentUser = user;
        }
    }
}]);