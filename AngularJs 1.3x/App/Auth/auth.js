﻿app.controller('authController', ['applicationGlobals', '$rootScope', '$scope', '$window', '$state', 'authDataService', 'authHelper', function (applicationGlobals, $rootScope, $scope, $window, $state, authDataService, authHelper) {

    $scope.username_validator = '(^\\d{6}$)|(^[^@\\"\']*?[^@]*?@[^@\\"\']*?\\.[^@.\\"\']{2,6}$)';

    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == 'Login') {

                if (toParams.extToken) {
                    authDataService.externalSignIn('ln_loader', toParams.extToken);
                }

                if (toParams.urlReturn) {
                    $scope.urlReturn = toParams.urlReturn;
                } else {
                    $scope.urlReturn = '/';
                }

                var postData = {
                    token: toParams.token
                };
                authDataService.emailon('emailOn', postData).then(function (resp) {
                    if (resp.succeed) {
                        $scope.Username = resp.data.email;
                    }
                });
            }
        });

    $scope.signIn = function () {
        $scope.signInAttempt = true;

        if ($scope.loginForm.$invalid === false) {
            var postData = {
                login: $scope.Username,
                password: $scope.Password,
                remember: $scope.Remember
            };

            authDataService.signin('ln_loader', postData)
                .then(function (resp) {
                    
                    $scope.signInAttempt = false;

                    if (resp.succeed) {

                        $rootScope.$emit('UserLoggedIn');

                        if (resp.data.accessCount <= 3) {
                            $window.location.href = "/?tut";
                            return;
                        }

                        $scope.loadingHomepage = true;
                        $window.location.href = $scope.urlReturn;
                    } else {
                        if (resp.errMsg == "UserNotExist") {
                            $scope.Message = "Dati di accesso non validi";
                        } else if (resp.errMsg == "InactiveUser") {
                            $scope.Message = "Dati di accesso non validi";
                        } else if (resp.errMsg == "WrongPassword") {
                            $scope.Message = "Username e/o password non validi";
                        } else if (resp.errMsg == "EMailNotVerified") {
                            $scope.Message = "E-mail non verificata";
                        } else {
                            $scope.Message = resp.errMsg;
                        }
                    }
                }, function (resp) {
                    if (resp.errMsg == "UserNotExist") {
                        $scope.Message = "Dati di accesso non validi";
                    } else if (resp.errMsg == "InactiveUser") {
                        $scope.Message = "Dati di accesso non validi";
                    } else if (resp.errMsg == "WrongPassword") {
                        $scope.Message = "Username e/o password non validi";
                    } else if (resp.errMsg == "EMailNotVerified") {
                        $scope.Message = "E-mail non verificata";
                    } else {
                        $scope.Message = resp.errMsg;
                    }
                })
                .catch(function() {
                    $scope.Message = "Some error has occurred. Try again";
                });
        } else {
            $scope.Message = "Dati di accesso non validi";
        }
    }

    $scope.signOut = function () {
        authDataService.signout('ln_loader')
            .then(function (resp) {
                if (resp.succeed) {
                    $window.location.href = '/Home/Login';
                } else {
                    //
                }
            })
            .catch(function () {
                $scope.errorMessage = "Some error has occurred. Try again";
                $('#errormessage').show();
                $('#errormessage').text("Some error has occurred. Try again");
            });
    }

    $scope.forgotPassword = function () {
        $window.location.href = '/ForgotPassword';
    }

}]);