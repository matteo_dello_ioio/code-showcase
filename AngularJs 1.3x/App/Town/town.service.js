﻿app.factory('townDataService', ['appDataHelper', function (appDataHelper) {
    return {
        serviceBaseUrl: '/api/townapi/',
        find: function(callId, searchString) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'Find', { town: searchString });
        }
    };
}]);