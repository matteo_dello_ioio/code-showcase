﻿app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('Login', {
            url: "^/Home/Login?urlReturn&token&extToken"
        })
        .state('ResetPassword', {
            url: "^/ResetPassword?token"
        })
        .state('ForgotPassword', {
            url: "^/ForgotPassword"
        })
        .state('Root', {
            abstract: true,
            resolve: {
                lookup: function (listingEditLookupDataService) {
                    //console.log("ROOT RESOLVE", window.dataLayer);
                    return listingEditLookupDataService.loadLookups('main');
                },
                currentUser: function (appDataService) {
                    return appDataService.loadCurrentUser('main');
                },
                counter: function (appDataService) {
                    return appDataService.loadCounters('main');
                },
                productConfigurations: function (catalogDataService) {
                    return catalogDataService.loadConfigurations('main');
                }
            }
        })
        .state('Root.CheckoutOK', {
            url: "^/Checkout/Confirmed/:transactionId?param",
            views: {
                'mainContent@': {
                    templateUrl: "App/PaymentLanding/checkout-ok.html",
                    controller: 'paymentConfirmationController'
                },
                'rightCol@Root.PaymentOk': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            resolve: {
                summary: function ($state, $stateParams, cartDataService) {
                    return cartDataService.getPurchaseSummary('main', $stateParams.transactionId, $stateParams.param).then(function (resp) {
                        if (resp)
                            return resp;
                    }, function (reason) {
                        $state.go('Root.Home');
                    })
                    .catch(function () {
                        console.log('...catch');
                        return null;
                    });
                },
                transactionProducts: function ($rootScope, $window, $state, $stateParams, summary, analytics) {
                    if (summary.cart.items.length > 0 && summary.cart.total > 0) {

                        var transactionTrackingData = {
                            'transactionId': $stateParams.transactionId,
                            'transactionAffiliation': summary.paymentMethod, // carta-di-credito | paypal
                            'transactionTotal': summary.cart.total,
                            'transactionProducts': []
                        };

                        for (var listingIndex = 0; listingIndex < summary.cart.items.length; ++listingIndex) {
                            var products = summary.cart.items[listingIndex].products;
                            for (var productIndex = 0; productIndex < products.length; ++productIndex) {
                                var productItem = products[productIndex];
                                var transactionProduct = {
                                    'sku': productItem.productSiteId,
                                    'name': productItem.name,
                                    'price': productItem.priceInclVAT,
                                    'quantity': 1
                                };
                                transactionTrackingData['transactionProducts'].push(transactionProduct);
                            }
                        }

                        //analytics.push(transactionTrackingData);
                        $rootScope.$emit('ECommerceTransactionCompleted', transactionTrackingData);
                    }
                },
                billingInfo: function (userProfileDataService) {
                    return userProfileDataService.get('main')
                            .then(function (response) {
                                if (response.succeed) {
                                    return response.data.billingInfo;
                                }
                            });
                },
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.CheckoutKO', {
            url: "^/Checkout/Aborted",
            views: {
                'mainContent@': {
                    templateUrl: "App/PaymentLanding/checkout-ko.html",
                    controller: ''
                },
                'rightCol@Root.PaymentKo': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            resolve: {
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.Home', {
            url: "/?tut",
            views: {
                'mainContent@': {
                    templateUrl: 'App/ListingManagement/ListingManagement.html',
                    controller: 'listingManagementController'
                },
                'rightCol@Root.Home': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                config: function(){
                    return {
                        'finalizeListingStateName': 'Root.ListingEdit.ProductsCatalog',
                        'cartStateName': 'Root.Cart'
                    };
                },
                tutorialtrigger: ['$stateParams', 'applicationGlobals', '$location', function ($stateParams, applicationGlobals, $location) {
                if ($stateParams.tut !== undefined) {
                    applicationGlobals.startTutorial();
                    $location.search('tut', null);

                }
                return;
            }],
                propertyListSource: ['listingManagerDataService', function (listingManagerDataService) {
                    return listingManagerDataService.get('main', 10, 0, '', '').then(function (resp) {
                        if (resp.succeed) {
                            return resp.data;
                        }
                        return null;
                    });
                }],
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.UserProfile', {
            url: "/UserProfile",
            views: {
                'mainContent@': {
                    templateUrl: 'App/UserProfile/UserProfile.html',
                    controller: 'userProfileController'
                },
                'rightCol@Root.UserProfile': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                userProfileSource: function (userProfileDataService) {
                    return userProfileDataService.get('main').then(function (resp) {
                        if (resp.succeed) {
                            return resp.data;
                        }
                        return null;
                    });
                },
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.ListingEdit', {
            abstract: true,
            url: "/ListingEdit/:listingId",
            views: {
                'mainContent@': {
                    templateUrl: 'App/ListingEdit/listing-edit.html',
                    controller: 'ListingEditController'
                },
                'rightCol@Root.ListingEdit': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            data: {
                prevState: null,
                nextState: null
            },
            resolve: {
                listing: ['$stateParams', 'listingEditDataService', function ($stateParams, listingEditDataService) {
                    if ($stateParams.listingId) {
                        return listingEditDataService.getByKey('main', $stateParams.listingId).then(function (resp) {
                            if (resp.succeed) {
                                listingEditDataService.listing = resp.data;
                                return resp.data;
                            }
                            return null;
                        });
                    }
                    else {
                        return listingEditDataService.getNew();
                    }
                }],
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }],
                energyEfficiencyValuesSource: ['listingEditLookupDataService', function (listingEditLookupDataService) {
                    return listingEditLookupDataService.getEnergyEfficiencyRatings('main').then(function (response) {
                        return response;
                    });
                }]
            },
            onEnter: function () {
                console.log("ONENTER ListingEdit");
            },
            onExit: function () {
                console.log("ONEXIT ListingEdit");
            }
        })
            .state('Root.ListingEdit.EstateData', {
                url: "/EstateData",
                views: {
                    'steps': {
                        templateUrl: 'App/ListingEdit/listing-edit-step-1.html',
                        controller: 'EstateDataController'
                    }
                },
                data: {
                    prevState: null,
                    nextState: 'Root.ListingEdit.GeoAndPicture'
                },
                onEnter: ['$rootScope', function ($rootScope) {
                    $rootScope.$broadcast('scroll', { scroll: 'top' });
                }],
                onExit: function () {

                }
            })
            .state('Root.ListingEdit.GeoAndPicture', {
                url: "/GeoAndPicture",
                views: {
                    'steps': {
                        templateUrl: 'App/ListingEdit/listing-edit-step-2.html',
                        controller: 'Step2Controller'
                    },
                    'geodata@Root.ListingEdit.GeoAndPicture': {
                        templateUrl: 'App/GeoData/geo-data-editor.html',
                        controller: 'GeoDataController'
                    }
                },
                resolve: {
                    canEditCity: ['listing', 'accountManagementService', function (listing, accountManagementService) {
                        return !accountManagementService.hasDepthActive(listing);
                    }],
                },
                data: {
                    prevState: 'Root.ListingEdit.EstateData',
                    nextState: 'Root.ListingEdit.ProductsCatalog'
                },
                onEnter: ['$rootScope', '$state', '$stateParams', 'listingEditDataService', 'listing', function ($rootScope, $state, $stateParams, listingEditDataService, listing) {
                    if (!$stateParams.listingId && !listingEditDataService.step1IsComplete(listing)) {
                        $state.go('Root.ListingEdit.EstateData', $stateParams);
                    }
                    else {
                        $rootScope.$broadcast('scroll', { scroll: 'top' });
                    }
                }],
                onExit: function () {

                }
            })
            .state('Root.ListingEdit.ProductsCatalog', {
                url: "/ProductsCatalog",
                views: {
                    'steps': {
                        templateUrl: 'App/ListingEdit/listing-edit-step-3.html',
                        controller: 'CatalogController'
                    }
                },
                data: {
                    prevState: 'Root.ListingEdit.GeoAndPicture',
                    nextState: 'Root.Cart'
                },
                resolve: {
                    productCatalogSource: ['$filter', '$stateParams', 'catalogDataService', function ($filter, $stateParams, catalogDataService) {
                        return catalogDataService.get('main', 1, $stateParams.listingId, '').then(function (resp) {
                            return {
                                availableProductsCount: resp.availableProductsCount,
                                subscriptionProducts: $filter('orderBy')(resp.subscriptionProducts, '+displayOrder'),
                                depthProducts: $filter('orderBy')(resp.depthProducts, '-displayOrder')
                            }
                        }, function (reason) {
                            console.log("Unable to retrieve products: ", reason);
                            return null;
                        })
                        .catch(function () {
                            return null;
                        });
                    }]
                },
                onEnter: ['$rootScope', function ($rootScope) {
                    $rootScope.$broadcast('scroll', { scroll: 'top' });
                }]
            })
        .state('Root.SpecificRequests', {
            url: "/SpecificRequests",
            views: {
                'mainContent@': {
                    templateUrl: 'App/SpecificRequests/specificRequests.html',
                    controller: 'specificRequestsController'
                },
                'rightCol@Root.SpecificRequests': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                specificRequestsSource: function (specificRequestsDataService) {
                    return specificRequestsDataService.get('main').then(function (resp) {
                        if (resp.succeed) {
                            return resp.data;
                        }
                        return null;
                    });
                },
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.SpecificRequestResponse', {
            url: "/SpecificRequestResponse/:requestId",
            views: {
                'mainContent@': {
                    templateUrl: 'App/SpecificRequests/specificRequestResponse.html',
                    controller: 'specificRequestResponseController'
                },
                'rightCol@Root.SpecificRequestResponse': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                responseListSource: function (specificRequestResponseDataService, $stateParams) {
                    return specificRequestResponseDataService.get('main', $stateParams.requestId).then(function (resp) {
                        if (resp.succeed) {
                            return resp.data;
                        }
                        return null;
                    });
                },
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.SpecificRequestResponseExt', {
            url: "/SpecificRequestResponse/:site/:extid",
            views: {
                'mainContent@': {
                    templateUrl: 'App/SpecificRequests/specificRequestResponse.html',
                    controller: 'specificRequestResponseController'
                },
                'rightCol@Root.SpecificRequestResponse': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                responseListSource: function (specificRequestResponseDataService, $stateParams) {
                    return specificRequestResponseDataService.getExt('main', $stateParams.site, $stateParams.extid).then(function (resp) {
                        if (resp.succeed) {
                            return resp.data;
                        }
                        return null;
                    });
                },
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.Cart', {
            url: "/Cart",
            views: {
                'mainContent@': {
                    templateUrl: 'App/Cart/shoppingCart.html',
                    controller: 'CartController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }]
        })
        .state('Root.EmailVerified', {
            url: "/EmailVerified?token"
        })
        .state('Root.Products', {
            url: "/Products",
            views: {
                'mainContent@': {
                    templateUrl: 'App/Products/productsPage.html',
                },
                'rightCol@Root.Products': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('scroll', { scroll: 'top' });
            }],
            resolve: {
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.Extra', {
            url: "/Extra",
            views: {
                'mainContent@': {
                    templateUrl: 'App/Extra/extraTools.html',
                    controller: 'extraTools'
                },
                'rightCol@Root.Extra': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            resolve: {
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        })
        .state('Root.Tutorial', {
            url: "/Tutorial",
            views: {
                'mainContent@': {
                    templateUrl: "App/Tutorial/step-0.html",
                    controller: ''
                },
                'rightCol@Root.Tutorial': {
                    templateUrl: 'App/RightSideInfo/RightSideInfo.html',
                    controller: 'rightSideInfoController'
                }
            },
            resolve: {
                activableProdSource: ['productDataService', function (productDataService) {
                    return productDataService.getActivableDepthProductsWidgetData('main').then(function (response) {
                        if (response) {
                            return response;
                        }
                        return null;
                    }, function () { return null; });
                }]
            }
        });
}]);