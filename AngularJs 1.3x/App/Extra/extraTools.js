﻿app.controller('extraTools', ['$filter', '$rootScope', '$scope', 'applicationGlobals', 'userProfileDataService', 'appHelper', 
function ($filter, $rootScope, $scope, applicationGlobals, userProfileDataService, appHelper) {

    $scope.isGold = null;

    $scope.init = function () {
        userProfileDataService.get('main').then(function (resp) {
            $scope.isGold = resp.data.isGold;
        });
    }

    $scope.init();
}]);
