﻿app.controller("rightSideInfoController", ['$scope', 'applicationGlobals', 'productDataService', 'activableProdSource',
function ($scope, applicationGlobals, productDataService, activableProdSource) {
    $scope.init = function () {
        $scope.availableDepthProducts = activableProdSource;
        $scope.applicationGlobals = applicationGlobals;
    };

    $scope.init();
}]);