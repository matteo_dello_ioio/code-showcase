﻿app.factory('appDataService', ['appDataHelper', 'applicationGlobals', function(appDataHelper, applicationGlobals) {
    return {
        serviceBaseUrl: '/api/Default/',
        loadCurrentUser: function(callId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetCurrentUser', {}).then(function(res) {
                if (res.succeed)
                    applicationGlobals.currentUser = res.data;
            });
        },
        loadCounters: function(callId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetCounters', {}).then(function (res) {
                if (res.succeed)
                    applicationGlobals.counters = res.data;
            });
        }
    };
}]);

app.factory('appHelper', ['$rootScope', 'applicationGlobals', 'MEDIA_SERVER', function ($rootScope, applicationGlobals, MEDIA_SERVER) {
    var helper = {
        setCookie: function (name, value, path, cookieExpiration) {
            if (cookieExpiration)
                $.cookie(name, value, { expires: cookieExpiration, path: path });
            else
                $.cookie(name, value, { path: path });
        },
        deleteCookie: function (name, cookiePath) {
            $.removeCookie(name, { path: cookiePath });
        },
        getCookie: function (name) {
            return $.cookie(name);
        },
        loaderShow: function (value, callId) {
            if (value === true)
                $rootScope.$broadcast("loader_show", callId);
            else if (value === false)
                $rootScope.$broadcast("loader_hide", callId);
        },
        setLoadingData: function (value, callId) {
            if (value === true) {
                if (!applicationGlobals.dataRequestCount[callId])
                    applicationGlobals.dataRequestCount[callId] = 0;
                applicationGlobals.dataRequestCount[callId]++;
                this.loaderShow(true, callId);
                window.IsAjaxCallInProgress = true;
            }
            else if (value === false) {
                applicationGlobals.dataRequestCount[callId]--;
                if (applicationGlobals.dataRequestCount[callId] == 0)
                    this.loaderShow(false, callId);
                window.IsAjaxCallInProgress = false;
            }
        },
        getBreadcrumbItemFromState: function (stateName, $stateObj) {
            if (!$stateObj || !$stateObj.href)
                return ['', '', ''];

            var state = $stateObj.get(stateName);
            if (!state || !state.data || !state.data.iconCss)
                return [stateName, $stateObj.href(stateName), ''];

            return [stateName, $stateObj.href(stateName), state.data.iconCss];
        },
        storeWindowWidth: function (width) {
            if (typeof width === 'number') {
                applicationGlobals.windowWidth = width;
            }
        },
        getImageUrl: function (item, listingExternalId, width, height) {
            var url = '';
            if (item) {
                var imageIdentifier = (item.hash) ? item.hash : item.guid;
                var imageType = (typeof item.imageTypeId != 'undefined' && item.imageTypeId != null) ? item.imageTypeId : item.imageType;
                var imageOrder = (typeof item.imageOrder != 'undefined' && item.imageOrder != null) ? item.imageOrder : item.orderIndex;
                var imageUrlKey = (item.url != null && item.url.slice(0, 4).toLowerCase() != 'http') ? item.url : imageIdentifier;
                return this.getImageUrlExt(imageIdentifier, imageUrlKey, imageType, imageOrder, item.uploadReference, item.uploadKey, listingExternalId, width, height);
            } else {
                return null;
            }
        },
        getImageUrlExt: function (imageIdentifier, imageUrlKey, imageType, imageOrder, uploadReference, uploadKey, listingExternalCode, width, height) {
            var url = '';
            if (uploadReference != null) {
                url = MEDIA_SERVER.url + '/' + uploadReference;
                if (uploadKey)
                    url = url + '/' + uploadKey;
                url = url + '/' + imageIdentifier + '.jpg.' + imageUrlKey + '.jpg.' + width + 'x' + height + 'cr.jpg';
            }
            else if (listingExternalCode != null) {
                var ImagesWebBasePath = MEDIA_SERVER.baseUrl + "annunci/";
                var RealEstateWebBasePath = (ImagesWebBasePath + applicationGlobals.currentUser.internalId + "/" + listingExternalCode + "/");
                var fileName = listingExternalCode + "_" + ((imageType == 1 || imageType == 'Photo') ? "F" : "P") + "_" + (imageOrder) + ".jpg";
                url = RealEstateWebBasePath + fileName + '.' + imageUrlKey + '.jpg.' + width + 'x' + height + 'cr.jpg';
            }
            return url;
        }
    }

    return helper;
}]);

app.factory('appDataHelper', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
    var helper = {
        httpGet: function (callId, _url, _params) {
            var deferred = $q.defer();
            $http({
                method: 'GET', headers: { _callId: callId }, url: _url, params: _params
            }).success(function (ajaxRespone, status, headers, config) {

                /*
                if (ajaxRespone.succeed == false && ajaxRespone.errCode == 1410) {
                    $rootScope.$broadcast("SessionExpired");
                }*/

                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        },
        httpPost: function (callId, _url, object) {
            var deferred = $q.defer();
            $http({
                method: 'POST', headers: { _callId: callId }, url: _url, data: object
            }).success(function (ajaxRespone, status, headers, config) {
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        }
    }

    return helper;
}]);

app.factory('applicationGlobals', ['breadcrumbManager', 'MEDIA_SERVER', function (breadcrumbManager, MEDIA_SERVER) {
    var applicationGlobals = {
        dataRequestCount: {},
        breadcrumb: breadcrumbManager,
        windowWidth: 0,
        xsScreenMode: false,
        currentUser: {},
        mediaUrl: MEDIA_SERVER.url,
        showTutorial: false
    };

    return applicationGlobals;
}]);

app.factory('breadcrumbManager', function () {
    var breadcrumbManager = {
        items: [],
        currentItem: {},
        addItem: function (name, url, iconCss) {
            this.items.push({
                name: name,
                url: url,
                iconCss: iconCss
            });

            this.currentItem = this.items[this.items.length - 1];
        },
        addItems: function (collection) {
            for (var i = 0; i < collection.length; i++) {
                this.addItem(collection[i][0], collection[i][1], collection[i][2]);
            }

            this.currentItem = this.items[this.items.length - 1];
        },
        init: function(collection) {
            this.clear();
            this.addItems(collection);
        },
        clear: function () {
            this.items = [];
            this.currentItem = {};
        }
    };

    return breadcrumbManager;
});