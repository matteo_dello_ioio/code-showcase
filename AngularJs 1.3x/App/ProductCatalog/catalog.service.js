﻿


/* HINT: this service can be used to share Estate data between controllers */
app.factory('catalogDataService', ['$q', '$rootScope', '$filter', 'appDataHelper', 'applicationGlobals', function ($q, $rootScope, $filter, appDataHelper, applicationGlobals) {

    // interface
    var service = {
        serviceBaseUrl: '/api/ProductApi/',
        selectedSubscription: null,
        selectedDepth: null,
        get: get,
        loadConfigurations: loadConfigurations,
        selectSubscription: selectSubscription,
        selectDepth: selectDepth
    };
    return service;


    // implementation
    function selectSubscription(product) {
        service.selectedSubscription = product;
        $rootScope.$broadcast('CatalogSelectionUpdated');
    }

    function selectDepth(product) {
        service.selectedDepth = product;
        $rootScope.$broadcast('CatalogSelectionUpdated');
    }

    function get(callId, siteId, listingId, promoCode) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetProductsCatalog', { siteId: siteId, listingId: listingId, promoCode: promoCode })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function loadConfigurations(callId) {
        return appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetConfigurations', {}).then(function (res) {
            var data = null;
            if (res.succeed) {
                data = {};
                angular.forEach(res.data, function(element, index) {
                    this[element.id] = element;
                }, data);
                applicationGlobals.productConfigurations = data;
            }
            return data;
        });
    }

}]);