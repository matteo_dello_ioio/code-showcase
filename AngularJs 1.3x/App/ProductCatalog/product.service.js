﻿


/* HINT: this service can be used to share Estate data between controllers */
app.factory('productDataService', ['$q', '$rootScope', '$filter', 'appDataHelper', 'applicationGlobals', function ($q, $rootScope, $filter, appDataHelper, applicationGlobals) {

    // interface
    var service = {
        serviceBaseUrl: '/api/ProductApi/',
        getActivableProducts: getActivableProducts,
        getActivableDepthProducts: getActivableDepthProducts,
        getActivableDepthProductsWidgetData: getActivableDepthProductsWidgetData
    };
    return service;


    // implementation
    function getActivableProducts(callId, siteId, listingId) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetActivableProducts', { siteId: siteId, listingId: listingId })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getActivableDepthProducts(callId, siteId, listingId) {

        var deferred = $q.defer();

        if (!siteId)
            siteId = '';
        if (!listingId)
            listingId = '';

        // set the right call to delete image
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetActivableDepthProductsCatalog', { siteId: siteId, listingId: listingId })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }

    function getActivableDepthProductsWidgetData(callId) {

        var deferred = $q.defer();

        // set the right call to delete image
        appDataHelper.httpGet(callId, service.serviceBaseUrl + 'GetActivableDepthProductsWidgetData', { })
        .then(function (ajaxResponse) {
            if (ajaxResponse.succeed) {
                deferred.resolve(ajaxResponse.data.items);
            } else {
                deferred.reject(ajaxResponse);
            }
        }, function (reason) {
            deferred.reject(reason);
        });

        return deferred.promise;
    }
}]);