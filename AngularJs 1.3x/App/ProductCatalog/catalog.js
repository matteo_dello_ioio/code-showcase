﻿
app.controller('CatalogController', ['$filter', '$state', '$stateParams', '$rootScope', '$scope', 'catalogDataService', 'applicationGlobals', 'cartDataService', 'listingEditDataService', 'listing', 'productCatalogSource', 'analytics',
function ($filter, $state, $stateParams, $rootScope, $scope, catalogDataService, applicationGlobals, cartDataService, listingEditDataService, listing, productCatalogSource, analytics) {

    $scope.Listing = listing;
    $scope.applicationGlobals = applicationGlobals;
    $scope.SelectedSubscriptionIndex = null;
    $scope.SelectedSubscription = null;
    $scope.SelectedDepth = null;

    // event handlers
    $rootScope.GoToPrevState = function () {
        //console.log("GoToPrevState");
        $rootScope.$broadcast('GoPrevious');
    }

    $rootScope.GoToNextState = function () {
        var subscription = catalogDataService.selectedSubscription;
        var depth = catalogDataService.selectedDepth;

        if (subscription && subscription.isAvailable) {
            var productId = subscription.productSiteId;
            var description = subscription.name;
            var priceVAT = subscription.priceInclVAT;
            var price = subscription.priceExcVAT;
            var quantity = 1;
            var listingId = $scope.Listing.id;

            cartDataService.addItem('main', null, productId, price, priceVAT, quantity, listingId, description)
            .then(function () {

                if (depth) {
                    if (depth.isAvailable) {
                        var productId = depth.id;
                        var description = depth.name;
                        var priceVAT = depth.priceInclVAT;
                        var price = depth.priceExcVAT;
                        var quantity = 1;
                        var listingId = $stateParams.listingId;

                        cartDataService.addItem('main', null, productId, price, priceVAT, quantity, listingId, description)
                        .then(function () {
                            $rootScope.$emit('ItemAddedInCart', { 'step-name': 'carrello', 'subscription': subscription, 'depth': depth });
                            $rootScope.$broadcast('GoNext');
                        });
                    } else {
                        $rootScope.$emit('ItemAddedInCart', { 'step-name': 'carrello', 'subscription': subscription });
                        $rootScope.$broadcast('GoNext');
                    }
                } else {
                    $rootScope.$emit('ItemAddedInCart', { 'step-name': 'carrello', 'subscription': subscription });
                    $rootScope.$broadcast('GoNext');
                }
            });
        } else {
            if (listing && listing.sites.length > 0) {
                listingEditDataService.publishChanges('main', $stateParams.listingId)
                .then(function () {
                    window.location = $state.href('Root.Home');
                });
            } else if ($scope.Listing.isInCart) {
                window.location = $state.href('Root.Cart');
            } else {
                window.location = $state.href('Root.Home');
            }
        }
    };

    $scope.setShowProd = function (index) {
        if ($scope.productCatalog.subscriptionProducts[index].isAvailable)
            $scope.SelectedSubscriptionIndex = index;
    }

    $scope.init = function () {
        $scope.productCatalog = productCatalogSource;

        $scope.$watch('SelectedSubscription', function (newValue, oldValue) {
            console.log("changed subscription: ", newValue);
            $scope.SelectedDepth = null; // reset depth product selection
            catalogDataService.selectSubscription(newValue);
        }, true);

        $scope.$watch('SelectedDepth', function (newValue, oldValue) {
            console.log("changed depth: ", newValue);
            catalogDataService.selectDepth(newValue);
        }, true);
    }

    $scope.init();
}]);
