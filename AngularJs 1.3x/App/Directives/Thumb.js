﻿'use strict';


angular.module('angularFileUpload')

    .directive('ngThumb', ['$window', function ($window) {
        var fullSize = new Image();
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function (item) {
                return angular.isObject(item);// && item instanceof $window.File;
            },
            isImage: function (file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            scope: '@',
            restrict: 'A',
            template: '<canvas  degrees=\'angle\' rotate width=\'94px\' height=\'71px\'   />',
            link: function (scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);
                //fullSize.src = element.attr('src');

                function onLoadFile() {
                    var img = new Image();
                    //canvas = document.getElementById('canvas');
                    //var ctx = canvas.getContext('2d');
                    var ctx = canvas[0].getContext('2d');
                    //img.onload = onLoadImage;
                    img.src = event.target.result;
                    img.onload = drawImageScaled(img, ctx);


                }


                function drawImageScaled(img, ctx) {
                    var canvas = ctx.canvas;
                    var hRatio = canvas.width / img.width;
                    var vRatio = canvas.height / img.height;
                    var ratio = Math.min(hRatio, vRatio);
                    var centerShift_x = (canvas.width - img.width * ratio) / 2;
                    var centerShift_y = (canvas.height - img.height * ratio) / 2;
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctx.drawImage(img, 0, 0, img.width, img.height,
                                       centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);
                }


                scope.rotateThumb = function (index) {

                    var a = index;
                    if (canvas.attrGrad == null || canvas.attrGrad === undefined) {
                        canvas.attrGrad = 0;
                    }
                    if (canvas.rotating == null || canvas.rotating === undefined) {
                        canvas.rotating = 0;
                    }
                    canvas.rotating >= 360 ? canvas.rotating = 0 : canvas.rotating = canvas.rotating;
                    canvas.rotating == 0 ? canvas.rotating = 90 : canvas.rotating += 90;
                    canvas.attrGrad = canvas.rotating;
                    var r = 'rotate(' + canvas.rotating + 'deg)';

                    canvas.css({
                        '-moz-transform': r,
                        '-webkit-transform': r,
                        '-o-transform': r,
                        '-ms-transform': r
                    });
                    RotateImage(canvas);
                };

                function RotateImage(canvas) {
                    console.log('start rotaterimage');
                    var _ele = canvas;
                    var img2 = new Image();
                    //img2.onload = function() {
                    var myImage2, rotating2 = false;
                    var canvas2 = document.createElement('canvas');
                    var ctx2 = canvas2.getContext('2d');
                    var cw2, ch2;
                    canvas2.width = img2.width;
                    canvas2.height = img2.height;
                    cw2 = canvas2.width;
                    ch2 = canvas2.height;
                    ctx2.drawImage(img2, 0, 0, img2.width, img2.height);
                    console.log(canvas2);
                    if (!rotating2) {
                        rotating2 = true;
                        // store current data to an image
                        myImage2 = new Image();
                        myImage2.src = canvas2.toDataURL('image/jpeg', 1);
                        myImage2.onload = function () {
                            // reset the canvas with new dimensions
                            canvas2.width = ch2;
                            canvas2.height = cw2;
                            cw2 = canvas2.width;
                            ch2 = canvas2.height;
                            ctx2.save();
                            // translate and rotate
                            ctx2.translate(parseInt(cw2), parseInt(ch2 / cw2));
                            ctx2.rotate(Math.PI / 2);
                            // draw the previows image, now rotated
                            ctx2.drawImage(myImage2, 0, 0);
                            ctx2.restore();
                            // clear the temporary image
                            myImage2 = null;
                            rotating2 = false;
                            var newImgSrc = canvas2.toDataURL('image/jpeg', 1);
                            canvas2 = null;
                            fullSize.src = newImgSrc;
                            console.log(newImgSrc);
                        };
                    } img2.src = fullSize.src;
                };


                // }
            }
        };
    }])


.directive('rotate', function () {
    return {
        scope: '@',
        restrict: 'A',
        link: function (scope, element, attrs) {

        }
    }

});
