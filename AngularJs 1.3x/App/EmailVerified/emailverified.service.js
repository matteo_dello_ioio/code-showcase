﻿app.factory('emailVerifiedDataService', ['$http', '$q', function ($http, $q) {
    return {
        updateuserdetail: function (sendData) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: '/api/EmailVerified/UpdateUserDetail', data: sendData }).success(function (ajaxRespone, status, headers, config) {
                deferred.resolve(ajaxRespone);
            }).error(function (ajaxRespone, status, headers, config) {
                deferred.reject(ajaxRespone);
            });
            return deferred.promise;
        }
    }
}]);