﻿app.controller('emailVerifiedController', ['$rootScope', '$scope', '$window', '$state', 'emailVerifiedDataService',
function ($rootScope, $scope, $window, $state, emailVerifiedDataService) {

    $rootScope.$on('$stateChangeSuccess',
    function (event, toState, toParams, fromState, fromParams) {
        if (toState.name == 'Root.EmailVerified') {
            if (toParams.token) {
                $scope.token = toParams.token;

                var postData = {
                    password: '',
                    token: $scope.token
                };

                emailVerifiedDataService.updateuserdetail(postData)
                    .then(function (resp) {
                        if (resp.succeed) {
                            $scope.Email = resp.data.email;
                            $scope.CodAge = resp.data.codage;
                        } else {

                        }
                    })
                    .catch(function () {
                        $scope.Message = "Some error has occurred. Try again";
                    });
            } else {
                $scope.token = '/';
            }
        }
    });

    $scope.publish = function () {
        $window.location.href = '/Home/Login?token=' + $scope.token;
    }

}]);