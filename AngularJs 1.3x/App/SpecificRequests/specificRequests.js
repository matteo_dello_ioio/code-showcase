﻿app.controller('specificRequestsController', ['specificRequestsDataService', 'specificRequestsSource', '$scope', '$state', '$stateParams', 'MEDIA_SERVER', 'appHelper',
    function (specificRequestsDataService, specificRequestsSource, $scope, $state, $stateParams, MEDIA_SERVER, appHelper) {

        $scope.MEDIA_SERVER = MEDIA_SERVER;

        $scope.list = specificRequestsSource
        $scope.reqList = specificRequestsSource;
        $scope.filter = "all";
        $scope.predicate = '-reqDate';
        $scope.selPred = '-reqDate';
        $scope.itemPerPage = 10;
        $scope.pageNumber = 1;
        $scope.pageLeftArrow = true;
        $scope.pageRightArrow = true;
        $scope.appHelper = appHelper;

        $scope.goToPage = function (p) {
            $scope.pageNumber = p;
        }

        $scope.pageList = function () {
            
            var ar = new Array();
            var pageCount = Math.ceil($scope.reqList.length / $scope.itemPerPage);

            if ($scope.pageNumber <= 1) {
                $scope.pageLeftArrow = false;
                $scope.pageNumber = 1;
            }
            else {
                $scope.pageLeftArrow = true;
            }

            if ($scope.pageNumber >= pageCount) {
                $scope.pageNumber = pageCount;
                $scope.pageRightArrow = false;
            }
            else {
                $scope.pageRightArrow = true;
            }

            if (pageCount <= 3) {
                for (var i = 1; i <= pageCount; i++) {
                    ar.push(i);                    
                }
                return ar;
            }

            var diff = pageCount - $scope.pageNumber;
            if (diff < 1) {                
                for (var i = $scope.pageNumber - 2 + diff; i <= $scope.pageNumber + diff; i++) {
                    ar.push(i);
                }
                return ar;
            }

            if ($scope.pageNumber < 2) {
                for (var i = 1; i <= 3; i++) {
                    ar.push(i);
                }
                return ar;
            }

            for (var i = $scope.pageNumber - 1; i <= $scope.pageNumber + 1; i++) {
                ar.push(i);
            }
            return ar;
        }

        $scope.orderList = function () {
            var p = $scope.selPred;
            if (p.indexOf("-") > -1) {
                $scope.predicate = p.substr(1, p.length-1);
                $scope.reverse = true;
            }
            else {
                $scope.predicate = p;
                $scope.reverse = false;
            }
        }

        $scope.filterList = function (filter) {

            $scope.filter = filter;
            var retList = [];

            for (var i = 0; i < $scope.list.length; i++) {

                switch ($scope.filter) {
                    case "answered":
                        if ($scope.list[i].isAnswered) {
                            retList.push($scope.list[i]);
                        }
                        break;
                    case "read":
                        if ($scope.list[i].hasBeenRead && !$scope.list[i].isAnswered) {
                            retList.push($scope.list[i]);
                        }
                        break;
                    case "toRead":
                        if (!$scope.list[i].hasBeenRead) {
                            retList.push($scope.list[i]);
                        }
                        break;
                    default:
                        retList.push($scope.list[i]);
                        break;
                }
            }
            
            $scope.reqList = retList;

        }

        $scope.fun1 = function () {
            alert("s");
            $scope.reqList.splice(0, 1);            
        }

        $scope.goToResponse = function (requestId) {
            $state.go('Root.SpecificRequestResponse', { requestId: requestId });
        }

        $scope.print = function () {
            window.print();
        }

        $scope.clickedDeleteId = 0;

        $scope.delete = function () {

         
            specificRequestsDataService.deleteReq('main', $scope.clickedDeleteId).then(function (resp) {
                if (resp.succeed) {                    
                    $state.reload();
                }
                else {
                    alert("Errore nella risposta");
                }
            }).catch(function () {
                alert("Errore http");
            });
        }

        $scope.openDeleteRequest = function (id){
            $scope.clickedDeleteId = id;

        }

        $scope.$watch('requestFilter', function (newValue, oldValue) {
            if (newValue != null || oldValue != null) {
                $scope.filterList(newValue);
            }
        }, true);

    }]);