﻿app.factory('specificRequestResponseDataService', ['appDataHelper', function (appDataHelper) {
    return {
        serviceBaseUrl: '/api/SpecificRequests/',
        get: function (callId, specId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetSpecificRequestResponse', { specificRequestId: specId });
        },
        getExt: function (callId, site, extId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetSpecificRequestExternal', { site: site, extid: extId });
        },
        answer: function (callId, specId, answerText) {            
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'Answer', { specificRequestId: specId, answerText: answerText });
        }
    };
}]);