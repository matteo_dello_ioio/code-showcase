﻿app.controller('specificRequestResponseController', ['specificRequestResponseDataService','responseListSource', '$scope', '$state', '$stateParams', 'MEDIA_SERVER', 'appHelper',
function (specificRequestResponseDataService, responseListSource, $scope, $state, $stateParams, MEDIA_SERVER, appHelper) {
        
    $scope.MEDIA_SERVER = MEDIA_SERVER;

    $scope.appHelper = appHelper;

        $scope.answerText = "";

        $scope.reqList = responseListSource;

        $scope.goBack = function (requestId) {
            $state.go('Root.SpecificRequests');
        }

        $scope.answer = function (id) {

            if ($scope.answerText.length < 1) {
                return;
            }

            specificRequestResponseDataService.answer('main', id, $scope.answerText).then(function (resp) {
                if (resp.succeed) {
                    $state.reload();
                }
                else {
                    alert("Errore nella risposta");
                }
            }).catch(function () {
                alert("Errore http");
            });
        }

        $scope.print = function () {
            window.print();
        }
    }]);