﻿app.factory('specificRequestsDataService', ['appDataHelper', function (appDataHelper) {
    return {
        serviceBaseUrl: '/api/SpecificRequests/',
        get: function (callId) {
            return appDataHelper.httpGet(callId, this.serviceBaseUrl + 'GetByCustomer', {qty : 3, idx : 0});
        },
        deleteReq: function (callId, specId) {
            return appDataHelper.httpPost(callId, this.serviceBaseUrl + 'DeleteSpecificRequest', { specificRequestId: specId } );
    }
    };
}]);