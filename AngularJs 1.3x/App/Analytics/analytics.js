﻿
app.factory('analytics', ['$rootScope', '$window', '$location', function ($rootScope, $window, $location) {

    // interface
    var service = {
        push: push
    };
    return service;

    // implementation
    function push(data) {
        $window.dataLayer.push(data);
    }

}]);