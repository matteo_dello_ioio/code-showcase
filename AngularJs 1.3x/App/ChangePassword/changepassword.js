﻿app.controller('changePasswordController', ['$rootScope', '$scope', '$window', '$state', 'changePasswordDataService', function ($rootScope, $scope, $window, $state, changePasswordDataService) {
    $scope.username_validator = '(^\\d{6}$)|(^[^@\\"\']*?[^@]*?@[^@\\"\']*?\\.[^@.\\"\']{2,6}$)';
    $scope.password_validator = '((?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})$)';
    $scope.data_sent = false;

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (toState.name == 'ResetPassword') {
            if (toParams.token) {
                $scope.token = toParams.token;

                var postData = {
                    password: '',
                    token: $scope.token
                };

                changePasswordDataService.getuserdetailbytoken(postData)
                    .then(function (resp) {
                        if (resp.succeed) {
                            $scope.Email = resp.data.email;
                            $scope.CodAge = resp.data.codage;
                        } else {

                        }
                    })
                    .catch(function () {
                        $scope.Message = "Some error has occurred. Try again";
                    });
            } else {
                $scope.token = '/';
            }
        }
    });

    $scope.sendMail = function() {
        changePasswordDataService.sendmail($scope.Username)
            .then(function(resp) {
                $scope.Succeed = resp.succeed;
                if (resp.succeed) {
                    $scope.Message = "Ti abbiamo inviato un'email all'indirizzo " + resp.data.email + " con il link per ripristinare la tua password di accesso alla tua Area Riservata";
                } else {
                    if (resp.errMsg == "UserNotExist") {
                        $scope.Message = "Dati di accesso non validi";
                    } else if (resp.errMsg == "InactiveUser") {
                        $scope.Message = "Dati di accesso non validi";
                    } else if (resp.errMsg == "WrongPassword") {
                        $scope.Message = "Username e/o password non validi";
                    }
                }
            })
            .catch(function() {
                $scope.Message = "Some error has occurred. Try again";
            });
    };

    $scope.changePassword = function() {
        var postData = {
            password: $scope.Password,
            token: $scope.token
        };

        changePasswordDataService.changepassword(postData)
            .then(function(resp) {
                if (resp.succeed) {
                    $scope.data_sent = true;
                } else {
                    $scope.Message = resp.errMsg;
                }
            })
            .catch(function() {
                $scope.Message = "Some error has occurred. Try again";
            });
    };

    /* $window.location.href = '/Home/Login';*/

    $scope.getTooltipPlacement = function() {
        return ($(window).width() < 768) ? 'top' : 'right';
    };

}]);

app.directive("passwordConfirm", function () {
    return {
        require: "ngModel",
        scope: {
            passwordConfirm: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;

                if (scope.passwordConfirm || ctrl.$viewValue) {
                    combined = scope.passwordConfirm + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordConfirm;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordConfirm", false);
                            return undefined;
                        } else {
                            ctrl.$setValidity("passwordConfirm", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});