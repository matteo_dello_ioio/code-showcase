﻿app.factory('changePasswordDataService', ['$http', '$q', 'appDataHelper', function ($http, $q, appDataHelper) {
    return {
        serviceBaseUrl: '/api/ChangePassword/',
        sendmail: function (username) {
            return appDataHelper.httpPost('', this.serviceBaseUrl + 'SendMail', { login: username});
        },
        changepassword: function (sendData) {
            return appDataHelper.httpPost('', this.serviceBaseUrl + 'ChangePassword', sendData);
        },
        getuserdetailbytoken: function (sendData) {
            return appDataHelper.httpPost('', this.serviceBaseUrl + 'GetUserDetailByToken', sendData);
        }
    }
}]);