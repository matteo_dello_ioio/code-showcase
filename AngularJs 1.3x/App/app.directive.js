﻿app.directive('loadingSpinner', function() {
    return {
        template: '<div class="overlay"></div><div class="loading-img"></div>',
        link: function(scope, element, attrs) {
            if (attrs.loadingSpinner && attrs.loadingSpinner != '') {
                element.addClass('ng-hide');

                scope.$on('loader_show', function (event, arg) {
                    if (arg == attrs.loadingSpinner) {
                        element.removeClass('ng-hide');
                    }
                });

                scope.$on('loader_hide', function (event, arg) {
                    if (arg == attrs.loadingSpinner) {
                        element.addClass('ng-hide');
                    };
                });
            }
        }
    };
});

app.directive('trimRight', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var expression = /[\s]+$/g;
            element.blur(element, function (event) {
                modelCtrl.$viewValue = event.data.val().replace(expression, '');
                modelCtrl.$commitViewValue();
                modelCtrl.$render();
            });
        }
    }
});

app.directive('isNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var expression = /[^\d]/g;
            if (attrs['isNumber'] == 'true' || attrs['isNumber'] == true) {
                expression = /[^\d.]/g;
                element.blur(element, function (event) {
                    if (event.data.val()) {
                        while ((event.data.val().match(/\./g) || []).length > 1) {
                            var pos = event.data.val().indexOf('.');
                            event.data.val(event.data.val().slice(0, pos) + event.data.val().slice(pos + 1));
                        }
                    }
                    var actual = event.data.val();
                    if (actual.indexOf('.') > -1) {
                        event.data.val(Number(actual).toFixed(2));
                    }

                    modelCtrl.$viewValue = event.data.val();
                    modelCtrl.$commitViewValue();
                });
            }
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(expression, '') : null;
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

app.directive('onlyAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text == null) {
                    return null;
                }
                var transformedInput = text.replace(/\d/, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('selectRui', ['$timeout', 'applicationGlobals', function($timeout, applicationGlobals) {
    return {
        scope: false,
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (applicationGlobals.useRuiSelect) {

                var ruiReplaceInvoked = false;

                $timeout(function () {
                    $("#" + element[0].id + " option[value^='? ']").remove();
                    if (!ruiReplaceInvoked) {
                        ruiReplaceInvoked = true;
                        RUI.Select.replaceSelectBySelector('#' + element[0].id + ':not(.rui-select-rendered)');
                    }
                }, 400);

                scope.$watch(function () {
                    return element[0].innerHTML;
                }, function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        //if (!ruiReplaceInvoked) {
                        //    ruiReplaceInvoked = true;
                        //    RUI.Select.replaceSelectBySelector('#' + element[0].id + ':not(.rui-select-rendered)');
                        //}
                        $timeout(function () {
                            if (!ruiReplaceInvoked) {
                                ruiReplaceInvoked = true;
                                RUI.Select.replaceSelectBySelector('#' + element[0].id + ':not(.rui-select-rendered)');
                            }
                            RUI.Select.refresh('#' + element[0].id);
                            var optionValue = (ngModel.$modelValue == null) ? "" : ngModel.$modelValue;
                            //element.val(optionValue);
                            $("#" + element[0].id + " option").filter(function () {
                                return $(this).val() == optionValue;
                            }).prop('selected', true);
                        }, 400);

                    }
                }, true);

                scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                    var optionValue = (newVal == null) ? "" : newVal;
                    //element.val(optionValue);
                    $("#" + element[0].id + " option").filter(function () {
                        return $(this).val() == optionValue;
                    }).prop('selected', true);
                });
            }
            else { //? object:null ?
                scope.$watch(function () {
                    return $("#" + element[0].id + " option").filter(function () {
                        return $(this).val() == '' || $(this).val().indexOf('? ')==0;// == '? object:null ?';
                    }).length;
                }, function (newVal, oldVal) {
                    var currentProgrammDefault = $("#" + element[0].id + " option").filter(function () {
                        return $(this).val() == '';
                    }).first();

                    var showPlaceHolder = element.hasClass('select-show-placeholder');
                    $("#" + element[0].id + " option").filter(function () {
                        return $(this).val().indexOf('? ') == 0;// == '? object:null ?';
                    }).each(function (index) {
                        $(this).remove();
                    });

                    if (currentProgrammDefault) {
                        currentProgrammDefault.val('null');
                        if (!showPlaceHolder)
                            currentProgrammDefault.prop('disabled', 'disabled').prop('hidden', 'hidden');
                    }
                });
                
            }
        }
    }
}]);

app.directive('scrollbarReset', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    return {
        link: function (scope, element, attrs) {
            $(".nano").nanoScroller();
            $(".nano").nanoScroller({ iOSNativeScrolling: true });

            $rootScope.$on('scroll', function (event, commandObj, timeout) {
                if (!timeout)
                    timeout = 0;
                $timeout(function () { $('.nano').nanoScroller(commandObj); $(".nano").nanoScroller({ iOSNativeScrolling: true }); }, timeout);
            });

            scope.$watch(function () {
                return $('.nano-content')[0].scrollHeight;
            }, function (n, o) {
                if (n !== o)
                    $timeout(function () { $(".nano").nanoScroller(); $(".nano").nanoScroller({ iOSNativeScrolling: true }); }, 200);
            });
        }
    }
}]);

app.directive('datepickerPopup', function () {
    return {
        restrict: 'EAC',
        require: 'ngModel',
        link: function (scope, element, attr, controller) {
            //remove the default formatter from the input directive to prevent conflict
            controller.$formatters.shift();
        }
    }
});

app.directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});

app.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            
            scope.$watch(model, function (value) {
                if (value === true) {
                    if (!scope.focusMeInitialized)
                        scope.focusMeInitialized = true;
                    else
                        $timeout(function () {
                            element[0].focus();
                        });
                }
            });
            // on blur event:
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
}]);

/*
searchNoAutocomplete directive for fixing the problem with Chrome browser, causing show of autocomplete list over application custom autocomplete/autosuggest. Chrome ignore totally attribute autocomplete set to off. 
The fix consist in setting type attribute of input, to search.
Search type is sematically correct for almost inputs that needs to switch off native autocomplete and switch on application custom autocomplete/autosuggest.
Some style fixes are needed in order to override rea-rui and browser predefined styles for search type inputs.
Results attribute (nonstandard attribute) set to 0, is needed for Safari browsers. It is used to control the maximum number of entries that should be displayed in the <input>'s native dropdown list of past search queries.
*/
app.directive('searchNoAutocomplete', function () {
    return {
        link: function (scope, element, attrs) {
            element.attr('type', 'search');
            element.attr('results', '0');
            element.addClass('no-search-decoration');
            var styles = {
                'appearance':'none',
                '-moz-appearance':'none',
                '-webkit-appearance': 'none',
                'box-sizing': 'border-box'
            }
            element.css(styles);
        }
    };
});
