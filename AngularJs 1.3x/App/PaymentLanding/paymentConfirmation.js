﻿
app.controller('paymentConfirmationController', ['applicationGlobals', '$q', '$filter', '$rootScope', '$scope', 'billingInfo', '$state', '$stateParams', 'summary', '$cookies',
function (applicationGlobals, $q, $filter, $rootScope, $scope, billingInfo, $state, $stateParams, summary, $cookies) {
    $scope.BillingInfo = billingInfo;
    $scope.Cart = summary.cart;
    $scope.Checkout = { succeed: true };

}]);