﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Infrastructure.Services
{
    public enum LoginValidationType
    {
        UserNotExist = 0,
        InactiveUser = 1,
        WrongPassword = 2,
        ValidUser = 3,
        EMailNotVerified = 4
    }
}
