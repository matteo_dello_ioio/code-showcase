﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Managers;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Infrastructure.Extensions
{
    public static class ManagerResponseExtension
    {
        public static APIResponse ToAPIResponse(this ManagerResponse o)
        {
            APIResponse response = null;
            if (o.Succeed)
            {
                response = new APIResponse()
                {
                    Succeed = true,
                    Data = o.Data
                };
            }
            else
            {
                response = new APIResponse()
                {
                    Succeed = false,
                    Error = o.Errors[0],
                    ErrCode = (int)o.Errors[0].ErrorCode,
                    ErrMsg = o.Errors[0].LongMessage
                };
            }
            return response;
        }
    }
}
