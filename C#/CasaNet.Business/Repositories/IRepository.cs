﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CasaNet.Business.Repositories
{

    /// <summary>
    /// 
    /// </summary>
    public class RepositoryException : ApplicationException
    {
        public RepositoryException()
            : base()
        {

        }

        public RepositoryException(string message)
            : base(message)
        {

        }

        public RepositoryException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }

    public interface IRepository<T>
    {
        List<T> Get(Expression<Func<T, bool>> filterExpression = null, bool lazyLoading = true, bool overrideLimit = false);
        T GetByKey(object o, bool lazyLoading = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="safeconcurrency"></param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        T Insert(T o, bool safeconcurrency = false);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="safeconcurrency"></param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        T Update(T o, bool safeconcurrency = false);
        
        bool Delete(T o, bool safeconcurrency = false);
    }
}
