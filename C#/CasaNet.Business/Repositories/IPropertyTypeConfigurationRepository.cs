﻿using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Repositories
{
    public interface IPropertyTypeConfigurationRepository
    {
        List<PropertyTypeConfiguration> GetConfiguration();
    }
}
