﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;
using System.Linq.Expressions;

namespace CasaNet.Business.Repositories
{
    public interface ISpecificReqResponsesRepository
    {
        List<SpecificReqResponses> Get(Expression<Func<SpecificReqResponses, bool>> filterExpression = null, bool lazyLoading = true, bool overrideLimit = false);
        SpecificReqResponses Insert(SpecificReqResponses srr);
    }
}
