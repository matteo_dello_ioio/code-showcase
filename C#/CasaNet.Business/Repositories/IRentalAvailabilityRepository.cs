﻿using System;
using System.Linq.Expressions;
using CasaNet.Business.Models;
using CasaNet.Business.Managers;
using System.Collections.Generic;

namespace CasaNet.Business.Repositories
{
    public interface IRentalAvailabilityRepository
    {
        List<RentalAvailability> GetByListing(Listing l);
        RentalAvailability Insert(RentalAvailability entry);
        RentalAvailability Update(RentalAvailability entry);
        bool Delete(RentalAvailability entry);

        RentalAvailability Insert(List<RentalAvailability> entries);
        RentalAvailability Update(List<RentalAvailability> entries);
    }
}
