﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface IListingRepository
    {
        /// <summary>
        /// Get all listings.
        /// </summary>
        /// <returns></returns>
        List<Listing> Get();
        
        /// <summary>
        /// Get all listings owned by the specified customer.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        List<Listing> GetAllByCustomer(int customerId);

        /// <summary>
        /// Retrieve the listing with the specified ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Listing GetByKey(int id);

        /// <summary>
        /// Create a new listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        Listing Insert(Listing listing);

        /// <summary>
        /// Update an existing listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        Listing Update(Listing listing);
    }

}
