﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface ITownRepository
    {
        List<Town> Get();
    }
}
