﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface IPurchaseTransactionRepository
    {
        PurchaseTransaction GetByKey(int key);
        PurchaseTransaction Insert(PurchaseTransaction customer);
        PurchaseTransaction Update(PurchaseTransaction customer);
    }
}
