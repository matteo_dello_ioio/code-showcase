﻿using System;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface ILoginRepository
    {
        LoginModel GetByCustomerId(int customerId);
        LoginModel GetByLogin(string login);
        LoginModel GetById(int id);
        LoginModel Insert(LoginModel loginModel);
        LoginModel Update(LoginModel loginModel);
    }
}
