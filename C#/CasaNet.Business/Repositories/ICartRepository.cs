﻿using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Repositories
{
    public interface ICartRepository
    {
        Cart GetByKey(string key);

        bool Update(string key, Cart cart);

        bool Delete(string key);
    }
}
