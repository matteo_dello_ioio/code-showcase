﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Repositories
{
    public enum RepositoryCacheType
    {
        UserCache,
        UserMandatoryCache,
        ApplicationCache,
        ApplicationMandatoryCache
    }
}
