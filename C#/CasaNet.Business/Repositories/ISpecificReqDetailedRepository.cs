﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;
using System.Linq.Expressions;

namespace CasaNet.Business.Repositories
{
    public interface ISpecificReqDetailedRepository
    {
        List<SpecificReqDetailed> Get(Expression<Func<SpecificReqDetailed, bool>> filterExpression = null, bool lazyLoading = true, bool overrideLimit = false);
    }
}
