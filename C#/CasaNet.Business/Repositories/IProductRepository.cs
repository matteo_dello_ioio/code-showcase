﻿using CasaNet.Business.Models;
using System.Collections.Generic;

namespace CasaNet.Business.Repositories
{
    public interface IProductRepository
    {
        Product GetByKey(int key);
        List<Product> Get(int? siteId, string promoCode);
    }

    public interface IProductPackageConfigurationRepository
    {
        List<PackageItem> GetProductPackageConfiguration(Product product);
    }

    public interface IProductConfigurationRepository
    {
        List<ProductConfiguration> GetAll();
        ProductConfiguration GetProductConfiguration(Product product);
    }
}
