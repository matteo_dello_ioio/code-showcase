﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{

    public interface IListingDescriptionRepository
    {
        List<ListingDescription> Get();
        List<ListingDescription> GetByListingId(int appListingId);
        List<ListingDescription> GetByListing(Listing listing);
        ListingDescription GetByKey(int id);
        ListingDescription Insert(ListingDescription description);
        ListingDescription Update(ListingDescription description);
    }

}
