﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;
using System.Linq.Expressions;

namespace CasaNet.Business.Repositories
{
    public interface ICustomerRepository
    {
        List<Customer> Get(Expression<Func<Customer, bool>> filterExpression = null, bool lazyLoading = true, bool overrideLimit = false);
        Customer GetByKey(int key);
        Customer GetByEmail(string email);
        Customer Insert(Customer customer);
        Customer Update(Customer customer);
    }

    public interface ICustomerBillingInfoRepository
    {
        CustomerBillingInfo GetByCustomerId(int customerId);
        CustomerBillingInfo Insert(CustomerBillingInfo customer);
        CustomerBillingInfo Update(CustomerBillingInfo customer);
    }
}
