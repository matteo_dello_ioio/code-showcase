﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CasaNet.Business.Models;
using System.Collections.Generic;
using CasaNet.Business.Models.Lookup;

namespace CasaNet.Business.Repositories
{
    public interface ILookupRepository
    {
        List<ContractTypes> GetContractTypes();
        List<DestinationTypes> GetDestinationTypes();
        List<PropertyTypes> GetPropertyTypes();
        List<RoomNumberTypes> GetRoomNumberTypes();
        List<FloorTypes> GetFloorTypes();
        List<EnergyEfficiencyRatings> GetEnergyEfficiencyRatings();
        List<HeatingTypes> GetHeatingTypes();
        List<ConditionTypes> GetConditionTypes();
        List<DeedStatus> GetDeedStatus();
        List<BoxTypes> GetBoxTypes();
        List<GardenTypes> GetGardenTypes();
        List<Town> GetAllCities();
        List<Town> GetCitiesByName(string name);
        List<CityZone> GetAllZones();
        List<CityZone> GetCityZones(int cityId);
    }

}