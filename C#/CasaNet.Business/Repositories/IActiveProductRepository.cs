﻿using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Repositories
{
    public interface IActiveProductRepository
    {
        List<ActiveProduct> GetByListing(Listing listing, bool onlyCurrentValid);

        ActiveProduct Insert(ActiveProduct activable);

        ActiveProduct Update(ActiveProduct activable);
    }
}
