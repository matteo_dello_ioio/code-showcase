﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface IListingSiteRepository
    {
        ListingSite GetByKey(int listingId, int siteId);

        List<ListingSite> GetByListing(Listing listing);

        ListingSite GetByListingIdSiteId(int listingId, int siteId);

        /// <summary>
        /// Create a new listing site.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        ListingSite Insert(ListingSite listing);

        /// <summary>
        /// Update an existing listing site.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        ListingSite Update(ListingSite listing);
    }
}
