﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{

    public interface IListingImageRepository
    {
        List<ListingImage> Get();
        List<ListingImage> GetByListingId(int listingId);
        ListingImage GetByKey(int id);
        ListingImage Insert(ListingImage listingImage);
        ListingImage Update(ListingImage listingImage);

        bool Delete(ListingImage entry);
    }
}
