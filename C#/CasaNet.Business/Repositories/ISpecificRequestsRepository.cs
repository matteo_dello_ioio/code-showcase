﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Models;
using System.Linq.Expressions;

namespace CasaNet.Business.Repositories
{
    public interface ISpecificRequestsRepository
    {
        List<SpecificRequests> Get(Expression<Func<SpecificRequests, bool>> filterExpression = null, bool lazyLoading = true, bool overrideLimit = false);
        SpecificRequests Update(SpecificRequests sr);
    }
}
