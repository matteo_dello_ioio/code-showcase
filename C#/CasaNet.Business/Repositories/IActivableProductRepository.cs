﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models;

namespace CasaNet.Business.Repositories
{
    public interface IActivableProductRepository
    {
        //bool HasTopAvailable(int customerId);

        /// <summary>
        /// Retrieve all the activable products owned by a customer.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        List<ActivableProduct> GetByCustomerId(int customerId);

        /// <summary>
        /// Retrieve all the activable products owned by the customer for the specified site.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        List<ActivableProduct> GetBySiteId(int customerId, int siteId);

        List<ActivableProduct> GetByProduct(int customerId, Product product);

        List<ActivableProduct> GetUsedByProductType(int customerId, ProductType productType);

        ActivableProduct Insert(ActivableProduct activable);

        ActivableProduct Update(ActivableProduct activable);

    }

}
