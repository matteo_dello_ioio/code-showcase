using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "SpecificRequests")]
    public class SpecificRequests
    {
        public int Id {get; set;}  //PK della tabella

        public int SiteId {get; set;}  //id del sito di provenienza

        public string ListingId {get; set;}  //id dello annuncio sul sistema esterno (specializzato)

        public string ReqFirstName {get; set;}  //nome del consumer richeidente

        public string ReqLastName {get; set;}  //cognome del consumer richiedente

        public string ReqEmail {get; set;}  //email del consumer richiedente

        public string ReqPhoneNr {get; set;}  //numero di telefono del consumer richiedente

        public string ReqMessage {get; set;}  //messaggio del consumer richiedente

        public string ReqIp {get; set;}  //indirizzo IP del consumer richiedente

        public string AgencyListingReference {get; set;}  //riferimento interno allo annuncio

        public bool HasBeenRead {get; set;}  //flag - il messaggio è stato letto

        public bool IsAnswered {get; set;}  //flag - al messaggio è stata inviata una risposta

        public bool IsDeleted {get; set;}  //flag - il messaggio è cancellato (cancellazione logica)

        public bool IsPreferred {get; set;}  //flag - il messaggio è nei preferiti
        
        public DateTime LastAnswerDate {get; set;}  //ultima data in cui è stata inviata una risposta alla richiesta specifica

        public string DeleteCod { get; set; }

        public DateTime DeleteDt { get; set; }
    }
}