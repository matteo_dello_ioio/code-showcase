﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;

namespace CasaNet.Business.Models
{
    public class PrincipalModel : IPrincipal
    {
        private IdentityModel m_identity = null;

        #region IPrincipal Members
        public IIdentity Identity
        {
            get { return m_identity; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
        #endregion

        public PrincipalModel(IdentityModel identity)
        {
            m_identity = identity;
        }
    }
}
