﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{
    public class PropertyTypeConfiguration
    {
        public int Id { get; set; }
        public bool MandatoryField { get; set; }
        public bool ShowField { get; set; }
        public string RoomsNrValue { get; set; }
    }
}
