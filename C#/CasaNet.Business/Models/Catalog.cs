﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CasaNet.Business.Models
{
    [Serializable]
    public class Catalog
    {
        List<CatalogItem> _items;

        public List<CatalogItem> SubscriptionProducts
        {
            get
            {
                return (from item in _items where item.CategoryID == (int)ProductCategoryEnum.AnnunciPrivati select item).ToList();
            }
        }
        public List<CatalogItem> DepthProducts
        {
            get
            {
                List<CatalogItem> depths = new List<CatalogItem>();
                var itemsDep = (from item in _items where item.CategoryID == (int)ProductCategoryEnum.Depth select item);
                var itemsSerial = Newtonsoft.Json.JsonConvert.SerializeObject(itemsDep);
                List<CatalogItem> depthsItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CatalogItem>>(itemsSerial);

                var queryDepthProductTypes =
                from product in depthsItems
                group product by product.Type into newGroup
                orderby newGroup.Key
                select newGroup;

                foreach (var prodType in queryDepthProductTypes)
                {
                    CatalogItem catalogItem = prodType.First();
                    
                    foreach (var opt in prodType)
                    {
                        catalogItem.Options.Add(new CatalogItemOption()
                        {
                            Id = opt.ProductSiteId,
                            ProductSiteId = opt.ProductSiteId,
                            Name = opt.Name,
                            Duration = opt.Duration,
                            IsAvailable = opt.IsAvailable,
                            PriceExcVAT = opt.PriceExcVAT,
                            PriceInclVAT = opt.PriceInclVAT
                        });
                    }
                    depths.Add(catalogItem);
                }

                return depths;
            }
        }

        public int AvailableProductsCount
        {
            get
            {
                return (from item in _items where item.IsAvailable == true select item).Count();
            }
        }
        public Catalog()
        {
            _items = new List<CatalogItem>();
        }

        public void AddItem(CatalogItem item)
        {
            _items.Add(item);
        }
    }

    [Serializable]
    public class CatalogItem
    {
        public int Id { get; set; }
        public int ProductSiteId { get; set; }
        public int SiteId { get; set; }
        public int CategoryID { get; set; }
        public string Category { get; set; } // tipologia di prodotto: abbonamento, depth, ...
        public string Type { get; set; } // famiglia di prodotti: top, supertop, premiere - utile per raggruppare i vari annunci TOP

        public string Duration { get; set; }

        public string PromoCode { get; set; }
        public decimal PriceInclVAT { get; set; }
        public decimal PriceExcVAT { get; set; }
        public List<CatalogItem> Gifts { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool MostSold { get; set; }
        public int DisplayOrder { get; set; }
        
        public string Image { get; set; }


        public bool Proposed { get; set; }

        /// <summary>
        /// Indicate if the item is available to purchase.
        /// </summary>
        public bool IsAvailable { get; set; }
        public bool IsDeleted { get; set; }

        public List<CatalogItemOption> Options { get; set; }

        public CatalogItem()
        {
            Options = new List<CatalogItemOption>();
        }
    }

    [Serializable]
    public class CatalogItemOption
    {
        public int Id { get; set; }
        public int ProductSiteId { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public decimal PriceInclVAT { get; set; }
        public decimal PriceExcVAT { get; set; }
        public bool IsAvailable { get; set; } // indica se il prodotto è disponibile per l'acquisto
    }
}