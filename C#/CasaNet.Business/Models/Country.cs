﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Core.Services.CasaAPI;
using Newtonsoft.Json;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Country")]
    public class Country
    {
        [JsonProperty("label")]
        public string Id { get; set; }

        [JsonProperty("value")]
        public string Name { get; set; }
    }
}
