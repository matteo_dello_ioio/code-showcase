﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{
    public enum ProductType
    {
        PubblicazioneAnnuncio = 1,
        BannerExtra,
        Dominio,
        PacchettoExtra,
        AnnunciTop,
        AnnunciFlash,
        Logo,
        Package,
        SitoSuMisura,
        AgenziaTop,
        AnnuncioLusso,
        FormulaAnnunciTop,
        Gratis,
        APagamento,
        ModificaAnnuncio,
        AnnunciPremiere,
        AnnunciSuperTOP,
        FormulaSuperTop = 19,
        AnnunciPremiereSpecial = 20,
        FormulaAnnunciPremiere = 21
    }
}
