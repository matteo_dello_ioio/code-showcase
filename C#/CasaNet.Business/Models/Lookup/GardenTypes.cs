using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "GardenTypes")]
    public class GardenTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string GardenTypeDesc {get; set;}  //Tipo di giardino
    }
}