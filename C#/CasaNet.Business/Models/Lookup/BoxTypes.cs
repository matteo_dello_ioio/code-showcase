using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "BoxTypes")]
    public class BoxTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string BoxTypeDesc {get; set;}  //Tipologia di box
    }
}