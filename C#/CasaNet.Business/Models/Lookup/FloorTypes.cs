using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "FloorTypes")]
    public class FloorTypes
    {
        public int Id {get; set;}

        public string FloorDesc { get; set; }  //Piano a cui � ubicata la propriet�

        public int DisplayOrder { get; set; }  //Posizione in cui viene visualizzata la voce su area admin
    }
}