using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "DeedStatus")]
    public class DeedStatus
    {
        public int Id {get; set;}  //PK della tabella

        public string Description {get; set;}  //Stato al rogito
    }
}