﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "CityZone")]
    public class CityZone
    {
        public int Id { get; set; }  //PK della tabella

        public int GeoTownId { get; set; }

        public string Zone { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}