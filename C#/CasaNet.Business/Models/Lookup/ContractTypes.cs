using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "ContractTypes")]
    public class ContractTypes
    {
        public int Id {get; set;}  //PK tabella - id tipo contratto

        public string Description {get; set;}  //descrizione tipologia di contratto

        public string ContractType {get; set;}  //tipo di contratto
    }
}