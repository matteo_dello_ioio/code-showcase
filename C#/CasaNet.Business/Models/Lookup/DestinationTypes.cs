using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "DestinationTypes")]
    public class DestinationTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string Description {get; set;}  //destinazione di uso dello immobile

        public string ListingTypeDesc {get; set;}  //tipologia di destinazione di uso dello immobile

        public int LuxSortOrder {get; set;}  //ordine di apparizione per il canale lusso
    }
}