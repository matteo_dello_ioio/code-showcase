using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "ConditionTypes")]
    public class ConditionTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string ConditionTypeDesc {get; set;}  //Condizione dello immobile
    }
}