using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "RoomNumbers")]
    public class RoomNumberTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string RoomDesc { get; set; }
    }
}