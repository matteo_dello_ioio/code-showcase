using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "HeatingTypes")]
    public class HeatingTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string HeatingTypeDesc {get; set;}  //Descrizione della tipoligia di riscaldamento
    }
}