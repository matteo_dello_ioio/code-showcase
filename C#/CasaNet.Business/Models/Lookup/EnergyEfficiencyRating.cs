﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "EnergyEfficiencyRatings")]
    public class EnergyEfficiencyRatings
    {
        public int Id {get; set;}  //Id della classe energetica

        public string EnergyEfficiencyRatingCode {get; set;}  //Codice della classe energetica (per import dati)

        public string EnergyEfficiencyRatingDesc {get; set;}  //Descrizione della classe energetica

        public int DisplayOrder {get; set;}  //Posizione in cui viene visualizzata la voce su area admin

        public string ClassificationEnum { get; set; }

        public string ClassificationLabel { get; set; }
    }
}