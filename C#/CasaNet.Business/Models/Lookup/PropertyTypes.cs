using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models.Lookup
{
    [CasaAPIEntityName("casanet", "PropertyTypes")]
    public class PropertyTypes
    {
        public int Id {get; set;}  //PK della tabella

        public string Description {get; set;}  //tipologia di immobile (ad es. monolocale, villetta, ...) 

        public string PluralDescription {get; set;}  //tipologia di immobile al plurale

        public string DestinationTypeIds {get; set;}  //destinationTypeId sepatari da | per abbinare alla tipologia ci contratto (foreign listingId gestita da applicativo)

        public int StandardPropertyTypeId {get; set;}  //per le tipologie di tipo lusso (isForLux = 1) indica la eventuale tipologia standard da usare quando lo immobile passa da lusso a standard

        public bool IsCommercialAllowed {get; set;}  //flag - indica se il tipo di immobile è utilizabile per gli annunci commerciali

        public bool IsForLux {get; set;}  //flag - indica se il tipo di immobile è utilizzabile per gli annunci del canale lusso

        public bool IsForStandard {get; set;}  //flag - indica se il tipo di immobile è utilizzabile per gli annunci del canale standard

        public bool IsHolidayAllow {get; set;}  //flag - indica se il tipo di immobile è utilizabile per gli annunci vacanza

        public bool IsResidentialAllow {get; set;}  //flag - indica se il tipo di immobile è utilizabile per gli annunci residenziali


    }
}