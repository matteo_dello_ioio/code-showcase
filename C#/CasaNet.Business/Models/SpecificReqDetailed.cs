using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "SpecificRequestsV")]
    public class SpecificReqDetailed
    {
        public int Id {get; set;}  //PK della tabella

        public int SpecificRequestExternalId { get; set; }  //id della richiesta sul sistema esterno

        public int SiteId {get; set;}  //id del sito di provenienza

        public string ListingExternalId {get; set;}  //id dello annuncio sul sistema esterno (specializzato)

        public int CustomerId {get; set;}  //id univoco cliente su casanet

        public DateTime ReqDate { get; set; }  

        public string ReqFirstName {get; set;}  //nome del consumer richeidente

        public string ReqLastName {get; set;}  //cognome del consumer richiedente

        public string ReqEmail {get; set;}  //email del consumer richiedente

        public string ReqPhoneNr {get; set;}  //numero di telefono del consumer richiedente

        public string ReqMessage {get; set;}  //messaggio del consumer richiedente

        public string ReqIp {get; set;}  //indirizzo IP del consumer richiedente

        public string AgencyListingReference {get; set;}  //riferimento interno allo annuncio

        public bool HasBeenRead {get; set;}  //flag - il messaggio è stato letto

        public bool IsAnswered {get; set;}  //flag - al messaggio è stata inviata una risposta

        public bool IsDeleted {get; set;}  //flag - il messaggio è cancellato (cancellazione logica)

        public bool IsPreferred {get; set;}  //flag - il messaggio è nei preferiti

        public DateTime LastAnswerDate {get; set;}  //ultima data in cui è stata inviata una risposta alla richiesta specifica

        public int ListingId { get; set; }
        public string Town { get; set; } 
        public string Address { get; set; } 
        public string AddressNr { get; set; }
        public string Url { get; set; }
        public string Hash { get; set; }
        public string UploadKey { get; set; }
        public string UploadReference { get; set; }

        public int? ImageOrder { get; set; }
        public int? ImageTypeId { get; set; }
    }
}