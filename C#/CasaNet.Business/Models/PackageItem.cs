﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models.Attributes;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "PackageItem")]
    public class PackageItem
    {
        [IsIdentity]
        public int Id { get; set; }
        public int ParentProductSiteId { get; set; }
        public int ChildProductSiteId { get; set; }
        public int Quantity { get; set; }
    }
}
