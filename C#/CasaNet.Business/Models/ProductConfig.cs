﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{
    public class ProductConfiguration
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
        public string Description { get; set; }
        public bool MostSold { get; set; }
        public int DisplayOrder { get; set; }
        public string ProductGroup { get; set; }
        public string Image { get; set; }
        public string CornerImage { get; set; }
        public ProductCategoryEnum Category { get; set; }
    }
}
