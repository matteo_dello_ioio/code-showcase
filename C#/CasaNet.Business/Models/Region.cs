using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Region")]
    public class Region
    {
        public int Id {get; set;}  //PK della tabella  - ID della regione

        public int GeoCountryId {get; set;}  //codice identificativo univoco della nazione

        public int GeoMacroAreaId {get; set;}  //ID della macro area di appartenenza

        public string RegionCode {get; set;}  //codice della regione

        public string RegionName {get; set;}  //denominazione della regione

        public decimal Latitude {get; set;}  //latitudine della regione

        public decimal Longitude {get; set;}  //longitudine della regione


    }
}