﻿using CasaNet.Business.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{

    public class IdentityModel : System.Security.Principal.IIdentity
    {
        /// <summary>
        /// Default constructor 
        /// </summary>
        public IdentityModel()
        {

        }

        /// <summary>
        /// Constructor that takes user name as argument
        /// </summary>
        /// <param name="userName"></param>
        public IdentityModel(string userName)
            : this()
        {
            Name = userName;
        }

        /// <summary>
        /// User ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Internal ID for customer
        /// </summary>
        public int InternalId { get; set; }

        /// <summary>
        /// User ID
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        ///     True if the email is confirmed, default is false
        /// </summary>
        public virtual bool EmailConfirmed { get; set; }

        /// <summary>
        ///     The salted/hashed form of the user password
        /// </summary>
        public virtual string PasswordHash { get; set; }

        /// <summary>
        ///     True if the user has accepted privacy disclosure, default is false
        /// </summary>
        public virtual bool PrivacyAuthorization { get; set; }

        /// <summary>
        ///     DateTime in UTC when lockout ends, any time in the past is considered not locked out.
        /// </summary>
        public virtual DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        ///     Is lockout enabled for this user
        /// </summary>
        public virtual bool LockoutEnabled { get; set; }

        /// <summary>
        ///     Used to count access
        /// </summary>
        public virtual int AccessCount { get; set; }

        /// <summary>
        ///     Used to record failures for the purposes of lockout
        /// </summary>
        public virtual int AccessFailedCount { get; set; }

        public string AuthenticationType
        {
            get { return "CustomAuthentication"; }
        }

        public bool IsAuthenticated
        {
            //get { return (!string.IsNullOrEmpty(this.Id) && this.EmailConfirmed); }
            get { return true; }
        }


        public LoginValidationType LoginValidationType { get; set; }

        public string Token { get; set; }

    }
}
