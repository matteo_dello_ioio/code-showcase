using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "CustomerBillingInfo")]
    public class CustomerBillingInfo
    {
        [IsIdentity]
        public int Id {get; set;}
        public int CustomerId {get; set;}
        public string FirstName {get; set;}
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Email {get; set;}
        public string FiscalCode {get; set;}
        public string VATCode { get; set; }
        public int GeoTownId {get; set;}
        public Town Town { get; set; }
        public int GeoCountryId {get; set;}
        public string Address {get; set;}
        public string AddressNr {get; set;}
        public string PostCode {get; set;}
    }
}