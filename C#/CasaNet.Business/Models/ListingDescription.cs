using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "ListingDescription")]
    public class ListingDescription
    {
        [IsIdentity]
        public int Id {get; set;}  //Pk della tabella immagini

        public int ListingId {get; set;}  //id dell' annuncio

        public int LanguageId { get; set; }  //id del tipo di immagine

        public string Description {get; set;}  //url dell'immagine
    }
}