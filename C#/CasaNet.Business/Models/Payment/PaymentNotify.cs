﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Core.Infrastructure.Extensions;

namespace CasaNet.Business.Models
{
    class PaymentNotify
    {

        public int Id { get; set; }
        public string AppId { get; set; }
        public string GatewayId { get; set; }
        public string TransactionId { get; set; }
        public string GatewayTransactionId { get; set; }
        public object Data { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TransactionFee { get; set; }
        public string StatusCode { get; set; }
        public string NotifyUrl { get; set; }
        public string PayerName { get; set; }
        public string PayerEmail { get; set; }
        public bool ToDeliver { get; set; }
        public bool IsDelivered { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }

        public string DataSerial
        {
            get
            {
                return Data.ToJson();
            }
        }

    }
}
