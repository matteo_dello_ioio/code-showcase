﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{
    class SetPaymentRequest
    {

        public string AppId { get; set; }
        public string GatewayId { get; set; }
        public int TransactionId { get; set; }
        public ItemToPay[] Items { get; set; }
        public string LandingPageOkUrl { get; set; }
        public string LandingPageKoUrl { get; set; }
        public string ServerNotificationUrl { get; set; }

    }
}
