﻿using CasaNet.Business.Models.Attributes;
using CasaNet.Core.Services.CasaAPI;
using System;

namespace CasaNet.Business.Models
{
    /// <summary>
    /// Contains listing publication informations.
    /// </summary>
    [CasaAPIEntityName("casanet", "ListingSite")]
    public class ListingSite
    {
        public enum PublicationStatusEnum
        {
            WaitingForPublish = 'W',
            Published = 'P'
        }

        /// <summary>
        /// Unique identifier.
        /// </summary>
        [IsIdentity]
        public int Id { get; set; }

        /// <summary>
        /// Internal unique identifier for the listing.
        /// </summary>
        public int ListingId { get; set; }

        /// <summary>
        /// Unique identifier of publication portal.
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// Unique identifier for the listing on publication portal.
        /// </summary>
        public int? ListingExternalId { get; set; }

        /// <summary>
        /// Unique code for the listing on publication portal.
        /// </summary>
        public string ListingExternalCode { get; set; }

        public int ListingViewsNr { get; set; }

        public int PhoneViewsNr { get; set; }

        public DateTime PublishDate { get; set; }

        public DateTime? PublishExpiryDate { get; set; }

        public char PublicationStatus { get; set; }
    }
}
