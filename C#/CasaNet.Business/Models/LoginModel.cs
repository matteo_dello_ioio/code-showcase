﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Login")]
    public class LoginModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsMainLogin { get; set; }
        public int AccessCount { get; set; }
    }
}