﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models.Attributes;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{

    public enum ProductCategoryEnum
    {
        ND,
        //Generic = 1,
        Depth = 2,
        AnnunciPrivati = 3,
        //pacchettiExtraAgenzie = 4,
        //Tecnici_Servizio = 5
        Flash = 8
    }

    [CasaAPIEntityName("casanet", "Product")]
    public class Product
    {
        [IsIdentity]
        public int Id { get; set; }

        /// <summary>
        /// Site where the product can be sold.
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// Product ID for the site.
        /// </summary>
        public int ProductSiteId { get; set; }

        /// <summary>
        /// Product category (subscription for private customers, depth products...)
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// Product type (top, supertop, premiere, ...)
        /// </summary>
        public string Type { get; set; }

        public int Duration { get; set; }
        public Duration DurationType { get; set; }

        public int DurationDays
        {
            get
            {
                int multiplier = 0;
                if (this.DurationType == Models.Duration.D)
                    multiplier = 1;
                if (this.DurationType == Models.Duration.M)
                    multiplier = 30;

                return this.Duration * multiplier;
            }
        }

        public string PromoCode { get; set; }
        public decimal PriceIncVAT { get; set; }
        public decimal PriceExcVAT { get; set; }
        public List<Product> Gifts { get; set; }
        public ProductConfiguration Config { get; set; }
        public bool Proposed { get; set; }

        /// <summary>
        /// !!! DON'T USE THIS FOR NEW IMPLEMENTATIONS!!!
        /// Indicate if the product is available.
        /// </summary>
        public bool IsAvailable { get; set; }
        public bool IsDeleted { get; set; }
    }

}
