using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Customer")]
    public class Customer
    {
        [IsIdentity]
        public int Id {get; set;}

        public int? InternalId { get; set; }

        /// <summary>
        /// Indicate the customer's level (the most expensive active product)
        /// </summary>
        public string Level { get; set; }

        public int CustomerTypeId {get; set;}

        public string Email {get; set;}

        public string EmailPec {get; set;}

        public bool IsEmailVerified {get; set;}

        public bool IsPhoneVerified { get; set; }

        public string TempNewEmailToReplace {get; set;} //email inserita in sede di modifica per sostituire quella attuale

        public bool IsNewEmailVerified {get; set;} //flag - indica se la nuova mail è stata verificata

        public bool IsNewEmailReplaced {get; set;} //flag - indica se la nuova mail è stata sostituita

        public string Password {get; set;}  //campo di appoggi per password (da verificare de già criptata)

        public int? GeoTownId {get; set;}  //codice del comune

        public int? GeoCountryId {get; set;}  //identificatore della nazione

        public string Address {get; set;}  //indirizzo

        public string AddressNr {get; set;}  //numero civico

        public string PostCode {get; set;}  //codice d'avviamento postale

        public string ZoneCode {get; set;}  //Codice Frazione

        public string PhoneMainNr {get; set;}  //numero di telefono principale

        public string PhoneSecondNr {get; set;}  //numero di telefono secondario

        public string MobileNr {get; set;}  //numero di telefono cellulare

        public string FaxNr {get; set;}  //numero di fax

        public string SmsAlertNr {get; set;}  //numero per SMS alert

        public string FiscalCode {get; set;}  //codice fiscale

        public string VatCode {get; set;}  //partita iva

        public string Picture {get; set;}  //Foto inserita dall'area amministrativa

        public string Source { get; set; }  //nome utente privato

        public string FirstName {get; set;}  //nome utente privato

        public string LastName {get; set;}  //cognome utente privato

        public bool HasCompleteReg {get; set;}  //flag - ha registrazione completa

        public bool HasSmsAlert {get; set;}  //flag - ha alert via sms

        public bool HasVisiblePhone {get; set;}  //flag - ha il numero di telefono visibile

        public bool IsInternalMailAllowed {get; set;}  //ha dato il consenso all'invio di comunicazioni commerciali interne

        public bool IsExternalMailAllowed {get; set;}  //ha dato il consenso all'invio di DEM commerciali

        public bool IsPro {get; set;}  //flag che indica se di tratti di un professionista

        public bool IsDeleted {get; set;}  //flag che indica un annuncio eliminato - cancellazione logica

        public bool IsActive {get; set;}  //flag che indica se il cliente e' attivo

        public CustomerBillingInfo BillingInfo { get; set; }

        public int RowNumber {get; set;}  //rowNumber - sostituisce timestamp, in quanto in MySql e SQLServer ha funzione differente

        public int MigrationSiteId {get; set;}  //id sito originario da migrazione

        public int MigrationCustomerId {get; set;}  //id customer originario da migrazione


    }
}