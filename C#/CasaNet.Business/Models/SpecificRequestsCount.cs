using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    //[CasaAPIEntityName("casanet", "SpecificRequests")]
    public class SpecificRequestsCount
    {

        public int ListingId {get; set;}
        public int Count {get; set;}
    }
}