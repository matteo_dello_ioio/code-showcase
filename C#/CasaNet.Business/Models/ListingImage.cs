using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "ListingImage")]
    public class ListingImage
    {
        [IsIdentity]
        public int Id {get; set;}  //Pk della tabella immagini

        public int ListingId {get; set;}  //id dell' annuncio

        public int ImageTypeId {get; set;}  //id del tipo di immagine

        public int ImageStatusEnum {get; set;}  //stato dell'immaigine

        public int ImageOrder {get; set;}  //ordine dell'immaigine

        public int ImageDimension {get; set;}  //dimensione del file dell'immagine

        public int Height {get; set;}  //altezza

        public int Width {get; set;}  //larghezza

        public string Hash {get; set;}  //codice hash

        public string BitmapHash { get; set; }  //codice hash della bitmap

        public string Description {get; set;}  //descrizione immagine

        public string InsertedBy {get; set;}  //inserito da

        public bool IsMainImage {get; set;}  //flag che stabilisce se si tratta dell'immagine principale

        public int MigrationSiteId {get; set;}  //id sito originario da migrazione

        public int MigrationListingId {get; set;}  //id annuncio originarioda migrazione 

        public int MigrationImageId {get; set;}  //id immagine originario da migrazione

        public string UploadKey { get; set; }  //identificativo dell'upload

        public string UploadReference { get; set; }  //codice di riferimento dell'upload

        public string Url { get; set; }  //url dell'immagine

        public string FilePath { get; set; }  //path dell'immagine nel servizio delle immagini
    }
}