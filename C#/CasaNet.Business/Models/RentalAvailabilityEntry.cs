﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{

    [CasaAPIEntityName("casanet", "ListingRentPeriods")]
    public class RentalAvailability
    {
        [IsIdentity]
        public int Id { get; set; }
        public int ListingId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DaysNrEnum { get; set; }
        public decimal Price { get; set; }
        public string Notes { get; set; }
        public bool IsAvailable { get; set; }
    }

}
