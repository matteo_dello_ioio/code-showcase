using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Business.Managers;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{

    [CasaAPIEntityName("casanet", "ActiveProduct")]
    public class ActiveProduct
    {
        [IsIdentity]
        public int Id {get; set;}  //PK della tabella - identificativo univoco

        public int? ActivableId { get; set; }

        /// <summary>
        /// ID of the transaction linked with the activable.
        /// </summary>
        public int? ApplicationTransactionId { get; set; }

        public int ListingId {get; set;}  //identificatore univoco annuncio

        public int ProductSiteId { get; set; }

        public DateTime ActivationDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool IsActiveAtDate(DateTime date)
        {
            return (this.ActivationDate <= date && this.ExpirationDate >= date);
        }

    }
}