using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "SpecificReqResponses")]
    public class SpecificReqResponses
    {
        public int Id {get; set;}  //PK della tabella

        public int SpecificRequestId {get; set;}  //ID della richiesta

        public string ResMessage {get; set;}  //messaggio di risposta

        public string ResAttachment {get; set;}  //allegato

        public DateTime ResDate { get; set; }


    }
}