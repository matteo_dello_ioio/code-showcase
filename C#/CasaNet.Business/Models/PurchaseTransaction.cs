﻿using CasaNet.Core.Services.CasaAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "PurchaseTransaction")]
    public class PurchaseTransaction
    {
        /// <summary>
        /// Unique ID of the transaction.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer who started the transaction.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Amount with VAT for the transaction.
        /// </summary>
        public decimal AmountVAT { get; set; }

        /// <summary>
        /// Amount net for the transaction.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gateway unique transaction identifier.
        /// </summary>
        public string GatewayTransactionId { get; set; }
        /// <summary>
        /// Fees charged to the merchant.
        /// </summary>
        public decimal? TransactionFee { get; set; }

        public bool? IsConfirmed { get; set; }

        public string TransactionObject { get; set; }

        public string PaymentGateway { get; set; }

        public string PromoCode { get; set; }
    }
}
