using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Province")]
    public class Province
    {
        public int Id {get; set;}  //PK - Identificativo univoco della provincia

        public int GeoRegionId {get; set;}  //identificativo della regione

        public string ProvinceCode {get; set;}  //codice della provicia

        public string ProvinceName {get; set;}  //denominazione della provicia

        public decimal Latitude {get; set;}  //latitudine della provicia

        public decimal Longitude {get; set;}  //longitudine della provicia

        public int Accuracy {get; set;}  //precisione

        public int SalesZoneId {get; set;}  //identificativo della zona di vendita


    }
}