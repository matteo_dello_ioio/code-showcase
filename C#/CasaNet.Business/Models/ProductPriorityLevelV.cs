using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "ProductPriorityLevelV")]
    public class ProductPriorityLevelV
    {
        public int CustomerId { get; set; }

        public int ProductPriorityLevel { get; set; }
    }
}