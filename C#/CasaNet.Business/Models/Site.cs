using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "site")]
    public class Site
    {
        public int Id {get; set;}  //PK della tabella  - ID del sito

        public string Name {get; set;}

        public string Url {get; set;}
    }
}