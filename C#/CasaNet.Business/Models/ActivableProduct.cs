﻿using System;
using CasaNet.Business.Models.Attributes;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    /// <summary>
    /// Represent a product waiting to be activated.
    /// </summary>
    [CasaAPIEntityName("casanet", "ActivableProduct", "ActivableProduct_ext")]
    public class ActivableProduct
    {
        public enum ActivationStatus
        {
            ReadyForActivation = 'R',
            WaitingForActivation = 'W',
            Activated = 'A',
            InError = 'E',
            ActivationSuspended = 'S'
        }

        [IsIdentity]
        public int Id { get; set; }

        /// <summary>
        /// Customer wich own the product.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Site where the product can be used.
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// Listing which the product can be activated (if it is constrained on a specific listing).
        /// </summary>
        public int? ListingId { get; set; }

        /// <summary>
        /// Product ID.
        /// </summary>
        public int ProductSiteId { get; set; }
        public Product Product { get; set; }
        public int ProductType { get; set; }

        /// <summary>
        /// ID of the transaction linked with the activable.
        /// </summary>
        public int? ApplicationTransactionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Admitted values in ActivationStatus enum.</remarks>
        public char Status { get; set; }

        /// <summary>
        /// Product can be activated starting from this date.
        /// </summary>
        public DateTime? ActivableFromDate { get; set; }

        /// <summary>
        /// Product will be deactivated starting from this date.
        /// </summary>
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// Specify if the product is a gift.
        /// </summary>
        public bool? IsGift { get; set; }

        /// <summary>
        /// Specify if the product has been consumed.
        /// </summary>
        public bool? IsUsed { get; set; }

        /// <summary>
        /// When the activable is consumed and activated.
        /// </summary>
        public DateTime? ActivationDate { get; set; }

        public ActivableProduct()
        {
            Status = (char)ActivationStatus.ReadyForActivation;
        }
    }

}
