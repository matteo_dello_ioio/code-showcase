using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Core.Services.CasaAPI;

namespace CasaNet.Business.Models
{
    [CasaAPIEntityName("casanet", "Town")]
    public class Town
    {
        public int Id {get; set;}  //Identificativo univoco della nazione

        public int GeoProvinceId {get; set;}  //Identificativo univoco della provincia

        public string TownName {get; set;}  //denominazione del comune

        public string TownAlias {get; set;}  //denominazione del comune

        public string PostCode {get; set;}  //CAP

        public bool HasMultiPostcode {get; set;}  //flag che indica se è un capoluogo di provincia

        public bool IsChiefTown {get; set;}  //flag che indica se è un capoluogo di provincia

        public decimal Latitude {get; set;}  //latitudine della città

        public decimal Longitude {get; set;}  //longitudine della città

        public int Height {get; set;}  //altezza sul mare della città

        public int Area {get; set;}  //superficie in kmq della città

        public int Accuracy {get; set;}  //precisione

        public Province Province { get; set; }

        public Region Region { get; set; }
    }
}