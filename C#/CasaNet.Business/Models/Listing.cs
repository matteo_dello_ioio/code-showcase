using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CasaNet.Business.Managers;
using CasaNet.Core.Services.CasaAPI;
using CasaNet.Business.Models.Attributes;

namespace CasaNet.Business.Models
{

    public enum ListingDescriptionLanguageEnum
    {
        Italian = 1,
        English = 2,
        French = 3,
        Spanish = 4,
        German = 5
    }

    [CasaAPIEntityName("casanet", "Listing", "Listing_ext")]
    public class Listing
    {
        public Listing()
        {
            this.Descriptions = new List<ListingDescription>();
            this.ActiveProducts = new List<ActiveProduct>();
            this.Sites = new List<ListingSite>();
        }
        /// <summary>
        /// Unique identifier
        /// </summary>
        [IsIdentity]
        public int Id {get; set;}

        public int? ListingId { get; set; }  //identificatore univoco annuncio tabella business

        /// <summary>
        /// Customer wich own the listing
        /// </summary>
        public int CustomerId {get; set;}

        public int ContractTypeId {get; set;}  //tipo di contratto (ad es. 1 = affitto, 2 = vendita) 

        /// <summary>
        /// PriceWithVAT.
        /// </summary>
        public decimal Price { get; set; }

        public decimal? Charges { get; set; }  //spese condominiali


        #region Property data

        public int DestinationTypeId { get; set; }  //codice destinazione di uso dello immobile (ad es. 1 = residenziale, 2 = vacanze, 4 = commerciale) 

        public int PropertyTypeId { get; set; }  //codice tipologia di immobile (es. monolocale, villetta, ...) 

        /// <summary>
        /// Surface in square meters.
        /// </summary>
        public int AreaMq {get; set;}

        /// <summary>
        /// Number of rooms.
        /// </summary>
        public int? RoomNr {get; set;}

        /// <summary>
        /// Number of bathrooms.
        /// </summary>
        public int? BathNr { get; set; }

        public int? ConditionTypeId {get; set;}  //condizioni

        public int? HeatingTypeId { get; set; }  //riscaldamento

        public int? GardenTypeId { get; set; }  //giardino

        public int? BoxTypeId { get; set; }  //box

        public int? FloorTypeId { get; set; }  //piano

        public int? FloorTotalNr {get; set;}  //numero di piani totali

        /// <summary>
        /// Year of construction.
        /// </summary>
        public int? BuildingYear {get; set;}

        public int? GardenMq {get; set;}  //area del giardino in metri quadri

        public int? BoxMq {get; set;}  //area del box in metri quadri

        public int? DeedStatusId {get; set;}  //stato al rogito

        public string EnergyEfficiencyRating {get; set;}  //certificazione energetica

        public int? EnergyEfficiencyRatingId {get; set;}  //id certificazione energetica

        /// <summary>
        /// Indicate if energetic efficiency value is in cube meters.
        /// </summary>
        public bool? IsCubeMeters {get; set;}

        /// <summary>
        /// Energy efficiency value.
        /// </summary>
        public decimal? EnergyEfficiencyValue {get; set;}

        public bool? HasTerrace { get; set; }  //flag - indica che ha terrazza

        public bool? HasBalcony { get; set; }  //flag - indica che ha balcone

        public bool? HasLift { get; set; }  //flag - indica che ha ascensore

        #endregion

        #region Geolocation data

        /// <summary>
        /// Latitude coordinate.
        /// </summary>
        public decimal? CoordLat { get; set; }

        /// <summary>
        /// Longitude coordinate.
        /// </summary>
        public decimal? CoordLon { get; set; }

        /// <summary>
        /// Map level of zoom.
        /// </summary>
        public decimal? MapZoom { get; set; }

        /// <summary>
        /// ISTAT code of the city.
        /// </summary>
        public int? GeoTownId { get; set; }

        public Town Town { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? GeoZoneId { get; set; }

        /// <summary>
        /// Postal code.
        /// </summary>
        public string PostCod { get; set; }

        /// <summary>
        /// Address of the listing property.
        /// </summary>
        public string Address { get; set; }  //indirizzo

        #endregion


        public List<ListingDescription> Descriptions { get; set; }

        #region Presentation options

        /// <summary>
        /// True if the listing is visible online
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// True if price must be visible
        /// </summary>
        public bool HasVisiblePrice { get; set; }

        /// <summary>
        /// True if address must be visible
        /// </summary>
        public bool HasVisibleAddress { get; set; }

        /// <summary>
        /// True if the pointer is visible on the map
        /// </summary>
        public bool HasVisibleMapPointer { get; set; }

        #endregion

        #region Luxury data

        public bool HasSwimmingPool {get; set;}  //flag - indica presenza di una piscina

        public bool HasGolf {get; set;}  //flag - indica che ha campo da golf (solo canale lusso)

        public bool HasGym {get; set;}  //flag - indica che ha la palestra (solo canale lusso)

        public bool HasGarden {get; set;}  //flag - indica che ha il giradino (solo canale lusso)

        public bool HasHorses {get; set;}  //flag - indica che ha il maneggio (solo canale lusso)

        public bool HasBerthForBoat {get; set;}  //flag - indica che ha il porto per la barca (solo canale lusso)

        public bool HasTennis {get; set;}  //flag - indica che ha campo-i da tennis (solo canale lusso)

        public bool HasHeliport {get; set;}  //flag - indica che ha eliporto (solo canale lusso)

        public bool HasTurkishBath {get; set;}  //flag - indica che ha il bagno turco (solo canale lusso)

        public bool HasSauna {get; set;}  //flag - indica che ha la sauna (solo canale lusso)

        public bool HasQualityFinishes {get; set;}  //flag - indica che ha finiture di qualità (solo canale lusso)

        public bool IsLuxBuilding {get; set;}  //flag che indica se appartiene alla categoria lusso - legge una tabella apposita

        #endregion

        /// <summary>
        /// Flag for logic deletion.
        /// </summary>
        public bool IsDeleted {get; set;}
        public List<ListingImage> Images { get; set; }
        //public DateTime? EndDt {get; set;}  //DBA - data di fine

        public List<ActiveProduct> ActiveProducts { get; set; }
        public List<ListingSite> Sites { get; set; }

        // ----- TODO: review this properties -----
        public bool? IsShare { get; set; }  //flag - indica che è in condivisione (principalmente per gli affiti)
        public bool IsWsUpdateAllowed { get; set; }  //flag - indica che è abilitato alla modifica da parte di fonte esterna come un web service
        public bool IsBuilding { get; set; }  //flag che indica se si tratta di un edificio (casa, villa, appartamente) - se falso e' un terreno e non prende in considerazione altri valori
        public string Reference { get; set; }  //riferimento
        public int? PriceMin { get; set; }  //prezzo minimo
        public int? PriceMax { get; set; }  //prezzo massimo
        public int? UnitsNr { get; set; }  //numero di untià abitative nello edificio
        public string EnergyEfficiencyNotes { get; set; }  //znote a corredo della efficienza energetica
        public int? MigrationSiteId { get; set; }  //DBA - id sito da migrazione
        public int? MigrationListingId { get; set; }  //DBA - id  annuncio da migrazione

        public decimal? EnergyEfficiencyValueRenew { get; set; }  //valore IPE globale rinnovabile
        public decimal? EnergyEfficiencyIndexWinter { get; set; }  //indice valore IPE involucro inverno
        public decimal? EnergyEfficiencyIndexSummer { get; set; }  //indice valore IPE involucro estate
        public bool? EnergyEfficiencyConsumptionAlmostZero { get; set; } //edificio con consumo di energia quasi 0
        // ----- TODO: review this properties -----

        /// <summary>
        /// Numero di richieste specifiche, campo non mappato
        /// </summary>
        public int SpecReqCount { get; set; }

        public DateTime InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}