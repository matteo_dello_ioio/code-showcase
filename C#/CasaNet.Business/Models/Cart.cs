﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CasaNet.Business.Models
{

    [Serializable]
    public class Cart
    {
        public List<CartItem> Items { get; set; }

        public decimal TotalWithVAT
        {
            get
            {
                decimal total = 0;
                Items.ForEach(i => { total += i.PriceWithVAT; });
                return total;
            }
        }

        public decimal TotalNet
        {
            get
            {
                decimal total = 0;
                Items.ForEach(i => { total += i.PriceNet; });
                return total;
            }
        }

        /// <summary>
        /// Promo code applied to the cart.
        /// </summary>
        public string PromoCode { get; set; }

        public Cart()
        {
            this.Items = new List<CartItem>();
        }
    }

    [Serializable]
    public class CartItem
    {
        public string Description { get; set; }
        public decimal PriceWithVAT { get; set; }
        public decimal PriceNet { get; set; }
        public int Quantity { get; set; }
        public int ProductID { get; set; }
        public string LinkedObjectType { get; set; }
        public int LinkedObjectID { get; set; }
    }
}