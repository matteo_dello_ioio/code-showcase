﻿using CasaNet.Core.Infrastructure.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CasaNet.Business.Providers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface ISiteGatewayProvider
    {
        ISiteGateway GetDefaultGateway();
        ISiteGateway GetGatewayBySite(int siteId);
        IEnumerable<ISiteGateway> GetAllGateways();
        void AddGateway(int siteId, ISiteGateway gateway);
    }
}
