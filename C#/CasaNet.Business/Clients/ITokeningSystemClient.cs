﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Clients
{
    public interface ITokeningSystemClient
    {
        bool Validate(string token);
    }
}
