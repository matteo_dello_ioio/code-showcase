﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Provide informations about the ability of a customer to purchase a product
    /// </summary>
    public class ProductPurchaseInfoService : IProductPurchaseInfoService
    {
        IListingManager _listingManager;
        ICustomerManager _customerManager;
        IProductManager _productManager;
        ICartService _cartService;
        IProductRenewInfoService _productRenewInfoService;
        IActivableProductManager _activableProductManager = null;

        public int DaysForRenewal = 15;


        public ProductPurchaseInfoService(IListingManager listingInfoService, ICustomerManager customerInfoService, IProductManager productManager, ICartService cartService, IProductRenewInfoService productRenewInfoService, IActivableProductManager activableProductManager)
        {
            _listingManager = listingInfoService;
            _customerManager = customerInfoService;
            _productManager = productManager;
            _cartService = cartService;
            _productRenewInfoService = productRenewInfoService;
            _activableProductManager = activableProductManager;
        }


        /// <summary>
        /// Check if a customer can buy subscription to Gold for a listing.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customer"></param>
        /// <param name="product"></param>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanBuyGold(int? siteId, Customer customer, Product product, Listing listing)
        {
            return (
                _listingManager.HasActiveProducts(listing) == false
                ||
                (_listingManager.HasGoldActive(listing) && _productRenewInfoService.CustomerCanRenewGold(listing))
                ||
                (_listingManager.HasSmartActive(listing) && _productRenewInfoService.CustomerCanRenewSmart(listing))
                ||
                (_listingManager.HasFreeActive(listing) && _productRenewInfoService.CustomerCanRenewFree(listing))
            );
        }

        /// <summary>
        /// Check if a customer can buy subscription to Smart for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanBuySmart(int? siteId, Customer customer, Product product, Listing listing)
        {
            return (
                _listingManager.HasActiveProducts(listing) == false
                ||
                (_listingManager.HasSmartActive(listing) && _productRenewInfoService.CustomerCanRenewSmart(listing))
                ||
                (_listingManager.HasFreeActive(listing) && _productRenewInfoService.CustomerCanRenewFree(listing))
                );
        }

        /// <summary>
        /// Check if a customer can buy subscription to Free for a listing.
        /// </summary>
        /// <param name="siteId">siteId</param>
        /// <param name="customer">customer</param>
        /// <param name="product">product</param>
        /// <param name="listing">listing</param>
        /// <param name="cart">cart</param>
        /// <returns></returns>
        public bool CustomerCanBuyFree(int? siteId, Customer customer, Product product, Listing listing, Cart cart)
        {
            ManagerResponse getAvailableActivablesResponse = _activableProductManager.GetAvailableActivableProducts(customer.Id, siteId, listing, product);
            if(getAvailableActivablesResponse.Succeed == false)
            {
                throw new ApplicationException();
            }
            List<ActivableProduct> activables = (List<ActivableProduct>)getAvailableActivablesResponse.Data;

            bool isCartToCheck = (cart.Items != null && cart.Items.Count > 0);
            bool hasFreeActivable = (
                                       (!isCartToCheck && activables.Count > 0 ) ||
                                       (isCartToCheck && activables.Count > cart.Items.Where(i => i.ProductID == product.ProductSiteId).Sum(i => i.Quantity))                                     
                                    );
            return (
                (hasFreeActivable && _listingManager.HasActiveProducts(listing) == false)
                ||
                (hasFreeActivable && _listingManager.HasFreeActive(listing) && _productRenewInfoService.CustomerCanRenewFree(listing))
            );
        }

        /// <summary>
        /// Check if a customer can buy an upgrade to Premiere for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanBuyPremiere(int? siteId, Customer customer, Product product, Listing listing)
        {
            ActiveProduct activeSubscription = (from p in listing.ActiveProducts where (p.ProductSiteId == 502 || p.ProductSiteId == 503 || p.ProductSiteId == 504) && p.ActivationDate.Date <= DateTime.Now.Date && p.ExpirationDate.Date >= DateTime.Now.Date select p).FirstOrDefault();

            if (_listingManager.HasPicture(listing) && (_listingManager.HasDepthProductActive(listing) == false))
            {
                if (activeSubscription != null)
                {
                    return (product.DurationDays <= activeSubscription.ExpirationDate.Subtract(DateTime.Now.Date).Days);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if a customer can buy an upgrade to SuperTop for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanBuySuperTop(int? siteId, Customer customer, Product product, Listing listing)
        {
            ActiveProduct activeSubscription = (from p in listing.ActiveProducts where (p.ProductSiteId == 502 || p.ProductSiteId == 503 || p.ProductSiteId == 504) && p.ActivationDate.Date <= DateTime.Now.Date && p.ExpirationDate.Date >= DateTime.Now.Date select p).FirstOrDefault();

            if (_listingManager.HasPicture(listing) && (_listingManager.HasDepthProductActive(listing) == false))
            {
                if (activeSubscription != null)
                {
                    return (product.DurationDays <= activeSubscription.ExpirationDate.Subtract(DateTime.Now.Date).Days);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if a customer can buy an upgrade to Top for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanBuyTop(int? siteId, Customer customer, Product product, Listing listing)
        {
            ActiveProduct activeSubscription = (from p in listing.ActiveProducts where (p.ProductSiteId == 502 || p.ProductSiteId == 503 || p.ProductSiteId == 504) && p.ActivationDate.Date <= DateTime.Now.Date && p.ExpirationDate.Date >= DateTime.Now.Date select p).FirstOrDefault();

            if (_listingManager.HasPicture(listing) && (_listingManager.HasDepthProductActive(listing) == false))
            {
                if (activeSubscription != null)
                {
                    return (product.DurationDays <= activeSubscription.ExpirationDate.Subtract(DateTime.Now.Date).Days);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public bool CustomerCanBuyFlash(int? siteId, Customer customer, Product product, Listing listing)
        {
            return (!_listingManager.HasFlashActive(listing));
        }

        /// <summary>
        /// Check if a customer can buy a product based on the business rules.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customer"></param>
        /// <param name="product"></param>
        /// <param name="listing">(optional) listing for wich customer wants to buy the product</param>
        /// <returns></returns>
        /// <remarks>This method act like a proxy through specific business rules.</remarks>
        public bool CustomerCanBuyProduct(int? siteId, Customer customer, Product product, Listing listing)
        {
            bool result = true;

            
            ManagerResponse getCartResponse = _cartService.Get(customer.Id.ToString());
            if(getCartResponse.Succeed == false)
            {
                // handle this
            }
            Cart cart = (Cart)getCartResponse.Data;


            List<Product> products = _productManager.GetAllProducts(null, null);
            if (products == null)
            {
                throw new ApplicationException("unable to retrieve product list");
            }

            // se il prodotto esiste già nel carrello per l'annuncio, allora il prodotto non è disponibile
            bool productAlreadyInCart = (from i in cart.Items where i.LinkedObjectID == listing.Id && i.ProductID == product.ProductSiteId select i).Count() > 0;
            if (productAlreadyInCart)
                return false;

            // se per l'annuncio specificato esiste già nel carrello un prodotto della stessa categoria, allora il prodotto non è disponibile
            bool productOfSameCategoryAlreadyInCart = false;
            cart.Items.ForEach(i => {
                bool productOfSameCategoryExistsInCart = (from p in products where p.CategoryID == (int)ProductCategoryEnum.AnnunciPrivati && i.LinkedObjectID == listing.Id /*&& p.ProductSiteId == i.ProductID*/ select p).Count() > 0;
                productOfSameCategoryAlreadyInCart = productOfSameCategoryAlreadyInCart || productOfSameCategoryExistsInCart;
            });

            if (productOfSameCategoryAlreadyInCart)
                return false;
            
            switch (product.ProductSiteId)
            {
                case 504: // GRATIS
                    {
                        result = (listing != null) ? CustomerCanBuyFree(siteId, customer, product, listing, cart) : true;
                        break;
                    }
                case 502: // SMART
                    {
                        result = (listing != null) ? CustomerCanBuySmart(siteId, customer, product, listing) : true;
                        break;
                    }
                case 503: // GOLD
                    {
                        result = (listing != null) ? CustomerCanBuyGold(siteId, customer, product, listing) : true;
                        break;
                    }
                case 522: // Premiere
                    {
                        result = (listing != null) ? CustomerCanBuyPremiere(siteId, customer, product, listing) : true;
                        break;
                    }
                case 523: // SuperTop
                    {
                        result = (listing != null) ? CustomerCanBuySuperTop(siteId, customer, product, listing) : true;
                        break;
                    }
                case 488: // Top 30 gg
                case 489: // Top 15 gg
                case 490: // Top 7 gg
                case 491: // Top 3 gg
                    {
                        result = (listing != null) ? CustomerCanBuyTop(siteId, customer, product, listing) : true;
                        break;
                    }
                case 483: // Flash
                    {
                        result = (listing != null) ? CustomerCanBuyFlash(siteId, customer, product, listing) : true;
                        break;
                    }
            }
            return result;
        }
    }
}
