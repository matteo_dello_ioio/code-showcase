﻿
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Contains the logic for products purchase.
    /// </summary>
    [Logger(LogLevel.Debug)]
    public interface IProductPurchaseInfoService
    {
        bool CustomerCanBuyProduct(int? siteId, Customer customer, Product product, Listing listing);
        bool CustomerCanBuyFree(int? siteId, Customer customer, Product product, Listing listing, Cart cart);
        bool CustomerCanBuyGold(int? siteId, Customer customer, Product product, Listing listing);
        bool CustomerCanBuyPremiere(int? siteId, Customer customer, Product product, Listing listing);
        bool CustomerCanBuySmart(int? siteId, Customer customer, Product product, Listing listing);
        bool CustomerCanBuySuperTop(int? siteId, Customer customer, Product product, Listing listing);
        bool CustomerCanBuyTop(int? siteId, Customer customer, Product product, Listing listing);
    }
}
