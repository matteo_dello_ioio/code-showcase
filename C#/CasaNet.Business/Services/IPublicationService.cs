﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CasaNet.Business.Managers;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business.Services
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IPublicationService
    {
        /// <summary>
        /// Prepare publication info for the listing.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="listingId"></param>
        /// <param name="publicationProduct"></param>
        /// <returns></returns>
        ManagerResponse PrepareForPublish(int siteId, int customerId, int listingId, int publicationProduct);
        ManagerResponse Publish(int siteId, int customerId, int listingId, int publicationProduct, int? purchaseTransactionId, DateTime expirationDate);
        ManagerResponse PublishChanges(int siteId, int customerId, int listingId);
        ManagerResponse ActivateProduct(int siteId, int customerId, int listingId, int productId);
    }
}
