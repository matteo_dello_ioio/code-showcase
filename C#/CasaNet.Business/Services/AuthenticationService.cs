﻿using CasaNet.Business.Infrastructure.Services;
using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using CasaNet.Core.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Threading;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        readonly ILoginManager _loginManager = null;
        readonly ICustomerManager _customerManager = null;

        public AuthenticationService(ILoginManager loginManager, ICustomerManager customerManager)
        {
            this._loginManager = loginManager;
            this._customerManager = customerManager;
        }

        public ManagerResponse CreateLogin(LoginModel login)
        {
            ManagerResponse response = null;

            LoginModel l = _loginManager.GetByLogin(login.Login);
            if (l != null)
            {
                //ManagerResponse getByLoginResponse = _loginManager.GetByLogin(login.Login);
                //if (getByLoginResponse.Succeed == false)
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { LongMessage = "Login already in use." } }
                };
            }


            login.Password = Common.ComputeMD5(login.Password);

            LoginModel newLogin = null;
            try
            {
                _loginManager.Save(login);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = newLogin
                };
            }
            catch (System.Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { LongMessage = ex.Message } }
                };
            }
            finally
            {

            }

            return response;
        }

        /// <summary>
        /// Identifica l'utente tramite un oggetto di tipo LoginModel e restituisce un oggetto di tipo IdentityModel rappresentante l'utente identificato.
        /// </summary>
        /// <param name="login">Oggetto di tipo LoginModel contenente le credenziali da verificare.</param>
        /// <returns>IdentityModel rappresentante l'utente identificato</returns>
        public ManagerResponse GetIdentityByLogin(LoginModel login)
        {
            if (login == null)
                throw new System.ArgumentException("login cannot be null");

            if (string.IsNullOrEmpty(login.Login) || string.IsNullOrEmpty(login.Password))
            {
                throw new System.ApplicationException("missing credentials");
            }

            login.Password = Common.ComputeMD5(login.Password);

            LoginModel l_login = this._loginManager.GetByLogin(login.Login);
            if (l_login != null)
            {
                if (login.Password != "")
                {
                    if (l_login.Password != login.Password)
                    {
                        //return new IdentityModel()
                        //{
                        //    LoginValidationType = LoginValidationType.WrongPassword
                        //};
                        return new ManagerResponse()
                        {
                            Succeed = true,
                            Data = new IdentityModel()
                            {
                                LoginValidationType = LoginValidationType.WrongPassword
                            }
                        };
                    }
                }

                Customer customer = _customerManager.GetByKey(l_login.CustomerId);                
                if (customer == null || !customer.IsActive || customer.IsDeleted)
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            LoginValidationType = LoginValidationType.InactiveUser
                        }
                    };
                }

                if (!customer.IsEmailVerified)
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            LoginValidationType = LoginValidationType.EMailNotVerified
                        }
                    };
                }

                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        Id = customer.Id,
                        CustomerId = customer.Id,
                        Name = customer.FirstName + " " + customer.LastName,
                        LoginValidationType = LoginValidationType.ValidUser
                    }
                };
            }
            else
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.UserNotExist
                    }
                };
            }
        }

        public ManagerResponse/*IdentityModel*/ GetIdentityByExternalToken(int customerId)
        {
            LoginModel l_login = this._loginManager.GetByCustomerId(customerId);
            if (l_login != null)
            {
                //verifico che l'utente sia attivo
                Customer customer = _customerManager.GetByKey(customerId);
                if (!customer.IsActive)
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            LoginValidationType = LoginValidationType.InactiveUser
                        }
                    };
                }

                if (!customer.IsEmailVerified)
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            LoginValidationType = LoginValidationType.EMailNotVerified
                        }
                    };
                }

                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        Id = customer.Id,
                        CustomerId = customer.Id,
                        Name = customer.FirstName + " " + customer.LastName,
                        LoginValidationType = LoginValidationType.ValidUser
                    }
                };
            }
            else
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.UserNotExist
                    }
                };
            }
        }

        /// <summary>
        /// Identifica l'utente tramite la sua email e restituisce un oggetto di tipo IdentityModel rappresentante l'utente identificato.
        /// </summary>
        /// <param name="login">Email usata dall'utente per effettuare la login.</param>
        /// <returns>IdentityModel rappresentante l'utente identificato</returns>
        public ManagerResponse/*IdentityModel*/ GetIdentityByMail(string login)
        {
            LoginModel l_login = this._loginManager.GetByLogin(login);
            if (l_login == null)
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.UserNotExist
                    }
                };
            }

            //verifico che l'utente sia attivo
            Customer customer = _customerManager.GetByKey(l_login.CustomerId);
            if (!customer.IsActive)
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.InactiveUser
                    }
                };
            }
            else
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.ValidUser
                    }
                };
            }


        }

        /// <summary>
        /// Identifica l'utente secondo il suo id
        /// </summary>
        /// <param name="id">L'id dell'utente da identificare</param>
        /// <returns></returns>
        public ManagerResponse/*IdentityModel*/ GetIdentityById(int id)
        {
            LoginModel l_login = this._loginManager.GetByCustomerId(id);
            if (l_login != null)
            {
                //verifico che l'utente sia attivo
                Customer customer = _customerManager.GetByKey(l_login.CustomerId);
                if (!customer.IsActive)
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            Id = customer.Id,
                            CustomerId = customer.Id,
                            Name = customer.FirstName + " " + customer.LastName,
                            LoginValidationType = LoginValidationType.InactiveUser,
                            InternalId = customer.InternalId ?? customer.InternalId.Value,
                            AccessCount = l_login.AccessCount
                        }
                    };
                }
                else
                {
                    return new ManagerResponse()
                    {
                        Succeed = true,
                        Data = new IdentityModel()
                        {
                            Id = customer.Id,
                            CustomerId = customer.Id,
                            Name = customer.FirstName + " " + customer.LastName,
                            LoginValidationType = LoginValidationType.ValidUser,
                            InternalId = customer.InternalId ?? customer.InternalId.Value
                        }
                    };
                }
            }
            else
            {
                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = new IdentityModel()
                    {
                        LoginValidationType = LoginValidationType.UserNotExist
                    }
                };
            }
        }

    }
}
