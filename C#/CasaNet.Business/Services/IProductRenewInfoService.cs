﻿
using System;
using CasaNet.Business.Models;

namespace CasaNet.Business.Services
{
    public interface IProductRenewInfoService
    {
        bool CustomerCanRenewFree(CasaNet.Business.Models.Listing listing);
        bool CustomerCanRenewGold(CasaNet.Business.Models.Listing listing);
        bool CustomerCanRenewSmart(CasaNet.Business.Models.Listing listing);
        bool ListingProductIsInTimeForRenew(ActiveProduct lp);
    }
}
