﻿
using System;
using CasaNet.Business.Models;
using CasaNet.Business.Managers;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Contains the logic for products activation.
    /// </summary>
    public interface IProductActivationInfoService
    {
        bool CustomerCanActivatePremiere(Listing listing);
        bool CustomerCanActivateProduct(ActivableProduct activableProduct, Listing listing);
        bool CustomerCanActivateSuperTop(Listing listing);
        bool CustomerCanActivateTop(Listing listing, Product product);
    }
}
