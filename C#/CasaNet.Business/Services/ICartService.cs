﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public interface ICartService
    {
        ManagerResponse Empty(string key);
        ManagerResponse GetItemsCount(string key);
        ManagerResponse AddItem(string key, CartItem item);
        ManagerResponse Get(string key);
        ManagerResponse RemoveItem(string key, CartItem item);
        ManagerResponse Update(string key, Cart cart);
    }
}
