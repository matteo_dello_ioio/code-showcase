﻿using CasaNet.Core.Infrastructure.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    [IoC]
    public interface IQueueService
    {
        string Enqueue<T>(Expression<Action<T>> methodCall);
    }
}
