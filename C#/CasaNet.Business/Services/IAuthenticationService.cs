﻿using System;
namespace CasaNet.Business.Services
{
    public interface IAuthenticationService
    {
        CasaNet.Business.Managers.ManagerResponse CreateLogin(CasaNet.Business.Models.LoginModel login);
        CasaNet.Business.Managers.ManagerResponse GetIdentityByExternalToken(int customerId);
        CasaNet.Business.Managers.ManagerResponse GetIdentityById(int id);
        CasaNet.Business.Managers.ManagerResponse GetIdentityByLogin(CasaNet.Business.Models.LoginModel login);
        CasaNet.Business.Managers.ManagerResponse GetIdentityByMail(string login);
    }
}
