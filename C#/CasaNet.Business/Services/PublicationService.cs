﻿using System.Collections.Generic;
using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using System.Linq;
using CasaNet.Business.Providers;
using System;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class PublicationService : IPublicationService
    {
        readonly ICustomerManager _customerManager = null;
        readonly IListingManager _listingManager = null;
        readonly IListingSiteManager _listingSiteManager = null;
        readonly RentalAvailabilityManager _rentalAvailManager = null;
        readonly IProductManager _productManager = null;
        readonly ISiteGatewayProvider _publicationGatewayProvider = null;
        readonly IListingImageManager _listingImageManager = null;
        readonly IActivableProductService _activableProductService = null;
        readonly IActiveProductManager _activeProductManager;

        public PublicationService(ICustomerManager customerManager,
            IListingManager listingManager,
            IListingSiteManager listingSiteManager,
            RentalAvailabilityManager rentalAvailManager,
            IProductManager productManager,
            ISiteGatewayProvider publicationGatewayProvider,
            IListingImageManager listingImageManager,
            IActivableProductService activableProductService,
            IActiveProductManager activeProductManager)
        {
            _customerManager = customerManager;
            _listingManager = listingManager;
            _listingSiteManager = listingSiteManager;
            _rentalAvailManager = rentalAvailManager;
            _productManager = productManager;
            _publicationGatewayProvider = publicationGatewayProvider;
            _listingImageManager = listingImageManager;
            _activableProductService = activableProductService;
            _activeProductManager = activeProductManager;
        }

        public ManagerResponse PrepareForPublish(int siteId, int customerId, int listingId, int publicationProductId)
        {
            ManagerResponse response = null;

            ManagerResponse getByKeyResponse = _listingSiteManager.GetByListingIdSiteId(listingId, publicationProductId);
            if (getByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingImagesUpdateError, ShortMessage = getByKeyResponse.Errors.First().ShortMessage, LongMessage = getByKeyResponse.Errors.First().LongMessage } }
                };
            }
            ListingSite listingSite = (ListingSite)getByKeyResponse.Data;
            if (listingSite == null)
            {
                listingSite = new ListingSite();
            }
            listingSite.ListingId = listingId;
            listingSite.ListingExternalId = null;
            listingSite.ListingExternalCode = null;
            listingSite.SiteId = siteId;
            listingSite.PublishDate = DateTime.Now;
            listingSite.PublishExpiryDate = null;
            listingSite.PublicationStatus = (char)ListingSite.PublicationStatusEnum.WaitingForPublish;

            ManagerResponse listingSiteSaveResponse = _listingSiteManager.Save(listingSite);
            if (listingSiteSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingImagesUpdateError, ShortMessage = listingSiteSaveResponse.Errors.First().ShortMessage, LongMessage = listingSiteSaveResponse.Errors.First().LongMessage } }
                };
            }

            response = new ManagerResponse()
            {
                Succeed = true,
                Data = listingSite
            };

            return response;
        }

        /// <summary>
        /// Send the listing <paramref name="listingId"/> to the publication gateway.
        /// </summary>
        /// <param name="siteId">Unique identifier of the publication platform.</param>
        /// <param name="listingId">Unique identifier of the listing.</param>
        /// <param name="publicationProductId">Product consumed for the publication.</param>
        /// <returns></returns>
        public ManagerResponse Publish(int siteId, int customerId, int listingId, int publicationProductId, int? purchaseTransactionId, DateTime expirationDate)
        {
            ManagerResponse response = null;

            #region customer
            Customer customer = _customerManager.GetByKey(customerId);
            if (customer == null)
                throw new ApplicationException("specified customer does not exists");
            #endregion

            #region product
            ManagerResponse getProductByKeyResponse = _productManager.GetByKey(publicationProductId);
            if (getProductByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getProductByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Product product = (Product)getProductByKeyResponse.Data;
            #endregion

            #region listing
            ManagerResponse listingGetByKeyResponse = _listingManager.GetByKey(listingId);
            if (listingGetByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = listingGetByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Listing listing = (Listing)listingGetByKeyResponse.Data;

            if (!_listingManager.HasMinimumRequiredDataForSave(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingMinimalRequiredData } }
                };
            if (!_listingManager.IsGeolocalized(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingGeolocalizationData } }
                };

            if (!_listingManager.HasMinimumPrice(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingPriceInvalid } }
                };
            listing.IsVisible = true; //Make listing visible, before publishing it.
            //listing.EndDt = System.DateTime.Now.Date.AddDays(product.DurationDays).Date;

            ManagerResponse listingManagerSaveResponse = _listingManager.Save(listing);
            if (listingManagerSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = listingManagerSaveResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            #region rental vacation
            ManagerResponse availability_response = _rentalAvailManager.GetByListing(listing);
            List<RentalAvailability> availabilities = null;
            if (availability_response.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = availability_response.Errors.First().LongMessage } }
                };
            }
            availabilities = (List<RentalAvailability>)availability_response.Data;
            #endregion

            #region publication
            ISiteGateway publicationGateway = _publicationGatewayProvider.GetGatewayBySite(siteId);
            //var expirationDate = System.DateTime.Now.Date.AddDays(product.DurationDays).Date;
            ManagerResponse publishResponse = publicationGateway.Publish(customer, listing, product, expirationDate, availabilities);
            if (publishResponse == null || publishResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = publishResponse.Errors.First().LongMessage } }
                };
            }
            int newID = (int)publishResponse.Data;

            #region images
            foreach (var image in listing.Images)
            {
                image.UploadKey = null;
                image.UploadReference = null;
                image.Url = Guid.NewGuid().ToString().Replace("-", string.Empty);
                var listingManagerResponse = _listingImageManager.Save(image);
                if (!listingManagerResponse.Succeed)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingImagesUpdateError, ShortMessage = listingManagerResponse.Errors.First().ShortMessage, LongMessage = listingManagerResponse.Errors.First().LongMessage } }
                    };
                }
            }
            #endregion

            ActiveProduct activeProduct = new ActiveProduct()
            {
                ActivationDate = DateTime.Now,
                ExpirationDate = expirationDate,
                ListingId = listing.Id,
                ProductSiteId = publicationProductId,
                ApplicationTransactionId = purchaseTransactionId
            };

            ManagerResponse activeProductSaveResponse = _activeProductManager.Save(activeProduct);
            if (activeProductSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = activeProductSaveResponse.Errors.First().LongMessage, ShortMessage = activeProductSaveResponse.Errors.First().ShortMessage } }
                };
            }

            if (publicationProductId == 504)
            {
                ManagerResponse activableProductServiceActivateRespnse = _activableProductService.Activate(siteId, customerId, listingId, publicationProductId);
                if (activableProductServiceActivateRespnse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingImagesUpdateError, ShortMessage = activableProductServiceActivateRespnse.Errors.First().ShortMessage, LongMessage = activableProductServiceActivateRespnse.Errors.First().LongMessage } }
                    };
                }
            }

            response = new ManagerResponse()
            {
                Succeed = true,
                Data = newID
            };

            #endregion

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="listingId"></param>
        /// <returns></returns>
        public ManagerResponse PublishChanges(int siteId, int customerId, int listingId)
        {
            ManagerResponse response = null;

            #region customer
            Customer customer = _customerManager.GetByKey(customerId);
            if (customer == null)
                throw new ApplicationException("specified customer does not exists");
            #endregion

            #region listing
            ManagerResponse customerGetByKeyResponse = _listingManager.GetByKey(listingId);
            if (customerGetByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (customerGetByKeyResponse.Errors != null && customerGetByKeyResponse.Errors.Any()) ? customerGetByKeyResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            Listing listing = (Listing)customerGetByKeyResponse.Data;

            if (!_listingManager.HasMinimumRequiredDataForSave(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingMinimalRequiredData } }
                };
            if (!_listingManager.IsGeolocalized(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingGeolocalizationData } }
                };

            if (!_listingManager.HasMinimumPrice(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingPriceInvalid } }
                };

            ManagerResponse listingManagerSaveResponse = _listingManager.Save(listing);
            if (listingManagerSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (listingManagerSaveResponse.Errors != null && listingManagerSaveResponse.Errors.Any()) ? listingManagerSaveResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            #endregion

            #region rental vacation
            ManagerResponse availability_response = _rentalAvailManager.GetByListing(listing);
            List<RentalAvailability> availabilities = null;
            if (availability_response.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (availability_response.Errors != null && availability_response.Errors.Any()) ? availability_response.Errors.First().LongMessage : string.Empty } }
                };
            }
            availabilities = (List<RentalAvailability>)availability_response.Data;
            #endregion

            #region retrieve the publication product active on the listing
            int[] subscriptionProductIDs = new int[] { 502, 503, 504 };
            ActiveProduct ap = (from activeSubscription in listing.ActiveProducts
                                where subscriptionProductIDs.Contains(activeSubscription.ProductSiteId)
                                select activeSubscription).First();
            ManagerResponse productManagerGetByKeyResponse = _productManager.GetByKey(ap.ProductSiteId);
            if (productManagerGetByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (productManagerGetByKeyResponse.Errors != null && productManagerGetByKeyResponse.Errors.Any()) ? productManagerGetByKeyResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            Product product = (Product)productManagerGetByKeyResponse.Data;
            #endregion

            #region publication info
            ManagerResponse getByListingIdSiteIdResponse = _listingSiteManager.GetByListingIdSiteId(listingId, siteId);
            if (getByListingIdSiteIdResponse.Succeed == false)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (getByListingIdSiteIdResponse.Errors != null && getByListingIdSiteIdResponse.Errors.Any()) ? getByListingIdSiteIdResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            ListingSite listingSite = (ListingSite)getByListingIdSiteIdResponse.Data;
            if (listingSite == null)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "publication info unavailable for the listing" } }
                };
            }
            #endregion

            ISiteGateway publicationGateway = _publicationGatewayProvider.GetGatewayBySite(siteId);
            //var expirationDate = System.DateTime.Now.Date.AddDays(product.DurationDays).Date;
            ManagerResponse publishResponse = publicationGateway.Publish(customer, listing, product, listingSite.PublishExpiryDate.Value, availabilities);
            if (publishResponse.Succeed)
            {
                int newID = (int)publishResponse.Data;

                #region images
                foreach (var image in listing.Images)
                {
                    image.UploadKey = null;
                    image.UploadReference = null;
                    image.Url = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    var listingManagerResponse = _listingImageManager.Save(image);
                    if (!listingManagerResponse.Succeed)
                    {
                        return new ManagerResponse()
                        {
                            Succeed = false,
                            Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingImagesUpdateError, ShortMessage = (publishResponse.Errors != null && publishResponse.Errors.Any()) ? publishResponse.Errors.First().ShortMessage : string.Empty, LongMessage = (publishResponse.Errors != null && publishResponse.Errors.Any()) ? publishResponse.Errors.First().LongMessage : string.Empty } }
                        };
                    }
                }
                #endregion

                if (listingSite == null)
                {
                    listingSite = new ListingSite();
                }
                listingSite.ListingId = listingId;
                listingSite.ListingExternalId = newID;
                listingSite.ListingExternalCode = newID.ToString();
                listingSite.SiteId = siteId;
                listingSite.PublicationStatus = (char)ListingSite.PublicationStatusEnum.Published;
                listingSite.PublishDate = DateTime.Now;

                ManagerResponse saveResponse = _listingSiteManager.Save(listingSite);
                if (saveResponse.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = true,
                        Data = listingSite
                    };
                }
            }
            else
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (publishResponse != null && publishResponse.Errors != null && publishResponse.Errors.Any()) ? publishResponse.Errors.First().LongMessage : "Errore durante la richiesta di pubblicazione delle modifiche." } }
                };
            }

            return response;
        }

        /// <summary>
        /// Send a product activation <paramref name="productId"/> on listing <paramref name="productId"/>.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="listingId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ManagerResponse ActivateProduct(int siteId, int customerId, int listingId, int productId)
        {
            ManagerResponse response = null;

            #region customer
            Customer customer = _customerManager.GetByKey(customerId);
            if (customer == null)
                throw new ApplicationException("specified customer does not exists");

            if (customer.IsActive == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "customer status is inactive. unable to activate products on inactive customers." } }
                };
            }
            #endregion

            #region product
            ManagerResponse getProductByKeyResponse = _productManager.GetByKey(productId);
            if (getProductByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (getProductByKeyResponse.Errors != null && getProductByKeyResponse.Errors.Any()) ? getProductByKeyResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            Product product = (Product)getProductByKeyResponse.Data;
            #endregion

            #region listing
            ManagerResponse listingManagerGetByKeyResponse = _listingManager.GetByKey(listingId);
            if (listingManagerGetByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (listingManagerGetByKeyResponse.Errors != null && listingManagerGetByKeyResponse.Errors.Any()) ? listingManagerGetByKeyResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            Listing listing = (Listing)listingManagerGetByKeyResponse.Data;

            if (!_listingManager.HasMinimumRequiredDataForSave(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingMinimalRequiredData } }
                };

            if (!_listingManager.IsGeolocalized(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingGeolocalizationData } }
                };

            if (!_listingManager.HasMinimumPrice(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingPriceInvalid } }
                };
            #endregion

            #region publication info
            ManagerResponse listingSiteManagerGetByKeyResponse = _listingSiteManager.GetByListingIdSiteId(listingId, siteId);
            if (listingSiteManagerGetByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = (listingSiteManagerGetByKeyResponse.Errors != null && listingSiteManagerGetByKeyResponse.Errors.Any()) ? listingSiteManagerGetByKeyResponse.Errors.First().LongMessage : string.Empty } }
                };
            }
            ListingSite publicationInfo = (ListingSite)listingSiteManagerGetByKeyResponse.Data;
            if (publicationInfo.ListingExternalId.HasValue == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { LongMessage = "casa.it listing id is missing" } }
                };
            }
            #endregion

            #region send to activation
            ISiteGateway publicationGateway = _publicationGatewayProvider.GetGatewayBySite(siteId);
            ManagerResponse publishResponse = publicationGateway.ActivateProduct(customer, publicationInfo, product);
            if (publishResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = publishResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            #region activate product
            ManagerResponse activateActivableResponse = _activableProductService.Activate(siteId, customerId, listingId, productId);
            if (activateActivableResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { LongMessage = activateActivableResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            response = new ManagerResponse()
            {
                Succeed = true
            };

            return response;
        }
    }
}
