﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Services.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Service for activable products.
    /// </summary>
    public class ActivableProductService : IActivableProductService
    {
        IListingManager _listingManager = null;
        IProductManager _productManager = null;
        IActivableProductManager _activableProductManager = null;
        IActiveProductManager _activeProductManager = null;
        IProductActivationInfoService _productActivationInfoService = null;
        IAppCache _appCache = null;
        const string CACHE_KEY = "CasaNet.Business.Services";

        public ActivableProductService(IListingManager listingManager, IProductManager productManager, IActivableProductManager activableProductManager, IProductActivationInfoService productActivationInfoService, IActiveProductManager activeProductManager, IAppCache appCache)
        {
            _listingManager = listingManager;
            _productManager = productManager;
            _activableProductManager = activableProductManager;
            _activeProductManager = activeProductManager;
            _productActivationInfoService = productActivationInfoService;
            _appCache = appCache;
        }

        /// <summary>
        /// Retrieve the list of all depth products the customer already owns wich can activate.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        public ManagerResponse GetActivableDepthProducts(int? siteId, int customerId, int? listingId)
        {
            ManagerResponse response = null;

            try
            {
                Listing listing = null;
                if (listingId.HasValue)
                {
                    ManagerResponse listingManagerResponse = _listingManager.GetByKey(listingId.Value);
                    if (listingManagerResponse.Succeed == false)
                    {
                        return new ManagerResponse()
                        {
                            Succeed = false,
                            Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = listingManagerResponse.Errors.First().LongMessage } }
                        };
                    }
                    listing = (Listing)listingManagerResponse.Data;
                }

                Func<int?, int, Listing, List<ActivableProduct>> getAllActivableProducts = (sId, cId, l) =>
                {
                    ManagerResponse getAllActivableProductsResponse = _activableProductManager.GetActivableDepthProducts(cId, sId, l, ActivableProduct.ActivationStatus.ReadyForActivation);
                    if (getAllActivableProductsResponse.Succeed)
                    {
                        List<ActivableProduct> retVal = (List<ActivableProduct>)getAllActivableProductsResponse.Data;
                        retVal.ForEach(i => i.Product.IsAvailable = _productActivationInfoService.CustomerCanActivateProduct(i, l));
                        return retVal;
                    }

                    throw new Exception(getAllActivableProductsResponse.Errors.First().LongMessage);
                };

                var cacheKey = string.Format("{0}:{1}:{2}:{3}:{4}", CACHE_KEY, "ActivableProductBySite_Customer_Listing", (siteId.HasValue) ? siteId.ToString() : "ND", customerId.ToString(), (listing != null) ? listing.Id.ToString() : "ND");
                var cached = _appCache.Exists(cacheKey);
                List<ActivableProduct> activableProducts = (cached) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivableProduct>>(_appCache.Get(cacheKey).ToString()) : getAllActivableProducts(siteId, customerId, listing);
                if (!cached)
                    _appCache.Set(cacheKey, activableProducts, new TimeSpan(0, 15, 0));

                return new ManagerResponse()
                {
                    Succeed = true,
                    Data = activableProducts
                };
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }
            return response;
        }

        /// <summary>
        /// Retrieve the list of all products the customer already owns wich can activate.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        public ManagerResponse GetAllActivableProducts(int? siteId, int customerId, int? listingId)
        {
            ManagerResponse response = null;
            try
            {
                Listing listing = null;
                if (listingId.HasValue)
                {
                    ManagerResponse listingManagerResponse = _listingManager.GetByKey(listingId.Value);
                    if (listingManagerResponse.Succeed == false)
                    {
                        return new ManagerResponse()
                        {
                            Succeed = false,
                            Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = listingManagerResponse.Errors.First().LongMessage } }
                        };
                    }
                    listing = (Listing)listingManagerResponse.Data;
                }

                ManagerResponse getAllActivableProductsResponse = _activableProductManager.GetAvailableActivableProducts(customerId, siteId, listing, null);
                if (getAllActivableProductsResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getAllActivableProductsResponse.Errors.First().LongMessage } }
                    };
                }
                List<ActivableProduct> activableProducts = (List<ActivableProduct>)getAllActivableProductsResponse.Data;
                foreach (ActivableProduct activableProduct in activableProducts)
                {
                    activableProduct.Product.IsAvailable = _productActivationInfoService.CustomerCanActivateProduct(activableProduct, listing);
                }

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = activableProducts
                };


            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }
            return response;
        }

        public int GetAssignedCount(int siteId, int customerId, int productId, int listingId, int purchaseTransactionId)
        {
            List<ActivableProduct> activables = _activableProductManager.Get(siteId, customerId);

            int alreadyAssignedCount = (from a in activables
                                        where a.SiteId == siteId
                                        && a.CustomerId == customerId
                                        && a.ProductSiteId == productId
                                        && a.ListingId.Value == listingId
                                        && a.ApplicationTransactionId.Value == purchaseTransactionId
                                        && a.Status == (char)ActivableProduct.ActivationStatus.ReadyForActivation
                                        select a).Count();

            return alreadyAssignedCount;
        }

        /// <summary>
        /// Assign an activable product to a customer.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="productId"></param>
        /// <param name="listingId"></param>
        /// <param name="validityStartDate"></param>
        /// <param name="isGift"></param>
        /// <returns></returns>
        public ManagerResponse AssignActivableProductToCustomer(int siteId, int customerId, int productId, int? listingId, DateTime validityStartDate, DateTime? expirationDate, int? purchaseTransactionId, bool isGift = false)
        {
            ManagerResponse getByKeyResponse = _productManager.GetByKey(productId);
            if (getByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Product product = (Product)getByKeyResponse.Data;


            ActivableProduct activable = new ActivableProduct();
            activable.SiteId = siteId;
            activable.CustomerId = customerId;
            activable.ProductSiteId = productId;
            activable.ListingId = listingId;
            activable.IsGift = isGift;
            activable.Product = product;
            activable.ActivableFromDate = validityStartDate.Date;
            if (expirationDate.HasValue)
                activable.ExpirationDate = expirationDate.Value.Date;
            activable.ApplicationTransactionId = purchaseTransactionId;
            activable.Status = (char)ActivableProduct.ActivationStatus.ReadyForActivation;


            ManagerResponse saveActivableResponse = _activableProductManager.Save(activable);
            if (saveActivableResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = saveActivableResponse.Errors.First().LongMessage } }
                };
            }

            #region reset cache
            ResetCache(siteId, customerId, listingId);
            #endregion

            return saveActivableResponse;
        }

        /// <summary>
        /// Find the activable product (specified by <paramref name="productId"/>) owned by the customer, then create an active product and update the activable as consumed.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="listingId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ManagerResponse Activate(int siteId, int customerId, int listingId, int productId)
        {
            #region product
            ManagerResponse getProductByKeyResponse = _productManager.GetByKey(productId);
            if (getProductByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getProductByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Product product = (Product)getProductByKeyResponse.Data;
            #endregion

            #region listing
            ManagerResponse getListingByKeyResponse = _listingManager.GetByKey(listingId);
            if (getListingByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getListingByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Listing listing = (Listing)getListingByKeyResponse.Data;

            if (!_listingManager.HasMinimumRequiredDataForSave(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingMinimalRequiredData } }
                };

            if (!_listingManager.IsGeolocalized(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingGeolocalizationData } }
                };

            if (!_listingManager.HasMinimumPrice(listing))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingPriceInvalid } }
                };
            listing.IsVisible = true; //Make listing visible, before publishing it.

            //if (product.ProductSiteId == 504)
            //    listing.EndDt = System.DateTime.Now.Date.AddDays(product.DurationDays).Date;

            ManagerResponse listingManagerSaveResponse = _listingManager.Save(listing);
            if (listingManagerSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = listingManagerSaveResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            #region activable
            ManagerResponse getAllActivableProductsResponse = _activableProductManager.GetAvailableActivableProducts(customerId, siteId, listing, product, ActivableProduct.ActivationStatus.WaitingForActivation);
            if (getAllActivableProductsResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getAllActivableProductsResponse.Errors.First().LongMessage } }
                };
            }
            List<ActivableProduct> activables = (List<ActivableProduct>)getAllActivableProductsResponse.Data;
            ActivableProduct activable = (from a in activables
                                              //where a.ProductSiteId == product.ProductSiteId
                                          select a).FirstOrDefault();

            if (activable == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "specified activable no longer exists. unable to proceed with the activation" } }
                };
            }
            #endregion

            #region create the active product
            ActiveProduct activeProduct = new ActiveProduct();
            activeProduct.ActivableId = activable.Id;
            activeProduct.ApplicationTransactionId = activable.ApplicationTransactionId;
            activeProduct.ListingId = listing.Id;
            activeProduct.ProductSiteId = product.ProductSiteId;
            activeProduct.ActivationDate = System.DateTime.Now;
            activeProduct.ExpirationDate = System.DateTime.Now.Date.AddDays(product.DurationDays).Date;

            ManagerResponse activeProductSaveResponse = _activeProductManager.Save(activeProduct);
            if (activeProductSaveResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = activeProductSaveResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            #region update activable
            activable.IsUsed = true;
            activable.Status = (char)CasaNet.Business.Models.ActivableProduct.ActivationStatus.Activated;
            activable.ActivationDate = System.DateTime.Now;

            ManagerResponse saveActivableResponse = _activableProductManager.Save(activable);
            if (saveActivableResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = saveActivableResponse.Errors.First().LongMessage } }
                };
            }
            #endregion

            if (productId == 504)
            {
                DateTime activableFrom = System.DateTime.Now.AddYears(1);
                DateTime expirationDate = activableFrom.AddDays(product.DurationDays).Date;
                AssignActivableProductToCustomer(siteId, customerId, productId, null, activableFrom, expirationDate, null, true);
            }

            #region reset cache
            ResetCache(siteId, customerId, listingId);
            #endregion

            return new ManagerResponse()
            {
                Succeed = true
            };
        }

        public ManagerResponse SetActivationInProgress(int siteId, int customerId, int listingId, int productId)
        {
            #region product
            ManagerResponse getProductByKeyResponse = _productManager.GetByKey(productId);
            if (getProductByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getProductByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Product product = (Product)getProductByKeyResponse.Data;
            #endregion

            #region listing
            ManagerResponse getListingByKeyResponse = _listingManager.GetByKey(listingId);
            if (getListingByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getListingByKeyResponse.Errors.First().LongMessage } }
                };
            }
            Listing listing = (Listing)getListingByKeyResponse.Data;
            #endregion

            ManagerResponse getAllActivableProductsResponse = _activableProductManager.GetAvailableActivableProducts(customerId, siteId, listing, product, ActivableProduct.ActivationStatus.ReadyForActivation);
            if (getAllActivableProductsResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getAllActivableProductsResponse.Errors.First().LongMessage } }
                };
            }
            List<ActivableProduct> activables = (List<ActivableProduct>)getAllActivableProductsResponse.Data;
            ActivableProduct activable = (from a in activables
                                          select a).FirstOrDefault();

            if (activable == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "specified activable no longer exists. unable to proceed with the activation" } }
                };
            }

            activable.Status = (char)ActivableProduct.ActivationStatus.WaitingForActivation;
            ManagerResponse saveActivableResponse = _activableProductManager.Save(activable);
            if (saveActivableResponse.Succeed == false)
            {
            return new ManagerResponse()
            {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = saveActivableResponse.Errors.First().LongMessage } }
                };
            }

            #region reset cache
            ResetCache(siteId, customerId, listingId);
            #endregion

            return new ManagerResponse()
            {
                Succeed = true
            };
        }

        private void ResetCache(int? siteId, int customerId, int? listingId)
        {
            //Clear the 2 cache keys that contains data about activables
            var cacheKey = string.Format("{0}:{1}:{2}:{3}:{4}", CACHE_KEY, "ActivableProductBySite_Customer_Listing", "ND", customerId.ToString(), "ND");
            var cached = _appCache.Exists(cacheKey);
            if (cached)
                _appCache.Remove(cacheKey);

            if (listingId.HasValue && siteId.HasValue)
            {
                cacheKey = string.Format("{0}:{1}:{2}:{3}:{4}", CACHE_KEY, "ActivableProductBySite_Customer_Listing", siteId.ToString(), customerId.ToString(), listingId.ToString());
                cached = _appCache.Exists(cacheKey);
                if (cached)
                    _appCache.Remove(cacheKey);
            }
        }
       

        public ManagerResponse GetLastUsed(int customerId, ProductType productType)
        {
            return _activableProductManager.GetLastUsed(customerId, productType);
        }

        public ManagerResponse Save(ActivableProduct activable)
        {
            return this._activableProductManager.Save(activable);
        }

    }
}
