﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Managers;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Provide logic for products activation.
    /// </summary>
    public class ProductActivationInfoService : IProductActivationInfoService
    {
        IListingManager _listingInfoService;

        public int DaysForRenewal = 15;


        public ProductActivationInfoService(IListingManager listingInfoService)
        {
            _listingInfoService = listingInfoService;
        }

        public bool CustomerCanActivatePremiere(Listing listing)
        {
            return (_listingInfoService.HasSubscriptionActive(listing) == true && _listingInfoService.HasDepthProductActive(listing) == false && listing.Images.Count >= 1);
        }
        public bool CustomerCanActivateSuperTop(Listing listing)
        {
            return (_listingInfoService.HasSubscriptionActive(listing) == true && _listingInfoService.HasDepthProductActive(listing) == false && listing.Images.Count >= 1);
        }
        public bool CustomerCanActivateTop(Listing listing, Product product)
        {
            return (_listingInfoService.HasSubscriptionActive(listing) == true && _listingInfoService.HasDepthProductActive(listing) == false && listing.Images.Count >= 1);
        }
        public bool CustomerCanActivateFlash(Listing listing)
        {
            return (_listingInfoService.HasSubscriptionActive(listing) == true && _listingInfoService.HasDepthProductActive(listing) == false && _listingInfoService.HasFlashActive(listing) == false);
        }

        /// <summary>
        /// Check if the activable product can be activated based on the business rules.
        /// </summary>
        /// <param name="activableProduct"></param>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanActivateProduct(ActivableProduct activableProduct, Listing listing)
        {
            bool result = true;
            switch (activableProduct.Product.ProductSiteId)
            {
                case 522: // Premiere
                    {
                        result = (listing != null) ? CustomerCanActivatePremiere(listing) : true;
                        break;
                    }
                case 523: // SuperTop
                    {
                        result = (listing != null) ? CustomerCanActivateSuperTop(listing) : true;
                        break;
                    }
                case 488: // Top 30 gg
                case 489: // Top 15 gg
                case 490: // Top 7 gg
                case 491: // Top 3 gg
                    {
                        result = (listing != null) ? CustomerCanActivateTop(listing, activableProduct.Product) : true;
                        break;
                    }
                case 483: // Flash
                    {
                        result = (listing != null) ? CustomerCanActivateFlash(listing) : true;
                        break;
                    }
            }
            return result;
        }

    }
}
