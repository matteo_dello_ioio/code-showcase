﻿using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public interface IBusinessConfigurationService
    {
        void PreloadAll();
        void FlushCache();
        List<ProductConfiguration> GetProductConfiguration();
        List<PropertyTypeConfiguration> GetPropertyTypeConfiguration();
    }
}
