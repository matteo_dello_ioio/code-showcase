﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public interface ICustomerService
    {
        /// <summary>
        /// Verify if the specified email address has been already used by some customer.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool EmailAddressIsAvailable(string email);

        /// <summary>
        /// Stage a new login and email address for the specified customer.
        /// Changes will be applied after customer confirmation.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="email">New login/email</param>
        void StageNewEmail(int customerId, string email);

        bool Disable(int customerId);
    }
}
