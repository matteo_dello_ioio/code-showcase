﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    [Logger(LogLevel.Debug)]
    public interface ICatalogService
    {
        ManagerResponse GetCatalog(int? siteId, int? customerId, int? listingId, string promoCode);
    }
}
