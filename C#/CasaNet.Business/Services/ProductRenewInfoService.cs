﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Provide informations about the ability of a customer to purchase a product
    /// </summary>
    public class ProductRenewInfoService : IProductRenewInfoService
    {
        IListingManager _listingManager;
        ICustomerManager _customerManager;
        ICartService _cartInfoService;

        public int DaysForRenewal = 15;


        public ProductRenewInfoService(IListingManager listingInfoService, ICustomerManager customerInfoService)
        {
            _listingManager = listingInfoService;
            _customerManager = customerInfoService;
        }


        #region Renew
        /// <summary>
        /// Check if the product linked with the listing is in the period valid for renewal.
        /// </summary>
        /// <param name="lp"></param>
        /// <returns></returns>
        public bool ListingProductIsInTimeForRenew(ActiveProduct lp)
        {
            return (lp.ExpirationDate.Subtract(DateTime.Now.Date).Days <= DaysForRenewal);
        }

        /// <summary>
        /// Check if a customer can renew subscription to Gold for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanRenewGold(Listing listing)
        {
            bool result = false;

            List<ActiveProduct> activeGoldList = (from lsp in listing.ActiveProducts
                                                  where lsp.ProductSiteId == 503
                                                  && (lsp.ActivationDate <= DateTime.Now.Date && lsp.ExpirationDate >= DateTime.Now.Date)
                                                  orderby lsp.Id descending
                                                  select lsp).ToList();
            if (activeGoldList.Count() > 0)
            {
                ActiveProduct latest = activeGoldList.First();
                result = ListingProductIsInTimeForRenew(latest);
            }
            else
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Check if a customer can renew subscription to Smart for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanRenewSmart(Listing listing)
        {
            bool result = false;

            List<ActiveProduct> activeSmartList = (from lsp in listing.ActiveProducts
                                                   where lsp.ProductSiteId == 502
                                                   && (lsp.ActivationDate <= DateTime.Now.Date && lsp.ExpirationDate >= DateTime.Now.Date)
                                                   orderby lsp.Id descending
                                                   select lsp).ToList();
            if (activeSmartList.Count() > 0)
            {
                ActiveProduct latest = activeSmartList.First();
                result = ListingProductIsInTimeForRenew(latest);
            }
            else
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Check if a customer can renew subscription to Free for a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool CustomerCanRenewFree(Listing listing)
        {
            bool result = false;

            List<ActiveProduct> activeFreeList = (from lsp in listing.ActiveProducts
                                                  where lsp.ProductSiteId == 504
                                                  && (lsp.ActivationDate <= DateTime.Now.Date && lsp.ExpirationDate >= DateTime.Now.Date)
                                                  orderby lsp.Id descending
                                                  select lsp).ToList();
            if (activeFreeList.Count() > 0)
            {
                ActiveProduct latest = activeFreeList.First();
                result = ListingProductIsInTimeForRenew(latest);
            }
            else
            {
                result = false;
            }
            return result;
        }
        #endregion

    }
}