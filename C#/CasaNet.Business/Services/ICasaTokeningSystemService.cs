﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public interface ICasaTokeningSystemService
    {
        bool Validate(string token);
    }
}
