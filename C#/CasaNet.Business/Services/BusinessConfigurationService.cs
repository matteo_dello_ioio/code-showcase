﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Managers;
using CasaNet.Core.Services.Cache;
using System.Linq.Expressions;

namespace CasaNet.Business.Services
{
    public class BusinessConfigurationService : IBusinessConfigurationService
    {
        readonly ProductConfigurationManager _productConfigurationManager = null;
        readonly IPropertyTypeConfigurationManager _propTypeConfManager = null;
        readonly IAppCache _appCache = null;

        const string CACHE_KEY = "CasaNet.Business.Services";

        public BusinessConfigurationService(ProductConfigurationManager productConfigurationManager, IPropertyTypeConfigurationManager propTypConfManager, IAppCache appCache)
        {
            _productConfigurationManager = productConfigurationManager;
            _propTypeConfManager = propTypConfManager;
            _appCache = appCache;
        }
        
        public void PreloadAll()
        {
            FlushCache();
            GetProductConfiguration();
            GetPropertyTypeConfiguration();
        }

        public void FlushCache()
        {
            var cacheKey = string.Format("{0}:{1}", CACHE_KEY, "ProductConfiguration");
            if (_appCache.Exists(cacheKey))
                _appCache.Remove(cacheKey);
            cacheKey = string.Format("{0}:{1}", CACHE_KEY, "PropertyTypologyConfiguration");
            if (_appCache.Exists(cacheKey))
                _appCache.Remove(cacheKey);
        }

        public List<ProductConfiguration> GetProductConfiguration()
        {
            Func<List<ProductConfiguration>> getFromManager = () => {
                var managerResp = _productConfigurationManager.GetConfigurations();
                if (!managerResp.Succeed || managerResp.Data == null)
                    return null;

                return (List<ProductConfiguration>)managerResp.Data;
            };

            var cacheKey = string.Format("{0}:{1}", CACHE_KEY, "ProductConfiguration");
            var cached = _appCache.Exists(cacheKey);
            List<ProductConfiguration> configuration = (cached) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProductConfiguration>>(_appCache.Get(cacheKey).ToString()) : getFromManager();
            if (!cached && configuration != null)
                _appCache.Set(cacheKey, configuration);

            return configuration;
        }

        public List<PropertyTypeConfiguration> GetPropertyTypeConfiguration()
        {
            var cacheKey = string.Format("{0}:{1}", CACHE_KEY, "PropertyTypologyConfiguration");
            var cached = _appCache.Exists(cacheKey);
            List<PropertyTypeConfiguration> configuration = (cached) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PropertyTypeConfiguration>>(_appCache.Get(cacheKey).ToString()) : _propTypeConfManager.GetConfiguration();
            if (!cached && configuration != null)
                _appCache.Set(cacheKey, configuration);

            return configuration;
        }
    }
}
