﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;
using System;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// Service for activable products.
    /// </summary>
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IActivableProductService
    {

        int GetAssignedCount(int siteId, int customerId, int productId, int listingId, int purchaseTransactionId);

        /// <summary>
        /// Assign an activable product to a customer.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="productId"></param>
        /// <param name="listingId">(optional)</param>
        /// <param name="validityStartDate"></param>
        /// <param name="purchaseTransactionId">(optional)</param>
        /// <param name="isGift">Default false.</param>
        /// <returns></returns>
        ManagerResponse AssignActivableProductToCustomer(int siteId, int customerId, int productId, int? listingId, DateTime validityStartDate, DateTime? expirationDate, int? purchaseTransactionId, bool isGift = false);

        /// <summary>
        /// Find the activable product (specified by <paramref name="productId"/>) owned by the customer, then create an active product and update the activable as consumed.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="customerId"></param>
        /// <param name="listingId"></param>
        /// <param name="productId">Activable product ID</param>
        /// <returns></returns>
        ManagerResponse Activate(int siteId, int customerId, int listingId, int productId);

        /// <summary>
        /// Retrieve the list of all products the customer already owns wich can activate.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        ManagerResponse GetAllActivableProducts(int? siteId, int customerId, int? listingId);

        /// <summary>
        /// Retrieve the list of all depth products the customer already owns wich can activate.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        ManagerResponse GetActivableDepthProducts(int? siteId, int customerId, int? listingId);

        ManagerResponse SetActivationInProgress(int siteId, int customerId, int listingId, int productId);

        ManagerResponse GetLastUsed(int customerId, ProductType productType);
        /// <summary>
        /// Update activable product data.
        /// </summary>
        /// <param name="activable"></param>
        /// <returns></returns>
        ManagerResponse Save(ActivableProduct activable);
    }
}
