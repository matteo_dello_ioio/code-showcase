﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public class CustomerService : ICustomerService
    {
        readonly ILoginManager _loginManager;
        readonly ICustomerManager _customerManager = null;
        readonly IListingManager _listingManager = null;

        public CustomerService(ILoginManager loginManager, ICustomerManager customerManager, IListingManager listingManager)
        {
            _loginManager = loginManager;
            _customerManager = customerManager;
            _listingManager = listingManager;
        }

        /// <summary>
        /// Verify if the specified email address has been already used by some customer.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailAddressIsAvailable(string email)
        {
            LoginModel login = _loginManager.GetByLogin(email);
            Customer customer = _customerManager.GetByEmail(email);

            return ((login == null) && (customer == null));
        }

        public void StageNewEmail(int customerId, string email)
        {
            Customer customer = _customerManager.GetByKey(customerId);
            customer.TempNewEmailToReplace = email;

            ManagerResponse updateResponse = _customerManager.Save(customer);
            if (updateResponse.Succeed == false)
                throw new ApplicationException(updateResponse.Errors.First().LongMessage);
        }

        public bool Disable(int customerId)
        {
            Customer customer = _customerManager.GetByKey(customerId);
            if (customer == null)
                throw new Exception("Customer id not valid or customer doesn't exists.");

            if (!customer.IsActive)
                throw new Exception("Customer already disabled.");

            customer.IsActive = false;
            var saveCustomerResponse = _customerManager.Save(customer);
            if (!saveCustomerResponse.Succeed)
                throw new Exception("Error saving customer.");

            var getListingResponse = _listingManager.GetActivesByCustomer(customer.Id);
            if (getListingResponse.Succeed)
            {
                foreach (var item in (List<Listing>)getListingResponse.Data)
                {
                    item.IsDeleted = true;
                    var managerResp = _listingManager.Save(item);
                }
            }
            return true;
        }
    }
}
