﻿using CasaNet.Business.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public class CasaTokeningSystemService : ICasaTokeningSystemService
    {
        readonly ITokeningSystemClient _client = null;
        public CasaTokeningSystemService(ITokeningSystemClient client)
        {
            _client = client;
        }

        public bool Validate(string token)
        {
            return _client.Validate(token);
        }
    }
}
