﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Managers;
using CasaNet.Business.Models;

namespace CasaNet.Business.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CartService : ICartService
    {
        readonly ICartManager _cartManager = null;

        public CartService(ICartManager cartManager)
        {
            _cartManager = cartManager;
        }

        /// <summary>
        /// Add <paramref name="item"/> in the cart.
        /// </summary>
        /// <param name="listingId">Unique identifier for the cart.</param>
        /// <param name="item">Item to add.</param>
        /// <returns></returns>
        public ManagerResponse AddItem(string key, CartItem item)
        {
            ManagerResponse response = null;
            try
            {
                ManagerResponse addItemResponse = _cartManager.AddItem(key, item);
                if (addItemResponse.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Data = null,//cart
                        Succeed = true
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = addItemResponse.Errors.First().LongMessage } }
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Empty the cart.
        /// </summary>
        /// <param name="listingId">Unique identifier for the cart.</param>
        /// <returns></returns>
        public ManagerResponse Empty(string key)
        {
            ManagerResponse response = null;
            try
            {
                ManagerResponse emptyResponse = _cartManager.Empty(key);
                if (emptyResponse.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Data = emptyResponse.Data,
                        Succeed = true
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = emptyResponse.Errors.First().LongMessage } }
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Retrieve the cart with the specified <paramref name="listingId"/>
        /// </summary>
        /// <param name="listingId">Unique identifier for the cart.</param>
        /// <returns></returns>
        public ManagerResponse Get(string key)
        {
            ManagerResponse response = null;
            try
            {
                ManagerResponse getResponse = _cartManager.Get(key);
                if (getResponse.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Data = getResponse.Data,
                        Succeed = true
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getResponse.Errors.First().LongMessage } }
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Retrieve the number of products in cart.
        /// </summary>
        /// <param name="listingId">Unique identifier for the cart.</param>
        /// <returns></returns>
        public ManagerResponse GetItemsCount(string key)
        {
            return _cartManager.GetItemsCount(key);
        }

        /// <summary>
        /// Remove <paramref name="item"/> from the cart.
        /// </summary>
        /// <param name="listingId">Unique identifier for the cart.</param>
        /// <param name="item">Item to remove.</param>
        /// <returns></returns>
        public ManagerResponse RemoveItem(string key, CartItem item)
        {
            ManagerResponse response = null;
            try
            {
                ManagerResponse removeItemResponse = _cartManager.RemoveItem(key, item);
                if (removeItemResponse.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Data = null,//cart
                        Succeed = true
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = removeItemResponse.Errors.First().LongMessage } }
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Replace the existing cart with <paramref name="cart"/>.
        /// </summary>
        /// <param name="listingId"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        public ManagerResponse Update(string key, Cart cart)
        {
            ManagerResponse response = null;

            try
            {
                ManagerResponse updateCartResponse = _cartManager.Update(key, cart);
                if (updateCartResponse.Succeed)
                {
                    Cart updatedCart = (Cart)updateCartResponse.Data;

                    response = new ManagerResponse()
                    {
                        Data = updatedCart,
                        Succeed = true
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = updateCartResponse.Errors.First().LongMessage } }
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                };
            }

            return response;
        }
    }
}
