
﻿using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public interface ICartProcessorService
    {
        void ProcessCart(/*int customerId, Cart cart,*/ int purchaseTransactionId);
    }
}