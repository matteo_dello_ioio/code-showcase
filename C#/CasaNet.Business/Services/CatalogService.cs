﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Services
{
    public class CatalogService : ICatalogService
    {
        readonly ICustomerManager _customerManager = null;
        readonly IListingManager _listingManager = null;
        readonly IProductManager _productManager = null;
        readonly IProductPurchaseInfoService _purchaseService = null;

        public CatalogService(ICustomerManager customerService, IListingManager listingService, IProductManager productInfoService, IProductPurchaseInfoService purchaseService)
        {
            _customerManager = customerService;
            _listingManager = listingService;
            _productManager = productInfoService;
            _purchaseService = purchaseService;
        }

        public ManagerResponse GetCatalog(int? siteId, int? customerId, int? listingId, string promoCode)
        {
            ManagerResponse response = null;

            Catalog catalog = new Catalog();

            Customer customer = null;
            if (customerId.HasValue)
            {
                customer = _customerManager.GetByKey(customerId.Value);
            }

            Listing listing = null;
            if (listingId.HasValue)
            {
                ManagerResponse getByKeyResponse = _listingManager.GetByKey(listingId.Value);
                if (getByKeyResponse.Succeed)
                {
                    listing = (Listing)getByKeyResponse.Data;
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "" } }
                    };
                }
            }

            List<Product> products = _productManager.GetAllProducts(siteId, promoCode);
            if (products == null)
            {
                throw new ApplicationException("unable to retrieve product list");
            }

            foreach (Product product in products)
            {
                CatalogItem catalogItem = MapProductEntity2CatalogItem(product);
                catalogItem.IsAvailable = _purchaseService.CustomerCanBuyProduct(siteId, customer, product, listing);

                catalog.AddItem(catalogItem);
            }

            response = new ManagerResponse()
            {
                Succeed = true,
                Data = catalog
            };

            return response;
        }

        CatalogItem MapProductEntity2CatalogItem(Product product)
        {
            string durationType = null;
            switch (product.DurationType)
            {
                case Duration.D:
                    durationType = "giorni";
                    break;
                case Duration.M:
                    durationType = "mesi";
                    break;
            }

            List<CatalogItem> gifts = new List<CatalogItem>();
            if (product.Gifts != null)
            {
                foreach (Product gift in product.Gifts)
                {
                    gifts.Add(MapProductEntity2CatalogItem(gift));
                }
            }

            CatalogItem catalogItem = new CatalogItem();
            catalogItem.Id = product.Id;
            catalogItem.ProductSiteId = product.ProductSiteId;
            catalogItem.CategoryID = product.CategoryID;
            catalogItem.Category = product.CategoryID.ToString();
            catalogItem.Type = product.Type;
            catalogItem.SiteId = product.SiteId;
            catalogItem.Duration = string.Format("{0} {1}", product.Duration, durationType);
            catalogItem.PromoCode = product.PromoCode;
            catalogItem.PriceInclVAT = product.PriceIncVAT;
            catalogItem.PriceExcVAT = product.PriceExcVAT;
            catalogItem.Gifts = gifts;
            catalogItem.Proposed = product.Proposed;
            //catalogItem.IsAvailable = product.IsAvailable;
            catalogItem.IsDeleted = product.IsDeleted;

            if (product.Config != null)
            {
                catalogItem.Name = product.Config.Name;
                catalogItem.Description = product.Config.Description;
                catalogItem.MostSold = product.Config.MostSold;
                catalogItem.DisplayOrder = product.Config.DisplayOrder;
                //catalogItem.Type = product.Config.ProductGroup;
                catalogItem.Image = product.Config.Image;
            }
            return catalogItem;
        }
    }


}
