﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;

namespace CasaNet.Business.Managers
{
    public class PropertyTypeConfigurationManager : IPropertyTypeConfigurationManager
    {
        readonly IPropertyTypeConfigurationRepository _propTypConfRepo = null;

        public PropertyTypeConfigurationManager(IPropertyTypeConfigurationRepository propTypConfRepo)
        {
            _propTypConfRepo = propTypConfRepo;
        }

        public List<PropertyTypeConfiguration> GetConfiguration()
        {
            return _propTypConfRepo.GetConfiguration();
        }
    }
}
