﻿using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;

namespace CasaNet.Business.Managers
{
    public class SpecificRequestManager
    {
        ISpecificReqResponsesRepository _specificReqResponsesRepo = null;
        ISpecificRequestsRepository _specificRequestsRepo = null;
        ISpecificReqDetailedRepository _specificReqDetailedRepo = null;

        public SpecificRequestManager(ISpecificRequestsRepository specificRequestsRepo, ISpecificReqDetailedRepository specificReqDetailedRepo, ISpecificReqResponsesRepository specificReqResponsesRepo)
        {
            _specificReqResponsesRepo = specificReqResponsesRepo;
            _specificRequestsRepo = specificRequestsRepo;
            _specificReqDetailedRepo = specificReqDetailedRepo;
        }

        private bool IsOwner(int customerId, int specificRequestId)
        {
            return _specificReqDetailedRepo.Get(e => e.CustomerId == customerId && e.IsDeleted == false && e.Id == specificRequestId).Count() > 0;
        }


        public List<SpecificReqDetailed> GetByCustomer(int customerId)
        {
            List<SpecificReqDetailed> list = _specificReqDetailedRepo.Get(e => e.CustomerId == customerId && e.IsDeleted == false);
            return list;
        }

        public List<SpecificReqResponses> GetSpecificReqResponsesByCustomer(int customerId, int specificRequestId)
        {
            if (!IsOwner(customerId, specificRequestId))
            {
                return null;
            }

            List<SpecificReqResponses> list = _specificReqResponsesRepo.Get(e => e.SpecificRequestId == specificRequestId);
            SpecificRequests req = _specificRequestsRepo.Get(e => e.Id == specificRequestId).First();

            if (!req.HasBeenRead)
            {
                req.HasBeenRead = true;
                _specificRequestsRepo.Update(req);
            }

            return list;
        }

        public SpecificReqDetailed GetSpecificRequestByCustomer(int customerId, int specificRequestId)
        {
            if (!IsOwner(customerId, specificRequestId))
            {
                return null;
            }

            SpecificReqDetailed retObj = _specificReqDetailedRepo.Get(e => e.Id == specificRequestId && e.IsDeleted == false).First();
            return retObj;
        }

        public List<SpecificRequestsCount> GetAllSpecificRequestsCountByCustomer(int customerId)
        {
            List<SpecificReqDetailed> list = _specificReqDetailedRepo.Get(e => e.CustomerId == customerId && e.IsDeleted == false).ToList();
            List<SpecificRequestsCount> ret = list.GroupBy(e => e.ListingId).Select(n => new SpecificRequestsCount() { ListingId = n.Key, Count = n.Count() }).ToList();
            return ret;
        }

        public List<SpecificRequestsCount> GetSpecificRequestsCount(int customerId)
        {
            List<SpecificReqDetailed> list = _specificReqDetailedRepo.Get(e => e.CustomerId == customerId).ToList();
            list = list.Where(e => e.IsDeleted == false && e.IsAnswered == false && e.HasBeenRead == false).ToList();
            List<SpecificRequestsCount> ret = list.GroupBy(e => e.ListingId).Select(n => new SpecificRequestsCount() { ListingId = n.Key, Count = n.Count() }).ToList();
            return ret;
        }

        public int GetIdSpecificRequestByExternal(int site, int extid)
        {
            SpecificReqDetailed retObj = _specificReqDetailedRepo.Get(e => e.SiteId == site && e.SpecificRequestExternalId == extid && e.IsDeleted == false).First();

            return retObj.Id;
        }

        public bool DeleteSpecificRequest(int customerId, int specificRequestId)
        {
            if (!IsOwner(customerId, specificRequestId))
            {
                return false;
            }
            SpecificRequests sr = _specificRequestsRepo.Get(e => e.Id == specificRequestId).First();
            sr.IsDeleted = true;
            sr.DeleteCod = "CasaNet";
            sr.DeleteDt = DateTime.Now;
            SpecificRequests ret = _specificRequestsRepo.Update(sr);
            return ret != null;
        }

        public bool Answer(int customerId, int specificRequestId, string message)
        {
            if (!IsOwner(customerId, specificRequestId))
            {
                return false;
            }

            DateTime now = DateTime.Now;
            SpecificRequests req = _specificRequestsRepo.Get(e => e.Id == specificRequestId).First();
            req.LastAnswerDate = now;
            req.IsAnswered = true;
            _specificRequestsRepo.Update(req);

            SpecificReqResponses srr = new SpecificReqResponses();
            srr.ResMessage = message;
            srr.SpecificRequestId = specificRequestId;
            srr.ResDate = DateTime.Now;
            SpecificReqResponses ret = _specificReqResponsesRepo.Insert(srr);
            return ret != null;
        }
    }
}
