﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Core.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    public class ListingDescriptionManager
    {
        IListingDescriptionRepository _repository = null;

        public ListingDescriptionManager(IListingDescriptionRepository repository)
        {
            _repository = repository;
        }
        public ManagerResponse GetByLanguage(ListingDescriptionLanguageEnum language)
        {
            throw new NotImplementedException();
        }

        public ManagerResponse GetByKey(int key)
        {
            ManagerResponse response = null;

            try
            {
                ListingDescription data = _repository.GetByKey(key);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        public ManagerResponse GetByListing(Listing listing)
        {
            if (listing == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                };
            }

            List<ListingDescription> listingDescriptions = _repository.GetByListingId(listing.Id);


            return new ManagerResponse()
                {
                    Succeed = true,
                    Data = listingDescriptions
                };;
        }

        public ManagerResponse Save(ListingDescription listingDescription)
        {
            if (listingDescription == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                };
            }

            // retrieve all descriptions for the listing
            List<ListingDescription> listingDescriptions = _repository.GetByListingId(listingDescription.ListingId);

            // retrieve descriptions for the specified language
            List<ListingDescription> descsLan = (from ld in listingDescriptions where ld.LanguageId == listingDescription.LanguageId select ld).ToList();

            ListingDescription result = null;
            if (descsLan.Count() == 0)
            {
                // if there are no descriptions for the language, insert it
                result = _repository.Insert(listingDescription);
            }
            else
            {
                // otherwise update it
                ListingDescription ld = descsLan.First();
                ld.Description = listingDescription.Description;
                result = _repository.Update(ld);
            }

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }

    }
}
