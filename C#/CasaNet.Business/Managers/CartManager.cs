﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    public class CartManager : ICartManager
    {
        readonly ICartRepository _cartRepository = null;

        public CartManager(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        public ManagerResponse Get(string key)
        {
            Cart cart =_cartRepository.GetByKey(key);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = cart
            };
        }

        public ManagerResponse GetItemsCount(string key)
        {
            Cart cart = _cartRepository.GetByKey(key);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = (cart != null && cart.Items != null)? cart.Items.Count : 0
            };
        }

        public ManagerResponse AddItem(string key, CartItem item)
        {
            Cart cart = _cartRepository.GetByKey(key);
            if (cart == null)
            {
                cart = new Cart();
            }
            CartItem existingItem = (from cartItem in cart.Items
                                     where cartItem.ProductID == item.ProductID && cartItem.LinkedObjectType == item.LinkedObjectType && cartItem.LinkedObjectID == item.LinkedObjectID
                                     select cartItem).FirstOrDefault();

            if (existingItem == null)
                cart.Items.Add(item);
            else
                existingItem.Quantity++;

            // 3 - update the cart on redis (overwrite)
            _cartRepository.Update(key, cart);

            return new ManagerResponse()
            {
                Succeed = true
            };
        }

        public ManagerResponse RemoveItem(string key, CartItem item)
        {
            ManagerResponse response = null;

            Cart cart = _cartRepository.GetByKey(key);
            if (cart == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "Cart not found" } }
                };
            }

            CartItem existingItem = (from cartItem in cart.Items
                                     where cartItem.ProductID == item.ProductID && cartItem.LinkedObjectType == item.LinkedObjectType && cartItem.LinkedObjectID == item.LinkedObjectID
                                     select cartItem).FirstOrDefault();

            if (existingItem == null)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "Item not present in cart" } }
                };
                return response;
            }
            else
            {
                if (existingItem.Quantity > 1)
                    existingItem.Quantity--;
                else
                {
                    cart.Items.Remove(existingItem);
                }
            }

            // 3 - update the cart on redis (overwrite)
            _cartRepository.Update(key, cart);


            return new ManagerResponse()
            {
                Succeed = true
            };
        }

        public ManagerResponse Empty(string key)
        {
            //Cart cart = _cartRepository.GetByKey(key);
            //cart.Items.Clear();
            //_cartRepository.Update(listingId, cart);
            _cartRepository.Delete(key);
            return new ManagerResponse()
            {
                Succeed = true,
                Data = null
            };
        }

        public ManagerResponse Update(string key, Cart cart)
        {
            // update the cart on redis (overwrite)
            _cartRepository.Update(key, cart);

            return new ManagerResponse()
            {
                Succeed = true
            };
        }
    }
}
