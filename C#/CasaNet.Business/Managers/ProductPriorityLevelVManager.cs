﻿using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;

namespace CasaNet.Business.Managers
{
    public class ProductPriorityLevelVManager : CasaNet.Business.Managers.IProductPriorityLevelVManager
    {
        IProductPriorityLevelVRepository _productPriorityLevelVRepository = null;

        public ProductPriorityLevelVManager(IProductPriorityLevelVRepository productPriorityLevelV)
        {
            _productPriorityLevelVRepository = productPriorityLevelV;
        }

        public bool privateIsGold(int customerId)
        {
            List<ProductPriorityLevelV> list = _productPriorityLevelVRepository.Get(e => e.CustomerId == customerId);
            return (list.Max(e => (int?)e.ProductPriorityLevel) ?? 0) == 30;
        }
    }
}
