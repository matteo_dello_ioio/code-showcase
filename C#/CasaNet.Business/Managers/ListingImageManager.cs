﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Core.Infrastructure.Helpers;

namespace CasaNet.Business.Managers
{
    public class ListingImageManager: IListingImageManager
    {
        IListingImageRepository _repository = null;

        public ListingImageManager(IListingImageRepository repository)
        {
            _repository = repository;
        }

        public ManagerResponse GetByListingId(int listingId)
        {
            ManagerResponse response = null;
            try
            {
                List<ListingImage> images = _repository.GetByListingId(listingId);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = images
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        public virtual bool HasMinimalRequiredDataForSave(ListingImage listingImage)
        {
            if (listingImage.ListingId == 0)
                return false;

            if (string.IsNullOrEmpty(listingImage.Hash))
                return false;

            return true;
        }

        public ManagerResponse Save(ListingImage listingImage)
        {
            if (listingImage == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                };
            }

            if (!HasMinimalRequiredDataForSave(listingImage))
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.MissingMinimalRequiredData } }
                };

            ListingImage result = null;
            if (listingImage.Id == 0)
                result = _repository.Insert(listingImage);
            else
                result = _repository.Update(listingImage);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }

        public bool DeleteById(int id)
        {
            ListingImage image = new ListingImage() { Id = id };
            return _repository.Delete(image);
        }
        public ManagerResponse Delete(ListingImage listingImage)
        {
            if (listingImage == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                };
            }
            bool result = _repository.Delete(listingImage);

            return new ManagerResponse()
            {
                Succeed = result
            };
        }
    }
}
