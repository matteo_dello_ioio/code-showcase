﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;

namespace CasaNet.Business.Managers
{
    public class PurchaseTransactionManager : IPurchaseTransactionManager
    {
        readonly IPurchaseTransactionRepository _purchaseTransactionRepository;

        public PurchaseTransactionManager(IPurchaseTransactionRepository purchaseTransactionRepository)
        {
            _purchaseTransactionRepository = purchaseTransactionRepository;
        }

        /// <summary>
        /// Create a transaction.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public ManagerResponse CreatePurchaseTransaction(PurchaseTransaction transaction)
        {
            ManagerResponse response = null;

            try
            {
                if (transaction == null)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                    };
                }

                PurchaseTransaction pt = _purchaseTransactionRepository.Insert(transaction);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = pt
                };
            }
            catch(Exception e)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Retrieve a transaction from its ID.
        /// </summary>
        /// <param name="listingId"></param>
        /// <returns></returns>
        public ManagerResponse GetByKey(int key)
        {
            ManagerResponse response = null;
            try
            {
                PurchaseTransaction transaction = _purchaseTransactionRepository.GetByKey(key);

                if(transaction != null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = true,
                        Data = transaction
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "unable to retrieve the specified transaction" } }
                    };
                }
            }
            catch(Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        public ManagerResponse Save(PurchaseTransaction transaction)
        {
            ManagerResponse response = null;
            try
            {
                if (transaction == null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull, LongMessage = "transaction cannot be null" } }
                    };
                }

                PurchaseTransaction updatedTransaction = _purchaseTransactionRepository.Update(transaction);

                if (transaction != null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = true,
                        Data = updatedTransaction
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "unable to update the specified transaction" } }
                    };
                }
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        /// <summary>
        /// Mark a transaction as succeeded.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public ManagerResponse SetPurchaseTransactionSucceeded(PurchaseTransaction transaction)
        {
            ManagerResponse response = null;
            try
            {
                if (transaction == null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull, LongMessage = "transaction cannot be null" } }
                    };
                }

                transaction.IsConfirmed = true;
                PurchaseTransaction updatedTransaction = _purchaseTransactionRepository.Update(transaction);

                if (transaction != null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = true,
                        Data = updatedTransaction
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "unable to update the specified transaction" } }
                    };
                }
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        /// <summary>
        /// Mark a transaction as failed.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public ManagerResponse SetPurchaseTransactionFailed(PurchaseTransaction transaction)
        {
            ManagerResponse response = null;
            try
            {
                transaction.IsConfirmed = false;
                PurchaseTransaction updatedTransaction = _purchaseTransactionRepository.Update(transaction);

                if (transaction != null)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = true,
                        Data = updatedTransaction
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "unable to update the specified transaction" } }
                    };
                }
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
    }
}
