﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Core.Infrastructure.Helpers;

namespace CasaNet.Business.Managers
{
    public class PackageManager
    {
        IProductPackageConfigurationRepository _productPackageConfigurationRepository = null;

        public PackageManager(IProductPackageConfigurationRepository repository)
        {
            _productPackageConfigurationRepository = repository;
        }

        public ManagerResponse GetProductGifts(Product product)
        {
            ManagerResponse response = null;
            try
            {
                List<PackageItem> productConf = _productPackageConfigurationRepository.GetProductPackageConfiguration(product);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = productConf
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

    }
}
