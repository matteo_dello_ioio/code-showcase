﻿using CasaNet.Business.Models;
using CasaNet.Business.Models.Lookup;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class LookupManager
    {
        readonly ILookupRepository _lookupRepository;

        public LookupManager(ILookupRepository lookupRepository)
        {
            _lookupRepository = lookupRepository;
        }

        public ManagerResponse GetContractTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<ContractTypes> data = _lookupRepository.GetContractTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetDestinationTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<DestinationTypes> data = _lookupRepository.GetDestinationTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetPropertyTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<PropertyTypes> data = _lookupRepository.GetPropertyTypes();
                data = (from d in data orderby d.Description select d).ToList();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetRoomNumberTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<RoomNumberTypes> data = _lookupRepository.GetRoomNumberTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetFloorTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<FloorTypes> data = _lookupRepository.GetFloorTypes();
                data = (from ft in data orderby ft.DisplayOrder select ft).ToList();

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetEnergyEfficiencyRatings()
        {
            ManagerResponse response = null;
            try
            {
                List<EnergyEfficiencyRatings> data = _lookupRepository.GetEnergyEfficiencyRatings();
                data = (from eer in data orderby eer.DisplayOrder select eer).ToList();

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetHeatingTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<HeatingTypes> data = _lookupRepository.GetHeatingTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetConditionTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<ConditionTypes> data = _lookupRepository.GetConditionTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetDeedStatus()
        {
            ManagerResponse response = null;
            try
            {
                List<DeedStatus> data = _lookupRepository.GetDeedStatus();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetBoxTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<BoxTypes> data = _lookupRepository.GetBoxTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetGardenTypes()
        {
            ManagerResponse response = null;
            try
            {
                List<GardenTypes> data = _lookupRepository.GetGardenTypes();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        public ManagerResponse GetAllCities()
        {
            ManagerResponse response = null;
            try
            {
                List<Town> data = _lookupRepository.GetAllCities();

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        public ManagerResponse GetCitiesByName(string name)
        {
            ManagerResponse response = null;
            try
            {
                List<Town> data = _lookupRepository.GetCitiesByName(name);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        public ManagerResponse GetAllZones()
        {
            ManagerResponse response = null;
            try
            {
                List<CityZone> data = _lookupRepository.GetAllZones();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetCityZones(int cityId)
        {
            ManagerResponse response = null;
            try
            {
                List<CityZone> data = _lookupRepository.GetCityZones(cityId);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
    }

}
