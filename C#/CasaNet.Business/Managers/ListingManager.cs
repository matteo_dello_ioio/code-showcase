﻿
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class ListingManager : IListingManager
    {
        readonly IListingRepository _listingRepository = null;

        public ListingManager(IListingRepository repository)
        {
            _listingRepository = repository;
        }

        /// <summary>
        /// Retrieve a listing by its unique ID.
        /// </summary>
        /// <param name="listingId"></param>
        /// <returns></returns>
        public virtual ManagerResponse GetByKey(int key)
        {
            ManagerResponse response = null;

            try
            {
                Listing listing = _listingRepository.GetByKey(key);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = listing
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Retrieve all active listings (not deleted) owned by a customer.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ManagerResponse GetActivesByCustomer(int customerId)
        {
            ManagerResponse response = null;
            try
            {
                List<Listing> listings = _listingRepository.GetAllByCustomer(customerId);

                listings = (from l in listings where l.IsDeleted == false select l).ToList();

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = listings
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        /// <summary>
        /// Retrieve all listings (deleted as well) owned by a customer.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ManagerResponse GetAllByCustomer(int customerId)
        {
            ManagerResponse response = null;

            try
            {
                List<Listing> listings = _listingRepository.GetAllByCustomer(customerId);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = listings
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Create or update a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public ManagerResponse Save(Listing listing)
        {
            if (listing == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingCannotBeNull } }
                };
            }

            if (!HasMinimumRequiredDataForSave(listing))
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingIsMissingMinimalRequiredData } }
                };
            }

            if (!HasMinimumPrice(listing))
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingPriceInvalid } }
                };
            }

            Listing result = null;
            if (listing.Id == 0)
            {
                listing.IsVisible = true;
                result = _listingRepository.Insert(listing);
            }
            else
            {
                result = _listingRepository.Update(listing);
            }

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }

        /// <summary>
        /// Put a listing in inactive state.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public ManagerResponse Delete(Listing listing)
        {
            if (listing == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingCannotBeNull } }
                };
            }

            if (listing.IsDeleted)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingAlreadyDeleted } }
                };
            }

            listing.IsDeleted = true;

            _listingRepository.Update(listing);

            return new ManagerResponse() { Succeed = true };
        }


        /// <summary>
        /// Check if the price specified in the listing respect business rules.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasMinimumPrice(Listing listing)
        {
            if (listing.ContractTypeId == 2 && listing.Price < 1000)
                return false;

            if (listing.ContractTypeId == 1 && listing.Price < 10)
                return false;

            return true;
        }

        /// <summary>
        /// Check if the listing has at least one picture.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasPicture(Listing listing)
        {
            return ((from i in listing.Images where i.ImageTypeId==1 select i).Count() > 0);
        }

        public virtual bool IsGeolocalized(Listing listing)
        {
            return (listing.GeoTownId != null);
        }

        /// <summary>
        /// Check if the listing has all data required to be saved.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public virtual bool HasMinimumRequiredDataForSave(Listing listing)
        {
            if (listing.ContractTypeId == 0)
                return false;

            if (listing.CustomerId == 0)
                return false;

            if (listing.PropertyTypeId == 0)
                return false;

            if (listing.DestinationTypeId == 0)
                return false;

            if (listing.Price == 0)
                return false;

            if (listing.AreaMq == 0)
                return false;

            if (listing.EnergyEfficiencyRatingId == null)
                return false;

            if (listing.Descriptions.Count == 0)
                return false;

            foreach(ListingDescription desc in listing.Descriptions)
            {
                if(desc.Description == null || desc.Description.Length < 100 || desc.Description.Length > 4000)
                    return false;
            }

            if (IsGeolocalized(listing) == false)
                return false;

            return true;
        }

        /// <summary>
        /// Check if exist a Gold product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasGoldActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activeGoldList = (from lp in listing.ActiveProducts
                                                   where lp.ProductSiteId == 503
                                                   && (lp.ActivationDate.Date <= DateTime.Now.Date && lp.ExpirationDate.Date >= DateTime.Now.Date)
                                                   orderby lp.Id descending
                                                   select lp).ToList();
            result = (activeGoldList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if exist a Smart product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasSmartActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activeSmartList = (from lsp in listing.ActiveProducts
                                                    where lsp.ProductSiteId == 502
                                                    && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                    orderby lsp.Id descending
                                                    select lsp).ToList();
            result = (activeSmartList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if exist a Free product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasFreeActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activeFreeList = (from lsp in listing.ActiveProducts
                                                  where lsp.ProductSiteId == 504
                                                  && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                  orderby lsp.Id descending
                                                  select lsp).ToList();
            result = (activeFreeList.Count() > 0);
            return result;
        }

        public bool HasSubscriptionActive(Listing listing)
        {
            return (this.HasGoldActive(listing) || this.HasSmartActive(listing) || (this.HasFreeActive(listing)));
        }

        public bool HasDepthProductActive(Listing listing)
        {
            return (this.HasPremiereActive(listing) || this.HasSuperTopActive(listing) || (this.HasTopActive(listing)));
        }

        /// <summary>
        /// Check if exist a Premiere product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasPremiereActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activePremiereList = (from lsp in listing.ActiveProducts
                                                       where lsp.ProductSiteId == 522
                                                       && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                       orderby lsp.Id descending
                                                       select lsp).ToList();
            result = (activePremiereList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if exist a SuperTop product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasSuperTopActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activeSuperTopList = (from lsp in listing.ActiveProducts
                                                       where lsp.ProductSiteId == 523
                                                       && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                       orderby lsp.Id descending
                                                       select lsp).ToList();
            result = (activeSuperTopList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if exist a Top product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasTopActive(Listing listing)
        {
            bool result = false;

            var topList = new[] { 488, 489, 490, 491 };
            List<ActiveProduct> activeTopList = (from lsp in listing.ActiveProducts
                                                  where topList.Contains(lsp.ProductSiteId)
                                                  && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                  orderby lsp.Id descending
                                                  select lsp).ToList();
            result = (activeTopList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if exist a Flash product active on the listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public bool HasFlashActive(Listing listing)
        {
            bool result = false;
            List<ActiveProduct> activeFlashList = (from lsp in listing.ActiveProducts
                                                  where lsp.ProductSiteId == 483
                                                  && (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                  orderby lsp.Id descending
                                                  select lsp).ToList();
            result = (activeFlashList.Count() > 0);
            return result;
        }
        /// <summary>
        /// Check if the listing has an active product.
        /// </summary>
        /// <param name="listingSite"></param>
        /// <returns></returns>
        public bool HasActiveProducts(Listing listing)
        {
            bool result = false;

            if (listing.ActiveProducts == null)
                return false;

            List<ActiveProduct> activeProductsList = (from lsp in listing.ActiveProducts
                                                       where (lsp.ActivationDate.Date <= DateTime.Now.Date && lsp.ExpirationDate.Date >= DateTime.Now.Date)
                                                       orderby lsp.Id descending
                                                       select lsp).ToList();
            result = (activeProductsList.Count() > 0);
            return result;
        }
    }
}
