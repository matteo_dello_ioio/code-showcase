﻿using System;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business.Managers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IListingSiteManager
    {
        //ManagerResponse GetByKey(int listingId, int siteId);
        ManagerResponse GetByListingIdSiteId(int listingId, int siteId);
        ManagerResponse Save(ListingSite listing);
    }
}
