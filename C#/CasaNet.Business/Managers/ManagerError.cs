﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    public class ManagerError
    {
        public ManagerErrorCode ErrorCode { get; set; }
        public string ShortMessage { get; set; }
        public string LongMessage { get; set; }
    }
}
