﻿
using System;
using CasaNet.Business.Models;
using CasaNet.Business.Managers;
using CasaNet.Core.Infrastructure.Log;
using System.Collections.Generic;

namespace CasaNet.Business.Managers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface ICustomerManager
    {
        Customer GetByKey(int key);

        Customer GetByEmail(string email);

        List<Customer> GetByPhoneNumber(string phoneNumber);

        ManagerResponse Save(Customer customer);
    }
}
