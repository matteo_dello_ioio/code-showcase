﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Managers;

namespace CasaNet.Business.Managers
{
    /// <summary>
    /// Manage activable products.
    /// </summary>
    public class ActivableProductManager : IActivableProductManager
    {
        IActivableProductRepository _activableProductRepository = null;
        IProductManager _productManager = null;

        public ActivableProductManager(IActivableProductRepository activableProductRepository, IProductManager productInfoService)
        {
            _activableProductRepository = activableProductRepository;
            _productManager = productInfoService;
        }

        public List<ActivableProduct> Get(int? siteId, int customerId)
        {
            List<ActivableProduct> allActivables = _activableProductRepository.GetByCustomerId(customerId);

            // apply site filter if present
            if (siteId.HasValue)
            {
                allActivables = (from activableProduct in allActivables where activableProduct.SiteId == siteId.Value select activableProduct).ToList();
            }

            return allActivables;
        }

        /// <summary>
        /// Retrieve the list of available activable products the customer already owns.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        public ManagerResponse GetAvailableActivableProducts(int customerId, int? siteId, Listing listing, Product product, ActivableProduct.ActivationStatus activationStatus = ActivableProduct.ActivationStatus.ReadyForActivation, bool isUsed = false)
        {
            ManagerResponse response = null;

            try
            {
                int? listingId = null;
                if (listing != null)
                    listingId = listing.Id;

                IEnumerable<ActivableProduct> activablesFiltered = (from a in _activableProductRepository.GetByCustomerId(customerId)
                                                                      where 
                                                                        a.SiteId == (siteId?? a.SiteId)
                                                                        && (a.ListingId == (listingId?? a.ListingId) || (a.ListingId == null))
                                                                        && a.ProductSiteId == (product != null? product.ProductSiteId : a.ProductSiteId)
                                                                        && a.Status == (char)activationStatus
                                                                        && (a.IsUsed?? false) == isUsed
                                                                && ActivableProductIsInTimeForActivation(a)
                                                                        select a);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = activablesFiltered.ToList()
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

        /// <summary>
        /// Retrieve the list of depth products the customer already owns.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        public ManagerResponse GetActivableDepthProducts(int customerId, int? siteId, Listing listing, ActivableProduct.ActivationStatus? activationStatus = null)
        {
            ManagerResponse response = null;

            try
            {
                //int? listingId = (listing != null) ? listing.Id : default(int?);

                ManagerResponse getAvailableActivableProductsResponse = GetAvailableActivableProducts(customerId, siteId, listing, null);
                if (getAvailableActivableProductsResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getAvailableActivableProductsResponse.Errors.First().LongMessage } }
                    };
                }
                List<ActivableProduct> activableProducts = (List<ActivableProduct>)getAvailableActivableProductsResponse.Data;
                char? status = (char?)(activationStatus?? null);
                activableProducts = (from ap in activableProducts where 
                                     _productManager.IsDepthProduct(ap.Product) == true
                                     && ap.Status == (char)(status?? ap.Status)
                                     select ap).ToList();

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = activableProducts
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Check if the activable product is in time for activation.
        /// </summary>
        /// <param name="activableProduct">The activable product.</param>
        /// <returns></returns>
        public bool ActivableProductIsInTimeForActivation(ActivableProduct activableProduct)
        {
            return (
                    ((activableProduct.ActivableFromDate.HasValue == false) || (activableProduct.ActivableFromDate.HasValue && activableProduct.ActivableFromDate.Value.Date <= DateTime.Now.Date))
                   &&
                   ((activableProduct.ExpirationDate.HasValue == false) || (activableProduct.ExpirationDate.HasValue && activableProduct.ExpirationDate.Value.Date > DateTime.Now.Date))
                );
        }


        public ManagerResponse GetLastUsed(int customerId, ProductType productType)
        {
            ManagerResponse response = null;
            try
            {
                var activables = _activableProductRepository.GetUsedByProductType(customerId, productType);
                var lastActived = (activables.Any()) ? activables.OrderByDescending(i => i.ActivationDate).First() : null;

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = lastActived
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }


        public ManagerResponse Save(ActivableProduct activable)
        {
            ActivableProduct result = null;
            if (activable.Id == 0)
                result = _activableProductRepository.Insert(activable);
            else
                result = _activableProductRepository.Update(activable);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }
    }
}
