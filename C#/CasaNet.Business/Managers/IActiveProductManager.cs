﻿
using System;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business.Managers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IActiveProductManager
    {
        ManagerResponse GetByListing(Listing listing);
        ManagerResponse Save(ActiveProduct activeProduct);
    }
}
