﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;

namespace CasaNet.Business.Managers
{
    public class ActiveProductManager : CasaNet.Business.Managers.IActiveProductManager
    {
        readonly IActiveProductRepository _activeProductRepository = null;

        public ActiveProductManager(IActiveProductRepository repository)
        {
            _activeProductRepository = repository;
        }

        /// <summary>
        /// Retrieve all active products for the specified listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public ManagerResponse GetByListing(Listing listing)
        {
            var response = new ManagerResponse();
            try
            {
                List<ActiveProduct> products = _activeProductRepository.GetByListing(listing, false);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = products
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Data = null,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message, ShortMessage = string.Empty } } 
                };
            }
            return response;
        }

        public ManagerResponse Save(ActiveProduct activeProduct)
        {
            ActiveProduct result = null;
            if (activeProduct.Id == 0)
                result = _activeProductRepository.Insert(activeProduct);
            else
                result = _activeProductRepository.Update(activeProduct);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }
    }
}
