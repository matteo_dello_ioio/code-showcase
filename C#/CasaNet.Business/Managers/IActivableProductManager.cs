
using CasaNet.Business.Models;
using System;
using System.Collections.Generic;

namespace CasaNet.Business.Managers
{
    public interface IActivableProductManager
    {
        bool ActivableProductIsInTimeForActivation(ActivableProduct pbi);

        List<ActivableProduct> Get(int? siteId, int customerId);

        ManagerResponse GetActivableDepthProducts(int customerId, int? siteId, Listing listing, ActivableProduct.ActivationStatus? activationStatus = null);

        /// <summary>
        /// Retrieve the list of available activable products the customer already owns.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="listingId">(optional) listing wich product is linked</param>
        /// <returns></returns>
        ManagerResponse GetAvailableActivableProducts(int customerId, int? siteId, Listing listing, Product product, ActivableProduct.ActivationStatus activationStatus = ActivableProduct.ActivationStatus.ReadyForActivation, bool isUsed = false);

        ManagerResponse GetLastUsed(int customerId, ProductType productType);

        ManagerResponse Save(ActivableProduct activable);
    }
}

