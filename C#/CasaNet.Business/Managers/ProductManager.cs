﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Services;
using CasaNet.Core.Services.Cache;

namespace CasaNet.Business.Managers
{
    /// <summary>
    /// Manage products.
    /// </summary>
    public class ProductManager : IProductManager
    {
        readonly IProductRepository _productRepository = null;
        readonly IProductPackageConfigurationRepository _packageRepository = null;
        readonly IActivableProductRepository _activableProductRepository = null;
        readonly IAppCache _appCache = null;
        const string CACHE_KEY = "CasaNet.Business.Managers";

        public ProductManager(IProductRepository catalogRepository, IProductPackageConfigurationRepository packageRepository, IActivableProductRepository productBucketRepository, IAppCache appCache)
        {
            _productRepository = catalogRepository;
            _packageRepository = packageRepository;
            _activableProductRepository = productBucketRepository;
            _appCache = appCache;
        }

        public bool IsDepthProduct(Product product)
        {
            return (product != null && (product.CategoryID == 2 || product.CategoryID == 8));
        }

        List<Product> _GetAllProducts_(int? siteId, string promoCode)
        {
            List<Product> products = _productRepository.Get(siteId, promoCode);
            foreach (Product p in products)
            {
                p.Gifts = new List<Product>();

                List<PackageItem> package = _packageRepository.GetProductPackageConfiguration(p);
                foreach (PackageItem pp in package)
                {
                    Product productGift = _productRepository.GetByKey(pp.ChildProductSiteId);
                    for (int i = 0; i < pp.Quantity; i++)
                    {
                        p.Gifts.Add(productGift);
                    }
                }
            }

            return products;
        }

        /// <summary>
        /// Retrieve the list of all products.
        /// </summary>
        /// <param name="siteId">(optional) owner site ID. site where products are sold</param>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        public List<Product> GetAllProducts(int? siteId, string promoCode)
        {
            var cacheKey = string.Format("{0}:{1}:{2}:{3}", CACHE_KEY, "ProductBySite_PromoCode", (siteId.HasValue) ? siteId.ToString() : "ND", (string.IsNullOrEmpty(promoCode)) ? "ND" : promoCode);
            var cached = _appCache.Exists(cacheKey);
            List<Product> products = (cached) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(_appCache.Get(cacheKey).ToString()) : _GetAllProducts_(siteId, promoCode);
            if (!cached)
                _appCache.Set(cacheKey, products, new TimeSpan(0, 15, 0));

            return products;
        }

        /// <summary>
        /// Retrieve a product by its unique ID.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ManagerResponse GetByKey(int key)
        {
            ManagerResponse response = null;

            try
            {
                Product data = _productRepository.GetByKey(key);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Check if the specified promo code does exists.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        public ManagerResponse VerifyPromoCode(int? siteId, string promoCode)
        {
            ManagerResponse response = null;

            var cacheKey = string.Format("{0}:{1}:{2}:{3}", CACHE_KEY, "ProductBySite_PromoCode", (siteId.HasValue) ? siteId.ToString() : "ND", (string.IsNullOrEmpty(promoCode)) ? "ND" : promoCode);
            var cached = _appCache.Exists(cacheKey);
            List<Product> productsInPromo = (cached) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(_appCache.Get(cacheKey).ToString()) : _GetAllProducts_(siteId, promoCode);
            if (!cached)
                _appCache.Set(cacheKey, productsInPromo, new TimeSpan(0, 15, 0));

            if (productsInPromo.Count == 0)
            {
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = false
                };
            }
            else
            {
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = true
                };
            }
            return response;
        }

    }
}
