﻿
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class ListingSiteManager : IListingSiteManager
    {
        readonly IListingSiteRepository _listingSiteRepository = null;

        public ListingSiteManager(IListingSiteRepository repository)
        {
            _listingSiteRepository = repository;
        }

        /// <summary>
        /// Retrieve a listing publication entry by listing ID and site ID.
        /// </summary>
        /// <param name="listingId">Internal listing ID.</param>
        /// <param name="siteId">Publication platform ID.</param>
        /// <returns></returns>
        public virtual ManagerResponse GetByListingIdSiteId(int listingId, int siteId)
        {
            ManagerResponse response = null;

            try
            {
                ListingSite listing = _listingSiteRepository.GetByListingIdSiteId(listingId, siteId);

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = listing
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        /// <summary>
        /// Create or update a listing.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public ManagerResponse Save(ListingSite listing)
        {
            if (listing == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ListingCannotBeNull } }
                };
            }

            ListingSite result = null;
            if (listing.Id == 0)
                result = _listingSiteRepository.Insert(listing);
            else
                result = _listingSiteRepository.Update(listing);

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }

    }
}
