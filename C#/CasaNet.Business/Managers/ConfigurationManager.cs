﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Core.Infrastructure.Helpers;

namespace CasaNet.Business.Managers
{
    public class ConfigurationManager
    {
        #region Data cache
        public static int DataCacheDefaultDuration = int.Parse(ConfigurationHelper.Current.GetValue("DataCacheDefaultDurationS"));
        #endregion

        #region SendMail
        public static string From = ConfigurationHelper.Current.GetValue("From");
        public static string ChangePwd = ConfigurationHelper.Current.GetValue("ChangePwd");
        public static string ChangePwdFrom = ConfigurationHelper.Current.GetValue("ChangePwdFrom");
        public static string ChangePwdSubject = ConfigurationHelper.Current.GetValue("ChangePwdSubject");
        public static string ChangeEmailVerifyFrom = ConfigurationHelper.Current.GetValue("ChangeEmailVerifyFrom");
        public static string ChangeEmailVerifyTemplate = ConfigurationHelper.Current.GetValue("ChangeEmailVerifyTemplate");
        public static string ChangeEmailVerifySubject = ConfigurationHelper.Current.GetValue("ChangeEmailVerifySubject");
        public static string VerifyEmailTemplate = ConfigurationHelper.Current.GetValue("VerifyEmailTemplate");
        #endregion
    }
}
