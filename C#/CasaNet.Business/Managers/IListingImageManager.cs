﻿using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IListingImageManager
    {
        ManagerResponse GetByListingId(int listingId);

        bool HasMinimalRequiredDataForSave(ListingImage listingImage);

        ManagerResponse Save(ListingImage listingImage);

        bool DeleteById(int id);

        ManagerResponse Delete(ListingImage listingImage);
    }
}
