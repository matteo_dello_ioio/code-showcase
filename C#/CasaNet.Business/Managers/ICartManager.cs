﻿
using System;
using CasaNet.Business.Models;

namespace CasaNet.Business.Managers
{
    public interface ICartManager
    {
        ManagerResponse Empty(string key);
        ManagerResponse GetItemsCount(string key);
        ManagerResponse AddItem(string key, CasaNet.Business.Models.CartItem item);
        ManagerResponse Get(string key);
        ManagerResponse RemoveItem(string key, CartItem item);
        ManagerResponse Update(string key, Cart cart);
    }
}
