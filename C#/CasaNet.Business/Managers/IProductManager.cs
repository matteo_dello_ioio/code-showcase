﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;
using System;
using System.Collections.Generic;

namespace CasaNet.Business.Managers
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IProductManager
    {
        bool IsDepthProduct(Product product);
        List<Product> GetAllProducts(int? siteId, string promoCode);
        ManagerResponse GetByKey(int key);
        ManagerResponse VerifyPromoCode(int? siteId, string promoCode);
    }
}
