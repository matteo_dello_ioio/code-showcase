﻿
using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using CasaNet.Core.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class LoginManager : ILoginManager
    {
        readonly ILoginRepository _loginRepository = null;

        public LoginManager(ILoginRepository repository)
        {
            _loginRepository = repository;
        }


        public LoginModel GetByLogin(string login)
        {
            LoginModel model = _loginRepository.GetByLogin(login);
            return model;
        }

        public LoginModel GetByCustomerId(int customerId)
        {
            LoginModel login = _loginRepository.GetByCustomerId(customerId);
            return login;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public LoginModel Save(LoginModel login)
        {            
            LoginModel result = null;
            if (login.Id == 0)
            {
                result = _loginRepository.Insert(login);
            }
            else
            {
                result = _loginRepository.Update(login);
            }
            /*
            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };*/
            return result;
        }

    }
}
