﻿using CasaNet.Business.Models;
using CasaNet.Business.Models.Lookup;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class SiteManager
    {
        readonly ISiteRepository _siteRepository;

        public SiteManager(ISiteRepository siteRepository)
        {
            _siteRepository = siteRepository;
        }

        public ManagerResponse Get()
        {
            ManagerResponse response = null;
            try
            {
                List<Site> data = _siteRepository.Get();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
        public ManagerResponse GetByKey(int key)
        {
            ManagerResponse response = null;
            try
            {
                Site data = _siteRepository.GetByKey(key);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }

    }

}
