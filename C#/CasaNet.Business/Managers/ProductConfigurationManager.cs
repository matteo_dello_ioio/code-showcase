﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using CasaNet.Business.Services;

namespace CasaNet.Business.Managers
{
    /// <summary>
    /// Manage products configurations.
    /// </summary>
    public class ProductConfigurationManager
    {
        IProductConfigurationRepository _productConfigurationRepository = null;

        public ProductConfigurationManager(IProductConfigurationRepository productConfigurationRepository)
        {
            _productConfigurationRepository = productConfigurationRepository;
        }

        public ManagerResponse GetConfigurations()
        {
            ManagerResponse response = null;
            try
            {
                var confs = _productConfigurationRepository.GetAll();
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = confs
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }
            return response;
        }
    }
}
