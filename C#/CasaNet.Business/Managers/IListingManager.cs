﻿
using System;
using CasaNet.Business.Models;
using CasaNet.Business.Managers;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business.Managers
{

    /// <summary>
    /// Provide informations on a listing.
    /// </summary>
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IListingManager
    {
        ManagerResponse Save(Listing listing);

        ManagerResponse GetByKey(int key);

        ManagerResponse GetActivesByCustomer(int customerId);

        ManagerResponse GetAllByCustomer(int customerId);

        ManagerResponse Delete(Listing listing);

        /// <summary>
        /// Check if the price specified in the listing respect business rules.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        bool HasMinimumPrice(Listing listing);

        /// <summary>
        /// Check if the listing has required geolocalization data.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        bool IsGeolocalized(Listing listing);

        /// <summary>
        /// Check if the listing has all data required to be saved.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        bool HasMinimumRequiredDataForSave(Listing listing);
        
        /// <summary>
        /// Check if the listing has at least one picture.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        bool HasPicture(Listing listing);

        /// <summary>
        /// Check if the listing has an active product.
        /// </summary>
        /// <param name="listingSite"></param>
        /// <returns></returns>
        bool HasActiveProducts(Listing listing);
        bool HasSubscriptionActive(Listing listing);
        bool HasDepthProductActive(Listing listing);

        bool HasFreeActive(Listing listing);
        bool HasGoldActive(Listing listing);
        bool HasPremiereActive(Listing listing);
        bool HasSmartActive(Listing listing);
        bool HasSuperTopActive(Listing listing);
        bool HasTopActive(Listing listing);
        bool HasFlashActive(Listing listing);
    }
}
