﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    public enum ManagerErrorCode
    {
        ND,

        #region Listing
        ListingAlreadyDeleted,
        ListingCannotBeNull,
        ListingIsMissingMinimalRequiredData,
        ListingIsMissingGeolocalizationData,
        ListingPriceInvalid,
        #endregion

        #region Listing image
        ListingImagesUpdateError,
        #endregion

        MissingMinimalRequiredData,

        ArgumentCannotBeNull
    }
}
