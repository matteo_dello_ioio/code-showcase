﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Core.Infrastructure.Helpers;

namespace CasaNet.Business.Managers
{
    public class RentalAvailabilityManager
    {
        IRentalAvailabilityRepository _repository = null;

        public RentalAvailabilityManager(IRentalAvailabilityRepository repository)
        {
            _repository = repository;
        }

        public ManagerResponse GetByListing(Listing l)
        {
            ManagerResponse response = null;

            try
            {
                List<RentalAvailability> data = _repository.GetByListing(l);
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = data
                };
            }
            catch (System.Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }


        public RentalAvailability Save(RentalAvailability entry)
        {
            if (entry == null)
                throw new ArgumentException("entry can not be null");

            RentalAvailability result = null;
            if (entry.Id == 0)
                result = _repository.Insert(entry);
            else
                result = _repository.Update(entry);

            return result;
        }

        public List<RentalAvailability> SaveBulk(List<RentalAvailability> entries)
        {
            if (entries == null)
                throw new ArgumentException("entries can not be null");

            List<RentalAvailability> results = new List<RentalAvailability>();
            foreach (RentalAvailability entry in entries)
            {
                RentalAvailability result = null;
                if (entry.Id == 0)
                    result = _repository.Insert(entry);
                else
                    result = _repository.Update(entry);
                results.Add(result);
            }
            return results;
        }

        public bool DeleteById(int id)
        {
            RentalAvailability ra = new RentalAvailability() { Id = id };
            return _repository.Delete(ra);
        }
    }
}
