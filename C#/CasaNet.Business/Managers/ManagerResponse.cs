﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Managers
{
    public class ManagerResponse
    {
        public bool Succeed { get; set; }
        public List<ManagerError> Errors { get; set; }
        public object Data { get; set; }
    }
}
