﻿using CasaNet.Business.Models;
using CasaNet.Business.Repositories;
using CasaNet.Business.Services;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Managers
{
    public class CustomerManager : ICustomerManager
    {
        readonly ICustomerRepository _customerRepository = null;

        public CustomerManager(ICustomerRepository repository)
        {
            _customerRepository = repository;
        }

        public List<Customer> GetByPhoneNumber(string phoneNumber)
        {
            List<Customer> customers = _customerRepository.Get(e => e.PhoneMainNr == phoneNumber);
            customers = customers.Where(e => e.IsActive).ToList();
            return customers;
        }


        public Customer GetByKey(int key)
        {
            Customer data = null;
            data = _customerRepository.GetByKey(key);

            return data;
        }

        public Customer GetByEmail(string email)
        {
            Customer customer = null;
            customer = _customerRepository.GetByEmail(email);
            return customer;
        }

        public ManagerResponse Save(Customer customer)
        {
            if (customer == null)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ArgumentCannotBeNull } }
                };
            }

            if (!HasMinimumRequiredDataForSave(customer))
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.MissingMinimalRequiredData } }
                };
            }

            Customer result = null;
            if (customer.Id == 0)
            {
                result = _customerRepository.Insert(customer);
            }
            else
            {
                result = _customerRepository.Update(customer);
            }

            return new ManagerResponse()
            {
                Succeed = true,
                Data = result
            };
        }

        /// <summary>
        /// Check if the customer has all data required to be saved.
        /// </summary>
        /// <param name="listing"></param>
        /// <returns></returns>
        public virtual bool HasMinimumRequiredDataForSave(Customer listing)
        {
            return true;
        }

    }
}
