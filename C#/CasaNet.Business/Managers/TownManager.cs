﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasaNet.Business.Models;
using CasaNet.Business.Repositories;

namespace CasaNet.Business.Managers
{
    public class TownManager
    {
        private readonly ITownRepository _townRepo = null;

        public TownManager(ITownRepository townRepo)
        {
            _townRepo = townRepo;
        }

        /// <summary>
        /// Restituisce una collezione di località che presentano il nome simile alla stringa fornita come attributo.
        /// </summary>
        /// <param name="value">Stringa di testo che determina quali località restituire</param>
        /// <returns>Collezione di località</returns>
        public List<Town> GetTownLike(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < 3)
            {
                return null;
            }

            return _townRepo.Get().Where(e => e.TownName.StartsWith(value, StringComparison.CurrentCultureIgnoreCase)).OrderBy(e => e.TownName)
                    .Take(10)
                    .ToList();
        }

        public Town GetTownFromCode(int geoTownId)
        {
            return _townRepo.Get().SingleOrDefault(e => e.Id == geoTownId);
        }
    }
}
