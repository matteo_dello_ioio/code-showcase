﻿using CasaNet.Business.Models;

namespace CasaNet.Business.Managers
{
    public interface IPurchaseTransactionManager
    {
        ManagerResponse CreatePurchaseTransaction(PurchaseTransaction transaction);

        ManagerResponse GetByKey(int key);

        ManagerResponse Save(PurchaseTransaction transaction);

        ManagerResponse SetPurchaseTransactionSucceeded(PurchaseTransaction transaction);

        ManagerResponse SetPurchaseTransactionFailed(PurchaseTransaction transaction);
    }
}
