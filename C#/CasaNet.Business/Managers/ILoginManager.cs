﻿
using System;
using CasaNet.Business.Models;

namespace CasaNet.Business.Managers
{
    public interface ILoginManager
    {
        LoginModel GetByCustomerId(int customerId);
        LoginModel GetByLogin(string login);
        LoginModel Save(LoginModel login);
    }
}
