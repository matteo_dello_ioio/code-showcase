﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;
using System;
using System.Collections.Generic;

namespace CasaNet.Business
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface ISiteGateway
    {
        string PlatformName { get; }

        /// <summary>
        /// Publish <paramref name="listing"/>.
        /// </summary>
        /// <param name="customer">Listing owner.</param>
        /// <param name="listing">Listing.</param>
        /// <param name="product">Product used for publication.</param>
        /// <param name="expirationDate">Expiration date for the listing.</param>
        /// <returns></returns>
        ManagerResponse Publish(Customer customer, Listing listing, Product product, DateTime expirationDate, List<RentalAvailability> availabilities);

        /// <summary>
        /// Activate <paramref name="product"/>.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="publicationInfo"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        ManagerResponse ActivateProduct(Customer customer, ListingSite publicationInfo, Product product);
    }
}
