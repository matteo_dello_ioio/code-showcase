
﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Infrastructure.Log;

namespace CasaNet.Business
{
    [Logger(LogLevel.Debug, LogAttributes = true)]
    public interface IPaymentGateway
    {
        CheckoutOutput CheckoutCart(int customerId, string paymentMethodId, Cart cart);
        ManagerResponse GetTransactionData(string param);
        ManagerResponse GetTransactionByKey(int transactionId);
        ManagerResponse ProcessTransactionSucceeded(int transactionId, string gatewayTransactionId, decimal transactionFee);
        ManagerResponse ProcessTransactionFailed(int transactionId);
    }

    public class CheckoutOutput
    {
        public PurchaseTransaction Transaction { get; set; }
        public string RedirectUrl { get; set; }
    }

}