﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Gateways
{
    /// <summary>
    /// Payment notification from CasaPay
    /// </summary>
    public class CasaPayPaymentNotification
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GatewayId { get; set; }
        /// <summary>
        /// Merchant transaction unique identifier.
        /// </summary>
        public int TransactionId { get; set; }
        /// <summary>
        /// Gateway transaction unique identifier.
        /// </summary>
        public string GatewayTransactionId { get; set; }
        /// <summary>
        /// Fees charged to the merchant.
        /// </summary>
        public decimal TransactionFee { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StatusCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NotifyUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PayerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PayerEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool ToDeliver { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDelivered { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrorCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrorDescription { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object Data { get; set; }
        //public string DataSerial
        //{
        //    get
        //    {
        //        return Data.ToJson();
        //    }
        //}
    }
}
