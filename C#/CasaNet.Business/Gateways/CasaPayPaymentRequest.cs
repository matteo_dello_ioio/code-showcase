﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Gateways
{
    /// <summary>
    /// Incapsulate a payment requests to CasaPay.
    /// </summary>
    public class CasaPayPaymentRequest
    {
        public string AppId { get; set; }
        public string GatewayId { get; set; }
        public string TransactionId { get; set; }
        public List<RequestItem> Items { get; set; }
        public string LandingPageOkUrl { get; set; }
        public string LandingPageKoUrl { get; set; }
        public string ServerNotificationUrl { get; set; }
        public string MerchantLogoUrl { get; set; }
        public string ItemsSerial { get; set; }
        public decimal TotalAmount { get; set; }

        public class RequestItem
        {
            public int Number { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public int Qty { get; set; }
            public decimal Amount { get; set; }
        }
    }
}
