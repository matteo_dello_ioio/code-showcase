
﻿using CasaNet.Business.Managers;
using CasaNet.Business.Models;
using CasaNet.Core.Services.CasaAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasaNet.Business.Gateways
{
    public class CasaPayGateway : IPaymentGateway
    {
        readonly CasaPayConfiguration _configuration = null;
        readonly ISystemWebClient _webClient = null;
        readonly IPurchaseTransactionManager _purchaseTransactionManager = null;

        public CasaPayGateway(CasaPayConfiguration configuration, ISystemWebClient webClient, IPurchaseTransactionManager purchaseTransactionManager)
        {
            _configuration = configuration;
            _webClient = webClient;
            _purchaseTransactionManager = purchaseTransactionManager;
        }

        List<CasaPayPaymentRequest.RequestItem> MapCartItems2PaymentItems(List<CartItem> cartItems)
        {
            List<CasaPayPaymentRequest.RequestItem> itemsToPay = new List<CasaPayPaymentRequest.RequestItem>();
            foreach (CartItem cartItem in cartItems)
            {
                itemsToPay.Add(new CasaPayPaymentRequest.RequestItem()
                {
                    Number = 0,
                    Name = cartItem.Description.ToString(),
                    Description = cartItem.Description.ToString(),
                    Amount = cartItem.PriceWithVAT,
                    Qty = cartItem.Quantity
                });
            }
            return itemsToPay;
        }

        /// <summary>
        /// Build a payment request for the gateway.
        /// </summary>
        /// <param name="paymentMethodId"></param>
        /// <param name="transactionId"></param>
        /// <param name="TotalWithVAT"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        CasaPayPaymentRequest BuildPaymentRequest(string paymentMethodId, int transactionId, decimal TotalWithVAT, List<CasaPayPaymentRequest.RequestItem> items)
        {
            CasaPayPaymentRequest paymentRequest = new CasaPayPaymentRequest();
            paymentRequest.AppId = "CASANET"; //TODO: from configuration
            paymentRequest.GatewayId = paymentMethodId; //GestPayGateway/PayPalGateway
            paymentRequest.TransactionId = transactionId.ToString();
            paymentRequest.LandingPageOkUrl = string.Format("{0}/{1}", _configuration.OkUrl, transactionId);
            paymentRequest.LandingPageKoUrl = _configuration.KoUrl;
            paymentRequest.ServerNotificationUrl = _configuration.NotificationUrl;
            // paymentRequest.ItemsSerial //DON'T SET THIS!
            paymentRequest.TotalAmount = TotalWithVAT;
            paymentRequest.Items = items;
            return paymentRequest;
        }

        /// <summary>
        /// Create a new transaction.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cart"></param>
        /// <param name="paymentMethodId"></param>
        /// <returns></returns>
        PurchaseTransaction CreateTransaction(int customerId, Cart cart, string paymentMethodId)
        {
            PurchaseTransaction newTransaction = new PurchaseTransaction()
            {
                AmountVAT = cart.TotalWithVAT,
                Amount = cart.TotalNet,
                CustomerId = customerId,
                TransactionObject = Newtonsoft.Json.JsonConvert.SerializeObject(cart),
                PaymentGateway = paymentMethodId,
                PromoCode = cart.PromoCode
            };
            ManagerResponse createPurchaseTransactionResponse = _purchaseTransactionManager.CreatePurchaseTransaction(newTransaction);

            if (createPurchaseTransactionResponse.Succeed == false)
            {
                throw new ApplicationException(createPurchaseTransactionResponse.Errors.First().LongMessage);
            }
            PurchaseTransaction transaction = (PurchaseTransaction)createPurchaseTransactionResponse.Data;

            return transaction;
        }

        /// <summary>
        /// Send a payment request to the gateway.
        /// </summary>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        object SendRequest(CasaPayPaymentRequest paymentRequest)
        {
            object result = null;

            string jsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(paymentRequest);

            string casapayResponse = null;
            try
            {
                _webClient.Headers[System.Net.HttpRequestHeader.ContentType] = "application/json";
                if (_configuration.UseProxy && string.IsNullOrEmpty(_configuration.ProxyUrl) == false)
                    casapayResponse = _webClient.Post(_configuration.CheckoutUrl, jsonRequest, _configuration.ProxyUrl);
                else
                    casapayResponse = _webClient.Post(_configuration.CheckoutUrl, jsonRequest);
            }
            catch (HttpException httpEx)
            {
                throw new ApplicationException("communication error with the payment gateway", httpEx);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("unexpected error with the payment gateway", ex);
            }

            Newtonsoft.Json.JsonSerializerSettings serializerSettings = new Newtonsoft.Json.JsonSerializerSettings();
            serializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

            APIResponseMessage<object> apiResponse = (APIResponseMessage<object>)Newtonsoft.Json.JsonConvert.DeserializeObject(casapayResponse, typeof(APIResponseMessage<object>), serializerSettings);

            if (apiResponse.Response.Succeed == false)
            {
                throw new ApplicationException(apiResponse.Response.ErrMsg);
            }

            result = apiResponse.Response.Data;

            return result;
        }

        public CheckoutOutput CheckoutCart(int customerId, string paymentMethodId, Cart cart)
        {
            CheckoutOutput response = null;

            PurchaseTransaction transaction = null;
            try
            {
                // create a purchase transaction
                transaction = CreateTransaction(customerId, cart, paymentMethodId);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("unable to create transaction", ex);
                //response = new ManagerResponse()
                //{
                //    Succeed = false,
                //    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                //};
            }

            if (cart.TotalWithVAT > 0)
            {
                List<CasaPayPaymentRequest.RequestItem> itemsToPay = MapCartItems2PaymentItems(cart.Items);

                CasaPayPaymentRequest paymentRequest = BuildPaymentRequest(paymentMethodId, transaction.Id, cart.TotalWithVAT, itemsToPay);

                string result = null;
                try
                {
                    result = SendRequest(paymentRequest).ToString();

                    response = new CheckoutOutput() { Transaction = transaction, RedirectUrl = result };
                    //response = new ManagerResponse()
                    //{
                    //    Succeed = true,
                    //    Data = new GatewayResponse (){ TransactionId = transaction.Id, RedirectUrl = result }
                    //};
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("unable to communicate with the payment gateway", ex);
                    //response = new ManagerResponse()
                    //{
                    //    Succeed = false,
                    //    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = ex.Message } }
                    //};
                }
            }
            else
            {
                response = new CheckoutOutput() { Transaction = transaction };
                //response = new ManagerResponse()
                //{
                //    Succeed = true,
                //    Data = new GatewayResponse() { TransactionId = transaction.Id }
                //};
            }


            return response;
        }

        public ManagerResponse GetTransactionData(string param)
        {
            ManagerResponse response = null;

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("param", param);

                string casapayResponse = null;

                if (_configuration.UseProxy && string.IsNullOrEmpty(_configuration.ProxyUrl) == false)
                    casapayResponse = _webClient.Get(_configuration.ForwardCompleteUrl, data, _configuration.ProxyUrl);
                else
                    casapayResponse = _webClient.Get(_configuration.ForwardCompleteUrl, data);

                Newtonsoft.Json.JsonSerializerSettings serializerSettings = new Newtonsoft.Json.JsonSerializerSettings();
                serializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

                APIResponseMessage<object> apiResponse = (APIResponseMessage<object>)Newtonsoft.Json.JsonConvert.DeserializeObject(casapayResponse, typeof(APIResponseMessage<object>), serializerSettings);

                if (apiResponse.Response.Succeed)
                {
                    response = new ManagerResponse()
                    {
                        Succeed = apiResponse.Response.Succeed,
                        Data = apiResponse.Response.Data
                    };
                }
                else
                {
                    response = new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = apiResponse.Response.ErrMsg } }
                    };
                }
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError> { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        public ManagerResponse GetTransactionByKey(int transactionId)
        {
            ManagerResponse response = null;

            ManagerResponse getByKeyResponse = _purchaseTransactionManager.GetByKey(transactionId);
            if (getByKeyResponse.Succeed == false)
            {
                return new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getByKeyResponse.Errors.First().LongMessage } }
                };
            }
            PurchaseTransaction transaction = (PurchaseTransaction)getByKeyResponse.Data;

            response = new ManagerResponse()
            {
                Succeed = true,
                Data = transaction
            };

            return response;
        }

        public ManagerResponse ProcessTransactionSucceeded(int transactionId, string gatewayTransactionId, decimal transactionFee)
        {
            ManagerResponse response = null;

            try
            {
                ManagerResponse getByKeyResponse = _purchaseTransactionManager.GetByKey(transactionId);
                if (getByKeyResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = getByKeyResponse.Errors.First().LongMessage } }
                    };
                }
                PurchaseTransaction transaction = (PurchaseTransaction)getByKeyResponse.Data;
                transaction.TransactionFee = transactionFee;
                transaction.GatewayTransactionId = gatewayTransactionId;
                transaction.IsConfirmed = true;


                ManagerResponse setPurchaseTransactionSucceededResponse = _purchaseTransactionManager.Save(transaction);
                if (setPurchaseTransactionSucceededResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = setPurchaseTransactionSucceededResponse.Errors.First().LongMessage } }
                    };
                }

                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = transaction
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }

        public ManagerResponse ProcessTransactionFailed(int transactionId)
        {
            ManagerResponse response = null;

            try
            {
                ManagerResponse getByKeyResponse = _purchaseTransactionManager.GetByKey(transactionId);
                if (getByKeyResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = "unable to find the specified transaction" } }
                    };
                }

                PurchaseTransaction transaction = (PurchaseTransaction)getByKeyResponse.Data;
                transaction.IsConfirmed = false;

                ManagerResponse setPurchaseTransactionFailedResponse = _purchaseTransactionManager.Save(transaction);
                if (setPurchaseTransactionFailedResponse.Succeed == false)
                {
                    return new ManagerResponse()
                    {
                        Succeed = false,
                        Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = setPurchaseTransactionFailedResponse.Errors.First().LongMessage } }
                    };
                }
                response = new ManagerResponse()
                {
                    Succeed = true,
                    Data = transaction
                };
            }
            catch (Exception e)
            {
                response = new ManagerResponse()
                {
                    Succeed = false,
                    Errors = new List<ManagerError>() { new ManagerError() { ErrorCode = ManagerErrorCode.ND, LongMessage = e.Message } }
                };
            }

            return response;
        }
    }

}
//>>>>>>> features/renewbackend
