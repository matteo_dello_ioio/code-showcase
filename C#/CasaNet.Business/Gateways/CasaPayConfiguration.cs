﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasaNet.Business.Gateways
{
    public class CasaPayConfiguration
    {
        public string OkUrl { get; set; }
        public string KoUrl { get; set; }
        public string ForwardCompleteUrl { get; set; }
        public string NotificationUrl { get; set; }
        public string CheckoutUrl { get; set; }
        public string ProxyUrl { get; set; }
        public bool UseProxy { get; set; }
    }
}
