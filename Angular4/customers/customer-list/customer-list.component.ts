import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { CustomerItem } from '../models/customer-item';
import { QueryObjectDTO } from 'treeplat-mobile-rest-client';
import { CustomerSearch } from '../models/customer-search.interface';

@Component({
  selector: 'app-customer-list',
  styles: [],
  templateUrl: './customer-list.component.html'
})

export class CustomerListComponent {

  searchParams: CustomerSearch = this._getDefaultSearch();

  customers: CustomerItem[];
  paging = [];
  sum = 6;

  get hasCustomFilters() {

    let customfilters = (
      (this.searchParams.Description != this._getDefaultSearch().Description)
      ||
      (this.searchParams.Type != this._getDefaultSearch().Type)
      ||
      (this.searchParams.PhoneNumber != this._getDefaultSearch().PhoneNumber)
      ||
      (this.searchParams.Email != this._getDefaultSearch().Email)
      ||
      (this.searchParams.WithActiveAssignment != this._getDefaultSearch().WithActiveAssignment)
      ||
      (this.searchParams.WithOpenRequest != this._getDefaultSearch().WithOpenRequest)
    );

    return customfilters;
  }

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    let qo = new QueryObjectDTO();
    qo.Paging.Limit = 12;
    this.loadCustomer(qo);
  }

  onScrollDown() {
    const start = this.sum;
    let $loadingWrapper = document.querySelector('.infinite-wrapper');
    this.sum += 6;

    $loadingWrapper.classList.add('loading');
    this.addItems(start, this.sum);
    $loadingWrapper.classList.remove('loading');
  }

  addItems(startIndex, endIndex) {
    let qo = new QueryObjectDTO();
    this.paging.push([' ', this.loadCustomer(qo)].join(''));
  }

  loadCustomer(listRequest: QueryObjectDTO) {
    this.customerService.readAll(listRequest)
      .subscribe(data => {
        this.customers = data;

      }, function (error) {
        console.log(error);
      });
  }


  private _getDefaultSearch() {
    return {
      Description: null,
      PhoneNumber: null,
      Email: null,
      Type: null,
      WithActiveAssignment: false,
      WithOpenRequest: false
    };
  }

  resetSearch() {
    this.searchParams = this._getDefaultSearch();
    this.search(this.searchParams);
  }

  search(search: CustomerSearch) {
    this.searchParams = search;
    this.customerService.search(search)
      .subscribe(data => {
        this.customers = data;
      }, function (error) {
        console.log(error);
      });
  }
}