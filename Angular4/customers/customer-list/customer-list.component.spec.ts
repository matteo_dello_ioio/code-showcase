
import { Component, Directive, Input, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomerListComponent } from './customer-list.component';
import { CustomerService } from '../customer.service';
import { Observable } from 'rxjs/Observable';
import { CustomerItem } from '../models/customer-item';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CustomerSearchComponent } from '../customer-search/customer-search.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';




let mockCustomerService = {
  readAll() { }
}


// @Component({ selector: 'customer-search', template: '' })
// class DummyCustomerSearchComponent {
// }


describe('CustomerListComponent', () => {

  let component: CustomerListComponent;
  let fixture: ComponentFixture<CustomerListComponent>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        CustomerListComponent,
        CustomerSearchComponent
      ],
      imports: [
        FormsModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        MaterialModule,
        InfiniteScrollModule
      ],
      providers: [
        { provide: CustomerService, useValue: mockCustomerService }
      ]/*,
      schemas: [CUSTOM_ELEMENTS_SCHEMA]*/
    });

    spyOn(mockCustomerService, 'readAll').and.returnValue(Observable.of([
      {
        ID: 1,
        Denominazione: 'Mickey Mouse',
        Emails: ['mickey@disney.it']
      }
    ]));

    fixture = TestBed.createComponent(CustomerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate customer list on init', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.customers[0].Denominazione).toBe('Mickey Mouse');
  }));

});