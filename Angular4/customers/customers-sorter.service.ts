import { Injectable } from '@angular/core';
import { CustomersSorterInterface } from './customers-sorter.interface';
import { CustomerItem } from './models/customer-item'

@Injectable()
export class CustomersSorterService implements CustomersSorterInterface {

  constructor() { }

  sortByDenominationASC(customers: CustomerItem[]): CustomerItem[] {
    return customers.sort(this.customerSortAsc);
  }

  sortByDenominationDESC(customers: CustomerItem[]): CustomerItem[] {
    return customers.sort(this.customerSortDesc);
  }

  customerSortAsc(value1: CustomerItem, value2: CustomerItem): number {
    if (value1.Denominazione > value2.Denominazione) {
      return 1;
    } else if (value1.Denominazione < value2.Denominazione) {
      return -1;
    } else {
      return 0;
    }
  }

  customerSortDesc(value1: CustomerItem, value2: CustomerItem): number {
    if (value1.Denominazione > value2.Denominazione) {
      return -1;
    } else if (value1.Denominazione < value2.Denominazione) {
      return 1;
    } else {
      return 0;
    }
  }

  sortByInsertDateASC(customers: CustomerItem[]): CustomerItem[] {
    return customers.sort((a, b) => { return a.InsertDate.getTime() - b.InsertDate.getTime() });
  }

  sortByInsertDateDESC(customers: CustomerItem[]): CustomerItem[] {
    return customers.sort((a, b) => { return b.InsertDate.getTime() - a.InsertDate.getTime() });
  }
}


