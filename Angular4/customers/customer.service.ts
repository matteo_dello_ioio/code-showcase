import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '../../environments/environment';
import {
  QueryObjectDTO,
  ListRequest,
  AbstractCustomerDataService, ClienteDTO,
  TypeAheadRequest,
  ClienteElencoTypeAheadDTO
} from 'treeplat-mobile-rest-client';
import { ICustomerService } from './customer-service.interface'
import { CustomerItem } from './models/customer-item';
import { CustomerModel } from './models/customer.model';
import { CustomersMapper } from './customers-mapper'
import { SessionService } from '../session/session.service';
import { CustomerMiniModel } from '../shared/models/customer-mini-model';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import { CommonsMapper } from '../shared/commons-mapper';
import { CustomerSearch } from './models/customer-search.interface';





@Injectable()
export class CustomerService implements ICustomerService {

  environment = null;

  constructor(
    private sessionService: SessionService,
    private dataService: AbstractCustomerDataService,
    private commonsMapper: CommonsMapper,
    private mapper: CustomersMapper
  ) {
    this.environment = environment;
  }

  readAllTypeAhead(searchValue: string): Observable<CustomerMiniModel[]> {

    if (!searchValue)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let request = new TypeAheadRequest();
      request.searchValue = searchValue;
      request.idGestione = this.sessionService.getSession().IdGestione;
      request.paging = null;

      this.dataService.getAllTypeAhead(this.environment.ServiceBaseWcfUrlAuth, request)
        .subscribe(response => {

          var items = new Array<CustomerMiniModel>();

          var customers = response as ClienteElencoTypeAheadDTO[];
          if (customers) {
            for (var i = 0; i < customers.length; i++) {
              var item = this.commonsMapper.clienteElencoTypeAheadDTO2CustomerMiniModel(customers[i]);
              items.push(item);
            }
          }

          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  readAll(queryObject: QueryObjectDTO): Observable<CustomerItem[]> {

    if (!queryObject)
      throw new Error("argument cannot be null");

    let request = new ListRequest();
    request.QueryObject = queryObject;

    return Observable.create(observer => {

      this.dataService.getAll(this.environment.ServiceBaseWcfUrlAuth, request)
        .subscribe(response => {
          //console.log(">>" + JSON.stringify(response.ResData));

          var items = new Array<CustomerItem>();

          var customers = response;

          if (customers) {
            for (var i = 0; i < customers.length; i++) {
              var item = this.mapper.mapClienteDTO2CustomerItem(customers[i]);
              items.push(item);
            }
          }

          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * 
   * @param id 
   */
  readById(id: number): Observable<CustomerModel> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.getById(this.environment.ServiceBaseWcfUrlAuth, id)
        .subscribe(response => {
          let model = new CustomerModel();

          let dto = response as ClienteDTO;

          if (dto) {
            model = this.mapper.mapClienteDTO2CustomerModel(dto);
          }

          observer.next(model);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });

  }

  /**
   * 
   * @param saveRequest 
   */
  save(customer: CustomerModel): Observable<CustomerModel> {

    customer.IdGestione = this.sessionService.getSession().IdGestione;

    return Observable.create(observer => {

      let saveRequest = this.mapper.createCustomerSaveRequest(customer);

      this.dataService.save(this.environment.ServiceBaseWcfUrlAuth, saveRequest)
        .subscribe(response => {
          let model = this.mapper.mapClienteDTO2CustomerModel(response);
          observer.next(model);
          observer.complete();
        },
          (error: Error) => {
            if (error instanceof Error) {
              console.log('An error occurred:', error.message);
            } else {
              console.log("Server-side error occured.");
              console.log('An error occurred:', error);
            }

            observer.error(error);
          });
    });
  }



  /**
   * 
   * @param searchParams 
   */
  search(searchParams: CustomerSearch): Observable<CustomerItem[]> {
    if (!searchParams)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.search(this.environment.ServiceBaseWcfUrlAuth,
        searchParams.Description,
        searchParams.PhoneNumber,
        searchParams.Email,
        searchParams.Type,
        searchParams.WithActiveAssignment,
        searchParams.WithOpenRequest)
        .subscribe(results => {

          let items = null;

          if (results) {
            items = new Array<CustomerItem>();
            for (let i = 0; i < results.length; i++) {
              let item = this.mapper.mapClienteDTO2CustomerItem(results[i]);
              items.push(item);
            }
          }
          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

}