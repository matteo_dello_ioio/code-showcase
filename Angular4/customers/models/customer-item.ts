import { PhoneNumberModel } from "../../shared/models/phone-number-model";

export class CustomerItem {
    ID: number;
    Denominazione: string;
    Referente: string;
    Emails: string[];
    PhoneNumbers: PhoneNumberModel[];
    InsertDate: Date;
    
    constructor(){
        this.Emails = new Array<string>();
        this.PhoneNumbers = new Array<PhoneNumberModel>();
    }
}