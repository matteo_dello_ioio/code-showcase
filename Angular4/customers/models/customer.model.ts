
import { AddressModel } from "../../shared/models/address-model";
import { PhoneNumberModel } from "../../shared/models/phone-number-model";
import { CustomerPrivacySettingsModel } from "./customer-privacy-settings.model";


export class CustomerModel {

    //IdAgenzia: number;
    //NomeAgenzia: string;
    //IdGruppo: number;
    //NomeGruppo: string;
    //Editori:EditoreMiniDTO[];
    //TipoGestione: number;
    //DataInserimento: Date;
    //DataModifica: Date;
    //IndirizzoComune: string;
    //IndirizzoDescrizione: string;
    //TipoImpiego: number; NOT USED IN THIS APP

    ID: number;

    TagCliente?: number[];
    Ruolo?: string;
    Editore?: string;

    IdGestione: number;

    //Descrizione: string;
    ControlCode: string;

    // dati gestionali
    CodicePadre: number;
    EsenteAUI: boolean;

    // dati anagrafici
    CodiceFiscale: string;
    Nome: string;
    Cognome: string;
    TipoSesso: number;
    Stato: number;
    StatoCivile: number;

    NascitaData: Date;
    NascitaLuogo: string;
    NascitaProvincia: string;





    DataRilascioDocumento: Date;
    DataScadenzaDocumento: Date;
    NumeroDocumentoPresentato: string;
    TipoDocumentoPresentato: number;
    AutoritaLuogoRilascio: string;

    Address: AddressModel;

    // dati aggiuntivi
    TipoTitolo: number;

    // dati aziendali
    PIva?: string;
    CompanyName?: string;


    // recapiti
    PhoneNumbers: PhoneNumberModel[];
    EMail: string;
    EMail2: string;
    SkypeID: string;
    SitoInternet: string;
    AltriRecapiti: string;
    TipoCanalePreferito: number;
    Referenti: Array<object>;
    Fonte?: string;
    Note: string;
    Settore?: string;
    Tag?: number[];
    TipoCliente: number;


    // *** Privacy ***
    PrivacyBitmask: number;
    PrivacySettings: CustomerPrivacySettingsModel;

    constructor() {
        this.Address = new AddressModel();
        this.PrivacySettings = new CustomerPrivacySettingsModel();
        this.TipoTitolo = 1;
        this.TipoSesso = 1;
        this.StatoCivile = 1;
        this.PhoneNumbers = new Array<PhoneNumberModel>();
    }
}