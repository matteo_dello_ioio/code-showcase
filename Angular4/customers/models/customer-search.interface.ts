


export interface CustomerSearch {
    Type: string,
    Description: string
    PhoneNumber: string,
    Email: string,
    WithActiveAssignment: boolean,
    WithOpenRequest: boolean
}