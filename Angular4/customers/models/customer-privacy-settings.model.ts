

export class CustomerPrivacySettingsModel {
    Network: boolean;
    Gruppo: boolean;
    Marketing: boolean;
    WebMarketing: boolean;
    Telefonico: boolean;
    Pubblicitario: boolean;
    RicercheMercato: boolean;
    GPScomunicazionealnetworkgabetti: boolean;
    GPSfinalitadimarketing: boolean;
    GPSTrattamentoDati: boolean;
    GPSricezioneemail: boolean;
}