import { DatePipe } from '@angular/common';
import { CookieService } from 'ng2-cookies';
import { TestBed, async, fakeAsync, inject } from '@angular/core/testing';
import { Observable } from 'rxjs';
import {
  QueryObjectDTO,
  AbstractCustomerDataService, 
  CustomerDataService
} from 'treeplat-mobile-rest-client';
import { CustomerService } from './customer.service';
import { CustomersMapper } from './customers-mapper'
import { UtilityService } from '../shared/utility-service';
import { CommonsMapper } from '../shared/commons-mapper';
import { GeoService } from '../services/geo.service';
import { SessionService } from '../session/session.service';
import { SessionModule } from '../session/session.module';
import { CustomerModel } from './models/customer.model';
import { environment } from '../../environments/environment';


let mockCustomerDataService = {
  getAll() { },
  getById() { },
  save() { }
}

describe('CustomerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DatePipe,
        CookieService,
        UtilityService,
        SessionService,
        GeoService,
        CommonsMapper,
        CustomersMapper,
        //{ provide: AbstractCustomerDataService, useClass: CustomerDataService },
        { provide: AbstractCustomerDataService, useValue: mockCustomerDataService },
        CustomerService
      ],
      imports: [
        SessionModule
      ],
    });
  });

  it('should be created', inject([CustomerService], (service: CustomerService) => {
    expect(service).toBeTruthy();
  }));

  describe('readAll method', () => {
    it('should exists',
      inject([CustomerService], (service: CustomerService) => {
        expect(service.readAll).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([CustomerService], (service: CustomerService) => {
        expect(function () { service.readAll(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock200Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 200,
              "ResMessage": "Ok",
              "SessionChanged": false,
              "ResData": [
                {
                  "ControlCode": null,
                  "DataInserimento": "/Date(1500908600000+0200)/",
                  "DataModifica": "/Date(1500908600000+0200)/",
                  "Descrizione": "Michele Porcu",
                  "IdAgenzia": 0,
                  "IdEntita": 650,
                  "IdGestione": 549,
                  "IdGruppo": 0,
                  "NomeAgenzia": "Admintest pcasa",
                  "NomeGruppo": null,
                  "TipoGestione": 0,
                  "Cognome": "Porcu",
                  "Email": "",
                  "Email2": "",
                  "Fonte": "",
                  "IndirizzoComune": null,
                  "IndirizzoDescrizione": null,
                  "Nome": "Michele",
                  "Note": "",
                  "CompanyName": "",
                  "Settore": "",
                  "Tag": [],
                  "Telefoni": [
                    {
                      "Numero": "3479222539",
                      "Tipo": 3
                    }
                  ],
                  "TipoCliente": 1
                },
                {
                  "ControlCode": null,
                  "DataInserimento": "/Date(1500908600000+0200)/",
                  "DataModifica": "/Date(1500908600000+0200)/",
                  "Descrizione": "Donatella Melis",
                  "IdAgenzia": 0,
                  "IdEntita": 649,
                  "IdGestione": 549,
                  "IdGruppo": 0,
                  "NomeAgenzia": "Admintest pcasa",
                  "NomeGruppo": null,
                  "TipoGestione": 0,
                  "Cognome": "Melis",
                  "Email": "",
                  "Email2": "",
                  "Fonte": "",
                  "IndirizzoComune": null,
                  "IndirizzoDescrizione": null,
                  "Nome": "Donatella",
                  "Note": "",
                  "CompanyName": "",
                  "Settore": "",
                  "Tag": [],
                  "Telefoni": [],
                  "TipoCliente": 1
                },
              ],
              "Total": 2
            };

            spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock200Response));//.and.callThrough();

            let queryObject = new QueryObjectDTO();
            queryObject.Paging.Start = 0;
            queryObject.Paging.Limit = 15;

            service.readAll(queryObject)
              .subscribe(response => {/*
              expect(response.ResCode).toBe(200);
              expect(response.Total).toBe(2);
              expect(response.ResMessage).toBe("Ok");*/
              });

            expect(dataService.getAll).toHaveBeenCalled();
          })
      )
    );

    it('should should handle 400 - Bad Request response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock400Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 400,
              "ResMessage": "Bad Request",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            }

            spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock400Response));//.and.callThrough();

            let queryObject = new QueryObjectDTO();
            queryObject.Paging.Start = 0;
            queryObject.Paging.Limit = 15;

            service.readAll(queryObject)
              .subscribe(response => {/*
            expect(response.ResCode).toBe(400);
            expect(response.ResMessage).toBe('Bad Request');
            expect(response.ResData).toBeFalsy();*/
              });

            expect(dataService.getAll).toHaveBeenCalled();
          })
      )
    );

    it('should should handle 401 - Unauthorized response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock401Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 401,
              "ResMessage": "Not logged",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            }

            spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock401Response));//.and.callThrough();

            let queryObject = new QueryObjectDTO();
            queryObject.Paging.Start = 0;
            queryObject.Paging.Limit = 15;

            service.readAll(queryObject)
              .subscribe(response => {/*
              expect(response.ResCode).toBe(401);
              expect(response.ResMessage).toBe('Not logged');
              expect(response.ResData).toBeFalsy();*/
              });

            expect(dataService.getAll).toHaveBeenCalled();
          })
      )
    );
  });

  describe('readById method', () => {
    it('should exists',
      inject([CustomerService], (service: CustomerService) => {
        expect(service.readById).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([CustomerService], (service: CustomerService) => {
        expect(function () { service.readById(null); }).toThrowError();
      })
    );

    it('should 200 - Ok response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock200Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9292
              },
              "ResCode": 200,
              "ResMessage": "Ok",
              "SessionChanged": false,
              "ResData": {
                "ControlCode": "-65913605",
                "DataInserimento": "\/Date(1501066960000+0200)\/",
                "DataModifica": "\/Date(1501066960000+0200)\/",
                "Descrizione": null,
                "IdAgenzia": 0,
                "IdEntita": 4516217,
                "IdGestione": 1041,
                "IdGruppo": 0,
                "NomeAgenzia": "",
                "NomeGruppo": null,
                "TipoGestione": 0,
                "AltriRecapiti": "",
                "AutoritaLuogoRilascio": "",
                "CodiceFiscale": "",
                "CodicePadre": 0,
                "Cognome": "Fuori",
                "DataRilascioDocumento": null,
                "DataScadenzaDocumento": null,
                "EMail": "",
                "Editore": null,
                "Editori": [{
                  "Descrizione": "Ufficio",
                  "IdEntita": 339
                }, {
                  "Descrizione": "Multiinvio",
                  "IdEntita": 491
                }, {
                  "Descrizione": "PrendiCasa",
                  "IdEntita": 520
                }, {
                  "Descrizione": "prova",
                  "IdEntita": 526
                }, {
                  "Descrizione": "Lamiacasa.it",
                  "IdEntita": 543
                }, {
                  "Descrizione": "Vetrina",
                  "IdEntita": 694
                }, {
                  "Descrizione": "Brik&Brac",
                  "IdEntita": 895
                }, {
                  "Descrizione": "Porta Portese",
                  "IdEntita": 1032
                }, {
                  "Descrizione": "gazzetta di todi",
                  "IdEntita": 1043
                }, {
                  "Descrizione": "CentoVani",
                  "IdEntita": 1185
                }, {
                  "Descrizione": "attico nuovo",
                  "IdEntita": 1310
                }, {
                  "Descrizione": "vetrina piu'",
                  "IdEntita": 1320
                }, {
                  "Descrizione": "TerzaMano",
                  "IdEntita": 1461
                }, {
                  "Descrizione": "portale web",
                  "IdEntita": 1519
                }, {
                  "Descrizione": "porta portese 2",
                  "IdEntita": 1548
                }, {
                  "Descrizione": "Attico Torino",
                  "IdEntita": 1641
                }, {
                  "Descrizione": "digimagazine",
                  "IdEntita": 1770
                }, {
                  "Descrizione": "Flavia Giordano",
                  "IdEntita": 1809
                }, {
                  "Descrizione": "Ufficio 40",
                  "IdEntita": 1941
                }, {
                  "Descrizione": "Cartello1",
                  "IdEntita": 1960
                }, {
                  "Descrizione": "true",
                  "IdEntita": 2005
                }, {
                  "Descrizione": "Portale Casa.it",
                  "IdEntita": 2209
                }, {
                  "Descrizione": "La casa in Pisa",
                  "IdEntita": 2211
                }, {
                  "Descrizione": "Test Elisabetta",
                  "IdEntita": 2225
                }, {
                  "Descrizione": "test Maria",
                  "IdEntita": 2226
                }, {
                  "Descrizione": "Portinaio Mario Rossi",
                  "IdEntita": 2290
                }, {
                  "Descrizione": "Il Piccolo",
                  "IdEntita": 2292
                }, {
                  "Descrizione": "Cerca e Trova",
                  "IdEntita": 2325
                }, {
                  "Descrizione": "Portiere",
                  "IdEntita": 2348
                }, {
                  "Descrizione": "Gabetti.it",
                  "IdEntita": 2353
                }, {
                  "Descrizione": "Il mattone",
                  "IdEntita": 2360
                }, {
                  "Descrizione": "ebay",
                  "IdEntita": 2365
                }, {
                  "Descrizione": "Giornale di Sicilia",
                  "IdEntita": 2395
                }, {
                  "Descrizione": "Mattino",
                  "IdEntita": 2468
                }, {
                  "Descrizione": "Altopiano Affari",
                  "IdEntita": 2469
                }, {
                  "Descrizione": "La voce dell'Ionio",
                  "IdEntita": 2657
                }, {
                  "Descrizione": "lasuacasa.it",
                  "IdEntita": 2658
                }, {
                  "Descrizione": "nuovo editore di test",
                  "IdEntita": 2684
                }, {
                  "Descrizione": "Mercatino di Torino",
                  "IdEntita": 2715
                }, {
                  "Descrizione": "La Nazione",
                  "IdEntita": 2716
                }, {
                  "Descrizione": "Gazzetta di Abbiategrasso",
                  "IdEntita": 2735
                }, {
                  "Descrizione": "WikiCasa",
                  "IdEntita": 2276
                }, {
                  "Descrizione": "Pocket",
                  "IdEntita": 2916
                }, {
                  "Descrizione": "Siti internet",
                  "IdEntita": 2939
                }, {
                  "Descrizione": "Volantino pubblicitario",
                  "IdEntita": 2940
                }, {
                  "Descrizione": "Baratto",
                  "IdEntita": 2967
                }],
                "Email2": "",
                "EsenteAUI": false,
                "Indirizzo": {
                  "ControlCode": "1919907",
                  "DataInserimento": "\/Date(-2208992400000+0100)\/",
                  "DataModifica": "\/Date(-2208992400000+0100)\/",
                  "Descrizione": "Via Tagliamento",
                  "IdAgenzia": 0,
                  "IdEntita": 5179286,
                  "IdGestione": 0,
                  "IdGruppo": 0,
                  "NomeAgenzia": null,
                  "NomeGruppo": null,
                  "TipoGestione": 0,
                  "Cap": "00198",
                  "Civico": "20",
                  "IdCitta": null,
                  "IdComune": 20769,
                  "Latitudine": 41.919502900000000,
                  "Longitudine": 12.502480200000036
                },
                "NascitaData": "\/Date(-2208992400000+0100)\/",
                "NascitaLuogo": "",
                "NascitaProvincia": "",
                "Nome": "Tirami",
                "Note": "",
                "NumeroDocumentoPresentato": "",
                "PIva": "",
                "RagSociale": "",
                "Referenti": [],
                "Ruolo": null,
                "Settore": null,
                "SitoInternet": "",
                "SkypeID": "",
                "Stato": 1,
                "StatoCivile": 3,
                "TagCliente": [],
                "Telefoni": [{
                  "Numero": "3482742087",
                  "Tipo": 1
                }],
                "TipoCanalePreferito": 1,
                "TipoCliente": 1,
                "TipoConsenso": 96,
                "TipoDocumentoPresentato": 0,
                "TipoImpiego": 4,
                "TipoSesso": 2,
                "TipoTitolo": 8
              }
            };

            spyOn(dataService, 'getById').and.returnValue(Observable.of(mock200Response));//.and.callThrough();

            service.readById(1)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              });

            expect(dataService.getById).toHaveBeenCalled();
          })
      )
    );

    it('should 400 - Bad Request response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock400Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 400,
              "ResMessage": "Bad Request",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            };

            spyOn(dataService, 'getById').and.returnValue(Observable.of(mock400Response));//.and.callThrough();

            service.readById(1)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              });

            expect(dataService.getById).toHaveBeenCalled();
          })
      )
    );

    it('should 401 - Unauthorized response',
      async(
        inject([
          AbstractCustomerDataService,
          CustomerService
        ], (
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            const mock401Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 401,
              "ResMessage": "Not logged",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            }

            spyOn(dataService, 'getById').and.returnValue(Observable.of(mock401Response));//.and.callThrough();

            service.readById(1)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              });

            expect(dataService.getById).toHaveBeenCalled();
          })
      )
    );

  });

  describe('save method', () => {
    it('should exists',
      inject([CustomerService], (service: CustomerService) => {
        expect(service.save).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([CustomerService], (service: CustomerService) => {
        expect(function () { service.save(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          SessionService,
          AbstractCustomerDataService,
          CustomerService
        ], (
          sessionService: SessionService,
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            let mockCustomerModel: CustomerModel = {
              "Address": {
                "ID": 4097285,
                "IdGestione": 0,
                "ControlCode": "-1659753103",
                "Municipality": {
                  "ID": 17671,
                  "Name": "Milano (MI)"
                },
                "PostalCode": "20125",
                "CivicNumber": "35",
                "Description": "viale monte nero",
                "IdGruppo": 0,
                "IdCity": null,
                "Latitude": 45.45675929999999,
                "Longitude": 9.204935299999988,
                "TipoGestione": 0
              },
              "ID": 852,
              "ControlCode": "408015840",
              "IdGestione": 2152,
              "AltriRecapiti": "",
              "AutoritaLuogoRilascio": "MCTCT",
              "CodiceFiscale": "",
              "CodicePadre": 0,
              "Nome": "Ruggero",
              "Cognome": "Leoncavallo",
              "DataRilascioDocumento": new Date("2015-10-03T16:00:00.000Z"),
              "DataScadenzaDocumento": new Date("2025-02-09T12:00:00.000Z"),
              "Editore": null,
              "EMail": "hjkghjk@hjgfhf.it",
              "EMail2": "",
              "EsenteAUI": false,
              "NascitaData": new Date("1899-12-30T21:00:00.000Z"),
              "NascitaLuogo": "",
              "NascitaProvincia": "",
              "Note": "fdsgdfsg",
              "NumeroDocumentoPresentato": "CT6546456",
              "PIva": "",
              "Referenti": [],
              "Ruolo": null,
              "SitoInternet": "",
              "SkypeID": "",
              "Stato": 1,
              "StatoCivile": 1,
              "TagCliente": [],
              "PhoneNumbers": [{
                "Number": "3335896485122",
                "Type": 1
              }, {
                "Number": "3335896485122",
                "Type": 3
              }],
              "TipoCanalePreferito": 1,
              "TipoCliente": 1,
              "TipoDocumentoPresentato": 2,
              "TipoSesso": 2,
              "TipoTitolo": 1,
              "PrivacyBitmask": 64,
              "PrivacySettings": {
                "Network": false,
                "Gruppo": false,
                "Marketing": false,
                "WebMarketing": false,
                "Telefonico": false,
                "Pubblicitario": false,
                "RicercheMercato": true,
                "GPScomunicazionealnetworkgabetti": false,
                "GPSfinalitadimarketing": false,
                "GPSTrattamentoDati": false,
                "GPSricezioneemail": false
              }
            };
            const mock200Response = {
              "ControlCode": "-2125772688",
              "DataInserimento": "\/Date(1514449970000+0100)\/",
              "DataModifica": "\/Date(1515751078025+0100)\/",
              "Descrizione": null,
              "IdAgenzia": 0,
              "IdEntita": 852,
              "IdGestione": 2152,
              "IdGruppo": 0,
              "NomeAgenzia": "Riccardo Promotore Carachino Promotore",
              "NomeGruppo": null,
              "TipoGestione": 0,
              "AltriRecapiti": "",
              "AutoritaLuogoRilascio": "MCTCT",
              "CodiceFiscale": "",
              "CodicePadre": 0,
              "Cognome": "Leoncavallo",
              "DataRilascioDocumento": "\/Date(1443902400000)\/",
              "DataScadenzaDocumento": "\/Date(1739109600000)\/",
              "EMail": "hjkghjk@hjgfhf.it",
              "Editore": null,
              "Editori": null,
              "Email2": "",
              "EsenteAUI": false,
              "Indirizzo": {
                "ControlCode": "-1659753103",
                "DataInserimento": "\/Date(-2208992400000+0100)\/",
                "DataModifica": "\/Date(-2208992400000+0100)\/",
                "Descrizione": "viale monte nero",
                "IdAgenzia": 0,
                "IdEntita": 4097285,
                "IdGestione": 0,
                "IdGruppo": 0,
                "NomeAgenzia": null,
                "NomeGruppo": null,
                "TipoGestione": 0,
                "Cap": "20125",
                "Civico": "35",
                "IdCitta": null,
                "IdComune": 17671,
                "Latitudine": 45.45675929999999,
                "Longitudine": 9.204935299999988
              },
              "NascitaData": "\/Date(-2209078800000)\/",
              "NascitaLuogo": "",
              "NascitaProvincia": "",
              "Nome": "Ruggero",
              "Note": "fdsgdfsg",
              "NumeroDocumentoPresentato": "CT6546456",
              "PIva": "",
              "RagSociale": "",
              "Referenti": null,
              "Ruolo": null,
              "Settore": null,
              "SitoInternet": "",
              "SkypeID": "",
              "Stato": 1,
              "StatoCivile": 1,
              "TagCliente": [],
              "Telefoni": [{
                "Numero": "3335896485122",
                "Tipo": 1
              }, {
                "Numero": "3335896485122",
                "Tipo": 3
              }],
              "TipoCanalePreferito": 1,
              "TipoCliente": 1,
              "TipoConsenso": 64,
              "TipoDocumentoPresentato": 2,
              "TipoImpiego": 1,
              "TipoSesso": 2,
              "TipoTitolo": 1
            };

            spyOn(sessionService, 'getSession').and.returnValue({ IdGestione: 1 });

            spyOn(dataService, 'save').and.returnValue(Observable.of(mock200Response));

            service.save(mockCustomerModel)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              });

            expect(dataService.save).toHaveBeenCalled();
          })
      )
    );

    it('should handle 400 - Bad Request response',
      async(
        inject([
          SessionService,
          AbstractCustomerDataService,
          CustomerService
        ], (
          sessionService: SessionService,
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            let mockCustomerModel: CustomerModel = {
              "Address": {
                "ID": 4097285,
                "IdGestione": 0,
                "ControlCode": "-1659753103",
                "Municipality": {
                  "ID": 17671,
                  "Name": "Milano (MI)"
                },
                "PostalCode": "20125",
                "CivicNumber": "35",
                "Description": "viale monte nero",
                "IdGruppo": 0,
                "IdCity": null,
                "Latitude": 45.45675929999999,
                "Longitude": 9.204935299999988,
                "TipoGestione": 0
              },
              "ID": 852,
              "ControlCode": "408015840",
              "IdGestione": 2152,
              "AltriRecapiti": "",
              "AutoritaLuogoRilascio": "MCTCT",
              "CodiceFiscale": "",
              "CodicePadre": 0,
              "Nome": "Ruggero",
              "Cognome": "Leoncavallo",
              "DataRilascioDocumento": new Date("2015-10-03T16:00:00.000Z"),
              "DataScadenzaDocumento": new Date("2025-02-09T12:00:00.000Z"),
              "Editore": null,
              "EMail": "hjkghjk@hjgfhf.it",
              "EMail2": "",
              "EsenteAUI": false,
              "NascitaData": new Date("1899-12-30T21:00:00.000Z"),
              "NascitaLuogo": "",
              "NascitaProvincia": "",
              "Note": "fdsgdfsg",
              "NumeroDocumentoPresentato": "CT6546456",
              "PIva": "",
              "Referenti": [],
              "Ruolo": null,
              "SitoInternet": "",
              "SkypeID": "",
              "Stato": 1,
              "StatoCivile": 1,
              "TagCliente": [],
              "PhoneNumbers": [{
                "Number": "3335896485122",
                "Type": 1
              }, {
                "Number": "3335896485122",
                "Type": 3
              }],
              "TipoCanalePreferito": 1,
              "TipoCliente": 1,
              "TipoDocumentoPresentato": 2,
              "TipoSesso": 2,
              "TipoTitolo": 1,
              "PrivacyBitmask": 64,
              "PrivacySettings": {
                "Network": false,
                "Gruppo": false,
                "Marketing": false,
                "WebMarketing": false,
                "Telefonico": false,
                "Pubblicitario": false,
                "RicercheMercato": true,
                "GPScomunicazionealnetworkgabetti": false,
                "GPSfinalitadimarketing": false,
                "GPSTrattamentoDati": false,
                "GPSricezioneemail": false
              }
            };
            const mock400Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 400,
              "ResMessage": "Bad Request",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            };

            spyOn(sessionService, 'getSession').and.returnValue({ IdGestione: 1 });

            spyOn(dataService, 'save').and.returnValue(Observable.of(mock400Response));

            service.save(mockCustomerModel)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              });

            expect(dataService.save).toHaveBeenCalled();
          })
      )
    );

    it('should handle 401 - Unauthorized response',
      async(
        inject([
          SessionService,
          AbstractCustomerDataService,
          CustomerService
        ], (
          sessionService: SessionService,
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            let mockCustomerModel: CustomerModel = {
              "Address": {
                "ID": 4097285,
                "IdGestione": 0,
                "ControlCode": "-1659753103",
                "Municipality": {
                  "ID": 17671,
                  "Name": "Milano (MI)"
                },
                "PostalCode": "20125",
                "CivicNumber": "35",
                "Description": "viale monte nero",
                "IdGruppo": 0,
                "IdCity": null,
                "Latitude": 45.45675929999999,
                "Longitude": 9.204935299999988,
                "TipoGestione": 0
              },
              "ID": 852,
              "ControlCode": "408015840",
              "IdGestione": 2152,
              "AltriRecapiti": "",
              "AutoritaLuogoRilascio": "MCTCT",
              "CodiceFiscale": "",
              "CodicePadre": 0,
              "Nome": "Ruggero",
              "Cognome": "Leoncavallo",
              "DataRilascioDocumento": new Date("2015-10-03T16:00:00.000Z"),
              "DataScadenzaDocumento": new Date("2025-02-09T12:00:00.000Z"),
              "Editore": null,
              "EMail": "hjkghjk@hjgfhf.it",
              "EMail2": "",
              "EsenteAUI": false,
              "NascitaData": new Date("1899-12-30T21:00:00.000Z"),
              "NascitaLuogo": "",
              "NascitaProvincia": "",
              "Note": "fdsgdfsg",
              "NumeroDocumentoPresentato": "CT6546456",
              "PIva": "",
              "Referenti": [],
              "Ruolo": null,
              "SitoInternet": "",
              "SkypeID": "",
              "Stato": 1,
              "StatoCivile": 1,
              "TagCliente": [],
              "PhoneNumbers": [{
                "Number": "3335896485122",
                "Type": 1
              }, {
                "Number": "3335896485122",
                "Type": 3
              }],
              "TipoCanalePreferito": 1,
              "TipoCliente": 1,
              "TipoDocumentoPresentato": 2,
              "TipoSesso": 2,
              "TipoTitolo": 1,
              "PrivacyBitmask": 64,
              "PrivacySettings": {
                "Network": false,
                "Gruppo": false,
                "Marketing": false,
                "WebMarketing": false,
                "Telefonico": false,
                "Pubblicitario": false,
                "RicercheMercato": true,
                "GPScomunicazionealnetworkgabetti": false,
                "GPSfinalitadimarketing": false,
                "GPSTrattamentoDati": false,
                "GPSricezioneemail": false
              }
            };
            const mock401Response = {
              "ControlCode": "",
              "ForceRelogin": true,
              "IdIdentita": 0,
              "MobileVersion": {
                "_Build": 2,
                "_Major": 1,
                "_Minor": 0,
                "_Revision": 9291
              },
              "ResCode": 401,
              "ResMessage": "Not logged",
              "SessionChanged": false,
              "ResData": null,
              "Total": 0
            }

            spyOn(sessionService, 'getSession').and.returnValue({ IdGestione: 1 });

            spyOn(dataService, 'save').and.returnValue(Observable.of(mock401Response));

            service.save(mockCustomerModel)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              },
                (err: string) => {
                  expect(err).toBeTruthy();
                  //expect(err).toBe('Server error');
                });

            expect(dataService.save).toHaveBeenCalled();
          })
      )
    );

    it('should handle 500 - Server error response',
      async(
        inject([
          SessionService,
          AbstractCustomerDataService,
          CustomerService
        ], (
          sessionService: SessionService,
          dataService: AbstractCustomerDataService,
          service: CustomerService) => {

            let mockCustomerModel: CustomerModel = {
              "Address": {
                "ID": 4097285,
                "IdGestione": 0,
                "ControlCode": "-1659753103",
                "Municipality": {
                  "ID": 17671,
                  "Name": "Milano (MI)"
                },
                "PostalCode": "20125",
                "CivicNumber": "35",
                "Description": "viale monte nero",
                "IdGruppo": 0,
                "IdCity": null,
                "Latitude": 45.45675929999999,
                "Longitude": 9.204935299999988,
                "TipoGestione": 0
              },
              "ID": 852,
              "ControlCode": "408015840",
              "IdGestione": 2152,
              "AltriRecapiti": "",
              "AutoritaLuogoRilascio": "MCTCT",
              "CodiceFiscale": "",
              "CodicePadre": 0,
              "Nome": "Ruggero",
              "Cognome": "Leoncavallo",
              "DataRilascioDocumento": new Date("2015-10-03T16:00:00.000Z"),
              "DataScadenzaDocumento": new Date("2025-02-09T12:00:00.000Z"),
              "Editore": null,
              "EMail": "hjkghjk@hjgfhf.it",
              "EMail2": "",
              "EsenteAUI": false,
              "NascitaData": new Date("1899-12-30T21:00:00.000Z"),
              "NascitaLuogo": "",
              "NascitaProvincia": "",
              "Note": "fdsgdfsg",
              "NumeroDocumentoPresentato": "CT6546456",
              "PIva": "",
              "Referenti": [],
              "Ruolo": null,
              "SitoInternet": "",
              "SkypeID": "",
              "Stato": 1,
              "StatoCivile": 1,
              "TagCliente": [],
              "PhoneNumbers": [{
                "Number": "3335896485122",
                "Type": 1
              }, {
                "Number": "3335896485122",
                "Type": 3
              }],
              "TipoCanalePreferito": 1,
              "TipoCliente": 1,
              "TipoDocumentoPresentato": 2,
              "TipoSesso": 2,
              "TipoTitolo": 1,
              "PrivacyBitmask": 64,
              "PrivacySettings": {
                "Network": false,
                "Gruppo": false,
                "Marketing": false,
                "WebMarketing": false,
                "Telefonico": false,
                "Pubblicitario": false,
                "RicercheMercato": true,
                "GPScomunicazionealnetworkgabetti": false,
                "GPSfinalitadimarketing": false,
                "GPSTrattamentoDati": false,
                "GPSricezioneemail": false
              }
            };
            const mock500Response = null;

            spyOn(sessionService, 'getSession').and.returnValue({ IdGestione: 1 });

            spyOn(dataService, 'save').and.returnValue(Observable.throw(new Error('Server error')));

            service.save(mockCustomerModel)
              .subscribe(response => {
                //expect(response.ResCode).toBe(403);
                //expect(response.Total).toBe(2);
                //expect(response.ResMessage).toBe("Ok");
              },
                (err: string) => {
                  expect(err).toBeTruthy();
                  //expect(err).toBe('Server error');
                });

            expect(dataService.save).toHaveBeenCalled();
          })
      )
    );
  });
  // /save

});
