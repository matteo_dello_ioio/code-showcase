import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig, MatTabChangeEvent } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { TipoTelefono } from 'treeplat-mobile-rest-client';
import { CustomerService } from '../customer.service';
import { CustomerModel } from '../models/customer.model';
import { Comune } from '../../models/comune';
import { LookupService } from '../../services/lookup.service';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/switchMap';
import 'rxjs/operators/share';
import { GeolocalizationComponent } from '../../geolocalization/components/geolocalization.component';
import { PhoneNumberModel } from '../../shared/models/phone-number-model';



@Component({
  selector: 'app-customer-detail',
  styles: [],
  templateUrl: './customer-detail.component.html'
})
export class CustomerDetailComponent implements OnInit, OnDestroy {

  @ViewChild(GeolocalizationComponent) geolocalViewChild: GeolocalizationComponent;

  isNew = false;

  isClicked: boolean = false;

  lat: number = 51.678418;
  lng: number = 7.809007;
  customer$: Observable<CustomerModel>;
  customer: CustomerModel;
  selectedTab: number = 0;

  filteredCities: any;
  citiesCtrl: FormControl;

  //email = new FormControl('', Validators.email);

  phone1: string;
  phone2: string;
  mobilePhone: string;
  fax: string;

  private routeParamSubscription: any;

  constructor(
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private lookup: LookupService,
    private customerService: CustomerService
  ) {
    this.citiesCtrl = new FormControl();

    this.filteredCities = this.citiesCtrl.valueChanges
      .pipe(
      startWith({} as Comune),
      //map(city => (city && typeof city === 'object') ? (city as any).P : city),
      map(name => name && typeof name === 'string' ? this.searchCity(name) : []/*this.cities.slice()*/)
      );
  }

  ngOnInit() {
    this.isNew = this.route.snapshot.data.isNew;
    if (!this.isNew) {
      this.routeParamSubscription = this.route.params.subscribe(params => {
        let id = +params['id']; // (+) converts string 'id' to a number
        this.getCustomerById(id);
      });
    } else {
      this.customer = new CustomerModel();
    }
  }

  ngOnDestroy() {
    if (this.routeParamSubscription) {
      this.routeParamSubscription.unsubscribe();
    }
  }

  goback() {
    this.location.back();
  }

  getErrorMessage() {
    //return this.email.hasError('email') ? 'Email non valida' : '';
  }

  openSnackBar(message: string, action?: string, stat?: string) {
    let config = new MatSnackBarConfig();
    config.duration = 2000;
    if (stat) config.extraClasses = [stat];
    this.snackBar.open(message, action, config);
  }

  goToLocalizeTab() {
    this.selectedTab = 1;
    window.scrollTo(document.body.scrollHeight, 0);
  }

  tabChanged(event: MatTabChangeEvent) {
    switch (event.index) {
      case 1:
        {
          //console.log("CLICK LOCALIZZAZIONE");
          //this.geolocalViewChild.change(this.customer);

          // Workaround for Google Maps glitch render
          var resizeEvent = new Event('resize');
          window.dispatchEvent(resizeEvent);

          this.geolocalViewChild.description = this.customer.Address.Description;
          this.geolocalViewChild.civicNumber = this.customer.Address.CivicNumber;
          if (this.customer.Address.Municipality)
            this.geolocalViewChild.municipality = this.customer.Address.Municipality.Name;
          this.geolocalViewChild.latitude = this.customer.Address.Latitude;
          this.geolocalViewChild.longitude = this.customer.Address.Longitude;

          this.geolocalViewChild.updateAddress();
          break;
        }
    }
  }

  selectedIndexChange(val) {
    this.selectedTab = val;
  }

  getCustomerById(id: number) {
    this.customer$ = this.customerService.readById(id);
    this.customer$.subscribe((customer) => {
      console.log(customer);
      this.customer = customer;

      if (customer.PhoneNumbers) {

        let pn1 = customer.PhoneNumbers.find(pn => { return pn.Type == TipoTelefono.Telefono; });
        if (pn1) {
          this.phone1 = pn1.Number;
        }

        let pn2 = customer.PhoneNumbers.find(pn => { return pn.Type == TipoTelefono.Telefono2; });
        if (pn2) {
          this.phone2 = pn2.Number;
        }

        let pn3 = customer.PhoneNumbers.find(pn => { return pn.Type == TipoTelefono.Telefono2; });
        if (pn3) {
          this.mobilePhone = pn3.Number;
        }

        let pn4 = customer.PhoneNumbers.find(pn => { return pn.Type == TipoTelefono.Telefono2; });
        if (pn4) {
          this.fax = pn4.Number;
        }
      }

    });
  }

  /**
   * event handler for geolocalization component
   * @param event has this structure { latitude: number, longitude: number }
   */
  onCoordinatesChange(event) {
    this.customer.Address.Latitude = event.latitude;
    this.customer.Address.Longitude = event.longitude;
  }

  searchCity(name: string): Comune[] {
    return this.lookup.findCityByName(name);
  }

  onSave(customerForm: NgForm) {

    if (!customerForm.valid)
      return;

    this.isClicked = true

    console.log('saving data...' + JSON.stringify(this.customer));

    this.customer.PhoneNumbers = new Array<PhoneNumberModel>();

    if (this.phone1) {
      let pn = new PhoneNumberModel();
      pn.Number = this.phone1;
      pn.Type = TipoTelefono.Telefono;
      this.customer.PhoneNumbers.push(pn);
    }

    if (this.phone2) {
      let pn = new PhoneNumberModel();
      pn.Number = this.phone2;
      pn.Type = TipoTelefono.Telefono2;
      this.customer.PhoneNumbers.push(pn);
    }

    if (this.mobilePhone) {
      let pn = new PhoneNumberModel();
      pn.Number = this.mobilePhone;
      pn.Type = TipoTelefono.Cellulare;
      this.customer.PhoneNumbers.push(pn);
    }

    if (this.fax) {
      let pn = new PhoneNumberModel();
      pn.Number = this.fax;
      pn.Type = TipoTelefono.Fax;
      this.customer.PhoneNumbers.push(pn);
    }


    this.customerService.save(this.customer)
      .subscribe(result => {
        console.log(result);
        this.isClicked = false;

        this.customer = result;

        this.openSnackBar('Salvato con successo', undefined, 'success');

        this.router.navigateByUrl('app/customers/lista/' + JSON.stringify(result.ID));
      },
      (err: string) => {
        this.isClicked = false;
        this.openSnackBar(err, undefined, 'error');
      });
  }

  displayFn(obj: any) {
    if (obj)
      return obj.Name;
  }


  /**
   * If some address field is populated, all address fields became required.
   */
  addressFieldsRequired(): boolean {
    return (
      Boolean(this.customer.Address.Description)
      ||
      Boolean(this.customer.Address.CivicNumber)
      ||
      Boolean(this.customer.Address.PostalCode)
      ||
      Boolean(this.customer.Address.Municipality)
    );
  }


  papersFieldRequired(): boolean {
    return (
      Boolean(this.customer.TipoDocumentoPresentato)
      ||
      Boolean(this.customer.NumeroDocumentoPresentato)
      ||
      Boolean(this.customer.DataRilascioDocumento)
      ||
      Boolean(this.customer.DataScadenzaDocumento)
      ||
      Boolean(this.customer.AutoritaLuogoRilascio)
    );
  }

}