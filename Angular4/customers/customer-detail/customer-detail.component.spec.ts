import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomerDetailComponent } from './customer-detail.component';
import { LookupService } from '../../services/lookup.service';
import { CustomerService } from '../customer.service';
import { GeolocalizationModule } from '../../geolocalization/geolocalization.module';
import { NumberOnlyDirective } from '../../custom-directives/number-only.directive';
import { CustomDirectivesModule } from '../../custom-directives/custom-directives.module';
import { inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { CustomerModel } from '../models/customer.model';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AbstractMapService } from '../../geolocalization/services/map-service.abstract';
import { QueryObjectDTO } from 'treeplat-mobile-rest-client';

class MockLookupService {

}

let mockCustomerService = {
  readAll() { },
  readAllTypeAhead() { },
  readById() { },
  save() { },
}

let mockMapService = {
  //getCurrentPosition(): Observable<Coordinates> { },

  getCurrentPosition() { },
  geocodeByAddress(address: string) { }
}

const loaderServiceStub = {
  load: () => Promise.resolve()
};

describe('CustomerDetailComponent', () => {
  let component: CustomerDetailComponent;
  let fixture: ComponentFixture<CustomerDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerDetailComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MaterialModule,
        RouterTestingModule,
        GeolocalizationModule,
        CustomDirectivesModule,
      ],
      providers: [
        MapsAPILoader,
        //LookupService,
        { provide: LookupService, useClass: MockLookupService },
        { provide: CustomerService, useValue: mockCustomerService },
        NumberOnlyDirective,

        { provide: AbstractMapService, useValue: mockMapService },
        { provide: MapsAPILoader, useValue: loaderServiceStub }
      ],
    });


    spyOn(mockCustomerService, 'readById').and.returnValue(Observable.of(new CustomerModel()));//.and.callThrough();

    fixture = TestBed.createComponent(CustomerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should handle 200 - Ok response',
  //   async(
  //     inject([
  //       CustomerService
  //     ], (
  //       service: CustomerService
  //     ) => {

  //         const mock200Response = [
  //           {
  //             "CodiceRiferimento": "mk-I",
  //             "Descrizione": "Appartamento Milano Viale Ortles, 42 - Vendita: 300.000,00 - 120 Mq",
  //             "IdEntita": 213,
  //             "Stato": 2
  //           },
  //           {
  //             "CodiceRiferimento": "mk-II",
  //             "Descrizione": "Appartamento Milano Via Marco D'Agrate, 11 - Vendita: 270.000,00 - 115 Mq",
  //             "IdEntita": 214,
  //             "Stato": 2
  //           },
  //           {
  //             "CodiceRiferimento": "mk-III",
  //             "Descrizione": "Appartamento Milano Via Broni, 1 - Locazione (mensile): 780,00 - 55 Mq",
  //             "IdEntita": 215,
  //             "Stato": 2
  //           },
  //           {
  //             "CodiceRiferimento": "mk-IV",
  //             "Descrizione": "Appartamento Milano Via Cortina D'Ampezzo, 10 - Vendita: 530.000,00Locazione (mensile): 5.000,00 - 1200 Mq",
  //             "IdEntita": 217,
  //             "Stato": 2
  //           },
  //           {
  //             "CodiceRiferimento": "34ec83e6-5761-4126-b",
  //             "Descrizione": "Appartamento Milano via crema, 8 - Vendita: 250.000,00 - 60 Mq",
  //             "IdEntita": 221,
  //             "Stato": 1
  //           },
  //           {
  //             "CodiceRiferimento": "5a8bd99e-1dba-4adf-b",
  //             "Descrizione": "Appartamento Milano viale regina margherita, 2 - Vendita: 350.000,00 - 70 Mq",
  //             "IdEntita": 224,
  //             "Stato": 1
  //           }
  //         ];


  //         spyOn(service, 'readById').and.returnValue(Observable.of(mock200Response));//.and.callThrough();

  //         let search = "viale monte nero";

  //         service.readAllRealEstatesTypeAhead(search)
  //           .subscribe(response => {
  //             expect(response.length).toBe(6);
  //             expect(response[0].ID).toBe(213);
  //           });

  //         expect(dataService.readAllRealEstatesTypeAhead).toHaveBeenCalled();
  //       })
  //   )
  // );

});