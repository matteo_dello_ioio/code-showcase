
import { CustomerItem } from './models/customer-item';

export interface CustomersSorterInterface {
    sortByDenominationASC(customers: CustomerItem[]): CustomerItem[];
    sortByInsertDateASC(customers: CustomerItem[]): CustomerItem[];
}
