
 import { TestBed, inject } from '@angular/core/testing';
 import { CustomersSorterService } from './customers-sorter.service';
 import { CustomerItem } from './models/customer-item';

 describe('CustomersSorterService', () => {

   beforeEach(() => {
     TestBed.configureTestingModule({
       providers: [CustomersSorterService]
     });
   });

   it('should be created', inject([CustomersSorterService], (service: CustomersSorterService) => {
    expect(service).toBeTruthy();
   }));

   describe('customerSortAsc', () => {

     it('must return -1 for A, B', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortAsc(c1, c2);
       expect(result).toBe(-1);
     }));

     it('must return 1 for B, A', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortAsc(c1, c2);
       expect(result).toBe(1);
     }));

     it('must return 0 for A, A', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortAsc(c1, c2);
       expect(result).toBe(0);
     }));
   });


   describe('customerSortDesc', () => {
     it('must return 1 for A, B', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortDesc(c1, c2);
       expect(result).toBe(1);
     }));

     it('must return -1 for B, A', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortDesc(c1, c2);
       expect(result).toBe(-1);
     }));

     it('must return 0 for A, A', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var c1: CustomerItem;
       var c2: CustomerItem;
       c1 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };
       c2 = { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null };

       var result = service.customerSortDesc(c1, c2);
       expect(result).toBe(0);
     }));
   });

   describe('sortByDenominationASC', () => {

     it('should exists', inject([CustomersSorterService], (service: CustomersSorterService) => {
       expect(service.sortByDenominationASC).toBeTruthy();
     }));

     it('should sort ascendent', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var customerList: CustomerItem[];
       customerList = [
         { ID: null, Denominazione: 'C', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
         { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
         { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
       ];

       var result = service.sortByDenominationASC(customerList);

       expect(result[0].Denominazione).toBe('A');
       expect(result[1].Denominazione).toBe('B');
       expect(result[2].Denominazione).toBe('C');
     }));

   });

   describe('sortByDenominationDESC', () => {

     it('should exists', inject([CustomersSorterService], (service: CustomersSorterService) => {
       expect(service.sortByDenominationDESC).toBeTruthy();
     }));

     it('should sort descendant', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var customerList: CustomerItem[];
       customerList = [
         { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
         { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
         { ID: null, Denominazione: 'C', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: null },
       ];

       var result = service.sortByDenominationDESC(customerList);

       expect(result[0].Denominazione).toBe('C');
       expect(result[1].Denominazione).toBe('B');
       expect(result[2].Denominazione).toBe('A');
     }));

   });


   describe('sortByInsertDateASC', () => {

     it('should exists', inject([CustomersSorterService], (service: CustomersSorterService) => {
       expect(service.sortByInsertDateASC).toBeTruthy();
     }));

     it('should sort ascendent', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var customerList: CustomerItem[];
       customerList = [
         { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 2, 1) },
         { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 1, 1) },
         { ID: null, Denominazione: 'C', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 3, 1) },
       ];

       var result = service.sortByInsertDateASC(customerList);

       expect(result[0].Denominazione).toBe('A');
       expect(result[1].Denominazione).toBe('B');
       expect(result[2].Denominazione).toBe('C');
     }));

   });

   describe('sortByInsertDateDESC', () => {

     it('should exists', inject([CustomersSorterService], (service: CustomersSorterService) => {
       expect(service.sortByInsertDateDESC).toBeTruthy();
     }));

     it('should sort ascendent', inject([CustomersSorterService], (service: CustomersSorterService) => {
       var customerList: CustomerItem[];
       customerList = [
         { ID: null, Denominazione: 'B', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 2, 1) },
         { ID: null, Denominazione: 'A', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 1, 1) },
         { ID: null, Denominazione: 'C', Referente: null, Emails: null, PhoneNumbers: null, InsertDate: new Date(2012, 3, 1) },
       ];

       var result = service.sortByInsertDateDESC(customerList);

       expect(result[0].Denominazione).toBe('C');
       expect(result[1].Denominazione).toBe('B');
       expect(result[2].Denominazione).toBe('A');
     }));

   });

 });
