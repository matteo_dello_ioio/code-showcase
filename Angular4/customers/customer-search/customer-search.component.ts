import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
// import { APPCONFIG } from '../../config';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { CustomerSearch } from '../models/customer-search.interface';

@Component({
  selector: 'customer-search',
  styles: [],
  templateUrl: './customer-search.component.html'
})

export class CustomerSearchComponent implements OnInit, OnDestroy {

  @Output() search: EventEmitter<CustomerSearch> = new EventEmitter<CustomerSearch>();

  searchParams: CustomerSearch = <CustomerSearch>{
    Description: null,
    PhoneNumber: null,
    Email: null,
    Type: null,
    WithActiveAssignment: false,
    WithOpenRequest: false
  }

  constructor() { }

  ngOnInit() { }

  ngOnDestroy() { }

  closeSearchOverlay() {
    let body = document.getElementById('body');
    body.classList.remove('overlay-active');
  }

  onSearch(customerSearchForm: NgForm) {
    
    console.log(customerSearchForm.value);

    //this.search.emit(this.searchParams);
    this.search.emit(customerSearchForm.value);

    this.closeSearchOverlay();
  }
}