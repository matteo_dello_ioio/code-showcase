
import { Observable } from 'rxjs/Observable';
import { CustomerModel } from './models/customer.model';
import { CustomerItem } from './models/customer-item';
import { QueryObjectDTO } from 'treeplat-mobile-rest-client';



export interface ICustomerService {
  readAll(queryObject: QueryObjectDTO): Observable<CustomerItem[]>
  readById(id: number): Observable<CustomerModel>;
  save(customer: CustomerModel): Observable<CustomerModel>;
}