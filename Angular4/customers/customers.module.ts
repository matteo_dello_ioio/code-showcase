import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module'
import { UtilityService } from '../shared/utility-service';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerSearchComponent } from './customer-search/customer-search.component';
import { AuthGuard } from '../services/auth-guard.service';
import { CustomerService } from './customer.service';
import { CustomersMapper } from './customers-mapper';

import { SessionModule } from '../session/session.module';
//import { AbstractCustomerDataService, CustomerDataService } from '../data-services';
import { AbstractCustomerDataService, CustomerDataService } from 'treeplat-mobile-rest-client';
import { AbstractStorageService } from '../session/storage-service.abstract';
import { SessionStorageService } from '../session/session-storage.service';
import { GeolocalizationModule } from '../geolocalization/geolocalization.module';
import { CustomDirectivesModule } from '../custom-directives/custom-directives.module';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
    imports: [
        MaterialModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        CommonModule,
        FormsModule,
        SessionModule,
        CustomersRoutingModule,
        GeolocalizationModule,
        CustomDirectivesModule,
        // InfiniteScrollModule
    ],
    declarations: [
        CustomerListComponent,
        CustomerDetailComponent,
        CustomerSearchComponent
    ],
    providers: [
        AuthGuard, 
        UtilityService, 
        { provide: AbstractStorageService, useClass: SessionStorageService },
        { provide: AbstractCustomerDataService, useClass: CustomerDataService }, 
        CustomersMapper, 
        CustomerService
    ]
})

export class CustomersModule { }