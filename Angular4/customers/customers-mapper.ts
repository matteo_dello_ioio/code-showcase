
import { Injectable } from '@angular/core';
import {
    TipoPrivacy,
    TipoTelefono,
    ClienteDTO,
    ClienteSaveRequest,
    ClienteCmd,
    IndirizzoDTO,
    ClienteReferenteCmd,
    IndirizzoCmd,
    ClienteElencoTypeAheadDTO
} from 'treeplat-mobile-rest-client';
import { UtilityService } from '../shared/utility-service'
import { AddressModel } from '../shared/models/address-model';
import { CustomerModel } from './models/customer.model';
import { CustomerItem } from './models/customer-item';
import { PhoneNumberModel } from '../shared/models/phone-number-model';
import { CommonsMapper } from '../shared/commons-mapper';
import { Comune } from '../models/comune';
import { GeoService } from '../services/geo.service';
import { CustomerPrivacySettingsModel } from './models/customer-privacy-settings.model';
import { CustomerMiniModel } from '../shared/models/customer-mini-model';



@Injectable()
export class CustomersMapper {

    constructor(
        private utility: UtilityService,
        private geoService: GeoService,
        private commonsMapper: CommonsMapper
    ) {

    }

    /**
     * 
     * @param dto 
     */
    mapClienteDTO2CustomerItem(dto: ClienteDTO): CustomerItem {
        var item = new CustomerItem();

        item.ID = +dto.IdEntita;

        if (!dto.RagioneSociale) {
            item.Denominazione = dto.Nome + ' ' + dto.Cognome;
        } else {
            item.Denominazione = dto.RagioneSociale;
            item.Referente = dto.Nome + ' ' + dto.Cognome;
        }

        item.Emails.push(dto.Email);

        if (dto.Telefoni) {
            item.PhoneNumbers = new Array<PhoneNumberModel>();
            dto.Telefoni.forEach((ref, i, array) => {
                let phone = this.commonsMapper.telefonoDTO2phoneNumberModel(ref);
                item.PhoneNumbers.push(phone);
            });
        }

        return item;
    }

    /**
     * 
     * @param dto map ClienteDTO -> CustomerModel
     */
    mapClienteDTO2CustomerModel(dto: ClienteDTO): CustomerModel {
        let model = new CustomerModel();
        //model.IdAgenzia = dto.IdAgenzia;
        //model.NomeAgenzia = dto.NomeAgenzia;
        //model.NomeGruppo = dto.NomeGruppo;
        //model.IdGruppo = dto.IdGruppo;
        //model.Settore = dto.Settore;
        //model.TipoImpiego = dto.TipoImpiego; NOT USED IN THIS APP
        //model.TipoGestione = dto.TipoGestione;
        //model.DataInserimento = this.utility._JSON2Date_(dto.DataInserimento);
        //model.DataModifica = this.utility._JSON2Date_(dto.DataModifica);
        //model.Descrizione = dto.Descrizione;
        //model.IndirizzoComune = dto.IndirizzoComune;
        //model.IndirizzoDescrizione = dto.IndirizzoDescrizione;

        model.ID = dto.IdEntita;
        model.ControlCode = dto.ControlCode;
        model.IdGestione = dto.IdGestione;
        model.AltriRecapiti = dto.AltriRecapiti;
        model.AutoritaLuogoRilascio = dto.AutoritaLuogoRilascio;
        model.CodiceFiscale = dto.CodiceFiscale;
        model.CodicePadre = dto.CodicePadre;
        model.Nome = dto.Nome;
        model.Cognome = dto.Cognome;
        model.DataRilascioDocumento = this.utility._JSON2Date_(dto.DataRilascioDocumento);
        model.DataScadenzaDocumento = this.utility._JSON2Date_(dto.DataScadenzaDocumento);
        model.Editore = dto.Editore;
        model.EMail = dto.EMail;
        model.EMail2 = dto.Email2;
        model.EsenteAUI = dto.EsenteAUI;
        model.Fonte = dto.Fonte;
        model.Address = new AddressModel();
        if (dto.Indirizzo) {
            model.Address.ID = dto.Indirizzo.IdEntita;
            model.Address.IdGestione = dto.Indirizzo.IdGestione;
            model.Address.ControlCode = dto.Indirizzo.ControlCode;
            //model.Address.Municipality = new Comune(dto.Indirizzo.IdComune);
            if (dto.Indirizzo.IdCitta)
                model.Address.Municipality = this.geoService.getComuneByIdComune(dto.Indirizzo.IdComune, dto.Indirizzo.IdCitta);
            else if (dto.Indirizzo.IdComune)
                model.Address.Municipality = this.geoService.getComuneByIdComune(dto.Indirizzo.IdComune);

            model.Address.PostalCode = dto.Indirizzo.Cap;
            model.Address.CivicNumber = dto.Indirizzo.Civico;
            model.Address.Description = dto.Indirizzo.Descrizione;
            model.Address.IdGruppo = dto.Indirizzo.IdGruppo;
            model.Address.IdCity = dto.Indirizzo.IdCitta;
            model.Address.Latitude = dto.Indirizzo.Latitudine;
            model.Address.Longitude = dto.Indirizzo.Longitudine;
            model.Address.TipoGestione = dto.Indirizzo.TipoGestione;
        }

        model.NascitaData = this.utility._JSON2Date_(dto.NascitaData);
        model.NascitaLuogo = dto.NascitaLuogo;
        model.NascitaProvincia = dto.NascitaProvincia;
        model.Note = dto.Note;
        model.NumeroDocumentoPresentato = dto.NumeroDocumentoPresentato;
        model.PIva = dto.PIva;
        model.CompanyName = dto.RagioneSociale;
        model.Referenti = dto.Referenti;
        model.Ruolo = dto.Ruolo;
        model.SitoInternet = dto.SitoInternet;
        model.SkypeID = dto.SkypeID;
        model.Stato = dto.Stato;
        model.StatoCivile = dto.StatoCivile;
        model.Tag = dto.Tag;
        model.TagCliente = dto.TagCliente;

        if (dto.Telefoni) {
            model.PhoneNumbers = new Array<PhoneNumberModel>();
            dto.Telefoni.forEach((ref, i, array) => {
                let pn = this.commonsMapper.telefonoDTO2phoneNumberModel(ref);
                model.PhoneNumbers.push(pn);
            });
        }

        model.TipoCanalePreferito = dto.TipoCanalePreferito;
        model.TipoCliente = dto.TipoCliente;
        model.TipoDocumentoPresentato = dto.TipoDocumentoPresentato;

        model.TipoSesso = dto.TipoSesso;
        model.TipoTitolo = dto.TipoTitolo;


        // *** PRIVACY ***
        model.PrivacyBitmask = dto.TipoConsenso;
        model.PrivacySettings = new CustomerPrivacySettingsModel();
        model.PrivacySettings.Network = (dto.TipoConsenso & TipoPrivacy.Network) != 0
        model.PrivacySettings.Gruppo = (dto.TipoConsenso & TipoPrivacy.Gruppo) != 0
        model.PrivacySettings.Marketing = (dto.TipoConsenso & TipoPrivacy.Marketing) != 0
        model.PrivacySettings.WebMarketing = (dto.TipoConsenso & TipoPrivacy.WebMarketing) != 0
        model.PrivacySettings.Telefonico = (dto.TipoConsenso & TipoPrivacy.Telefonico) != 0
        model.PrivacySettings.Pubblicitario = (dto.TipoConsenso & TipoPrivacy.Pubblicitario) != 0;
        model.PrivacySettings.RicercheMercato = (dto.TipoConsenso & TipoPrivacy.RicercheMercato) != 0;
        model.PrivacySettings.GPScomunicazionealnetworkgabetti = ((dto.TipoConsenso & TipoPrivacy.GPScomunicazionealnetworkgabetti) != 0);
        model.PrivacySettings.GPSfinalitadimarketing = ((dto.TipoConsenso & TipoPrivacy.GPSfinalitadimarketing) != 0);
        model.PrivacySettings.GPSTrattamentoDati = ((dto.TipoConsenso & TipoPrivacy.GPSTrattamentoDati) != 0);
        model.PrivacySettings.GPSricezioneemail = ((dto.TipoConsenso & TipoPrivacy.GPSricezioneemail) != 0);

        return model;
    }

    /**
     * map CustomerModel -> ClienteDTO
     * @param model 
     */
    mapCustomerModel2ClienteDTO(model: CustomerModel): ClienteDTO {

        //dto.IdGruppo = model.IdGruppo;
        //dto.NomeGruppo = model.NomeGruppo;
        //dto.IdAgenzia = model.IdAgenzia;
        //dto.NomeAgenzia = model.NomeAgenzia;
        // dto.DataInserimento = this.utility._Date2JSON_(model.DataInserimento);
        // dto.DataModifica = this.utility._Date2JSON_(model.DataModifica);
        //dto.Descrizione = model.Descrizione;
        //dto.Settore = model.Settore;
        //dto.TipoGestione = model.TipoGestione;
        //dto.TipoImpiego = model.TipoImpiego; NOT USED IN THIS APP
        //dto.IndirizzoComune = model.IndirizzoComune;

        let dto = new ClienteDTO();
        dto.IdEntita = model.ID;
        dto.ControlCode = model.ControlCode;
        dto.AltriRecapiti = model.AltriRecapiti;
        dto.AutoritaLuogoRilascio = model.AutoritaLuogoRilascio;
        dto.CodiceFiscale = model.CodiceFiscale;
        dto.CodicePadre = model.CodicePadre;
        dto.Cognome = model.Cognome;
        dto.DataRilascioDocumento = this.utility._Date2JSON_(model.DataRilascioDocumento);
        dto.DataScadenzaDocumento = this.utility._Date2JSON_(model.DataScadenzaDocumento);
        dto.Editore = model.Editore;
        dto.EMail = model.EMail;
        dto.Email2 = model.EMail2;
        dto.EsenteAUI = model.EsenteAUI;
        dto.Fonte = model.Fonte;
        dto.IdGestione = model.IdGestione;
        //dto.IndirizzoDescrizione = model.IndirizzoDescrizione;
        dto.NascitaData = this.utility._Date2JSON_(model.NascitaData);
        dto.NascitaLuogo = model.NascitaLuogo;
        dto.NascitaProvincia = model.NascitaProvincia;
        dto.Nome = model.Nome;
        dto.Note = model.Note;
        dto.NumeroDocumentoPresentato = model.NumeroDocumentoPresentato;
        dto.PIva = model.PIva;
        dto.RagioneSociale = model.CompanyName;
        dto.Referenti = model.Referenti;
        dto.Ruolo = model.Ruolo;
        dto.SitoInternet = model.SitoInternet;
        dto.SkypeID = model.SkypeID;
        dto.Stato = model.Stato;
        dto.StatoCivile = model.StatoCivile;
        dto.Tag = model.Tag;
        dto.TagCliente = model.TagCliente;

        if (model.PhoneNumbers) {
            model.PhoneNumbers.forEach((ref, i, array) => {
                let telefono = this.commonsMapper.phoneNumberModel2telefonoDTO(ref);
                dto.Telefoni.push(telefono);
            });
        }

        dto.TipoCanalePreferito = model.TipoCanalePreferito;
        dto.TipoCliente = model.TipoCliente;
        dto.TipoConsenso = model.PrivacyBitmask;
        dto.TipoDocumentoPresentato = model.TipoDocumentoPresentato;
        dto.TipoSesso = model.TipoSesso;
        dto.TipoTitolo = model.TipoTitolo;

        if (model.Address) {
            dto.Indirizzo = new IndirizzoDTO();
            dto.Indirizzo.IdEntita = model.Address.ID;
            dto.Indirizzo.Cap = model.Address.PostalCode;
            dto.Indirizzo.Civico = model.Address.CivicNumber;
            dto.Indirizzo.ControlCode = model.Address.ControlCode;
            dto.Indirizzo.IdGestione = model.Address.IdGestione;
            dto.Indirizzo.Descrizione = model.Address.Description;
            dto.Indirizzo.IdGruppo = model.Address.IdGruppo;
            dto.Indirizzo.IdCitta = model.Address.IdCity;
            dto.Indirizzo.IdComune = model.Address.Municipality.ID;
            dto.Indirizzo.Latitudine = model.Address.Latitude;
            dto.Indirizzo.Longitudine = model.Address.Longitude;
            dto.Indirizzo.TipoGestione = model.Address.TipoGestione;
        }

        return dto;
    }


    /**
     * 
     * @param customer 
     */
    createCustomerSaveRequest(model: CustomerModel): ClienteSaveRequest {

        var request = new ClienteSaveRequest();

        request.Cliente = new ClienteCmd();
        request.Cliente.ControlCode = model.ControlCode;
        request.Cliente.IdEntita = model.ID;
        request.Cliente.IdGestione = model.IdGestione;
        request.Cliente.IdSettore = 0;//customer.Settore?
        request.Cliente.IdEditore = 0;//customer.Editore?
        request.Cliente.NascitaLuogo = model.NascitaLuogo;
        request.Cliente.NascitaProvincia = model.NascitaProvincia;
        request.Cliente.NascitaData = this.utility._Date2JSON_(model.NascitaData);
        request.Cliente.Stato = model.Stato;
        request.Cliente.StatoCivile = model.StatoCivile;
        request.Cliente.TipoCliente = model.TipoCliente;
        request.Cliente.TipoSesso = model.TipoSesso;
        request.Cliente.TipoTitolo = model.TipoTitolo;
        request.Cliente.Nome = model.Nome;
        request.Cliente.Cognome = model.Cognome;
        request.Cliente.RagSociale = model.CompanyName;

        if (model.PhoneNumbers && model.PhoneNumbers[0])
            request.Cliente.Telefono = model.PhoneNumbers[0].Number;
        if (model.PhoneNumbers) {
            var telefono2 = model.PhoneNumbers.find((x) => { return x.Type == TipoTelefono.Telefono2 });
            request.Cliente.Telefono2 = (telefono2) ? telefono2.Number : null;
            var cellulare = model.PhoneNumbers.find((x) => { return x.Type == TipoTelefono.Cellulare });
            request.Cliente.Cellulare = (cellulare) ? cellulare.Number : null;
            var fax = model.PhoneNumbers.find((x) => { return x.Type == TipoTelefono.Fax });
        }
        request.Cliente.Fax = (fax) ? fax.Number : null;
        request.Cliente.EMail = model.EMail;
        request.Cliente.Email2 = model.EMail2;
        request.Cliente.SitoInternet = model.SitoInternet;
        request.Cliente.SkypeID = model.SkypeID;
        request.Cliente.AltriRecapiti = model.AltriRecapiti;
        request.Cliente.TipoCanalePreferito = model.TipoCanalePreferito;
        request.Cliente.Note = model.Note;
        request.Cliente.EsenteAUI = model.EsenteAUI;
        request.Cliente.CodicePadre = model.CodicePadre;
        request.Cliente.CodiceFiscale = model.CodiceFiscale;
        request.Cliente.PIva = model.PIva;
        request.Cliente.NumeroDocumentoPresentato = model.NumeroDocumentoPresentato;
        request.Cliente.DataRilascioDocumento = this.utility._Date2JSON_(model.DataRilascioDocumento);
        request.Cliente.DataScadenzaDocumento = this.utility._Date2JSON_(model.DataScadenzaDocumento);
        request.Cliente.TipoDocumentoPresentato = model.TipoDocumentoPresentato;
        request.Cliente.AutoritaLuogoRilascio = model.AutoritaLuogoRilascio;
        request.Cliente.TagCliente = model.Tag;
        if (model.Address) {
            request.Cliente.Indirizzo = new IndirizzoCmd();
            if (model.Address.Municipality) {
                request.Cliente.Indirizzo.IdComune = model.Address.Municipality.ID;
            }
            request.Cliente.Indirizzo.IdEntita = model.Address.ID;
            request.Cliente.Indirizzo.IdGestione = model.Address.IdGestione;
            request.Cliente.Indirizzo.ControlCode = model.Address.ControlCode;
            request.Cliente.Indirizzo.CodiceIstat = null;
            request.Cliente.Indirizzo.IdCitta = model.Address.IdCity;
            request.Cliente.Indirizzo.Descrizione = model.Address.Description;
            request.Cliente.Indirizzo.Civico = model.Address.CivicNumber;
            request.Cliente.Indirizzo.Latitudine = model.Address.Latitude;
            request.Cliente.Indirizzo.Longitudine = model.Address.Longitude;
            request.Cliente.Indirizzo.Cap = model.Address.PostalCode;
        }
        if (model.Referenti) {
            request.Cliente.Referenti = new Array<ClienteReferenteCmd>();
            model.Referenti.forEach((ref, i, array) => {
                var crCmd = new ClienteReferenteCmd();
                //crCmd =  ??
                request.Cliente.Referenti.push(crCmd);
            });
        }

        if (model.Address) {
            request.SaveIndirizzo = (!model.Address.Description) ? false : true;
        } else {
            request.SaveIndirizzo = false;
        }
        request.SaveReferenti = (model.Referenti && model.Referenti.length > 0) ? true : false;

        // *** PRIVACY ***
        request.Cliente.TipoConsenso = this.calculatePrivacyAgreementBitmask(model.PrivacySettings);


        // *** MANUAL FIX ***
        request.Cliente.TipoImpiego = 1;//model.TipoImpiego;

        return request;
    }

    calculatePrivacyAgreementBitmask(privacySettings: CustomerPrivacySettingsModel): number {
        let privacyAgreement = 0;
        //let privacyGps = (sessionService.session.TipoGruppo & sessionService.Enumerativi.ETipoGruppo.GPSPrivacy) == sessionService.Enumerativi.ETipoGruppo.GPSPrivacy;

        //if (!this.privacyGps) {
        if (privacySettings.Pubblicitario)
            privacyAgreement += TipoPrivacy.Pubblicitario;
        if (privacySettings.RicercheMercato)
            privacyAgreement += TipoPrivacy.RicercheMercato;
        // }
        // else {
        //     if (this.privacyGPScomunicazionealnetworkgabetti)
        //         consensoPrivacy += that.sessionService.Enumerativi.ETipoPrivacy.GPScomunicazionealnetworkgabetti;
        //     if (this.privacyGPSfinalitadimarketing)
        //         consensoPrivacy += that.sessionService.Enumerativi.ETipoPrivacy.GPSfinalitadimarketing;
        //     if (this.privacyGPSTrattamentoDati)
        //         consensoPrivacy += that.sessionService.Enumerativi.ETipoPrivacy.GPSTrattamentoDati;
        //     if (this.privacyGPSricezioneemail)
        //         consensoPrivacy += that.sessionService.Enumerativi.ETipoPrivacy.GPSricezioneemail;
        // }
        return privacyAgreement;
    }

}