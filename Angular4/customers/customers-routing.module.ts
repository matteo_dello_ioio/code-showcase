import { RouterModule, Routes } from '@angular/router';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';

import { AuthGuard } from '../services/auth-guard.service';

const customerRoutes: Routes = [
	{
		path: '',
		children: [
			// { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
			{ path: 'lista', component: CustomerListComponent/*, canActivate: [AuthGuard]*/ },
			{ path: 'lista/:id', component: CustomerDetailComponent, canActivate: [AuthGuard] },
			{ path: 'new', component: CustomerDetailComponent, canActivate: [AuthGuard], data: { isNew: true } },
		]
	}
];

export const CustomersRoutingModule = RouterModule.forChild(customerRoutes);