import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { CommonsMapper } from '../shared/commons-mapper';
import { RealEstateModel } from '../real-estate/models/real-estate.model';
import { environment } from '../../environments/environment';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';



export interface IAdvertisementService {

  publish(model: RealEstateModel, idEditore: number): Observable<boolean>;
  unpublish(model: RealEstateModel, idEditore: number): Observable<boolean>;
  isListingPublished(realEstateID: number, editorID: number): Observable<boolean>;

}
