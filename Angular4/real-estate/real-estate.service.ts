
import { Injectable, Inject } from '@angular/core'
import { HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import {
  AbstractRealEstateDataService,
  QueryObjectDTO,
  ImmobileDTO,
  ImmobileIncaricoDTO,
  AttivitaStoricoImmobileDTO,
  ImmagineDTO,
  ImmobileClasseEnergeticaNewDTO,
  ImmobileTypeAheadRequest,
  ImmobileElencoTypeAhead,
  ImmobileChangeStateRequest,
  TypeAheadRequest, ClienteElencoTypeAheadDTO
} from 'treeplat-mobile-rest-client';
import { IRealEstateService } from './real-estate-service.interface'
import { RealEstateItem } from './models/real-estate-item'
import { RealEstateModel } from './models/real-estate.model'
import { RealEstateMapper } from './real-estate-mapper'
import { RealEstateAssignmentModel } from './models/real-estate-assignment.model'
import { ImageModel } from './models/image.model';
import { ImageMiniModel } from './models/image-mini.model';
import { ActivityLogRealEstateModel } from './models/activity-log-real-estate.model';
import { SessionService } from '../session/session.service';
import { RealEstateMicro } from './models/real-estate-micro.model';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { EnergyPerformanceCertificateModel } from './models/energy-performance-certificate.model';
import { RealEstateSearch } from './models/real-estate-search.interface';
import { CustomerMiniModel } from '../shared/models/customer-mini-model';
import { CommonsMapper } from '../shared/commons-mapper';
import { environment } from '../../environments/environment'
import 'rxjs/add/operator/map'

@Injectable()
export class RealEstateService implements IRealEstateService {

  environment = null;

  constructor(
    private sessionService: SessionService,
    private dataService: AbstractRealEstateDataService,
    private commonsMapper: CommonsMapper,
    private mapper: RealEstateMapper
  ) {
    this.environment = environment;
  }



  getBlobFromUrl(url: string): Observable<Blob> {
    if (!url)
      throw new Error("argument cannot be null");

    return this.dataService.getBlobFromUrl(url);
  }


  /**
   * retrieve a list or real estate item
   * @param queryObject 
   */
  readAll(queryObject: QueryObjectDTO): Observable<RealEstateItem[]> {

    if (!queryObject)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.getAll(this.environment.ServiceBaseWcfUrlAuth, queryObject)
        .subscribe(response => {

          let items = null;

          let realEstates = response;

          if (realEstates) {
            items = new Array<RealEstateItem>();
            for (let i = 0; i < realEstates.length; i++) {
              let item = this.mapper.mapImmobileElencoDTO2RealEstateItem(realEstates[i]);
              items.push(item);
            }
          }

          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }


  /**
   * Returns a real estate by its unique identifier.
   * @param id 
   */
  readById(id: number): Observable<RealEstateModel> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let realEstateObservable = this.dataService.getById(this.environment.ServiceBaseWcfUrlAuth, id);
      let energyPerformanceCertificateObservable = this.dataService.getNewEnergeticClassById(this.environment.ServiceBaseWcfUrlAuth, id);

      forkJoin(realEstateObservable, energyPerformanceCertificateObservable)
        .subscribe(results => {

          let model = new RealEstateModel();
          let energeticClassModel: EnergyPerformanceCertificateModel = null;

          let immobileDTO = results[0] as ImmobileDTO;
          let energyPerformanceCertificateDTO = results[1] as ImmobileClasseEnergeticaNewDTO;

          if (immobileDTO) {
            model = this.mapper.immobileDTO2RealEstateModel(immobileDTO);
          }

          if (energyPerformanceCertificateDTO) {
            energeticClassModel = this.mapper.immobileClasseEnergeticaNewDTO2EnergyPerformanceCertificateModel(energyPerformanceCertificateDTO);
          }
          //console.log(energeticClassModel);

          if (!energeticClassModel)
            model.EnergyPerformanceCertificate = new EnergyPerformanceCertificateModel();
          else
            model.EnergyPerformanceCertificate = energeticClassModel;

          observer.next(model);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * 
   * @param realEstate 
   */
  save(realEstate: RealEstateModel): Observable<RealEstateModel> {

    // TODO: remove workaround in mapper and put here mapping IdGestione
    realEstate.IdGestione = this.sessionService.getSession().IdGestione;

    if (!realEstate.Address.PostalCode)
      realEstate.Address.PostalCode = '';

    if (realEstate.SellInformation && !realEstate.SellInformation.VariationDate)
      realEstate.SellInformation.VariationDate = new Date();

    if (realEstate.RentInformation && !realEstate.RentInformation.VariationDate)
      realEstate.RentInformation.VariationDate = new Date();

    if (realEstate.BareOwnershipInformation && !realEstate.BareOwnershipInformation.VariationDate)
      realEstate.BareOwnershipInformation.VariationDate = new Date();


    return Observable.create(observer => {

      let saveRequest = this.mapper.createRealEstateSaveRequest(realEstate);

      this.dataService.save(this.environment.ServiceBaseWcfUrlAuth, saveRequest)
        .subscribe(response => {
          let model = new RealEstateModel();
          let dto = response as ImmobileDTO;

          if (dto) {
            model = this.mapper.immobileDTO2RealEstateModel(dto);
          }

          observer.next(model);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * 
   * @param id 
   */
  readAllImages(id: number): Observable<ImageMiniModel[]> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.getAllImages(this.environment.ServiceBaseWcfUrlAuth, id)
        .subscribe(response => {

          let items = null;
          let images = response;

          if (images) {
            items = new Array<ImageMiniModel>();
            for (let i = 0; i < images.length; i++) {
              let item = this.mapper.immagineMiniDTO2ImageMiniModel(images[i]);
              items.push(item);
            }
          }

          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * 
   * @param immagine 
   */
  saveImage(immagine: ImageModel): Observable<ImageModel> {

    //immagine.IdGestione = this.sessionService.getSession().IdGestione;

    return Observable.create(observer => {

      let saveRequest = this.mapper.createImageSaveRequest(immagine);

      this.dataService.saveImage(this.environment.ServiceBaseWcfUrlAuth, saveRequest)
        .subscribe(response => {

          let model = new ImageModel();
          let dto = response as ImmagineDTO;

          if (dto) {
            model = this.mapper.immagineDTO2ImageModel(dto);
          }

          observer.next(model);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });

  }

  /**
   * 
   * @param immagine 
   */
  deleteImageById(id: number): Observable<boolean> {

    return Observable.create(observer => {

      this.dataService.deleteImageById(this.environment.ServiceBaseWcfUrlAuth, id)
        .subscribe(response => {
          let result = response as boolean;
          observer.next(result);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });

  }

  /**
   * 
   * @param id real estate ID
   */
  getActiveAssignment(id: number): Observable<RealEstateAssignmentModel> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.getAssignmentList(id)
        .subscribe((assignments) => {

          let model = null;

          if (assignments) {
            model = assignments.find((value) => {
              return value.Active == true;
            });
          }

          observer.next(model);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * retrieve assignments for the specified real estate
   * @param id 
   */
  getAssignmentList(id: number): Observable<RealEstateAssignmentModel[]> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.getAssignmentList(this.environment.ServiceBaseWcfUrlAuth, id)
        .subscribe(response => {

          let models = new Array<RealEstateAssignmentModel>();

          let dto = response as ImmobileIncaricoDTO;

          if (dto) {
            if (dto.IncaricoAttivo) {
              dto.IncaricoAttivo.forEach((value, index, array) => {
                let model = this.mapper.immobileIncaricoElencoDTO2RealEstateAssignmentModel(value);
                models.push(model);
              });
            }
            if (dto.IncarichiPassati) {
              dto.IncarichiPassati.forEach((value, index, array) => {
                let model = this.mapper.immobileIncaricoElencoDTO2RealEstateAssignmentModel(value);
                models.push(model);
              });
            }
          }

          observer.next(models);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * retrieve activities for the specified real estate
   * @param id 
   */
  getActivityList(id: number): Observable<ActivityLogRealEstateModel[]> {
    if (!id)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      this.dataService.getActivityList(this.environment.ServiceBaseWcfUrlAuth, id)
        .subscribe(response => {

          let models = null;

          let dto = response as AttivitaStoricoImmobileDTO[];

          if (dto) {
            if (dto.length > 0) {
              models = new Array<ActivityLogRealEstateModel>();

              dto.forEach((value, index, array) => {
                let model = this.mapper.attivitaStoricoImmobileDTO2ActivityLogRealEstateModel(value);
                models.push(model);
              });
            }
          }

          observer.next(models);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  /**
   * 
   * @param id 
   * @param newState 
   */
  changeState(id: number, newState: number): Observable<boolean> {
    if (!newState)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let request = new ImmobileChangeStateRequest();
      request.IDImmobile = id;
      request.NextState = newState;
      let realEstateObservable = this.dataService.changeState(this.environment.ServiceBaseWcfUrlAuth, request);

      realEstateObservable.subscribe(result => {
        observer.next(result);
        observer.complete();
      },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', err.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
          }
        });
    });
  }

  /**
   * 
   * @param searchParams 
   */
  search(searchParams: RealEstateSearch): Observable<RealEstateItem[]> {
    if (!searchParams)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let priceMin = Boolean(searchParams.Price) ? searchParams.Price.Min : null;
      let priceMax = Boolean(searchParams.Price) ? searchParams.Price.Max : null;
      let areaMin = Boolean(searchParams.Area) ? searchParams.Area.Min : null;
      let areaMax = Boolean(searchParams.Area) ? searchParams.Area.Max : null;

      this.dataService.search(this.environment.ServiceBaseWcfUrlAuth,
        searchParams.RealEstateID,
        searchParams.CustomerId,
        searchParams.ContractType,
        searchParams.Status,
        priceMin,
        priceMax,
        searchParams.Typology,
        searchParams.IntendedUseBitmask,
        areaMin,
        areaMax,
        searchParams.RoomNumber)
        .subscribe(results => {

          let items = null;

          if (results) {
            items = new Array<RealEstateItem>();
            for (let i = 0; i < results.length; i++) {
              let item = this.mapper.mapImmobileElencoDTO2RealEstateItem(results[i]);
              items.push(item);
            }
          }
          observer.next(items);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  getDefaultSearch(): RealEstateSearch {
    return {
      RealEstateID: null,
      CustomerId: null,
      ContractType: '',
      Status: 0,
      Typology: 0,
      IntendedUseBitmask: null,
      RoomNumber: new Array<number>(),
      Price: {
        Min: null,
        Max: null
      },
      Area: {
        Min: null,
        Max: null
      }
    };
  }

  /**
   * Returns true if the passed parameter has some difference form the default search.
   * @param searchParams 
   */
  hasCustomFilters(searchParams: RealEstateSearch): boolean {

    let defaultSearch = this.getDefaultSearch();

    let result = (
      (Boolean(searchParams.RealEstateID) && (searchParams.RealEstateID != defaultSearch.RealEstateID))
      ||
      (searchParams.CustomerId != defaultSearch.CustomerId)
      ||
      // (this.searchParams.ContractType != defaultSearch.ContractType)
      // ||
      (searchParams.Status != defaultSearch.Status)
      ||
      (searchParams.Typology != defaultSearch.Typology)
      ||
      (searchParams.IntendedUseBitmask != defaultSearch.IntendedUseBitmask)
      ||
      (!searchParams.RoomNumber || (searchParams.RoomNumber.length != defaultSearch.RoomNumber.length))
      ||
      (!searchParams.Price || (Boolean(searchParams.Price.Min) != Boolean(defaultSearch.Price.Min)))
      ||
      (!searchParams.Price || (Boolean(searchParams.Price.Max) != Boolean(defaultSearch.Price.Max)))
      ||
      (!searchParams.Area || (Boolean(searchParams.Area.Min) != Boolean(defaultSearch.Area.Min)))
      ||
      (!searchParams.Area || (Boolean(searchParams.Area.Max) != Boolean(defaultSearch.Area.Max)))
    );

    return result;
  }
}