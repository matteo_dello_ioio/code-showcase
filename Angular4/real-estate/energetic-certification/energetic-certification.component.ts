
import { Component, Input, forwardRef } from '@angular/core';
import { ControlContainer, NgForm, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EnergeticCertificationModel } from '../models/energetic-certification.model';


@Component({
  selector: 'energetic-certification',
  styles: [],
  templateUrl: './energetic-certification.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EnergeticCertificationComponent),
      multi: true
    }
  ]
})
export class EnergeticCertificationComponent implements ControlValueAccessor {

  //private _energyPerformanceCertificate: EnergyPerformanceCertificateModel = <EnergyPerformanceCertificateModel>{};
  private _energeticCertification: EnergeticCertificationModel = new EnergeticCertificationModel();

  writeValue(value: any) {
    this._energeticCertification = value;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    //throw new Error("Method not implemented.");
  }
  setDisabledState?(isDisabled: boolean): void {
    //throw new Error("Method not implemented.");
  }


  constructor() { }

  /**
   * 
   */
  get energyClass() {
    return this._energeticCertification.EnergyClass;
  }
  set energyClass(value) {
    this._energeticCertification.EnergyClass = value;
    this.propagateChange(this._energeticCertification);
  }

  /**
   * 
   */
  get energyPerformanceIndex() {
    return this._energeticCertification.IPE;
  }
  set energyPerformanceIndex(value) {
    this._energeticCertification.IPE = value;
    this.propagateChange(this._energeticCertification);
  }

  /**
   * 
   */
  get energyPerformanceIndexUnitOfMeasure() {
    return this._energeticCertification.IPEUnit;
  }
  set energyPerformanceIndexUnitOfMeasure(value) {
    this._energeticCertification.IPEUnit = value;
    this.propagateChange(this._energeticCertification);
  }

}