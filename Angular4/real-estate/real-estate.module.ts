// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Material
import { MaterialModule, MdNativeDateModule } from '@angular/material';

// module
import { RealEstateRoutingModule } from './real-estate-routing.module';
import { RealEstateListComponent } from './real-estate-list/real-estate-list.component';
import { RealEstateDetailComponent } from './real-estate-detail/real-estate-detail.component';

// Services
import { UtilityService } from '../shared/utility-service';
import { GeoService } from '../services/geo.service';
import { LookupService } from '../services/lookup.service';
import { AuthGuard } from '../services/auth-guard.service';

import {
    AbstractRealEstateDataService,
    RealEstateDataService,
    AbstractAdvertisementDataService, AdvertisementDataService
} from 'treeplat-mobile-rest-client';
import { RealEstateService } from './real-estate.service';
import { RealEstateMapper } from './real-estate-mapper'

// Pipes
import { CustomPipesModule } from '../custom-pipes/custom-pipes.module';
import { CurrencyFormatPipe } from '../custom-pipes/currency-format.pipe';

// Detail
import { DetailGeneralComponent } from './real-estate-detail/detail-general/detail-general.component';
import { DetailGeolocalComponent } from './real-estate-detail/detail-geolocal/detail-geolocal.component';
import { DetailAssignmentComponent } from './real-estate-detail/detail-assignment/detail-assignment.component';
import { DetailImagesComponent } from './real-estate-detail/detail-images/detail-images.component';
import { DetailActivityComponent } from './real-estate-detail/detail-activity/detail-activity.component';
import { DetailTipologyComponent } from './real-estate-detail/detail-tipology/detail-tipology.component';
import { EnergyPerformanceCertificateComponent } from './energy-performance-certificate/energy-performance-certificate.component';

// Third part
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { UploaderImagePreview } from '../shared/image-preview.directive';
import { GeolocalizationModule } from '../geolocalization/geolocalization.module';
import { SessionModule } from '../session/session.module';
import { AdvertisementService } from './advertisement.service';
import { AdvertisementMapper } from './advertisement-mapper';
import { EnergeticCertificationComponent } from './energetic-certification/energetic-certification.component';
import { DetailResumeComponent } from './real-estate-detail/detail-resume/detail-resume.component';
import { CustomDirectivesModule } from '../custom-directives/custom-directives.module';
import { RealEstateSearchComponent } from './real-estate-search/real-estate-search.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MdNativeDateModule,
        RealEstateRoutingModule,
        FileUploadModule,
        SessionModule,
        CustomPipesModule,
        GeolocalizationModule,
        CustomDirectivesModule,
        InfiniteScrollModule
    ],
    declarations: [
        RealEstateListComponent,
        RealEstateDetailComponent,
        UploaderImagePreview,
        EnergyPerformanceCertificateComponent,
        EnergeticCertificationComponent,
        DetailResumeComponent,
        // Detail
        DetailGeneralComponent,
        DetailGeolocalComponent,
        DetailAssignmentComponent,
        DetailImagesComponent,
        DetailActivityComponent,
        DetailTipologyComponent,
        RealEstateSearchComponent,
    ],
    providers: [
        AuthGuard,
        UtilityService,
        LookupService,
        GeoService,
        { provide: AbstractRealEstateDataService, useClass: RealEstateDataService },
        { provide: AbstractAdvertisementDataService, useClass: AdvertisementDataService },
        RealEstateMapper,
        RealEstateService,
        CurrencyFormatPipe,
        AdvertisementService,
        AdvertisementMapper
    ],
})

export class RealEstateModule { }