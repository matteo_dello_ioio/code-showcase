import { Observable } from 'rxjs/Observable';
import { QueryObjectDTO } from 'treeplat-mobile-rest-client';
import { RealEstateItem } from './models/real-estate-item';
import { RealEstateModel } from './models/real-estate.model';
import { ImageModel } from './models/image.model';
import { ActivityLogRealEstateModel } from './models/activity-log-real-estate.model';
import { RealEstateAssignmentModel } from './models/real-estate-assignment.model';
import { ImageMiniModel } from './models/image-mini.model';
import { RealEstateSearch } from './models/real-estate-search.interface';


export interface IRealEstateService {
  readAll(queryObject: QueryObjectDTO): Observable<RealEstateItem[]>
  readById(id: number): Observable<RealEstateModel>;
  save(realEstate: RealEstateModel): Observable<RealEstateModel>

  readAllImages(id: number): Observable<ImageMiniModel[]>
  saveImage(immagine: ImageModel): Observable<ImageModel>;
  deleteImageById(id: number): Observable<boolean>;

  getAssignmentList(id: number): Observable<RealEstateAssignmentModel[]>;
  getActiveAssignment(id: number): Observable<RealEstateAssignmentModel>;
  getActivityList(id: number): Observable<ActivityLogRealEstateModel[]>;

  search(searchParams: RealEstateSearch): Observable<RealEstateItem[]>;
}
