

export class EnergeticCertificationModel {
   
    /**
     * map EnergyClass: TipoClasseEnergetica
     */
    EnergyClass: number;

    /**
     * Indice Prestazione Energetica
     */
    IPE: number;

    /**
     * IPE Unità di misura
     */
    IPEUnit: number;
}