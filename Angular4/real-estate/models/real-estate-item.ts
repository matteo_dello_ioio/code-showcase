import { AddressModel } from "../../shared/models/address-model";
import { CommercialInformation } from "./commercial-information.model";

/**
 * Model for real estate list item.
 */
export class RealEstateItem {

    ID: number;

    MainImage: string;

    ImagesCount: number;

    Typology: string;

    SubTypology: string;

    Address: AddressModel;

    Area: number;
    
    //Price: number;
    
    
    SellInformation: CommercialInformation;

    RentInformation: CommercialInformation;

    BareOwnershipInformation: CommercialInformation;
    
    RoomNumbers: number;
    
    BathroomNumbers: number;

    IntendedUsesDescriptions: string;

    constructor() {
        this.SellInformation = new CommercialInformation();
        this.RentInformation = new CommercialInformation();
        this.BareOwnershipInformation = new CommercialInformation();
    }
}