

export class ActivityLogRealEstateModel {

    StartDate: Date

    EndDate: Date

    Notes: string;

    Comment: string;

    Customers: string;

    ActivityType: number;

    ResultType: number;

}