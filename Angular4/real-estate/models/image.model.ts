

export class ImageModel {

    /**
     * map to IdEntita
     */
    Id: number;

    IdImmobile: number;

    //commentato per non avere riferimento a DTO
    //Immaginedata: ImmaginedataDTO;


    BinaryData: string;

    TipoImmagine: number;


    Lunghezza: number;


    Larghezza: number;


    Nome: string;


    Didascalia: string;


    DataImmissione: Date;


    Ordine: number;


    Url: string;


    UrlThumb: string;
}