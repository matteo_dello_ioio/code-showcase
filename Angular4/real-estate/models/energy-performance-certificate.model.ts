

export class EnergyPerformanceCertificateModel {

    IdEntita: number;
    IdGestione: number;
    IdAgenzia: number;
    NomeAgenzia: string;
    
    IdGruppo: number;
    NomeGruppo: string;
    
    TipoGestione: number;
    DataInserimento: string;
    DataModifica: string;
    Descrizione: string;
    ControlCode: string;

    
    ClasseEnergetica: number;


    IndiceRinnovabile: number;


    EPGlobale: number;


    Inverno: number;


    Estate: number;


    EnergiaZero: boolean;
}