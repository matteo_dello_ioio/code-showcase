
export class Collaborator {

    ID: number;
    
    ControlCode: string;

    IdGestione: number;
    
    IdCollaboratore: number;

    IdIncarico: number;

    PercentageCommission: number;
}