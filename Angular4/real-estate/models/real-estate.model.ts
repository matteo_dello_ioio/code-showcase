import { CustomerMiniModel } from "../../shared/models/customer-mini-model";
import { AddressModel } from "../../shared/models/address-model";
import { Collaborator } from "./collaborator.model";
import { DestinazioneUso } from "../../models/destinazione-uso";
import { ApartmentCardModel } from "./apartment-card.model";
import { TipologiaModel } from "../../models/tipologia";
import { RealEstateAssignmentModel } from "./real-estate-assignment.model";
import { ManagementModel } from "./management-model";
import { ManagementMiniModel } from "../../shared/models/management-mini.model";
import { CollaboratorMiniModel } from "../../shared/models/collaborator-mini.model";
import { ImageMiniModel } from "./image-mini.model";
import { CommercialInformation } from "./commercial-information.model";
import { EnergyPerformanceCertificateModel } from "./energy-performance-certificate.model";


/**
 * Model per gli immobili.
 */
export class RealEstateModel {

    /**
     * IdEntita
     */
    ID: number;

    Agency: {
        Id: number;
        Name: string;
    }

    IdGestione: number;

    Group: {
        Id: number;
        Name: string;
    }

    ControlCode: string;

    Status: number;

    ReferralCode: string;

    Description: string;

    ManagementType: number;

    IdTypology: number;

    TypologyDescription: string;

    IdSubTypology: number;

    SubTypologyDescription: string;

    IdParent: number;

    /**
     * map IsFrazionato
     */
    IsSplitted: boolean;

    DescriptionShort: string;

    DescriptionLong: string;

    Area: number;

    Address: AddressModel;

    IsForRent: boolean;

    IsForSell: boolean;

    IsBareOwnership: boolean;

    /**
     * map StatoProposta
     */
    ProposalStatus: number;

    /**
     * map TipoDestinazioneUso
     */
    IntendedUseBitMask: number;

    IntendedUses: DestinazioneUso[];

    HasVirtualTour: boolean;

    VirtualTourUrl: string;

    CodiceImmobileMandante: number;

    /**
     * map Gestionemandante
     */
    ClientManagement: ManagementModel;

    /**
     * map GestionemandanteSub
     */
    SubClientManagement: ManagementMiniModel;

    CodiceFrazionato: number;

    InSubincarico: boolean;



    SellInformation: CommercialInformation;

    RentInformation: CommercialInformation;

    BareOwnershipInformation: CommercialInformation;

    Notes: {

        Private: string;

        Public: string;
    }

    Customers: CustomerMiniModel[];

    Collaborators: CollaboratorMiniModel[];


    ApartmentCard?: ApartmentCardModel;

    /**
     * October 2015 classification
     */
    EnergyPerformanceCertificate: EnergyPerformanceCertificateModel;

    /**
     * Assignment
     */
    ActiveAssignment: RealEstateAssignmentModel;

    /**
     * Closed assignments
     */
    PastAssignments: RealEstateAssignmentModel[];

    /**
     * Images
     */
    Images: ImageMiniModel[];
    
    PublishedToGrimaldi: boolean; //537
    PublishedToWikiCasa: boolean; //2276

    InsertDate: Date;
    
    constructor() {
        this.Agency = { Id: null, Name: null };
        this.Group = { Id: null, Name: null };
        this.Address = new AddressModel();
        this.IntendedUses = new Array<DestinazioneUso>();
        this.SellInformation = new CommercialInformation();
        this.RentInformation = new CommercialInformation();
        this.BareOwnershipInformation = new CommercialInformation();
        this.Notes = { Private: null, Public: null };
        //this.Assignment = new RealEstateAssignmentModel();
    }
}