


export interface RealEstateSearch {
    RealEstateID: number;
    CustomerId: number;
    ContractType: string;
    Status: number;
    Typology: number;
    IntendedUseBitmask: number;
    RoomNumber: number[];
    Price: {
        Min: number
        Max: number
    };
    Area: {
        Min: number
        Max: number
    };
}