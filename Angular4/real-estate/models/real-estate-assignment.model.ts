
/**
 * ATTENTION! this class map ImmobileIncaricoElenco, NOT ImmobileIncarico!!!
 * The 1 to 1 dto/model implementation should be this:
 * 
 * export class RealEstateAssignmentModel {
 *    ActiveDuties: RealEstateAssignmentModel[];
 *    PastDuties: RealEstateAssignmentModel[];
 * }
 * 
 */
export class RealEstateAssignmentModel {

    ID: number;

    ControlCode: string;

    Active: boolean;

    Description: string

    Notes: string

    StatoPrimoContatto: number

    FirstExpirationDate: Date//DateTime? 

    SecondExpirationDate: Date//DateTime? 

    ThirdExpirationDate: Date//DateTime? 

    StartDate: Date//DateTime 

    Status: number

    Type: number

    Sell: boolean


    SellPrice: number


    SellCommission: number


    Rent: boolean


    RentPrice: number


    RentCommission: number


    BareOwnership: boolean


    BareOwnershipPrice: number


    BareOwnershipCommission: number


    AutoRenew: boolean
}