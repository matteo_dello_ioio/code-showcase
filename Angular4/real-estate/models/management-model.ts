

export class ManagementModel {
    
    IdCollaborator : number;

    /**
     * map Matricola
     */
    RegistrationNumber: string;

    /**
     * map Ruolo
     */
    Role: string;

    /**
     * map RagioneSociale
     */
    CompanyName: string;


    /**
     * map CollaborazioneInizio
     */
    CollaborationStart: Date;


    /**
     * map CollaborazioneFine
     */
    CollaborationEnd: Date;
}