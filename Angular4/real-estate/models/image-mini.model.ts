import { ImageDataModel } from "./image-data.model";



export class ImageMiniModel {

    ID: number;


    Data: ImageDataModel;


    ImageType: number;


    Height: number;


    Width: number;


    Name: string;


    Caption: string;


    CreationDate: string;//DateTime


    OrderIndex: number;


    Url: string;


    UrlThumb: string;

}