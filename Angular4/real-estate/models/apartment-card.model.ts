import { EnergeticCertificationModel } from "./energetic-certification.model";


/**
 * 
 */
export class ApartmentCardModel {

    ID: number;

    ControlCode: string;
    
    /**
     * Allacciamento rete idrica.
     */
    HasWaterSupplyConnection: boolean;

    /**
     * Gas
     */
    HasGasNetworkConnection: boolean

    /**
     * Allaccio telefonico
     */
    HasTelephoneNetworkConnection: boolean;

    /**
     * CablaggioInternet
     */
    HasInternetCabling: boolean

    /**
     * Anno costruzione
     */
    ConstructionYear: number;

    /**
     * Anno ristrutturazione
     */
    RenovationYear: number

    /**
     * Antifurto
     */
    HasAlarm: boolean

    /**
     * Antincendio
     */
    HasAntiFire: boolean

    /**
     * Arredato
     */
    HasForniture: boolean

    /**
     * Ascensore
     */
    HasElevator: boolean

    /**
     * CameraDiServizio
     */
    HasServiceRoom: boolean

    /**
     * Camino
     */
    HasChimney: boolean

    /**
     * Cantina
     */
    HasCellar: boolean

    /**
     * Citofono
     */
    HasIntercom: boolean

    /**
     * Fognatura
     */
    HasDrainage: boolean

    /**
     * GiardinoCondominiale
     */
    HasCommonGarden: boolean

    /**
     * GiardinoPrivato
     */
    HasPrivateGarden: boolean

    /**
     * ImpiantoElettricoANorma
     */
    HasCompliantElectricalSystem: boolean

    /**
     * ImpiantoRiscaldamento
     */
    HasHeatingSystem: boolean

    /**
     * 
     */
    Interno: string

    /**
     * 
     */
    Luce: number

    /**
     * 
     */
    Mansarda: number

    /**
     * MqBalconi
     */
    BalconyArea: number

    /**
     * MqBox
     */
    GarageArea: number

    /**
     * MqCantina
     */
    CellarArea: number

    /**
     * MqGiardinoCondominiale
     */
    CommonGardenArea: number

    /**
     * MqGiardinoPrivato
     */
    PrivateGardenArea: number

    /**
     * MqMansarda
     */
    MansardArea: number

    /**
     * MqSolaio
     */
    LoftArea: number

    /**
     * MqSoppalco
     */
    MezzanineArea: number

    /**
     * MqTaverna
     */
    TavernArea: number

    /**
     * MqTerrazzi
     */
    TerraceArea: number

    /**
     * MqVerande
     */
    PorchArea: number

    /**
     * NumeroAppartamenti
     */
    ApartmentsCount: number

    /**
     * NumeroBagni
     */
    BathroomsCount: number

    /**
     * NumeroBalconi
     */
    BalconyCount: number

    /**
     * NumeroBox (di pertinenza)
     */
    GaragesCount: number

    /**
     * NumeroBoxStabile (di tutto l'edificio)
     */
    BuildingGaragesCount: number

    /**
     * NumeroCamereDaLetto
     */
    BedroomsCount: number

    /**
     * NumeroIngressi
     */
    EntrancesCount: number

    /**
     * 
     */
    NumeroIngressiStabile: number

    /**
     * NumeroLivelli
     */
    LevelsCount: number

    /**
     * NumeroLocali
     */
    RoomsCount: number

    /**
     * 
     */
    NumeroPassiCarrabili: number

    /**
     * 
     */
    NumeroPassiCarrabiliStabile: number

    /**
     * NumeroPiani
     */
    FloorsCount: number

    /**
     * 
     */
    NumeroPostiAutoCoperti: number

    /**
     * 
     */
    NumeroPostiAutoCopertiStabile: number

    /**
     * 
     */
    NumeroPostiAutoScoperti: number

    /**
     * 
     */
    NumeroPostiAutoScopertiStabile: number

    /**
     * 
     */
    NumeroScale: number

    /**
     * NumeroTerrazzi
     */
    TerraceCount: number

    /**
     * 
     */
    NumeroVerande: number

    /**
     * 
     */
    Parabola: number

    /**
     * 
     */
    PiscinaCondominiale: number

    /**
     * PiscinaPrivata
     */
    HasPrivateSwimmingPool: boolean

    /**
     * Portineria
     */
    HasReception: boolean

    /**
     * 
     */
    Ripostiglio: number

    /**
     * 
     */
    SalaHobby: number

    /**
     * 
     */
    Scala: string

    /**
     * 
     */
    Solaio: number

    /**
     * 
     */
    Soppalco: number

    /**
     * 
     */
    SpeseCondominialiMensili: number

    /**
     * map StatoInterno
     */
    InternalStatus: number

    /**
     * 
     */
    StatoStabile: number

    /**
     * 
     */
    Studio: number

    /**
     * 
     */
    Taverna: number

    /**
     * 
     */
    TipoBox: number

    /**
     * 
     */
    TipoCondizionamento: number

    /**
     * 
     */
    TipoCostruzione: number

    /**
     * TipoCucina
     */
    KitchenType: number

    /**
     * 
     */
    TipoEdilizia: number

    /**
     * 
     */
    TipoEsposizione: number

    /**
     * 
     */
    TipoFacciataEsterna: number

    /**
     * 
     */
    TipoFacciataInterna: number

    /**
     * 
     */
    TipoIngresso: number

    /**
     * 
     */
    TipoOrientamento: number

    /**
     * 
     */
    TipoPavimentoBagno: number

    /**
     * 
     */
    TipoPavimentoCamere: number

    /**
     * 
     */
    TipoPavimentoCucina: number

    /**
     * 
     */
    TipoPavimentoSoggiorno: number

    /**
     * Piano (map to TipoPiano)
     */
    Floor: number

    /**
     * 
     */
    TipoRiscaldamentoApparecchi: number

    /**
     * 
     */
    TipoRiscaldamentoFonte: number

    /**
     * 
     */
    TipoRiscaldamentoImpianto: number

    /**
     * 
     */
    TipoSalone: number

    /**
     * 
     */
    TipoSerramenti: number

    /**
     * 
     */
    TipoSoggiorno: number

    /**
     * 
     */
    IsLastFloor: boolean

    /**
     * 
     */
    VicinanzaMezzi: string

    /**
     * VideoCitofono
     */
    HasVideoIntercom: boolean

    /**
     * 
     */
    EnergeticCertification: EnergeticCertificationModel;
    
    // /**
    //  * TipoClasseEnergetica
    //  */
    // EnergyClass: number
    // /**
    //  * 
    //  */
    // IPE: number
    // /**
    //  * IPEMisura
    //  */
    // IPEUnit: number

    constructor() {
        this.EnergeticCertification = new EnergeticCertificationModel();
    }
}