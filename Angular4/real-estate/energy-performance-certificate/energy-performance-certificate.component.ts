
import { Component, Input, forwardRef, OnInit, OnChanges } from '@angular/core';
import { ControlContainer, NgForm, ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl, NG_VALIDATORS } from '@angular/forms';
import { EnergyPerformanceCertificateModel } from '../models/energy-performance-certificate.model';
import { validateConfig } from '@angular/router/src/config';

export function createEnergyPerformanceCertificateValidator() {
  return function validateEnergyPerformanceCertificate(c: FormControl) {

    let result = null; // null => no errors

    let err = {
      requiredError: {
        given: c.value
      }
    };

    //return (c.value > 10 || c.value < 0) ? err : null;
    if (c.value !== null) {
      let allEmpty = (
        c.value.ClasseEnergetica === undefined
        &&
        !c.value.IndiceRinnovabile
        &&
        c.value.EPGlobale === undefined
        &&
        (c.value.EnergiaZero === undefined || c.value.EnergiaZero == false)
        &&
        c.value.Inverno === undefined
        &&
        c.value.Estate === undefined
      );

      let allDefined = (
        c.value.ClasseEnergetica
        &&
        c.value.IndiceRinnovabile
        &&
        c.value.EPGlobale
        &&
        c.value.EnergiaZero !== undefined
        &&
        c.value.Inverno
        &&
        c.value.Estate
      );

      if (allEmpty) {
        result = null;
      } else {
        result = (!allDefined) ? err : null;
      }
    }
    return result;
  }
}


@Component({
  selector: 'energy-performance-certificate',
  styles: [],
  templateUrl: './energy-performance-certificate.component.html',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EnergyPerformanceCertificateComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EnergyPerformanceCertificateComponent),
      multi: true,
    }
  ]
})
export class EnergyPerformanceCertificateComponent implements ControlValueAccessor, OnInit, OnChanges {

  //private _energyPerformanceCertificate: EnergyPerformanceCertificateModel = <EnergyPerformanceCertificateModel>{};
  private _energyPerformanceCertificate: EnergyPerformanceCertificateModel = new EnergyPerformanceCertificateModel();

  allFieldsRequired: boolean = false;

  propagateChange = (_: any) => { };
  onTouched = () => { };

  validateFn: Function;


  constructor() { 
    
  }

  ngOnInit() {
    console.log("COMPONENT INIT!!!");
    this.validateFn = createEnergyPerformanceCertificateValidator();
  }

  ngOnChanges(changes) {
    console.log("COMPONENT CHANGE!!!");
    if (changes) {
      this.validateFn = createEnergyPerformanceCertificateValidator();
      this.propagateChange(this._energyPerformanceCertificate);
    }
  }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  writeValue(value: any) {
    this._energyPerformanceCertificate = value;
  }

  // from Angular documentation...don't understand how is it supposed to work...
  // host: {
  //   (change): 'propagateChange($event.target.value)'
  // }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    //throw new Error("Method not implemented.");
  }

  updateChanges(val) {
    this.propagateChange(val);
  }

  updateBlur() {
    this.onTouched();
  }


  allFieldRequired(): boolean  {
    let result = false;

    if (this._energyPerformanceCertificate !== null) {
      let allEmpty = (
        (this._energyPerformanceCertificate.EnergiaZero === undefined || this._energyPerformanceCertificate.EnergiaZero == false)
        &&
        !Boolean(this._energyPerformanceCertificate.ClasseEnergetica)
        &&
        !Boolean(this._energyPerformanceCertificate.EPGlobale)
        &&
        !Boolean(this._energyPerformanceCertificate.IndiceRinnovabile)
        &&
        !Boolean(this._energyPerformanceCertificate.Inverno)
        &&
        !Boolean(this._energyPerformanceCertificate.Estate)
      );

      let allDefined = (
        this._energyPerformanceCertificate.EnergiaZero !== undefined
        &&
        Boolean(this._energyPerformanceCertificate.ClasseEnergetica)
        &&
        Boolean(this._energyPerformanceCertificate.IndiceRinnovabile)
        &&
        Boolean(this._energyPerformanceCertificate.EPGlobale)
        &&
        Boolean(this._energyPerformanceCertificate.Inverno)
        &&
        Boolean(this._energyPerformanceCertificate.Estate)
      );

      if (allEmpty) {
        result = false;
      } else {
        result = (!allDefined) ? true : false;
      }
      //console.log("ALL REQUIRED: " + result + " | ALL EMPTY: " + allEmpty + " | ALL DEFINED: " + allDefined);
    }
    return result;
  }

  // allFieldRequired(): boolean {
  //   let someDefined = (
  //     Boolean(this._energyPerformanceCertificate.ClasseEnergetica)
  //     ||
  //     Boolean(this._energyPerformanceCertificate.IndiceRinnovabile)
  //     ||
  //     Boolean(this._energyPerformanceCertificate.EPGlobale)
  //     ||
  //     Boolean(!this._energyPerformanceCertificate.EnergiaZero)
  //     ||
  //     Boolean(this._energyPerformanceCertificate.Inverno)
  //     ||
  //     Boolean(this._energyPerformanceCertificate.Estate)
  //   );

  //   return someDefined;
  // }

  /**
   * 
   */
  get zeroEnergy() {
    return this._energyPerformanceCertificate.EnergiaZero;
  }
  set zeroEnergy(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.EnergiaZero = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

  /**
   * 
   */
  get winterSatisfaction() {
    return this._energyPerformanceCertificate.Inverno;
  }
  set winterSatisfaction(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.Inverno = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

  /**
   * 
   */
  get summerSatisfaction() {
    return this._energyPerformanceCertificate.Estate;
  }
  set summerSatisfaction(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.Estate = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

  /**
   * 
   */
  get energeticClass() {
    return this._energyPerformanceCertificate.ClasseEnergetica;
  }
  set energeticClass(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.ClasseEnergetica = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

  /**
   * 
   */
  get globalEP() {
    return this._energyPerformanceCertificate.EPGlobale;
  }
  set globalEP(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.EPGlobale = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

  /**
   * 
   */
  get renewableIndex() {
    return this._energyPerformanceCertificate.IndiceRinnovabile;
  }
  set renewableIndex(value) {
    if (value !== undefined)
      this.allFieldsRequired = this.allFieldRequired();
    this._energyPerformanceCertificate.IndiceRinnovabile = value;
    this.propagateChange(this._energyPerformanceCertificate);
  }

}