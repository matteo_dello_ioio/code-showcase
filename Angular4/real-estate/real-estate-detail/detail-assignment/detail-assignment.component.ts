import { Component, Input, Output, EventEmitter, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-assignment',
  styles: [],
  templateUrl: './detail-assignment.component.html',
})

export class DetailAssignmentComponent implements OnInit, OnChanges {
  @Input() childData: any;
  @Output() childVarChange = new EventEmitter();
  
  isNew = false;
  showNewPrice = false;
  showPriceHistory = false;

  private startDate: Date;
  private endDate: Date;
  private associateCustomer: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.isNew = this.route.snapshot.data.isNew;
    if (this.isNew === true) {
      this.startDate = new Date();
      this.endDate = new Date();
      this.endDate.setFullYear(this.endDate.getFullYear() + 1);
    }
  }

  change(newValue) {
    console.log("NEW VALUE " + JSON.stringify(newValue));
    this.childData = newValue;
    this.childVarChange.emit(newValue);
  }

}