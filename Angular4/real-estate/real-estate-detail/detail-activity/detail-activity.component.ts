import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-detail-activity',
  styles: [],
  templateUrl: './detail-activity.component.html',
})

export class DetailActivityComponent {
  @Input() childData: any;
}