import { Component, Input, Output, EventEmitter, OnInit, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Comune } from '../../../models/comune';
import { LookupService } from '../../../services/lookup.service';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators';
import { GeolocalizationComponent } from '../../../geolocalization/components/geolocalization.component';


@Component({
  selector: 'app-detail-geolocal',
  styles: [],
  templateUrl: './detail-geolocal.component.html',
})

export class DetailGeolocalComponent implements OnInit {
  
  @Input() childData: any;
  @Output() childVarChange = new EventEmitter();

  showMarker: boolean;
  __address__: string;

  lat: number = 51.678418;
  lng: number = 7.809007;

  @ViewChild(GeolocalizationComponent) geolocalizationChild: GeolocalizationComponent;

  constructor(
    private lookup: LookupService,
  ) {

  }

  ngOnInit() {

  }



  change(newValue) {
    this.childData = newValue;
    this.childVarChange.emit(newValue);

    this.geolocalizationChild.description = newValue.Address.Description;
    this.geolocalizationChild.civicNumber = newValue.Address.CivicNumber;
    this.geolocalizationChild.municipality = newValue.Address.Municipality.Name;
    this.geolocalizationChild.latitude = newValue.Address.Latitude;
    this.geolocalizationChild.longitude = newValue.Address.Longitude;

    this.geolocalizationChild.updateAddress();
  }


  /**
   * event handler for geolocalization component
   * @param event has this structure { latitude: number, longitude: number }
   */
  onCoordinatesChange(event) {
    this.childData.Address.Latitude = event.latitude;
    this.childData.Address.Longitude = event.longitude;
  }

}