import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RealEstateDetailComponent } from './real-estate-detail.component';
import { RealEstateService } from '../real-estate.service';
import { LookupService } from '../../services/lookup.service';
import { GeoService } from '../../services/geo.service';
import { TYPOLOGIES_LOOKUP } from '../../data/typologies.data';
import { Typology } from '../../shared/models/lookup.models';
import { DestinazioneUso } from '../../models/destinazione-uso';
import { INTENDED_USE_LOOKUP } from '../../data/destinazioni-uso.data';
import { DetailGeneralComponent } from './detail-general/detail-general.component';
import { DetailGeolocalComponent } from './detail-geolocal/detail-geolocal.component';
import { DetailImagesComponent } from './detail-images/detail-images.component';
import { DetailActivityComponent } from './detail-activity/detail-activity.component';
import { DetailAssignmentComponent } from './detail-assignment/detail-assignment.component';
import { DetailTipologyComponent } from './detail-tipology/detail-tipology.component';
import { FileUploader, FileUploadModule, } from 'ng2-file-upload';
import { UploaderImagePreview } from '../../shared/image-preview.directive';
import { CustomPipesModule } from '../../custom-pipes/custom-pipes.module';
import { CurrencyFormatPipe } from '../../custom-pipes/currency-format.pipe';
import { GeolocalizationModule } from '../../geolocalization/geolocalization.module';
import { EnergyPerformanceCertificateComponent } from '../energy-performance-certificate/energy-performance-certificate.component';
import { EnergeticCertificationComponent } from '../energetic-certification/energetic-certification.component';
import { DetailResumeComponent } from './detail-resume/detail-resume.component';
import { AdvertisementService } from '../advertisement.service';
import { Observable } from 'rxjs/Observable';
import { RealEstateModel } from '../models/real-estate.model';
import { RealEstateAssignmentModel } from '../models/real-estate-assignment.model';
import { ImageMiniModel } from '../models/image-mini.model';
import { CustomerService } from '../../customers/customer.service';


let NEW_TYPOLOGIES: Typology[] = [
  {
    IdTipologia: 1,
    Descrizione: 'Appartamento',
    TipiDestinazioneUso: [
      { IdDestinazioneUso: 1, Descrizione: 'Abitativo' },
      { IdDestinazioneUso: 16, Descrizione: 'Turistico' },
      { IdDestinazioneUso: 128, Descrizione: 'Ufficio' }
    ],
    IdDestUsoDefault: 1,
    IdSottotipDefault: 28,
    Sottotipologie: [
      { IdSottotipologia: 1, Descrizione: 'Loft' },
      { IdSottotipologia: 2, Descrizione: 'Mansarda' },
      { IdSottotipologia: 3, Descrizione: 'Attico' },
      { IdSottotipologia: 4, Descrizione: 'Duplex' },
      { IdSottotipologia: 28, Descrizione: 'Appartamento' }
    ]
  }
];

let TEST_DESTINAZIONI_USO: DestinazioneUso[] = [
  { IdDestinazioneUso: 1, Descrizione: 'Abitativo', Ordine: 1 },
  { IdDestinazioneUso: 2, Descrizione: 'Agricolo', Ordine: 2 },
  { IdDestinazioneUso: 4, Descrizione: 'EdificabileCommerciale', Ordine: 3 },
  { IdDestinazioneUso: 16, Descrizione: 'Turistico', Ordine: 4 },
  { IdDestinazioneUso: 32, Descrizione: 'Albergo', Ordine: 5 },
  { IdDestinazioneUso: 128, Descrizione: 'Ufficio', Ordine: 6 },
  { IdDestinazioneUso: 256, Descrizione: 'Laboratorio', Ordine: 7 },
  { IdDestinazioneUso: 1024, Descrizione: 'Commerciale', Ordine: 8 },
  { IdDestinazioneUso: 2048, Descrizione: 'Ristorante', Ordine: 9 },
  { IdDestinazioneUso: 8192, Descrizione: 'Industriale', Ordine: 10 }
];

let mockCustomerService = {
  readAll() { },
  readAllTypeAhead() { },
  readById() { },
  save() { },
}
let mockRealEstateService = {
  readById() { },
  getActiveAssignment() { },
  readAllImages() { }
}
let mockAdvService = {
  isListingPublished () { }
}

// class MockRealEstateService {
// }

// class MockAdvertisementService {
// }

describe('RealEstateDetailComponent', () => {
  let component: RealEstateDetailComponent;
  let fixture: ComponentFixture<RealEstateDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DetailGeneralComponent,
        DetailGeolocalComponent,
        DetailImagesComponent,
        DetailActivityComponent,
        DetailAssignmentComponent,
        DetailTipologyComponent,
        RealEstateDetailComponent,
        UploaderImagePreview,
        EnergyPerformanceCertificateComponent,
        EnergeticCertificationComponent,
        DetailResumeComponent,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AgmCoreModule.forRoot({ apiKey: 'TEST-API-KEY' }),
        RouterTestingModule,
        FileUploadModule,
        CustomPipesModule,
        GeolocalizationModule
      ],
      providers: [
        GeoService,
        { provide: TYPOLOGIES_LOOKUP, useValue: NEW_TYPOLOGIES },
        { provide: INTENDED_USE_LOOKUP, useValue: TEST_DESTINAZIONI_USO },
        LookupService,
        //{ provide: RealEstateService, useClass: MockRealEstateService },
        //{ provide: AdvertisementService, useClass: MockAdvertisementService }
        { provide: CustomerService, useValue: mockCustomerService },
        { provide: RealEstateService, useValue: mockRealEstateService },
        { provide: AdvertisementService, useValue: mockAdvService }
      ],
    });

    spyOn(mockRealEstateService, 'readById').and.returnValue(Observable.of(new RealEstateModel()));
    spyOn(mockRealEstateService, 'getActiveAssignment').and.returnValue(Observable.of(new RealEstateAssignmentModel()));
    spyOn(mockRealEstateService, 'readAllImages').and.returnValue(Observable.of(new Array<ImageMiniModel>()));
    spyOn(mockAdvService, 'isListingPublished').and.returnValue(Observable.of(true));

    fixture = TestBed.createComponent(RealEstateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

});