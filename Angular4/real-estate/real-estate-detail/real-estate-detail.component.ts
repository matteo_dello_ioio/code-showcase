
import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormControl, NgForm, FormGroup } from '@angular/forms';
import { MdTabChangeEvent, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { RealEstateService } from '../real-estate.service';
import { RealEstateModel } from '../models/real-estate.model';
import { TipologiaModel } from '../../models/tipologia';
import { LookupService } from '../../services/lookup.service';
import { DestinazioneUso } from '../../models/destinazione-uso';
import { TipologiaLookup } from '../../data/tipologie.data';
import { Collaborator } from '../models/collaborator.model';
import { Comune } from '../../models/comune';
import { Typology, SubTypology } from '../../shared/models/lookup.models';
import { RealEstateAssignmentModel } from '../models/real-estate-assignment.model';
import { ActivityLogRealEstateModel } from '../models/activity-log-real-estate.model';
import { forkJoin } from "rxjs/observable/forkJoin";
import { DetailGeolocalComponent } from './detail-geolocal/detail-geolocal.component';
import { ApartmentCardModel } from '../models/apartment-card.model';
import { EnergyPerformanceCertificateModel } from '../models/energy-performance-certificate.model';
import { AdvertisementService } from '../advertisement.service';



@Component({
  selector: 'app-real-estate-detail',
  styles: [],
  templateUrl: './real-estate-detail.component.html'
})
export class RealEstateDetailComponent implements OnInit, OnDestroy {

  public RealEstateFormGroup: FormGroup; // our form model

  @ViewChild(DetailGeolocalComponent) geolocalViewChild: DetailGeolocalComponent;

  isNew = false;

  isClicked: boolean = false;

  realEstate$: Observable<RealEstateModel>;
  realEstate: RealEstateModel;
  selectedTab: any;

  typologiesLookup: Typology[];
  subTypologiesLookup: SubTypology[];

  availableIntendedUseList: DestinazioneUso[];

  clientsCtrl: FormControl;

  private routeParamSubscription: any;

  selectedItem: {};
  searchText: '';


  multiInvioUrl = 'http://sp.piattaformaimmobiliare.com/iframe/project/widget_pubblicazione/pubblicazione.php?username=ProfessioneCasa543&password=5e0b049cec2e79f374419e3d7f384ddd';

  //assignment: RealEstateAssignmentModel;
  activities: ActivityLogRealEstateModel[];

  contractType: string;

  showPublicationControls: boolean;

  constructor(
    public snackBar: MdSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    public location: Location,
    private lookup: LookupService,
    private realEstateService: RealEstateService,
    private advService: AdvertisementService,
  ) {
    this.clientsCtrl = new FormControl();
  }


  ngOnInit() {
    //console.log("##" + this.route.snapshot.data.isNew);
    this.isNew = this.route.snapshot.data.isNew;
    this.typologiesLookup = this.lookup.getTipologieList();

    this.route.data.subscribe((routeData) => {
      //console.log("CONTRACTTYPE: " + JSON.stringify(routeData));

      switch (routeData.contractType) {
        case 'rent':
          {
            this.contractType = 'rent';
            break;
          }
        case 'sell':
          {
            this.contractType = 'sell';
            break;
          }
      }
    });

    if (this.isNew === true) {
      this.realEstate = new RealEstateModel();
      this.realEstate.ApartmentCard = new ApartmentCardModel();
      this.realEstate.EnergyPerformanceCertificate = new EnergyPerformanceCertificateModel();
    } else {
      this.routeParamSubscription = this.route.params.subscribe(params => {
        let id = +params['id']; // (+) converts string 'id' to a number
        this.getRealEstateById(id);
      });
    }
  }

  ngOnDestroy() {
    if (this.routeParamSubscription) {
      this.routeParamSubscription.unsubscribe();
    }
  }

  getDataFromChild(event: Event) {
    this.selectedTab = event;
  }

  goback() {
    this.location.back();
  }

  openSnackBar(message: string, action?: string, stat?: string) {
    let config = new MdSnackBarConfig();
    config.duration = 2000;
    if (stat) config.extraClasses = [stat];
    this.snackBar.open(message, action, config);
  }

  showAdvertisementPortals(): boolean {
    return (!this.isNew) && this.realEstate.Status == 2;
  }

  getRealEstateById(id: number) {
    let realEstateObservable = this.realEstateService.readById(id);
    let assignmentObservable = this.realEstateService.getActiveAssignment(id);
    let imageObservable = this.realEstateService.readAllImages(id);



    let wikiPublishStateObservable = this.advService.isListingPublished(id, 2276);
    let grimaldiPublishStateObservable = this.advService.isListingPublished(id, 537);


    forkJoin(
      realEstateObservable,
      assignmentObservable,
      imageObservable,
      wikiPublishStateObservable,
      grimaldiPublishStateObservable
    )
      .subscribe(results => {

        let realEstate = results[0];
        let assignment = results[1];
        let images = results[2];
        let publishedOnWiki = results[3];
        let publishedOnGrimaldi = results[4];

        realEstate.ActiveAssignment = assignment;
        realEstate.Images = images;
        realEstate.PublishedToWikiCasa = publishedOnWiki;
        realEstate.PublishedToGrimaldi = publishedOnGrimaldi;
        this.realEstate = realEstate;

        /**
         * !!! IMPORTANT !!!
         * This variable is updated ONLY HERE, in order to DO NOT display controls
         * when user change the Status. In this case, controls will be displayed only after save.
         */
        this.showPublicationControls = this.showAdvertisementPortals();

        if (!this.realEstate.ApartmentCard)
          this.realEstate.ApartmentCard = new ApartmentCardModel();
        if (!this.realEstate.EnergyPerformanceCertificate)
          this.realEstate.EnergyPerformanceCertificate = new EnergyPerformanceCertificateModel();

        this.onTypologyChanged();
      });
  }

  onTypologyChanged() {
    if (!this.realEstate.IdTypology)
      this.subTypologiesLookup = this.lookup.getSottotipologieList();
    else
      this.subTypologiesLookup = this.lookup.getSottotipologieListByIdTipologia(this.realEstate.IdTypology);

    this.availableIntendedUseList = this.lookup.getTipoDestinazioneUsoListByIdTipologia(this.realEstate.IdTypology);
  }

  showTypologyComponent(): boolean {
    return (this.realEstate.IdTypology == 1 || this.realEstate.IdTypology == 6 || this.realEstate.IdTypology == 16);
  }

  onSave(realEstateForm: NgForm) {

    //console.log(this.findInvalidControls(realEstateForm));
    if (!realEstateForm.valid)
      return;

    this.isClicked = true;

    console.log('saving data...' + JSON.stringify(this.realEstate));
    this.realEstateService.save(this.realEstate).subscribe(result => {
      this.openSnackBar('Salvato con successo', undefined, 'success');

      this.isClicked = false;
      
      if(result.IsForSell)
        this.router.navigateByUrl('app/real-estates/lista-vendita/' + JSON.stringify(result.ID));

      if(result.IsForRent)
        this.router.navigateByUrl('app/real-estates/lista-affitto/' + JSON.stringify(result.ID));
    },
      (err: string) => {
        this.isClicked = false;
        this.openSnackBar(err, undefined, 'error');
      });
  }

  /**
   * utility function for debug purpose
   * @param form
   */
  findInvalidControls(form: NgForm) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(controls[name]);
      }
    }
    return invalid;
  }


  tabChanged(event: MdTabChangeEvent) {

    switch (event.index) {
      case 1:
        {
          //console.log("CLICK LOCALIZZAZIONE");
          this.geolocalViewChild.change(this.realEstate);
          // Workaround for Google Maps glitch render
          var resizeEvent = new Event('resize');
          window.dispatchEvent(resizeEvent);
          break;
        }
      case 2:
        {
          //console.log("CLICK INCARICHI");
          if (!this.isNew)
            this.getAssignmentList();
          break;
        }
      case 3:
        {
          if (!this.isNew) {
            console.log("CLICK IMMAGINI");
            this.getImageList();
          }
          break;
        }
      case 4:
        {
          console.log("CLICK ATTIVITA");
          if (!this.isNew) {
            this.getActivityList();
          }
          break;
        }
    }
  }

  /**
   * recupera l'incarico attivo associato
   */
  getAssignmentList(): void {
    this.realEstateService.getActiveAssignment(this.realEstate.ID)
      .subscribe(assignment => {
        //console.log(assignment);
        //this.assignment = assignment;
        this.realEstate.ActiveAssignment = assignment;
      });
  }

  getImageList(): void {
    this.realEstateService.readAllImages(this.realEstate.ID)
      .subscribe(images => {
        //console.log(images);
        //this.images = images;
      });
  }

  getActivityList(): void {
    this.realEstateService.getActivityList(this.realEstate.ID)
      .subscribe(activities => {
        //console.log(activities);
        this.activities = activities;
      });
  }

}
