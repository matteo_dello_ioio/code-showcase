import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { RealEstateService } from '../../real-estate.service';
import { ImageModel } from '../../models/image.model';
import { ImageMiniModel } from '../../models/image-mini.model';
import { MdSnackBarConfig, MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-detail-images',
  styles: [],
  templateUrl: './detail-images.component.html',
})

export class DetailImagesComponent implements OnInit {

  @Input() childData: any;
  @Output() childVarChange = new EventEmitter();

  public uploader: FileUploader;

  imageType: number;
  imageDescription: string;

  constructor(
    public snackBar: MdSnackBar,
    private realEstateService: RealEstateService
  ) {
    this.uploader = new FileUploader({}/*{ url: 'https://evening-anchorage-3159.herokuapp.com/api/' }*/); // <-- Change to our API webpoint
  }

  ngOnInit() {
    // this.uploader.onBeforeUploadItem = f => {
    // }

    // this.uploader.onAfterAddingFile = f => {
    //   if (this.uploader.queue.length > 1) {
    //     this.uploader.removeFromQueue(this.uploader.queue[0]);
    //   }
    // }

    // this.uploader._onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //   this.uploader.progress = 0;
    // }

    if (this.childData.Images) {
      this.childData.Images.forEach((ref, i, array) => {

        let imageData = this.realEstateService.getBlobFromUrl(ref.Url)
          .subscribe((blob) => {
            let imgFile = new File([blob], ref.Name)

            let dummy = new FileItem(this.uploader, imgFile, { url: ref.Url });
            dummy.headers.ID = ref.ID;
            dummy._file = imgFile;
            dummy.progress = 100;
            dummy.isUploaded = true;
            dummy.isSuccess = true;
            this.uploader.queue.push(dummy);
          });
      });
    }

  }

  change(newValue) {
    this.childData = newValue;
    this.childVarChange.emit(newValue);
  }

  openSnackBar(message: string, action?: string, stat?: string) {
    let config = new MdSnackBarConfig();
    config.duration = 2000;
    if (stat) config.extraClasses = [stat];
    this.snackBar.open(message, action, config);
  }

  saveImages() {

    let fileCount: number = this.uploader.queue.length;
    if (fileCount > 0) {

      this.uploader.queue.forEach((val, i, array) => {

        let fileReader = new FileReader();
        fileReader.onloadend = (e) => {
          let imageData = fileReader.result;//e.target.result;
          let rawData = imageData.split("base64,");

          if (rawData.length > 1) {
            rawData = rawData[1];

            //console.log("raw data: " + rawData);

            let image = new ImageModel();
            image.IdImmobile = this.childData.ID;
            image.BinaryData = rawData;//btoa(rawData);
            image.Didascalia = this.imageDescription;
            image.TipoImmagine = this.imageType;
            image.Nome = val.file.name;

            this.realEstateService.saveImage(image)
              .subscribe((response) => {
                console.log(response);
              });
          }
        }
        //fileReader.readAsText(val._file, "UTF-8");
        fileReader.readAsDataURL(val._file);

      });
    }

  }

  onRemove(item: FileItem) {
    console.log(item);

    if (!item.headers.ID) {
      item.remove();
    } else {
      this.realEstateService.deleteImageById(item.headers.ID)
        .subscribe((blob) => {
          item.remove();

          this.openSnackBar('Immagine rimossa con successo', undefined, 'success');
          setTimeout(() => {
            //this.goback();
          }, 3000);
        },
        (err: string) => {
          this.openSnackBar(err, undefined, 'error');
        });
    }
  }
}