
import { Component, Input, forwardRef } from '@angular/core';

interface DetailResume {
  TypologyDescription: string;
  SubTypologyDescription: string;
  Area: number;
  AddressDescription: string;
  MunicipalityName: string;
  ContractType: string;
  Price: number;
}

@Component({
  selector: 'detail-resume',
  styles: [],
  templateUrl: './detail-resume.component.html',
})
export class DetailResumeComponent {

  @Input() TypologyDescription: string;
  @Input() SubTypologyDescription: string;
  @Input() Area: number;
  @Input() AddressDescription: string;
  @Input() MunicipalityName: string;
  @Input() ContractType: string;
  @Input() Price: number;

}