import { Component, Input, Output, EventEmitter, OnInit, forwardRef, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ControlContainer, NgForm } from '@angular/forms';
import { MdCheckboxChange, MdSnackBarConfig, MdSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { QueryObjectDTO } from 'treeplat-mobile-rest-client';
import { Typology, SubTypology } from '../../../shared/models/lookup.models';
import { LookupService } from '../../../services/lookup.service';
import { DestinazioneUso } from '../../../models/destinazione-uso';
import { CurrencyFormatPipe } from '../../../custom-pipes/currency-format.pipe';
import { Comune } from '../../../models/comune';
import { AddressModel } from '../../../shared/models/address-model';
import { CustomerService } from '../../../customers/customer.service';
import { CustomerItem } from '../../../customers/models/customer-item';
import { CustomerMiniModel } from '../../../shared/models/customer-mini-model';
import { AdvertisementService } from '../../advertisement.service';
import { RealEstateModel } from '../../models/real-estate.model';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';



export function createGeneralDataValidator() {

  return function validateGeneralData(c: FormControl) {

    let result = null; // null => no errors

    let err = null;/*{
      requiredError: {
        gno: c.value
      }
    };*/


    if (c.value !== null) {

      if (!c.value.Status)
        return err;

      if (!c.value.IdTypology)
        return err;


      if (
        !Boolean(c.value.Address)
        ||
        !Boolean(c.value.Address.Description)
        ||
        !Boolean(c.value.Address.CivicNumber)
        ||
        !Boolean(c.value.Address.PostalCode)
        ||
        !Boolean(c.value.Address.Municipality)
      )
        return err;

      if (!c.value.IntendedUses) {
        return err;
      }
      result = null;
    } else {
      result = err;
    }
    return result;
  }
}


@Component({
  selector: 'app-detail-general',
  styles: [],
  templateUrl: './detail-general.component.html',
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DetailGeneralComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DetailGeneralComponent),
      multi: true,
    }
  ]
})

export class DetailGeneralComponent implements ControlValueAccessor, OnInit {

  public childData: RealEstateModel;

  propagateChange = (_: any) => { };
  onTouched = () => { };

  validateFn: Function;

  validate(c: FormControl) {
    return this.validateFn(c);
  }
  writeValue(value: any): void {
    //throw new Error("Method not implemented.");
    this.childData = value;
    this.updateTypologyDependant();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    //throw new Error("Method not implemented.");
  }


  /**
   * 
   */
  get status() {
    return (this.childData) ? this.childData.Status : null;
  }
  set status(value) {
    this.childData.Status = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get typology() {
    return (this.childData) ? this.childData.IdTypology : null;
  }
  set typology(value) {
    this.childData.IdTypology = value;
    //this.onTypologyChanged();
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get subtypology() {
    return (this.childData) ? this.childData.IdSubTypology : null;
  }
  set subtypology(value) {
    this.childData.IdSubTypology = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get area() {
    return this.childData.Area;
  }
  set area(value) {
    this.childData.Area = value;
    this.propagateChange(this.childData);
  }


  /**
   * 
   */
  get intendedUseBitmask() {
    return this.childData.IntendedUseBitMask;
  }
  set intendedUseBitmask(value) {
    this.childData.IntendedUseBitMask = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get address() {
    return this.childData.Address.Description;
  }
  set address(value) {
    this.childData.Address.Description = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get civicNumber() {
    return this.childData.Address.CivicNumber;
  }
  set civicNumber(value) {
    this.childData.Address.CivicNumber = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get postalCode() {
    return this.childData.Address.PostalCode;
  }
  set postalCode(value) {
    this.childData.Address.PostalCode = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get latitude() {
    return this.childData.Address.Latitude;
  }
  set latitude(value) {
    this.childData.Address.Latitude = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get longitude() {
    return this.childData.Address.Longitude;
  }
  set longitude(value) {
    this.childData.Address.Longitude = value;
    this.propagateChange(this.childData);
  }

  /**
   * This method is used for form validation. A hidden field is bound to this property to trigger validation easily.
   */
  get contractType() {
    let value = null;
    let _values = [];
    if (this.childData.IsForSell)
      _values.push('sell');
    if (this.childData.IsForRent)
      _values.push('rent');

    if (_values && _values.length > 0)
      value = _values.join('|');
    return value;
  }
  set contractType(value) {
    this.childData.IsForSell = false;
    this.childData.IsForRent = false;

    let values = value.split('|');
    if (values.findIndex((el) => { el == 'sell' }) != -1)
      this.childData.IsForSell = true;
    if (values.findIndex((el) => { el == 'rent' }) != -1)
      this.childData.IsForRent = true;

    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get isForSell() {
    return this.childData.IsForSell;
  }
  set isForSell(value) {
    this.childData.IsForSell = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get sellPrice() {
    return this.childData.SellInformation.Price;
  }
  set sellPrice(value) {
    if (!value)
      return;
    this.childData.SellInformation.Price = value;
    this.propagateChange(this.childData);
  }


  /**
   * 
   */
  get isForRent() {
    return this.childData.IsForRent;
  }
  set isForRent(value) {
    this.childData.IsForRent = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get rentPrice() {
    return this.childData.RentInformation.Price;
  }
  set rentPrice(value) {
    if (!value)
      return;
    this.childData.RentInformation.Price = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get isBareOwnership() {
    return this.childData.IsBareOwnership;
  }
  set isBareOwnership(value) {
    this.childData.IsBareOwnership = value;
    this.propagateChange(this.childData);
  }

  /**
   * 
   */
  get bareOwnershipPrice() {
    return this.childData.BareOwnershipInformation.Price;
  }
  set bareOwnershipPrice(value) {
    if (!value)
      return;
    this.childData.BareOwnershipInformation.Price = value;
    this.propagateChange(this.childData);
  }



  @Input('group')
  public adressForm: FormGroup;

  //@Input() childData: any;
  @Output() childVarChange = new EventEmitter();

  typologiesLookup: Typology[];
  subTypologiesLookup: SubTypology[];

  availableIntendedUseList: DestinazioneUso[];

  citiesCtrl: FormControl;
  filteredCities: any;

  customersCtrl: FormControl;
  filteredCustomers: any;
  selectedCustomer: any;


  isNew: boolean = false;
  showNewPrice: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private lookup: LookupService,
    private customerService: CustomerService,
    private advService: AdvertisementService,
    public snackBar: MdSnackBar,
    private cdref: ChangeDetectorRef
  ) {
    this.citiesCtrl = new FormControl();
    this.filteredCities = this.citiesCtrl.valueChanges
      .pipe(
      startWith({} as Comune),
      //map(city => (city && typeof city === 'object') ? (city as any).P : city),
      map(name => name && typeof name === 'string' ? this.searchCity(name) : []/*this.cities.slice()*/)
      );

    this.customersCtrl = new FormControl();
  }

  ngOnInit() {
    this.isNew = Boolean(this.route.snapshot.data.isNew);

    if (this.isNew)
      this.childData = new RealEstateModel();

    this.validateFn = createGeneralDataValidator();


    this.typologiesLookup = this.lookup.getTipologieList();
    this.updateTypologyDependant();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.isNew)
        this.setDefaultValues();
    }, 100)
  }

  onPublishPortalChange(event: MdCheckboxChange) {
    console.log(event);
    let observable: Observable<boolean> = null;
    if (event.checked) {
      observable = this.advService.publish(this.childData, +event.source.value)
    } else {
      observable = this.advService.unpublish(this.childData, +event.source.value);
    }

    observable.subscribe(response => {
      this.openSnackBar('Annuncio pubblicato con successo', undefined, 'success');
    },
      (err: string) => {
        this.openSnackBar(err, undefined, 'error');
      });
  }


  change(newValue) {
    this.childData = newValue;
    this.childVarChange.emit(newValue);
    this.updateTypologyDependant();
  }

  updateTypologyDependant() {
    if (Boolean(this.childData) && Boolean(this.childData.IdTypology)) {
      this.subTypologiesLookup = this.lookup.getSottotipologieListByIdTipologia(this.childData.IdTypology);
      this.availableIntendedUseList = this.lookup.getTipoDestinazioneUsoListByIdTipologia(this.childData.IdTypology);
    }
  }

  setDefaultValues() {
    this.typology = 1;
    this.updateTypologyDependant();
    this.subtypology = 28;
    this.isForSell = true;
    this.cdref.detectChanges();
  }

  onTypologyChanged() {
    this.subtypology = null;
    this.updateTypologyDependant();


    /**
     * WARNING! 
     * I added this to get rid of this bug, ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
     * 
     * The problem happened because of the condition Subtypology.hasError('required') in template.
     */
    this.cdref.detectChanges();
  }

  onSubTypologyChanged() {
    //this.availableIntendedUseList = this.realEstate.
  }

  onCustomerSearchChanged(event) {
    console.log(this.customersCtrl.value);
    this.searchCustomer(this.customersCtrl.value);
  }

  isIntendedUseSelected(id: number): boolean {
    let result = this.childData.IntendedUses.find(iu => {
      return iu.IdDestinazioneUso === id;
    });

    return result != undefined;
  }

  onIntendedUsesChange(event: MdCheckboxChange): void {
    //console.log(event);
    if (event.checked) {
      let selectedIntendedUse: DestinazioneUso = this.lookup.DestinazioniUso.find((el) => { return el.IdDestinazioneUso == +event.source.value; });
      this.childData.IntendedUses.push(selectedIntendedUse)
    } else {
      let removeAtIndex: number = this.childData.IntendedUses.findIndex((el) => { return el.IdDestinazioneUso == +event.source.value; })
      this.childData.IntendedUses.splice(removeAtIndex, 1);
    }

    this.intendedUseBitmask = this.destinationUseArray2Int(this.childData.IntendedUses);
  }

  searchCity(name: string): Comune[] {
    return this.lookup.findCityByName(name);
  }

  searchCustomer(name: string)/*: CustomerItem[]*/ {
    this.customerService.readAllTypeAhead(name)
      .subscribe((response) => {
        this.filteredCustomers = response as CustomerMiniModel[];
      }, (err: Error) => {
        console.log("ERROR!");
        return null;
      });
  }

  onCustomerSelected(event: CustomerMiniModel) {
    //console.log(event);
    if (!event || typeof event === 'string')
      return;
    if (!this.childData.Customers)
      this.childData.Customers = new Array<CustomerMiniModel>();
    this.childData.Customers.push(event);

    this.filteredCustomers.splice();
    this.customersCtrl.setValue(null);
  }

  displayFn(obj: any) {
    if (obj)
      return obj.Name;
  }

  displayCustomerFn(obj: any) {
    if (obj)
      return obj.Description;
  }

  calculateCommission(price: number, percentuale: number): number {
    let result = price * percentuale / 100;
    return (!isNaN(result) ? result : null);
  }

  onMunicipalityChanged(event: Comune): void {
    //console.log(event);
    this.childData.Address.Municipality = event;
  }


  onDeleteCustomer(customerID: number) {
    console.log("delete customer " + customerID);

    let customers: CustomerMiniModel[] = this.childData.Customers;
    let index: number = customers.findIndex((c) => {
      return c.ID == customerID;
    });

    this.childData.Customers.splice(index, 1);
  }


  openSnackBar(message: string, action?: string, stat?: string) {
    let config = new MdSnackBarConfig();
    config.duration = 2000;
    if (stat) config.extraClasses = [stat];
    this.snackBar.open(message, action, config);
  }

  isSubTypologyRequired(): boolean {
    return this.lookup.getSottotipologieListByIdTipologia(this.childData.IdTypology).length > 1;
  }


  /**
   * create a bit mask from destination use id's array 
   * @param intendedUses 
   */
  destinationUseArray2Int(intendedUses: DestinazioneUso[]) {
    let result = 0;
    intendedUses.forEach((value, index, array) => {
      if (Boolean(value))
        result += value.IdDestinazioneUso;
    });

    if (result == 0)
      result = null; // return NULL instead of zero in order to trigger validation

    return result;
  }


  goToLocalizeTab() {
    this.childVarChange.emit(1);
    window.scrollTo(document.body.scrollHeight, 0);
  }
}