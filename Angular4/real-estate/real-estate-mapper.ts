
import { Injectable } from '@angular/core';
import {
    TipoTelefono,
    ImmobileDTO,
    ImmobileElencoDTO,
    ImmobileSaveRequest,
    ImmobileCmd,
    ImmobileSchedaAppartamentoDTO,
    AttivitaStoricoImmobileDTO,
    ImmagineSaveRequest,
    ImmagineCmd,
    ImmagineDTO,
    IndirizzoCmd,
    IncaricoCmd,
    ImmobileIncaricoElencoDTO,
    CollaboratoreIncaricoCmd,
    IncaricoClienteReferenteCmd, ImmobileClasseEnergeticaNewDTO,
    GestioneDTO,
    GestioneMiniDTO,
    ImmagineMiniDTO,
    ImmagineDataDTO
} from 'treeplat-mobile-rest-client';
import { UtilityService } from '../shared/utility-service'
import { CommonsMapper } from '../shared/commons-mapper';
import { AddressModel } from '../shared/models/address-model';
import { PhoneNumberModel } from '../shared/models/phone-number-model';
import { CustomerMiniModel } from '../shared/models/customer-mini-model';
import { RealEstateModel /*SellInformation, RentInformation, BareOwnershipInformation*/ } from './models/real-estate.model';
import { RealEstateItem } from './models/real-estate-item';
import { LookupService } from '../services/lookup.service';
import { environment } from '../../environments/environment';
import { Collaborator } from './models/collaborator.model';
import { ApartmentCardModel } from './models/apartment-card.model';
import { GeoService } from '../services/geo.service';
import { RealEstateAssignmentModel } from './models/real-estate-assignment.model';
import { DestinazioneUso } from '../models/destinazione-uso';
import { ImageModel } from './models/image.model';
import { ActivityLogRealEstateModel } from './models/activity-log-real-estate.model';
import { ManagementModel } from './models/management-model';
import { ManagementMiniModel } from '../shared/models/management-mini.model';
import { CollaboratorMiniModel } from '../shared/models/collaborator-mini.model';
import { ImageMiniModel } from './models/image-mini.model';
import { ImageDataModel } from './models/image-data.model';

import { CommercialInformation } from './models/commercial-information.model';
import { EnergyPerformanceCertificateModel } from './models/energy-performance-certificate.model';
import { EnergeticCertificationModel } from './models/energetic-certification.model';




@Injectable()
export class RealEstateMapper {

    constructor(
        private lookup: LookupService,
        private utility: UtilityService,
        private geoService: GeoService,
        private commonsMapper: CommonsMapper
    ) {

    }


    // *** mapping for reading objects ***  
    immagineMiniDTO2ImageMiniModel(dto: ImmagineMiniDTO): ImageMiniModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ImageMiniModel();
        model.ID = dto.IdEntita;
        model.CreationDate = dto.DataImmissione;
        model.Caption = dto.Didascalia;
        if (dto.Immaginedata)
            model.Data = this.immagineDataDTO2ImageDataModel(dto.Immaginedata);
        model.Width = dto.Larghezza;
        model.Height = dto.Lunghezza;
        model.Name = dto.Nome;
        model.OrderIndex = dto.Ordine;
        model.ImageType = dto.TipoImmagine;
        model.Url = dto.Url;
        model.UrlThumb = dto.UrlThumb;

        return model;
    }

    immagineDataDTO2ImageDataModel(dto: ImmagineDataDTO): ImageDataModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ImageDataModel();
        model.IdImmagine = dto.IdImmagine;
        model.Data = dto.Data;
        model.DataThumb = dto.DataThumb;
        return model;
    }


    /**
     * 
     * @param dto 
     */
    mapImmobileElencoDTO2RealEstateItem(dto: ImmobileElencoDTO): RealEstateItem {

        if (!dto)
            throw new Error("argument cannot be null");

        let typology = this.lookup.getTipologiaByIdTipologia(dto.IdTipologia);
        let subtypology = this.lookup.getSottotipologiaByIdSottotipologia(dto.IdSottotipologia);
        let destUsoArray = this.lookup.getTipoDestinazioneUsoListFromBitMask(dto.TipoDestinazioneUso);


        let model = new RealEstateItem();
        model.ID = dto.IdEntita;
        if (!typology)
            model.Typology = '-';
        else
            model.Typology = typology.Descrizione;

        if (!subtypology)
            model.SubTypology = '-';
        else
            model.SubTypology = subtypology.Descrizione;

        if (dto.Vendita) {
            //model.Price = dto.PrezzoVendita;
            model.SellInformation = new CommercialInformation();
            model.SellInformation.Price = dto.PrezzoVendita;
            model.SellInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneVendita);
        }

        if (dto.Locazione) {
            //model.Price = dto.PrezzoLocazione;
            model.RentInformation = new CommercialInformation();
            model.RentInformation.Price = dto.PrezzoLocazione;
            model.RentInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneLocazione);
        }

        if (dto.NudaProprieta) {
            //model.Price = dto.PrezzoNudaProprieta;
            model.BareOwnershipInformation = new CommercialInformation();
            model.BareOwnershipInformation.Price = dto.PrezzoNudaProprieta;
            model.BareOwnershipInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneNudaProprieta);
        }




        model.MainImage = dto.ImmaginePrincipale;
        model.ImagesCount = dto.ImageCount;
        model.RoomNumbers = dto.NumeroLocali;
        model.BathroomNumbers = dto.NumeroBagni;
        model.Area = dto.Mq;

        model.Address = new AddressModel();
        model.Address.ID = dto.Indirizzo.IdEntita;
        model.Address.Description = dto.Indirizzo.Descrizione;
        model.Address.CivicNumber = dto.Indirizzo.Civico;
        model.Address.PostalCode = dto.Indirizzo.Cap;
        model.Address.IdCity = dto.Indirizzo.IdCitta;
        model.Address.Municipality = this.geoService.getComuneByIdComune(dto.Indirizzo.IdComune);

        if (destUsoArray) {
            //destUsoArray.forEach((val, i, array)=> {
            model.IntendedUsesDescriptions = destUsoArray.map((el) => { return el.Descrizione }).join(', ');
            //                model.IntendedUsesDescriptions.push(val.Descrizione);
            //});

        }
        return model;
    }

    /**
     * 
     * @param dto map ImmobileDTO -> RealEstateModel
     */
    immobileDTO2RealEstateModel(dto: ImmobileDTO): RealEstateModel {

        if (!dto)
            throw new Error("argument cannot be null");

        let typology = this.lookup.getTipologiaByIdTipologia(dto.IdTipologia);
        let subtypology = this.lookup.getSottotipologiaByIdSottotipologia(dto.IdSottotipologia);

        let model = new RealEstateModel();
        // metadata
        model.ID = dto.IdEntita;
        model.IdGestione = dto.IdGestione;
        model.Agency = { Id: dto.IdAgenzia, Name: dto.NomeAgenzia };
        model.Group = { Id: dto.IdGruppo, Name: dto.NomeGruppo };
        model.ManagementType = dto.TipoGestione;
        model.Description = dto.Descrizione;
        model.ControlCode = dto.ControlCode;

        // data
        model.ReferralCode = dto.CodiceRiferimento;
        model.IdTypology = dto.IdTipologia;
        model.IdSubTypology = dto.IdSottotipologia;
        if (typology)
            model.TypologyDescription = typology.Descrizione;

        if (subtypology)
            model.SubTypologyDescription = subtypology.Descrizione;

        model.IdParent = dto.IdPadre;
        model.Area = dto.Mq;


        // sell
        model.IsForSell = dto.Vendita;
        model.SellInformation = new CommercialInformation();
        model.SellInformation.Price = dto.PrezzoVendita;
        model.SellInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneVendita);

        // rent
        model.IsForRent = dto.Locazione;
        model.RentInformation = new CommercialInformation();
        model.RentInformation.Price = dto.PrezzoLocazione;
        model.RentInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneLocazione);

        // bare ownership
        model.IsBareOwnership = dto.NudaProprieta;
        model.BareOwnershipInformation = new CommercialInformation();
        model.BareOwnershipInformation.Price = dto.PrezzoNudaProprieta;
        model.BareOwnershipInformation.VariationDate = this.utility._JSON2Date_(dto.DataVariazioneNudaProprieta);


        model.DescriptionShort = dto.DescrizioneSintetica;
        model.DescriptionLong = dto.DescrizioneRiassuntiva;
        if (dto.Indirizzo) {
            model.Address = this.commonsMapper.addressDTO2AddressModel(dto.Indirizzo);
        }
        model.Notes = { Private: dto.NotePrivate, Public: dto.NotePubbliche };
        model.Status = dto.Stato;
        model.ProposalStatus = dto.StatoProposta;
        model.IntendedUseBitMask = dto.TipoDestinazioneUso;
        model.IntendedUses = this.lookup.getTipoDestinazioneUsoListFromBitMask(dto.TipoDestinazioneUso);
        model.HasVirtualTour = dto.VirtualTour;
        model.VirtualTourUrl = dto.VirtualTourUrl;
        model.CodiceImmobileMandante = dto.CodiceImmobileMandante;
        if (dto.Gestionemandante) {
            model.ClientManagement = this.gestioneDTO2ManagementModel(dto.Gestionemandante);
        }
        if (dto.GestionemandanteSub) {
            model.SubClientManagement = this.gestioneMiniDTO2ManagementMiniModel(dto.GestionemandanteSub);
        }
        model.IsSplitted = dto.IsFrazionato;
        model.CodiceFrazionato = dto.CodiceFrazionato;
        model.InSubincarico = dto.InSubincarico;

        if (dto.SchedeImmobili) {
            if (dto.SchedeImmobili.SchedaAppartamento) {
                model.ApartmentCard = this.immobileSchedaAppartamentoDTO2ApartmentCardModel(dto.SchedeImmobili.SchedaAppartamento);
            }
        }

        if (dto.Clienti) {
            model.Customers = new Array<CustomerMiniModel>();
            dto.Clienti.forEach((ref, i, array) => {
                let clienteMini = this.commonsMapper.clienteMiniDTO2CustomerMiniModel(ref.Cliente);
                model.Customers.push(clienteMini);
            });
        }

        if (dto.Collaboratori) {
            model.Collaborators = new Array<CollaboratorMiniModel>();
            dto.Collaboratori.forEach((ref, i, array) => {
                let collaboratorMini = this.commonsMapper.collaboratoreMiniDTO2CollaboratorMiniModel(ref);
                model.Collaborators.push(collaboratorMini);
            });
        }

        model.InsertDate = this.utility._JSON2Date_(dto.DataInserimento);
        return model;
    }

    /**
     * 
     * @param dto 
     */
    immobileIncaricoElencoDTO2RealEstateAssignmentModel(dto: ImmobileIncaricoElencoDTO): RealEstateAssignmentModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new RealEstateAssignmentModel();
        model.Active = dto.Attivo;
        model.Description = dto.Descrizione;
        model.Notes = dto.Note;
        model.StatoPrimoContatto = dto.StatoPrimoContatto;
        model.FirstExpirationDate = this.utility._JSON2Date_(dto.DataPrimaScadenza);
        model.SecondExpirationDate = this.utility._JSON2Date_(dto.DataSecondaScadenza);
        model.ThirdExpirationDate = this.utility._JSON2Date_(dto.DataTerzaScadenza);
        model.StartDate = this.utility._JSON2Date_(dto.DataInizio);
        model.Status = dto.StatoIncarico;
        model.Type = dto.TipoIncarico;
        model.Sell = dto.Vendita;
        model.SellPrice = dto.PrezzoVendita;
        model.SellCommission = dto.ProvvigioneVendita;
        model.Rent = dto.Locazione;
        model.RentPrice = dto.PrezzoLocazione;
        model.RentCommission = dto.ProvvigioneAffitto;
        model.BareOwnership = dto.NudaProprieta;
        model.BareOwnershipPrice = dto.PrezzoNudaProprieta;
        model.BareOwnershipCommission = dto.ProvvigioneNudaProprieta;
        model.AutoRenew = dto.RinnovoAutomatico;
        return model;
    }

    /**
     * 
     * @param dto 
     */
    immobileSchedaAppartamentoDTO2ApartmentCardModel(dto: ImmobileSchedaAppartamentoDTO): ApartmentCardModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ApartmentCardModel();
        model.ID = dto.IdEntita;
        model.ControlCode = dto.ControlCode;
        model.HasWaterSupplyConnection = this.commonsMapper.ndYesNo2Boolean(dto.Acqua);
        model.HasTelephoneNetworkConnection = this.commonsMapper.ndYesNo2Boolean(dto.AllaccioTelefonico);
        model.ConstructionYear = dto.AnnoCostruzione;
        model.RenovationYear = dto.AnnoRistrutturazione;
        model.HasAlarm = this.commonsMapper.ndYesNo2Boolean(dto.Antifurto);
        model.HasAntiFire = this.commonsMapper.ndYesNo2Boolean(dto.Antincendio);
        model.HasForniture = this.commonsMapper.ndYesNo2Boolean(dto.Arredato);
        model.HasElevator = this.commonsMapper.ndYesNo2Boolean(dto.Ascensore);
        model.HasInternetCabling = this.commonsMapper.ndYesNo2Boolean(dto.CablaggioInternet);
        model.HasServiceRoom = this.commonsMapper.ndYesNo2Boolean(dto.CameraDiServizio);
        model.HasChimney = this.commonsMapper.ndYesNo2Boolean(dto.Camino);
        model.HasCellar = this.commonsMapper.ndYesNo2Boolean(dto.Cantina);
        model.HasIntercom = this.commonsMapper.ndYesNo2Boolean(dto.Citofono);
        model.HasDrainage = this.commonsMapper.ndYesNo2Boolean(dto.Fognatura);
        model.HasGasNetworkConnection = this.commonsMapper.ndYesNo2Boolean(dto.Gas);
        model.HasCommonGarden = this.commonsMapper.ndYesNo2Boolean(dto.GiardinoCondominiale);
        model.HasPrivateGarden = this.commonsMapper.ndYesNo2Boolean(dto.GiardinoPrivato);
        model.HasCompliantElectricalSystem = this.commonsMapper.ndYesNo2Boolean(dto.ImpiantoElettricoANorma);
        model.HasHeatingSystem = this.commonsMapper.ndYesNo2Boolean(dto.ImpiantoRiscaldamento);
        model.Interno = dto.Interno;
        model.Luce = dto.Luce;
        model.Mansarda = dto.Mansarda;
        model.BalconyArea = dto.MqBalconi;
        model.GarageArea = dto.MqBox;
        model.CellarArea = dto.MqCantina;
        model.CommonGardenArea = dto.MqGiardinoCondominiale;
        model.PrivateGardenArea = dto.MqGiardinoPrivato;
        model.MansardArea = dto.MqMansarda;
        model.LoftArea = dto.MqSolaio;
        model.MezzanineArea = dto.MqSoppalco;
        model.TavernArea = dto.MqTaverna;
        model.TerraceArea = dto.MqTerrazzi;
        model.PorchArea = dto.MqVerande;
        model.ApartmentsCount = dto.NumeroAppartamenti;
        model.BathroomsCount = dto.NumeroBagni;
        model.BalconyCount = dto.NumeroBalconi;
        model.GaragesCount = dto.NumeroBox;
        model.BuildingGaragesCount = dto.NumeroBoxStabile;
        model.BedroomsCount = dto.NumeroCamereDaLetto;
        model.EntrancesCount = dto.NumeroIngressi;
        model.NumeroIngressiStabile = dto.NumeroIngressiStabile;
        model.LevelsCount = dto.NumeroLivelli;
        model.RoomsCount = dto.NumeroLocali;
        model.NumeroPassiCarrabili = dto.NumeroPassiCarrabili;
        model.NumeroPassiCarrabiliStabile = dto.NumeroPassiCarrabiliStabile;
        model.FloorsCount = dto.NumeroPiani;
        model.NumeroPostiAutoCoperti = dto.NumeroPostiAutoCoperti;
        model.NumeroPostiAutoCopertiStabile = dto.NumeroPostiAutoCopertiStabile;
        model.NumeroPostiAutoScoperti = dto.NumeroPostiAutoScoperti;
        model.NumeroPostiAutoScopertiStabile = dto.NumeroPostiAutoScopertiStabile;
        model.NumeroScale = dto.NumeroScale;
        model.TerraceCount = dto.NumeroTerrazzi;
        model.NumeroVerande = dto.NumeroVerande;
        model.Parabola = dto.Parabola;
        model.PiscinaCondominiale = dto.PiscinaCondominiale;
        model.HasPrivateSwimmingPool = this.commonsMapper.ndYesNo2Boolean(dto.PiscinaPrivata);
        model.HasReception = this.commonsMapper.ndYesNo2Boolean(dto.Portineria);
        model.Ripostiglio = dto.Ripostiglio;
        model.SalaHobby = dto.SalaHobby;
        model.Scala = dto.Scala;
        model.Solaio = dto.Solaio;
        model.Soppalco = dto.Soppalco;
        model.SpeseCondominialiMensili = dto.SpeseCondominialiMensili;
        model.InternalStatus = dto.StatoInterno || 1;
        model.StatoStabile = dto.StatoStabile || 1;
        model.Studio = dto.Studio;
        model.Taverna = dto.Taverna;
        model.TipoBox = dto.TipoBox || 1;
        model.TipoCondizionamento = dto.TipoCondizionamento || 1;
        model.TipoCostruzione = dto.TipoCostruzione || 1;
        model.KitchenType = dto.TipoCucina || 1;
        model.TipoEdilizia = dto.TipoEdilizia || 1;
        model.TipoEsposizione = dto.TipoEsposizione || 1;
        model.TipoFacciataEsterna = dto.TipoFacciataEsterna || 1;
        model.TipoFacciataInterna = dto.TipoFacciataInterna || 1;
        model.TipoIngresso = dto.TipoIngresso || 1;
        model.TipoOrientamento = dto.TipoOrientamento || 1;
        model.TipoPavimentoBagno = dto.TipoPavimentoBagno || 1;
        model.TipoPavimentoCamere = dto.TipoPavimentoCamere || 1;
        model.TipoPavimentoCucina = dto.TipoPavimentoCucina || 1;
        model.TipoPavimentoSoggiorno = dto.TipoPavimentoSoggiorno || 1;
        model.Floor = dto.TipoPiano || 1;
        model.TipoRiscaldamentoApparecchi = dto.TipoRiscaldamentoApparecchi || 1;
        model.TipoRiscaldamentoFonte = dto.TipoRiscaldamentoFonte || 1;
        model.TipoRiscaldamentoImpianto = dto.TipoRiscaldamentoImpianto || 1;
        model.TipoSalone = dto.TipoSalone || 1;
        model.TipoSerramenti = dto.TipoSerramenti || 1;
        model.TipoSoggiorno = dto.TipoSoggiorno || 1;
        model.IsLastFloor = this.commonsMapper.ndYesNo2Boolean(dto.UltimoPiano);
        model.VicinanzaMezzi = dto.VicinanzaMezzi;
        model.HasVideoIntercom = this.commonsMapper.ndYesNo2Boolean(dto.VideoCitofono);

        model.EnergeticCertification = new EnergeticCertificationModel();
        model.EnergeticCertification.EnergyClass = dto.TipoClasseEnergetica || 1;
        model.EnergeticCertification.IPE = dto.IPE;
        model.EnergeticCertification.IPEUnit = dto.IPEMisura;
        // model.EnergyClass = dto.TipoClasseEnergetica || 1;
        // model.IPE = dto.IPE;
        // model.IPEUnit = dto.IPEMisura;
        return model;
    }

    /**
     * 
     * @param dto 
     */
    attivitaStoricoImmobileDTO2ActivityLogRealEstateModel(dto: AttivitaStoricoImmobileDTO): ActivityLogRealEstateModel {

        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ActivityLogRealEstateModel();
        model.Customers = dto.Clienti;
        model.Comment = dto.Commento;
        model.EndDate = this.utility._JSON2Date_(dto.DataInizio);
        model.EndDate = this.utility._JSON2Date_(dto.DataFine);
        model.Notes = dto.Note;
        model.ActivityType = dto.TipoAttivita;
        model.ResultType = dto.TipoEsito;

        return model;
    }

    /**
     * 
     * @param dto 
     */
    immagineDTO2ImageModel(dto: ImmagineDTO): ImageModel {

        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ImageModel();
        model.Nome = dto.Nome;
        model.DataImmissione = this.utility._JSON2Date_(dto.DataImmissione);
        model.Didascalia = dto.Didascalia;
        model.IdImmobile = dto.IdImmobile;
        model.Larghezza = dto.Larghezza;
        model.Lunghezza = dto.Lunghezza;
        model.Ordine = dto.Ordine;
        model.TipoImmagine = dto.TipoImmagine;
        model.Url = dto.Url;
        model.UrlThumb = dto.UrlThumb;

        return model;
    }

    /**
     * 
     * @param dto 
     */
    gestioneDTO2ManagementModel(dto: GestioneDTO): ManagementModel {

        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ManagementModel();
        model.IdCollaborator = dto.IdCollaboratore;
        model.RegistrationNumber = dto.Matricola;
        model.Role = dto.Ruolo;
        model.CompanyName = dto.RagioneSociale;
        model.CollaborationStart = this.utility._JSON2Date_(dto.CollaborazioneInizio);
        model.CollaborationEnd = this.utility._JSON2Date_(dto.CollaborazioneFine);

        return model;
    }

    /**
     * 
     * @param dto 
     */
    gestioneMiniDTO2ManagementMiniModel(dto: GestioneMiniDTO): ManagementMiniModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new ManagementMiniModel();
        model.IdAgenzia = dto.IdAgenzia;
        model.IdGestione = dto.IdGestione;
        model.NomeAgenzia = dto.NomeAgenzia;

        return model;
    }


    // *** mapping for writing objects ***

    /**
     * 
     * @param model 
     */
    realEstateModel2ImmobileCmd(model: RealEstateModel): ImmobileCmd {

        let immobileCmd = new ImmobileCmd();
        immobileCmd.IdEntita = model.ID;
        immobileCmd.IdGestione = model.IdGestione;
        immobileCmd.ControlCode = model.ControlCode;
        immobileCmd.IdTipologia = model.IdTypology;
        immobileCmd.IdSottotipologia = model.IdSubTypology;
        immobileCmd.Stato = model.Status;
        immobileCmd.StatoProposta = model.ProposalStatus;
        immobileCmd.TipoDestinazioneUso = this.commonsMapper.destinationUseArray2Int(model.IntendedUses);
        immobileCmd.Padre = (model.IdParent > 0) ? model.IdParent : 0;
        immobileCmd.Mq = model.Area;
        immobileCmd.CodiceRiferimento = model.ReferralCode;
        immobileCmd.Descrizione = model.Description;
        immobileCmd.DescrizioneSintetica = model.DescriptionShort;

        // sell
        immobileCmd.Vendita = model.IsForSell;
        if (model.SellInformation) {
            immobileCmd.PrezzoVendita = model.SellInformation.Price || 0;
            immobileCmd.DataVariazioneVendita = this.utility._Date2JSON_(model.SellInformation.VariationDate);
        } else {
            immobileCmd.PrezzoVendita = 0;
            immobileCmd.DataVariazioneVendita = this.utility._Date2JSON_(new Date());
        }

        // rent
        immobileCmd.Locazione = model.IsForRent;
        if (model.RentInformation) {
            immobileCmd.PrezzoLocazione = model.RentInformation.Price || 0;
            immobileCmd.DataVariazioneLocazione = this.utility._Date2JSON_(model.RentInformation.VariationDate);
        } else {
            immobileCmd.PrezzoLocazione = 0;
            immobileCmd.DataVariazioneLocazione = this.utility._Date2JSON_(new Date());
        }

        // bare ownership
        immobileCmd.NudaProprieta = model.IsBareOwnership;
        if (model.BareOwnershipInformation) {
            immobileCmd.PrezzoNudaProprieta = model.BareOwnershipInformation.Price || 0;
            immobileCmd.DataVariazioneNudaProprieta = this.utility._Date2JSON_(model.BareOwnershipInformation.VariationDate);
        } else {
            immobileCmd.PrezzoNudaProprieta = 0;
            immobileCmd.DataVariazioneNudaProprieta = this.utility._Date2JSON_(new Date());
        }

        if (model.Address) {
            immobileCmd.Indirizzo = this.commonsMapper.addressModel2IndirizzoCmd(model.Address);
        }

        if (model.Notes) {
            immobileCmd.NotePrivate = model.Notes.Private;
            immobileCmd.NotePubbliche = model.Notes.Public;
        }
        immobileCmd.VirtualTour = model.HasVirtualTour;
        immobileCmd.VirtualTourUrl = model.VirtualTourUrl;
        immobileCmd.CodiceImmobileMandante = model.CodiceImmobileMandante;
        immobileCmd.IsFrazionato = model.IsSplitted;
        immobileCmd.CodiceFrazionato = model.CodiceFrazionato;
        immobileCmd.InSubincarico = model.InSubincarico;
        //immobileCmd.Gestionemandante = (dto.GestioneMandante != null) ? dto.GestioneMandante.IdEntita : 0, // TODO: fix

        if (model.Customers) {
            immobileCmd.Clienti = new Array<number>();
            model.Customers.forEach((ref, i, aray) => {
                immobileCmd.Clienti.push(ref.ID);
            });
        }

        if (model.ActiveAssignment) {
            immobileCmd.Incarico = this.realEstateAssignmentModel2IncaricoCmd(model.ActiveAssignment);

            immobileCmd.Incarico.TipoDurata = 0; // TODO: fix
            immobileCmd.Incarico.TipoIncarico = model.ActiveAssignment.Type; // TODO: fix



            // *** WORKAROUND: DON'T DELETE THIS! ***
            immobileCmd.Incarico.IdGestione = model.IdGestione;
        }

        if (model.ApartmentCard) {
            immobileCmd.SchedaAppartamento = this.apartmentCardModel2ImmobileSchedaAppartamentoDTO(model.ApartmentCard);
        }

        if (model.EnergyPerformanceCertificate)
            immobileCmd.ClasseEnergeticaNew = this.energyPerformanceCertificateModel2ImmobileClasseEnergeticaNewDTO(model.EnergyPerformanceCertificate);

        // 
        immobileCmd.IncrocioAutomatico = false;

        return immobileCmd;
    }



    realEstateAssignmentModel2IncaricoCmd(model: RealEstateAssignmentModel): IncaricoCmd {
        let incaricoCmd = new IncaricoCmd();
        incaricoCmd.IdEntita = model.ID;
        incaricoCmd.ControlCode = model.ControlCode;
        incaricoCmd.IdGestione = 0; // TODO: fix?
        incaricoCmd.Descrizione = model.Description; // TODO: fix
        incaricoCmd.CodicePadre = 0; // TODO: fix
        incaricoCmd.DataInizio = this.utility._Date2JSON_(model.StartDate);
        incaricoCmd.DataPrimaScadenza = this.utility._Date2JSON_(model.FirstExpirationDate);
        incaricoCmd.DataSecondaScadenza = this.utility._Date2JSON_(model.SecondExpirationDate);
        incaricoCmd.DataTerzaScadenza = this.utility._Date2JSON_(model.ThirdExpirationDate);
        incaricoCmd.Durata = 0; // TODO: fix

        incaricoCmd.Locazione = model.Rent;
        incaricoCmd.PrezzoLocazione = model.RentPrice;
        incaricoCmd.ProvvigioneAffitto = model.RentCommission;

        incaricoCmd.Vendita = model.Sell;
        incaricoCmd.PrezzoVendita = model.SellPrice;
        incaricoCmd.ProvvigioneVendita = model.SellCommission;

        incaricoCmd.NudaProprieta = model.BareOwnership;
        incaricoCmd.PrezzoNudaProprieta = model.BareOwnershipPrice;
        incaricoCmd.ProvvigioneNudaProprieta = model.BareOwnershipCommission;

        incaricoCmd.Note = model.Notes;


        incaricoCmd.ProvvigioneAffittoAcquirente = 0; // TODO: fix
        incaricoCmd.ProvvigioneNudaProprietaAcquirente = 0; // TODO: fix
        incaricoCmd.ProvvigioneVenditaAcquirente = 0; // TODO: fix

        incaricoCmd.RinnovoAutomatico = model.AutoRenew;
        incaricoCmd.Stato = model.Status;
        incaricoCmd.StatoPrimoContatto = model.StatoPrimoContatto;
        incaricoCmd.TipoDurata = 0; // TODO: fix
        incaricoCmd.TipoIncarico = 0; // TODO: fix


        //if(model.Immobili){
        incaricoCmd.Immobili = new Array<number>();

        incaricoCmd.ImmobiliPassati = new Array<number>();
        //}

        incaricoCmd.Referenti = new Array<IncaricoClienteReferenteCmd>();

        return incaricoCmd;
    }

    saveSchedaTipologiaUso(model: ApartmentCardModel): boolean {

        if (!model)
            return false;

        return (
            model.InternalStatus !== 1
            ||
            model.LevelsCount !== 0
        );
    }



    saveClasseEnergeticaNew(model: RealEstateModel): boolean {
        if (!model.EnergyPerformanceCertificate)
            return false;

        return (
            model.EnergyPerformanceCertificate.IndiceRinnovabile !== undefined
            ||
            model.EnergyPerformanceCertificate.EPGlobale !== undefined
            ||
            model.EnergyPerformanceCertificate.EnergiaZero !== undefined
            ||
            model.EnergyPerformanceCertificate.Inverno !== undefined
            ||
            model.EnergyPerformanceCertificate.Estate !== undefined
        );
    }

    /**
     * 
     * @param customer 
     */
    createRealEstateSaveRequest(model: RealEstateModel): ImmobileSaveRequest {

        if (!model)
            throw new Error("argument cannot be null");

        var request = new ImmobileSaveRequest();
        request.Immobile = this.realEstateModel2ImmobileCmd(model);
        request.SaveClasseEnergeticaNew = (!request.Immobile.ClasseEnergeticaNew) ? false : this.saveClasseEnergeticaNew(model);
        request.SaveClienti = true;
        request.SaveCollaboratori = false;
        request.SaveIncarico = false;
        request.SaveReferenti = false;
        request.SaveSchedaAltriDati = false;//true;
        request.SaveSchedaConcorrenza = false;
        request.SaveSchedeTipologiaUso = (!request.Immobile.SchedaAppartamento) ? false : this.saveSchedaTipologiaUso(model.ApartmentCard);
        request.SaveSchedaLss = false;
        request.SaveSchedaMls = false;
        
        return request;
    }


    createImageSaveRequest(model: ImageModel): ImmagineSaveRequest {
        if (!model)
            throw new Error("argument cannot be null");

        let image = new ImmagineCmd();
        image.Data = model.BinaryData;
        image.IdImmobile = model.IdImmobile;
        image.Didascalia = model.Didascalia || ' '; // ATTENTION! space character IS NOT an error! left intentionally couse service need at least 1 character 
        image.TipoImmagine = model.TipoImmagine || 1;

        let nomeImmaginePath = model.Nome || '';//$('#file4').val();
        let name = null;
        if (nomeImmaginePath.indexOf('fakepath\\') > -1) {
            name = nomeImmaginePath.split('fakepath\\')[1];
        } else {
            name = nomeImmaginePath;
        }
        if (name == null)
            name = 'predefinito.jpg';

        image.Nome = name;

        let request = new ImmagineSaveRequest();
        request.Immagine = image;

        return request;
    }

    apartmentCardModel2ImmobileSchedaAppartamentoDTO(model: ApartmentCardModel): ImmobileSchedaAppartamentoDTO {
        if (!model)
            throw new Error("argument cannot be null");

        let dto = new ImmobileSchedaAppartamentoDTO();
        dto.IdEntita = model.ID;
        dto.ControlCode = model.ControlCode;
        dto.Acqua = this.commonsMapper.boolean2NDYesNo(model.HasWaterSupplyConnection);
        dto.AllaccioTelefonico = this.commonsMapper.boolean2NDYesNo(model.HasTelephoneNetworkConnection);
        dto.AnnoCostruzione = model.ConstructionYear;
        dto.AnnoRistrutturazione = model.RenovationYear;
        dto.Antifurto = this.commonsMapper.boolean2NDYesNo(model.HasAlarm);
        dto.Antincendio = this.commonsMapper.boolean2NDYesNo(model.HasAntiFire);
        dto.Arredato = this.commonsMapper.boolean2NDYesNo(model.HasForniture);
        dto.Ascensore = this.commonsMapper.boolean2NDYesNo(model.HasElevator);
        dto.CablaggioInternet = this.commonsMapper.boolean2NDYesNo(model.HasInternetCabling);
        dto.CameraDiServizio = this.commonsMapper.boolean2NDYesNo(model.HasServiceRoom);
        dto.Camino = this.commonsMapper.boolean2NDYesNo(model.HasChimney);
        dto.Cantina = this.commonsMapper.boolean2NDYesNo(model.HasCellar);
        dto.Citofono = this.commonsMapper.boolean2NDYesNo(model.HasIntercom);
        dto.Fognatura = this.commonsMapper.boolean2NDYesNo(model.HasDrainage);
        dto.Gas = this.commonsMapper.boolean2NDYesNo(model.HasGasNetworkConnection);
        dto.GiardinoCondominiale = this.commonsMapper.boolean2NDYesNo(model.HasCommonGarden);
        dto.GiardinoPrivato = this.commonsMapper.boolean2NDYesNo(model.HasPrivateGarden);
        dto.ImpiantoElettricoANorma = this.commonsMapper.boolean2NDYesNo(model.HasCompliantElectricalSystem);
        dto.ImpiantoRiscaldamento = this.commonsMapper.boolean2NDYesNo(model.HasHeatingSystem);
        dto.Interno = model.Interno || '';
        dto.Luce = model.Luce;
        dto.Mansarda = model.Mansarda;
        dto.MqBalconi = model.BalconyArea;
        dto.MqBox = model.GarageArea;
        dto.MqCantina = model.CellarArea;
        dto.MqGiardinoCondominiale = model.CommonGardenArea;
        dto.MqGiardinoPrivato = model.PrivateGardenArea;
        dto.MqMansarda = model.MansardArea;
        dto.MqSolaio = model.LoftArea;
        dto.MqSoppalco = model.MezzanineArea;
        dto.MqTaverna = model.TavernArea;
        dto.MqTerrazzi = model.TerraceArea;
        dto.MqVerande = model.PorchArea;
        dto.NumeroAppartamenti = model.ApartmentsCount;
        dto.NumeroBagni = model.BathroomsCount;
        dto.NumeroBalconi = model.BalconyCount;
        dto.NumeroBox = model.GaragesCount;
        dto.NumeroBoxStabile = model.BuildingGaragesCount;
        dto.NumeroCamereDaLetto = model.BedroomsCount;
        dto.NumeroIngressi = model.EntrancesCount;
        dto.NumeroIngressiStabile = model.NumeroIngressiStabile;
        dto.NumeroLivelli = model.LevelsCount;
        dto.NumeroLocali = model.RoomsCount;
        dto.NumeroPassiCarrabili = model.NumeroPassiCarrabili;
        dto.NumeroPassiCarrabiliStabile = model.NumeroPassiCarrabiliStabile;
        dto.NumeroPiani = model.FloorsCount;
        dto.NumeroPostiAutoCoperti = model.NumeroPostiAutoCoperti;
        dto.NumeroPostiAutoCopertiStabile = model.NumeroPostiAutoCopertiStabile;
        dto.NumeroPostiAutoScoperti = model.NumeroPostiAutoScoperti;
        dto.NumeroPostiAutoScopertiStabile = model.NumeroPostiAutoScopertiStabile;
        dto.NumeroScale = model.NumeroScale;
        dto.NumeroTerrazzi = model.TerraceCount;
        dto.NumeroVerande = model.NumeroVerande;
        dto.Parabola = model.Parabola;
        dto.PiscinaCondominiale = model.PiscinaCondominiale;
        dto.PiscinaPrivata = this.commonsMapper.boolean2NDYesNo(model.HasPrivateSwimmingPool);
        dto.Portineria = this.commonsMapper.boolean2NDYesNo(model.HasReception);
        dto.Ripostiglio = model.Ripostiglio;
        dto.SalaHobby = model.SalaHobby;
        dto.Scala = model.Scala || '';
        dto.Solaio = model.Solaio;
        dto.Soppalco = model.Soppalco;
        dto.SpeseCondominialiMensili = model.SpeseCondominialiMensili;
        dto.StatoInterno = model.InternalStatus || 1;
        dto.StatoStabile = model.StatoStabile || 1;
        dto.Studio = model.Studio;
        dto.Taverna = model.Taverna;
        dto.TipoBox = model.TipoBox || 1;
        dto.TipoCondizionamento = model.TipoCondizionamento || 1;
        dto.TipoCostruzione = model.TipoCostruzione || 1;
        dto.TipoCucina = model.KitchenType || 1;
        dto.TipoEdilizia = model.TipoEdilizia || 1;
        dto.TipoEsposizione = model.TipoEsposizione || 1;
        dto.TipoFacciataEsterna = model.TipoFacciataEsterna || 1;
        dto.TipoFacciataInterna = model.TipoFacciataInterna || 1;
        dto.TipoIngresso = model.TipoIngresso || 1;
        dto.TipoOrientamento = model.TipoOrientamento || 1;
        dto.TipoPavimentoBagno = model.TipoPavimentoBagno || 1;
        dto.TipoPavimentoCamere = model.TipoPavimentoCamere || 1;
        dto.TipoPavimentoCucina = model.TipoPavimentoCucina || 1;
        dto.TipoPavimentoSoggiorno = model.TipoPavimentoSoggiorno || 1;
        dto.TipoPiano = model.Floor || 1;
        dto.TipoRiscaldamentoApparecchi = model.TipoRiscaldamentoApparecchi || 1;
        dto.TipoRiscaldamentoFonte = model.TipoRiscaldamentoFonte || 1;
        dto.TipoRiscaldamentoImpianto = model.TipoRiscaldamentoImpianto || 1;
        dto.TipoSalone = model.TipoSalone || 1;
        dto.TipoSerramenti = model.TipoSerramenti || 1;
        dto.TipoSoggiorno = model.TipoSoggiorno || 1;
        dto.UltimoPiano = this.commonsMapper.boolean2NDYesNo(model.IsLastFloor);
        dto.VicinanzaMezzi = model.VicinanzaMezzi;
        dto.VideoCitofono = this.commonsMapper.boolean2NDYesNo(model.HasVideoIntercom);

        // dto.TipoClasseEnergetica = model.EnergyClass || 1;
        // dto.IPE = model.IPE;
        // dto.IPEMisura = model.IPEUnit;
        if (model.EnergeticCertification) {
            dto.TipoClasseEnergetica = model.EnergeticCertification.EnergyClass || 1;
            dto.IPE = model.EnergeticCertification.IPE;
            dto.IPEMisura = model.EnergeticCertification.IPEUnit;
        }

        return dto;
    }


    immobileClasseEnergeticaNewDTO2EnergyPerformanceCertificateModel(dto: ImmobileClasseEnergeticaNewDTO): EnergyPerformanceCertificateModel {
        if (!dto)
            throw new Error("argument cannot be null");

        let model = new EnergyPerformanceCertificateModel();
        model.IdEntita = dto.IdEntita;
        model.ControlCode = dto.ControlCode;
        model.Descrizione = dto.Descrizione;
        model.IdAgenzia = dto.IdAgenzia;
        model.IdGestione = dto.IdGestione;
        model.IdGruppo = dto.IdGruppo;
        model.NomeAgenzia = dto.NomeAgenzia;
        model.NomeGruppo = dto.NomeGruppo;
        model.TipoGestione = dto.TipoGestione;

        model.ClasseEnergetica = dto.ClasseEnergetica;
        model.EnergiaZero = dto.EnergiaZero;
        model.EPGlobale = dto.EPGlobale;
        model.Estate = dto.Estate;
        model.IndiceRinnovabile = dto.IndiceRinnovabile;
        model.Inverno = dto.Inverno;
        return model;
    }


    energyPerformanceCertificateModel2ImmobileClasseEnergeticaNewDTO(model: EnergyPerformanceCertificateModel): ImmobileClasseEnergeticaNewDTO {
        if (!model)
            throw new Error("argument cannot be null");

        let dto = new ImmobileClasseEnergeticaNewDTO();
        dto.IdEntita = model.IdEntita;
        dto.ControlCode = model.ControlCode;
        dto.ClasseEnergetica = model.ClasseEnergetica;
        dto.IndiceRinnovabile = model.IndiceRinnovabile;
        dto.EPGlobale = model.EPGlobale;
        dto.Inverno = model.Inverno;
        dto.Estate = model.Estate;
        dto.EnergiaZero = model.EnergiaZero;
        return dto;
    }
}