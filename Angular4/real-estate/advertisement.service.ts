import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {
  AbstractAdvertisementDataService,
  InserzioneDTO
} from 'treeplat-mobile-rest-client';
import { CommonsMapper } from '../shared/commons-mapper';
import { AdvertisementMapper } from './advertisement-mapper';
import { IAdvertisementService } from './advertisement-service.interface';
import { environment } from '../../environments/environment';
import { RealEstateModel } from '../real-estate/models/real-estate.model';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';




@Injectable()
export class AdvertisementService implements IAdvertisementService {

  environment = null;

  constructor(
    //private sessionService: SessionService,
    private dataService: AbstractAdvertisementDataService,
    private commonsMapper: CommonsMapper,
    private mapper: AdvertisementMapper
  ) {
    this.environment = environment;
  }


  publish(model: RealEstateModel, idEditore: number): Observable<boolean> {
    if (!model)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let publishRequest = this.mapper.createPubblicitaPubblicazioneRequest(model, idEditore, model.ID, true);
      this.dataService.publish(this.environment.AdvertisementServiceWcfUrl, publishRequest)
        .subscribe(response => {

          observer.next(response);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client - side or network error occurred.Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  unpublish(model: RealEstateModel, editorID: number): Observable<boolean> {
    if (!model)
      throw new Error("argument cannot be null");

    return Observable.create(observer => {

      let publishRequest = this.mapper.createPubblicitaPubblicazioneRequest(model, editorID, model.ID, true);
      this.dataService.publish(this.environment.AdvertisementServiceWcfUrl, publishRequest)
        .subscribe(response => {

          observer.next(response);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client - side or network error occurred.Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

  isListingPublished(realEstateID: number, editorID: number): Observable<boolean> {
    if (!realEstateID)
      throw new Error("argument cannot be null");

    if (!editorID)
      throw new Error("argument cannot be null");


    return Observable.create(observer => {

      let advRequest = this.mapper.createPubblicitaRequest(editorID, realEstateID);
      this.dataService.getListing(this.environment.AdvertisementServiceWcfUrl, advRequest)
        .subscribe(response => {

          let result: boolean = false;
          let listing: InserzioneDTO = response;

          if (Boolean(listing))
            result = listing.PubblicaImmobile;

          observer.next(result);
          observer.complete();
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client - side or network error occurred.Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
            }
          });
    });
  }

}
