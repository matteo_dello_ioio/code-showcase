import { ReactiveFormsModule } from '@angular/forms';
import { TestBed, async, fakeAsync, inject } from '@angular/core/testing';
import { environment } from '../../environments/environment';
import { UtilityService } from '../shared/utility-service';
import { RealEstateMapper } from './real-estate-mapper';
import { LookupService } from '../services/lookup.service';
import { GeoService } from '../services/geo.service';
import { CommonsMapper } from '../shared/commons-mapper';
import { Typology } from '../shared/models/lookup.models';
import { DestinazioneUso } from '../models/destinazione-uso';
import { TYPOLOGIES_LOOKUP } from '../data/typologies.data';
import { INTENDED_USE_LOOKUP } from '../data/destinazioni-uso.data';
import { RealEstateModel } from './models/real-estate.model';



describe('RealEstateMapper', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: TYPOLOGIES_LOOKUP, useValue: [] },
        { provide: INTENDED_USE_LOOKUP, useValue: [] },
        LookupService,
        UtilityService,
        GeoService,
        CommonsMapper,
        RealEstateMapper
      ],
      imports: [],
    });
  });

  it('should be created', inject([RealEstateMapper], (service: RealEstateMapper) => {
    expect(service).toBeTruthy();
  }));

  describe('realEstateDTO2RealEstateItem method', () => {
    it('should exists',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(service.mapImmobileElencoDTO2RealEstateItem).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(function () { service.mapImmobileElencoDTO2RealEstateItem(null); }).toThrowError();
      })
    );

  });

  describe('realEstateDTO2RealEstateModel method', () => {
    it('should exists',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(service.immobileDTO2RealEstateModel).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(function () { service.immobileDTO2RealEstateModel(null); }).toThrowError();
      })
    );

  });


  describe('createRealEstateSaveRequest method', () => {
    it('should exists',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(service.createRealEstateSaveRequest).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateMapper], (service: RealEstateMapper) => {
        expect(function () { service.createRealEstateSaveRequest(null); }).toThrowError();
      })
    );


    it('should map the request',
      inject([RealEstateMapper], (service: RealEstateMapper) => {

        let model = new RealEstateModel();
        model.ID = 999;
        model.Agency = { Id: 1, Name: '' };
        model.IntendedUses = [{ IdDestinazioneUso: 1, Descrizione: '' }, { IdDestinazioneUso: 8, Descrizione: '' }, { IdDestinazioneUso: 1024, Descrizione: '' }];

        let request = service.createRealEstateSaveRequest(model);

        // expect(request.IdEntita).toBe(999);
        // expect(request.IdAgenzia).toBe(1);
        expect(request.Immobile.TipoDestinazioneUso).toBe(1033);
      })
    );

  });


});