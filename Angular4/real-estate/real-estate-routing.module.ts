import { RouterModule, Routes } from '@angular/router';
import { RealEstateListComponent } from './real-estate-list/real-estate-list.component';
import { RealEstateDetailComponent } from './real-estate-detail/real-estate-detail.component';

import { AuthGuard } from '../services/auth-guard.service';

const realEstateRoutes: Routes = [
	{
		path: '',
		children: [
			{ path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
			{ path: 'lista-affitto', component: RealEstateListComponent, canActivate: [AuthGuard], data: { contractType: 'rent' } },
			{ path: 'lista-vendita', component: RealEstateListComponent, canActivate: [AuthGuard], data: { contractType: 'sell' } },
			{ path: 'lista-affitto/:id', component: RealEstateDetailComponent, canActivate: [AuthGuard], data: { contractType: 'rent' } },
			{ path: 'lista-vendita/:id', component: RealEstateDetailComponent, canActivate: [AuthGuard], data: { contractType: 'sell' } },
			{ path: 'new', component: RealEstateDetailComponent, canActivate: [AuthGuard], data: { isNew: true } },
		]
	}
];

export const RealEstateRoutingModule = RouterModule.forChild(realEstateRoutes);