import { Observable } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { TestBed, async, fakeAsync, inject } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ng2-cookies';
import {
  RealEstateDataService,
  QueryObjectDTO,
  AbstractRealEstateDataService,
  ImmobileTypeAheadRequest
} from 'treeplat-mobile-rest-client';
import { RealEstateService } from './real-estate.service';
import { UtilityService } from '../shared/utility-service';
import { RealEstateMapper } from './real-estate-mapper';
import { LookupService } from '../services/lookup.service';
import { GeoService } from '../services/geo.service';
import { CommonsMapper } from '../shared/commons-mapper';
import { TYPOLOGIES_LOOKUP } from '../data/typologies.data';
import { Typology } from '../shared/models/lookup.models';
import { DestinazioneUso } from '../models/destinazione-uso';
import { INTENDED_USE_LOOKUP } from '../data/destinazioni-uso.data';
import { environment } from '../../environments/environment';
import { SessionService } from '../session/session.service';
//import { AbstractStorageService } from '../session/storage-service.abstract';
import { SessionModule } from '../session/session.module';



let NEW_TYPOLOGIES: Typology[] = [
  {
    IdTipologia: 1,
    Descrizione: 'Appartamento',
    TipiDestinazioneUso: [
      { IdDestinazioneUso: 1, Descrizione: 'Abitativo' },
      { IdDestinazioneUso: 16, Descrizione: 'Turistico' },
      { IdDestinazioneUso: 128, Descrizione: 'Ufficio' }
    ],
    IdDestUsoDefault: 1,
    IdSottotipDefault: 28,
    Sottotipologie: [
      { IdSottotipologia: 1, Descrizione: 'Loft' },
      { IdSottotipologia: 2, Descrizione: 'Mansarda' },
      { IdSottotipologia: 3, Descrizione: 'Attico' },
      { IdSottotipologia: 4, Descrizione: 'Duplex' },
      { IdSottotipologia: 28, Descrizione: 'Appartamento' }
    ]
  }
];

let TEST_DESTINAZIONI_USO: DestinazioneUso[] = [
  { IdDestinazioneUso: 1, Descrizione: 'Abitativo', Ordine: 1 },
  { IdDestinazioneUso: 2, Descrizione: 'Agricolo', Ordine: 2 },
  { IdDestinazioneUso: 4, Descrizione: 'EdificabileCommerciale', Ordine: 3 },
  { IdDestinazioneUso: 16, Descrizione: 'Turistico', Ordine: 4 },
  { IdDestinazioneUso: 32, Descrizione: 'Albergo', Ordine: 5 },
  { IdDestinazioneUso: 128, Descrizione: 'Ufficio', Ordine: 6 },
  { IdDestinazioneUso: 256, Descrizione: 'Laboratorio', Ordine: 7 },
  { IdDestinazioneUso: 1024, Descrizione: 'Commerciale', Ordine: 8 },
  { IdDestinazioneUso: 2048, Descrizione: 'Ristorante', Ordine: 9 },
  { IdDestinazioneUso: 8192, Descrizione: 'Industriale', Ordine: 10 }
];

let mockRealEstateDataService = {
  getAll() { },
  getAllTypeAhead() { },
  getById() { },
  save() { },
  getAllImages() { },
  getAssignmentList() { },
  getNewEnergeticClassById() { }
}

describe('RealEstateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DatePipe,
        //{ provide: AbstractStorageService, useClass: AbstractStorageService },
        CookieService,
        SessionService,
        GeoService,
        { provide: TYPOLOGIES_LOOKUP, useValue: NEW_TYPOLOGIES },
        { provide: INTENDED_USE_LOOKUP, useValue: TEST_DESTINAZIONI_USO },
        LookupService,
        UtilityService,
        CommonsMapper,
        RealEstateMapper,
        //{ provide: AbstractRealEstateDataService, useClass: RealEstateDataService },
        { provide: AbstractRealEstateDataService, useValue: mockRealEstateDataService },
        RealEstateService
      ],
      imports: [
        SessionModule,
        ReactiveFormsModule
      ],
    });
  });

  it('should be created', inject([RealEstateService], (service: RealEstateService) => {
    expect(service).toBeTruthy();
  }));

  describe('readAll method', () => {

    it('should exists',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(service.readAll).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(function () { service.readAll(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock200Response = [{
            "ControlCode": "-1321093706",
            "DataInserimento": "\/Date(1511781740000+0100)\/",
            "DataModifica": "\/Date(1511781740000+0100)\/",
            "Descrizione": "",
            "IdAgenzia": 0,
            "IdEntita": 1175706,
            "IdGestione": 549,
            "IdGruppo": 0,
            "NomeAgenzia": "Admintest pcasa",
            "NomeGruppo": null,
            "TipoGestione": 0,
            "Clienti": [{
              "Descrizione": "Giulia Riccardi",
              "Email": "",
              "IdEntita": 3159631,
              "Stato": 1,
              "Telefoni": [{
                "Numero": "33665222",
                "Tipo": 3
              }]
            }],
            "CodiceFrazionato": 0,
            "CodiceImmobileMandante": 0,
            "CodiceRiferimento": "f6f98313-abf8-4559-8",
            "CondivisioneLSS": false,
            "CondivisioneMLS": false,
            "DataElaborazioneIncrocioAutomatico": "\/Date(-2208992400000+0100)\/",
            "DataVariazioneLocazione": "\/Date(-2208992400000+0100)\/",
            "DataVariazioneNudaProprieta": "\/Date(-2208992400000+0100)\/",
            "DataVariazioneVendita": "\/Date(-2208992400000+0100)\/",
            "DescrizioneSintetica": "",
            "Gestionemandante": null,
            "IdSottotipologia": 28,
            "IdTipologia": 1,
            "InSubincarico": false,
            "IncrocioAutomatico": false,
            "Indirizzo": {
              "ControlCode": "-1878022962",
              "DataInserimento": "\/Date(-2208992400000+0100)\/",
              "DataModifica": "\/Date(-2208992400000+0100)\/",
              "Descrizione": "via palmanova",
              "IdAgenzia": 0,
              "IdEntita": 5235191,
              "IdGestione": 0,
              "IdGruppo": 0,
              "NomeAgenzia": null,
              "NomeGruppo": null,
              "TipoGestione": 0,
              "Cap": "20132",
              "Civico": "100",
              "IdCitta": null,
              "IdComune": 17671,
              "Latitudine": 45.499462500000000,
              "Longitudine": 9.240438600000061
            },
            "IsFrazionato": false,
            "Locazione": false,
            "Mq": 105.00,
            "NotePrivate": "",
            "NotePubbliche": "",
            "NudaProprieta": false,
            "PrezzoLocazione": 0.0000,
            "PrezzoNudaProprieta": 0.0000,
            "PrezzoVendita": 110000.0000,
            "Stato": 2,
            "StatoProposta": 0,
            "TipoDestinazioneUso": 1,
            "Vendita": true,
            "VirtualTour": false,
            "VirtualTourUrl": ""
          }];

          spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock200Response));//.and.callThrough();

          let queryObject = new QueryObjectDTO();
          queryObject.Paging.Start = 0;
          queryObject.Paging.Limit = 15;

          service.readAll(queryObject)
            .subscribe(response => {
              expect(response.length).toBe(1);
              expect(response[0].ID).toBe(1175706);/*
              expect(response.Total).toBe(2);
              expect(response.ResMessage).toBe("Ok");*/
            });

          expect(dataService.getAll).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 400 - Bad Request response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock400Response = null;

          spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock400Response));

          let queryObject = new QueryObjectDTO();
          queryObject.Paging.Start = 0;
          queryObject.Paging.Limit = 15;

          service.readAll(queryObject)
            .subscribe(response => {
              expect(response).toBeFalsy();
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAll).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 401 - Unauthorized response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock401Response = null;

          spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock401Response));

          let queryObject = new QueryObjectDTO();
          queryObject.Paging.Start = 0;
          queryObject.Paging.Limit = 15;

          service.readAll(queryObject)
            .subscribe(response => {/*
              expect(response.ResCode).toBe(401);
              expect(response.ResMessage).toBe('Not logged');
              expect(response.ResData).toBeFalsy();*/
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAll).toHaveBeenCalled();
        })
      )
    );
  });

  describe('realAllImages method', () => {

    it('should exists',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(service.readAllImages).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(function () { service.readAllImages(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock200Response = [{
            "DataImmissione": "\/Date(1512553030000+0100)\/",
            "Didascalia": "Loft_Open_Space_vendita_Milano_foto_print_605701256.jpg",
            "IdEntita": 248,
            "Immaginedata": null,
            "Larghezza": 399,
            "Lunghezza": 600,
            "Nome": "Loft_Open_Space_vendita_Milano_foto_print_605701256.jpg",
            "Ordine": 1,
            "TipoImmagine": 4,
            "Url": "http:\/\/staging.treeplat.it\/WebHandler\/ViewImage.ashx?thumb=0&imgid=248&usr=th3rAgec&pwd=fe3rAdra&cfr=1065164822",
            "UrlThumb": "http:\/\/staging.treeplat.it\/WebHandler\/ViewImage.ashx?thumb=1&imgid=248&usr=th3rAgec&pwd=fe3rAdra&cfr=1065164822"
          }, {
            "DataImmissione": "\/Date(1515074230000+0100)\/",
            "Didascalia": "asd",
            "IdEntita": 261,
            "Immaginedata": null,
            "Larghezza": 0,
            "Lunghezza": 0,
            "Nome": "Schermata 2018-01-03 alle 10.23.51.png",
            "Ordine": 2,
            "TipoImmagine": 2,
            "Url": "http:\/\/staging.treeplat.it\/WebHandler\/ViewImage.ashx?thumb=0&imgid=261&usr=th3rAgec&pwd=fe3rAdra&cfr=-187753643",
            "UrlThumb": "http:\/\/staging.treeplat.it\/WebHandler\/ViewImage.ashx?thumb=1&imgid=261&usr=th3rAgec&pwd=fe3rAdra&cfr=-187753643"
          }];

          spyOn(dataService, 'getAllImages').and.returnValue(Observable.of(mock200Response));//.and.callThrough();

          service.readAllImages(1)
            .subscribe(response => {
              expect(response.length).toBe(2);
              expect(response[0].ID).toBe(248);
            });

          expect(dataService.getAllImages).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 400 - Bad Request response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock400Response = null;

          spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock400Response));

          let queryObject = new QueryObjectDTO();
          queryObject.Paging.Start = 0;
          queryObject.Paging.Limit = 15;

          service.readAll(queryObject)
            .subscribe(response => {
              expect(response).toBeFalsy();
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAll).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 401 - Unauthorized response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock401Response = null;

          spyOn(dataService, 'getAll').and.returnValue(Observable.of(mock401Response));

          let queryObject = new QueryObjectDTO();
          queryObject.Paging.Start = 0;
          queryObject.Paging.Limit = 15;

          service.readAll(queryObject)
            .subscribe(response => {/*
              expect(response.ResCode).toBe(401);
              expect(response.ResMessage).toBe('Not logged');
              expect(response.ResData).toBeFalsy();*/
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAll).toHaveBeenCalled();
        })
      )
    );
  });

  describe('readById method', () => {
    it('should exists',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(service.readById).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(function () { service.readById(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock200Response = { "ControlCode": "1553904881", "DataInserimento": "\/Date(1511861940000+0100)\/", "DataModifica": "\/Date(1512554240000+0100)\/", "Descrizione": "", "IdAgenzia": 0, "IdEntita": 217, "IdGestione": 2152, "IdGruppo": 0, "NomeAgenzia": null, "NomeGruppo": null, "TipoGestione": 0, "Clienti": [{ "ControlCode": null, "IdEntita": 217, "Cliente": { "Descrizione": "Sonzogno SpA - [Giovanni Battista Sonzogno]", "Email": "sonza@gmail.com", "IdEntita": 653, "Stato": 1, "Telefoni": [{ "Numero": "02555555", "Tipo": 1 }, { "Numero": "0288484848", "Tipo": 4 }] } }], "CodiceFiglio": 0, "CodiceFrazionato": 0, "CodiceGenitore": 0, "CodiceImmobileMandante": 0, "CodiceRiferimento": "mk-IV", "Collaboratori": [{ "Descrizione": "Riccardo Promotore Carachino Promotore", "IdEntita": 11077, "IdGestione": 2152 }], "CollaboratoriDisponibili": [{ "Descrizione": "Riccardo Promotore Carachino Promotore", "IdEntita": 11077, "IdGestione": 2152 }], "CondivisioneLSS": false, "CondivisioneMLS": false, "DataElaborazioneIncrocioAutomatico": "\/Date(-2208992400000+0100)\/", "DataVariazioneLocazione": "\/Date(-2208992400000+0100)\/", "DataVariazioneNudaProprieta": "\/Date(-2208992400000+0100)\/", "DataVariazioneVendita": "\/Date(-2208992400000+0100)\/", "DescrizioneRiassuntiva": "Appartamento Milano Via Cortina D'Ampezzo, 10 - Vendita: 530.000,00Locazione (mensile): 5.000,00 - 1200 Mq", "DescrizioneSintetica": "", "FrazioneAbilitato": false, "Frazioni": null, "Gestionemandante": null, "GestionemandanteSub": null, "Giacenze": null, "IdPadre": 0, "IdSottotipologia": 4, "IdTipologia": 1, "Immobilelss": null, "Immobilemls": { "ControlCode": "0", "IdEntita": -1, "AttoProvenienzaCondivisione": false, "AttoProvenienzaPossesso": false, "CertificazioneAgibilitaCondivisione": false, "CertificazioneAgibilitaPossesso": false, "CertificazioneEnergeticaCondivisione": false, "CertificazioneEnergeticaPossesso": false, "CertificazioneImpiantiCondivisione": false, "CertificazioneImpiantiPossesso": false, "DocumentiIpotecariCondivisione": false, "DocumentiIpotecariPossesso": false, "IncaricoVenditaCondivisione": false, "IncaricoVenditaPossesso": false, "NotaTrascrizioneCondivisione": false, "NotaTrascrizionePossesso": false, "Note": "", "PlanimetriaCatastaleCondivisione": false, "PlanimetriaCatastalePossesso": false, "PraticaEdiliziaCondivisione": false, "PraticaEdiliziaPossesso": false, "ProvvigioneMantenimentoCliente": false, "ProvvigioneNote": "", "VisuraCatastaleCondivisione": false, "VisuraCatastalePossesso": false, "VisuraIpotecariaCondivisione": false, "VisuraIpotecariaPossesso": false }, "InSubincarico": false, "IncrocioAutomatico": false, "Indirizzo": { "ControlCode": "137161057", "DataInserimento": "\/Date(-2208992400000+0100)\/", "DataModifica": "\/Date(-2208992400000+0100)\/", "Descrizione": "Via Cortina D'Ampezzo", "IdAgenzia": 0, "IdEntita": 4097246, "IdGestione": 0, "IdGruppo": 0, "NomeAgenzia": null, "NomeGruppo": null, "TipoGestione": 0, "Cap": "20139", "Civico": "10", "IdCitta": null, "IdComune": 17671, "Latitudine": 45.436018800000000, "Longitudine": 9.208083699999975 }, "IsFrazionato": false, "Locazione": true, "LssAbilitato": false, "MlsAbilitato": true, "Mq": 1200.00, "NotePrivate": "", "NotePubbliche": "", "NudaProprieta": false, "Partner": "", "PrezzoLocazione": 5000.0000, "PrezzoNudaProprieta": 0.0000, "PrezzoVendita": 530000.0000, "Referenti": null, "ReferentiDisponibili": [], "SchedeImmobili": { "Frazione": null, "SchedaAlbergo": null, "SchedaAppartamento": { "ControlCode": "0", "IdEntita": -1, "Acqua": 0, "AllaccioTelefonico": 0, "AnnoCostruzione": null, "AnnoRistrutturazione": null, "Antifurto": 0, "Antincendio": 0, "Arredato": 0, "Ascensore": 0, "CablaggioInternet": 0, "CameraDiServizio": 0, "Camino": 0, "Cantina": 0, "Citofono": 0, "Fognatura": 0, "Gas": 0, "GiardinoCondominiale": 0, "GiardinoPrivato": 0, "IPE": 0, "IPEMisura": 0, "ImpiantoElettricoANorma": 0, "ImpiantoRiscaldamento": 0, "Interno": "", "Luce": 0, "Mansarda": 0, "MqBalconi": 0, "MqBox": 0, "MqCantina": 0, "MqGiardinoCondominiale": 0, "MqGiardinoPrivato": 0, "MqMansarda": 0, "MqSolaio": 0, "MqSoppalco": 0, "MqTaverna": 0, "MqTerrazzi": 0, "MqVerande": 0, "NumeroAppartamenti": 0, "NumeroBagni": 0, "NumeroBalconi": 0, "NumeroBox": 0, "NumeroBoxStabile": 0, "NumeroCamereDaLetto": 0, "NumeroIngressi": 0, "NumeroIngressiStabile": 0, "NumeroLivelli": 0, "NumeroLocali": 0, "NumeroPassiCarrabili": 0, "NumeroPassiCarrabiliStabile": 0, "NumeroPiani": 0, "NumeroPostiAutoCoperti": 0, "NumeroPostiAutoCopertiStabile": 0, "NumeroPostiAutoScoperti": 0, "NumeroPostiAutoScopertiStabile": 0, "NumeroScale": 0, "NumeroTerrazzi": 0, "NumeroVerande": 0, "Parabola": 0, "PiscinaCondominiale": 0, "PiscinaPrivata": 0, "Portineria": 0, "Ripostiglio": 0, "SalaHobby": 0, "Scala": "", "Solaio": 0, "Soppalco": 0, "SpeseCondominialiMensili": 0, "StatoInterno": 1, "StatoStabile": 1, "Studio": 0, "Taverna": 0, "TipoBox": 1, "TipoClasseEnergetica": 1, "TipoCondizionamento": 1, "TipoCostruzione": 1, "TipoCucina": 1, "TipoEdilizia": 1, "TipoEsposizione": 1, "TipoFacciataEsterna": 1, "TipoFacciataInterna": 1, "TipoIngresso": 1, "TipoOrientamento": 1, "TipoPavimentoBagno": 1, "TipoPavimentoCamere": 1, "TipoPavimentoCucina": 1, "TipoPavimentoSoggiorno": 1, "TipoPiano": 1, "TipoRiscaldamentoApparecchi": 1, "TipoRiscaldamentoFonte": 1, "TipoRiscaldamentoImpianto": 1, "TipoSalone": 1, "TipoSerramenti": 1, "TipoSoggiorno": 1, "UltimoPiano": 0, "VicinanzaMezzi": "", "VideoCitofono": 0 }, "SchedaBox": null, "SchedaCapannone": null, "SchedaCatasto": { "ControlCode": "0", "IdEntita": -1, "Canoneannuo": 0, "Foglio": "", "Occupato": 0, "Oneri": "", "OneriPagati": "", "Particella": "", "PossessoChiavi": "", "Progetto": 0, "RenditaCatastale": 0, "Scadenzacontratto": null, "StatoLocativo": 1, "Sub": "", "TipoCategoriaCatastale": 2 }, "SchedaConcorrenza": null, "SchedaFrazione": null, "SchedaMutuo": { "ControlCode": "0", "IdEntita": -1, "BancaErogatrice": "", "CapitaleResiduo": 0, "DescrizioneMutuo": "", "Durata": 0, "ResiduoAl": null, "TipoMutuo": 0, "TipoRata": 0 }, "SchedaNegozio": null, "SchedaPalazzina": null, "SchedaRistorante": null, "SchedaTerreno": null, "SchedaUfficio": null, "SchedaUsoTuristico": { "ControlCode": "0", "IdEntita": -1, "DenominazioneLocalitaTuristica": "", "MeteTuristiche": "", "Posizione": "", "TipoLocalita": 1 }, "SchedaUsoUfficio": null, "SchedaVilla": null }, "Stato": 2, "StatoProposta": 0, "TipoDestinazioneUso": 17, "TipoRelazione": 0, "Vendita": true, "VirtualTour": false, "VirtualTourUrl": "" };

          spyOn(dataService, 'getById').and.returnValue(Observable.of(mock200Response));

          service.readById(1)
            .subscribe(response => {
              //expect(response.ResCode).toBe(403);
              //expect(response.Total).toBe(2);
              //expect(response.ResMessage).toBe("Ok");
            });

          expect(dataService.getById).toHaveBeenCalled();
        })
      )
    );



  });

  describe('getAssignmentList method', () => {

    it('should exists',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(service.getAssignmentList).toBeTruthy();
      })
    );

    it('should raise error if parameter is null',
      inject([RealEstateService], (service: RealEstateService) => {
        expect(function () { service.getAssignmentList(null); }).toThrowError();
      })
    );

    it('should handle 200 - Ok response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock200Response = {
            "ControlCode": "",
            "ForceRelogin": true,
            "IdIdentita": 0,
            "MobileVersion": {
              "_Build": 2,
              "_Major": 1,
              "_Minor": 0,
              "_Revision": 9291
            },
            "ResCode": 200,
            "ResMessage": "Ok",
            "SessionChanged": false,
            "ResData": {
              "IncarichiPassati": null,
              "IncaricoAttivo": [{
                "ControlCode": "488719230",
                "IdEntita": 765100,
                "Attivo": true,
                "DataInizio": "\/Date(1511726400000+0100)\/",
                "DataPrimaScadenza": "\/Date(1543262400000+0100)\/",
                "DataSecondaScadenza": null,
                "DataTerzaScadenza": null,
                "Descrizione": "INCARICO DI PIPPUZZO",
                "Locazione": false,
                "Note": "",
                "NudaProprieta": false,
                "PrezzoLocazione": 0.0000,
                "PrezzoNudaProprieta": 0.0000,
                "PrezzoVendita": 150000.0000,
                "ProvvigioneAffitto": 0.00,
                "ProvvigioneNudaProprieta": 0.00,
                "ProvvigioneVendita": 1.00,
                "RinnovoAutomatico": false,
                "StatoIncarico": 1,
                "StatoPrimoContatto": 2,
                "TipoIncarico": 1,
                "Vendita": true
              }]
            },
          };

          spyOn(dataService, 'getAssignmentList').and.returnValue(Observable.of(mock200Response));

          service.getAssignmentList(444)
            .subscribe(response => {/*
              expect(response.ResCode).toBe(200);
              expect(response.Total).toBe(2);
              expect(response.ResMessage).toBe("Ok");*/
            });

          expect(dataService.getAssignmentList).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 400 - Bad Request response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock400Response = {
            "ControlCode": "",
            "ForceRelogin": true,
            "IdIdentita": 0,
            "MobileVersion": {
              "_Build": 2,
              "_Major": 1,
              "_Minor": 0,
              "_Revision": 9291
            },
            "ResCode": 400,
            "ResMessage": "Bad Request",
            "SessionChanged": false,
            "ResData": null
          }

          spyOn(dataService, 'getAssignmentList').and.returnValue(Observable.of(mock400Response));

          service.getAssignmentList(444)
            .subscribe(response => {/*
            expect(response.ResCode).toBe(400);
            expect(response.ResMessage).toBe('Bad Request');
            expect(response.ResData).toBeFalsy();*/
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAssignmentList).toHaveBeenCalled();
        })
      )
    );

    it('should should handle 401 - Unauthorized response',
      async(
        inject([
          AbstractRealEstateDataService,
          RealEstateService
        ], (dataService: AbstractRealEstateDataService, service: RealEstateService) => {

          const mock401Response = {
            "ControlCode": "",
            "ForceRelogin": true,
            "IdIdentita": 0,
            "MobileVersion": {
              "_Build": 2,
              "_Major": 1,
              "_Minor": 0,
              "_Revision": 9291
            },
            "ResCode": 401,
            "ResMessage": "Not logged",
            "SessionChanged": false,
            "ResData": null
          }

          spyOn(dataService, 'getAssignmentList').and.returnValue(Observable.of(mock401Response));

          service.getAssignmentList(444)
            .subscribe(response => {/*
              expect(response.ResCode).toBe(401);
              expect(response.ResMessage).toBe('Not logged');
              expect(response.ResData).toBeFalsy();*/
            }, (error) => {
              expect(error).toBeTruthy();
            });

          expect(dataService.getAssignmentList).toHaveBeenCalled();
        })
      )
    );
  });

});
