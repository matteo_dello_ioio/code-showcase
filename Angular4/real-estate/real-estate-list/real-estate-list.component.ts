
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  QueryObjectDTO,
  FilterDTO
} from 'treeplat-mobile-rest-client';
import { RealEstateService } from '../real-estate.service';
import { RealEstateItem } from '../models/real-estate-item';
import { CurrencyFormatPipe } from '../../custom-pipes/currency-format.pipe';
import { RealEstateSearch } from '../models/real-estate-search.interface';




@Component({
  selector: 'app-real-estate-list',
  styles: [],
  templateUrl: './real-estate-list.component.html',
  //providers: [CurrencyFormatPipe]
})

export class RealEstateListComponent {

  searchParams: RealEstateSearch;


  realEstates: RealEstateItem[];
  contractType: string;

  pagingStart: number = 0;
  pagingLimit: number = 15;


  get hasCustomFilters() {

    // return customfilters;
    return this.realEstateService.hasCustomFilters(this.searchParams);
  }

  constructor(
    private route: ActivatedRoute,
    private realEstateService: RealEstateService
  ) {
    this.searchParams = this.realEstateService.getDefaultSearch();
  }

  ngOnInit() {

    this.route.data.subscribe((routeData) => {
      this.searchParams = this.realEstateService.getDefaultSearch();
      this.contractType = routeData.contractType;
      this.searchParams.ContractType = this.contractType;
      this.search(this.searchParams);
    });
  }

  resetSearch() {
    this.searchParams = this.realEstateService.getDefaultSearch();
    this.search(this.searchParams);
  }

  search(search: RealEstateSearch) {
    this.searchParams = search;
    this.realEstateService.search(search)
      .subscribe(data => {
        this.realEstates = data;
      }, function (error) {
        console.log(error);
      });
  }
}