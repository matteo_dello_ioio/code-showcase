
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "@angular/material";
import { RouterTestingModule } from "@angular/router/testing";
import { RealEstateListComponent } from "./real-estate-list.component";
import { RealEstateService } from "../real-estate.service";
import { CurrencyFormatPipe } from "../../custom-pipes/currency-format.pipe";
import { CustomPipesModule } from "../../custom-pipes/custom-pipes.module";
//import { RealEstateSearchComponent } from "../real-estate-search/real-estate-search.component";
import { Observable } from "rxjs/Observable";
import { RealEstateItem } from "../models/real-estate-item";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Component, Input } from "@angular/core";




let mockRealEstateService = {
  readById() { },
  readAll() { },
  getActiveAssignment() { },
  readAllImages() { },
  search() { },
  getDefaultSearch() {
    return {
      RealEstateID: null,
      CustomerId: null,
      ContractType: '',
      Status: 0,
      Typology: 0,
      IntendedUseBitmask: null,
      RoomNumber: new Array<number>(),
      Price: {
        Min: null,
        Max: null
      },
      Area: {
        Min: null,
        Max: null
      }
    }
  },
  hasCustomFilters() { }
}


@Component({ selector: 'real-estate-search', template: '' })
class DummyRealEstateSearchComponent {
  @Input() ContractTypeParam: string;
}

describe('RealEstateListComponent', () => {

  let component: RealEstateListComponent;
  let fixture: ComponentFixture<RealEstateListComponent>;

  beforeEach(() => {


    TestBed.configureTestingModule({
      declarations: [
        RealEstateListComponent,
        DummyRealEstateSearchComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        CustomPipesModule
      ],
      providers: [
        { provide: RealEstateService, useValue: mockRealEstateService }
      ],
    });

    spyOn(mockRealEstateService, 'search').and.returnValue(Observable.of(new Array<RealEstateItem>()));

    fixture = TestBed.createComponent(RealEstateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});