import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { APPCONFIG } from '../../config';
import { RealEstateSearch } from '../models/real-estate-search.interface';
import { RealEstateService } from '../real-estate.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Typology } from '../../shared/models/lookup.models';
import { LookupService } from '../../services/lookup.service';
import { DestinazioneUso } from '../../models/destinazione-uso';
import { MdCheckboxChange } from '@angular/material';
import { FormControl } from '@angular/forms';
import { CustomerMiniModel } from '../../shared/models/customer-mini-model';
import { CustomerService } from '../../customers/customer.service';

@Component({
  selector: 'real-estate-search',
  styles: [],
  templateUrl: './real-estate-search.component.html'
})

export class RealEstateSearchComponent implements OnInit, OnDestroy {

  @Input() ContractTypeParam: string;


  @Output() search: EventEmitter<RealEstateSearch> = new EventEmitter<RealEstateSearch>();

  searchParams: RealEstateSearch;

  typologiesLookup: Typology[];
  availableIntendedUseList: DestinazioneUso[];


  customersCtrl: FormControl;
  filteredCustomers: any;
  selectedCustomer: any;


  get hasCustomFilters() {
    return this.service.hasCustomFilters(this.searchParams);
  }

  constructor(
    private lookup: LookupService,
    private service: RealEstateService,
    private customerService: CustomerService,
  ) {
    //console.log(this.searchParams);
    

    this.customersCtrl = new FormControl();
  }

  ngOnInit() {
    this.searchParams = this.service.getDefaultSearch();
    this.searchParams.ContractType = this.ContractTypeParam;
    this.updateIntendedUse(this.searchParams.Typology);
    this.typologiesLookup = this.lookup.getTipologieList();
  }

  ngOnDestroy() {
  }


  onTypologyChanged(event) {
    this.updateIntendedUse(this.searchParams.Typology);
  }

  updateIntendedUse(IdTypology: number) {
    this.availableIntendedUseList = this.lookup.getTipoDestinazioneUsoListByIdTipologia(IdTypology);
  }

  closeSearchOverlay() {
    let body = document.getElementById('body');
    body.classList.remove('overlay-active');
  }

  onSearch(realEstateSearchForm: NgForm) {

    console.log(realEstateSearchForm.value);

    let search: RealEstateSearch = <RealEstateSearch>{};
    search.RealEstateID = realEstateSearchForm.value.RealEstateID;
    if(Boolean(this.selectedCustomer))
      search.CustomerId = this.selectedCustomer.ID;
    search.ContractType = this.ContractTypeParam; //realEstateSearchForm.value.ContractType;
    search.Status = realEstateSearchForm.value.Status;
    search.Typology = realEstateSearchForm.value.Typology;
    search.IntendedUseBitmask = realEstateSearchForm.value.IntendedUseBitmask;
    search.RoomNumber = this.searchParams.RoomNumber;


    search.Price = { Min: realEstateSearchForm.value.Price.Min, Max: realEstateSearchForm.value.Price.Max };
    search.Area = { Min: realEstateSearchForm.value.Area.Min, Max: realEstateSearchForm.value.Area.Max };

    console.log(search);
    this.search.emit(search);

    this.closeSearchOverlay();
  }


  onCustomerSearchChanged(event) {
    console.log(this.customersCtrl.value);
    this.searchCustomer(this.customersCtrl.value);
  }

  searchCustomer(name: string)/*: CustomerItem[]*/ {
    if (!name)
      return;

    this.customerService.readAllTypeAhead(name)
      .subscribe((response) => {
        this.filteredCustomers = response as CustomerMiniModel[];
      }, (err: Error) => {
        console.log("ERROR!");
        return null;
      });
  }

  onCustomerSelected(event: CustomerMiniModel) {
    if (!event || typeof event === 'string')
      return;


    console.log(event);
    this.selectedCustomer = event;
    this.filteredCustomers.splice();
    this.customersCtrl.setValue(null);
  }

  displayCustomerFn(obj: any) {
    if (obj)
      return obj.Description;
  }


  onRoomNumberFilterChange(event) {
    //console.log(event);

    if(!Boolean(this.searchParams.RoomNumber))
      this.searchParams.RoomNumber = new Array<number>();

    if(event.source.checked){
      this.searchParams.RoomNumber.push(event.source.value);
    } else {
      let removeAtIndex: number = this.searchParams.RoomNumber.findIndex((el) => { return el == +event.source.value; })
      this.searchParams.RoomNumber.splice(removeAtIndex, 1);
    }
  }

  resetSearch() {
    this.searchParams = this.service.getDefaultSearch();
    //this.search(this.searchParams);

    this.search.emit(this.searchParams);
  }
}