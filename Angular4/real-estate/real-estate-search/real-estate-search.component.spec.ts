
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "@angular/material";
import { RouterTestingModule } from "@angular/router/testing";
import { RealEstateService } from "../real-estate.service";
import { CurrencyFormatPipe } from "../../custom-pipes/currency-format.pipe";
import { CustomPipesModule } from "../../custom-pipes/custom-pipes.module";
import { RealEstateSearchComponent } from "../real-estate-search/real-estate-search.component";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { RealEstateSearch } from "../models/real-estate-search.interface";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LookupService } from "../../services/lookup.service";
import { CustomerService } from "../../customers/customer.service";



let mockupLookupService = {
  getTipoDestinazioneUsoListByIdTipologia() { },
  getTipologieList() { }
}

let mockRealEstateService = {
  readById() { },
  readAll() { },
  getActiveAssignment() { },
  readAllImages() { },
  search() { },
  getDefaultSearch () {
    return {
      RealEstateID: null,
      CustomerId: null,
      ContractType: '',
      Status: 0,
      Typology: 0,
      IntendedUseBitmask: null,
      RoomNumber: new Array<number>(),
      Price: {
        Min: null,
        Max: null
      },
      Area: {
        Min: null,
        Max: null
      }
    }
  },
  hasCustomFilters() { }
}

describe('RealEstateSearchComponent', () => {

  let component: RealEstateSearchComponent;
  let fixture: ComponentFixture<RealEstateSearchComponent>;


  let priceMinElement: DebugElement;
  let priceMaxElement: DebugElement;
  let submitSearchElement: DebugElement;


  let mockCustomerService = {
    
  }
  
  beforeEach(() => {

    // let mockRealEstateService = {
    //   readAll() {
    //     return Promise.resolve({
    //       ResCode: 200,
    //       ResMessage: "Ok",
    //       // ResData: [
    //       //   { id: 1, firstName: 'Mickey', lastName: 'Mouse' }
    //       // ],
    //       Total: 1
    //     });
    //   }
    // }

    TestBed.configureTestingModule({
      declarations: [
        RealEstateSearchComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        CustomPipesModule
      ],
      providers: [
        { provide: RealEstateService, useValue: mockRealEstateService },
        { provide: LookupService, useValue: mockupLookupService },
        { provide: CustomerService, useValue: mockCustomerService }
      ],
    });
    fixture = TestBed.createComponent(RealEstateSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    priceMinElement = fixture.debugElement.query(By.css('#PriceMin'));
    priceMaxElement = fixture.debugElement.query(By.css('#PriceMax'));
    submitSearchElement = fixture.debugElement.query(By.css('[name="SearchButton"]'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  
});