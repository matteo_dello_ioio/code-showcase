
import { Injectable } from '@angular/core';
import { UtilityService } from '../shared/utility-service'
import { environment } from '../../environments/environment';
import {
    PubblicitaPubblicazioneRequest,
    InserzioneDTO,
    PubblicitaRequest
} from 'treeplat-mobile-rest-client';
import { RealEstateModel } from '../real-estate/models/real-estate.model';
import { RealEstateAssignmentModel } from '../real-estate/models/real-estate-assignment.model';



@Injectable()
export class AdvertisementMapper {

    constructor() {

    }

    createPubblicitaRequest(idEditore: number, idImmobile: number): PubblicitaRequest {
        let dto = new PubblicitaRequest();
        dto.IDEditore = idEditore;
        dto.IDImmobile = idImmobile;
        return dto;
    }

    createPubblicitaPubblicazioneRequest(model: RealEstateModel, idEditore: number, idImmobile: number, pubblica: boolean): PubblicitaPubblicazioneRequest {
        let dto = new PubblicitaPubblicazioneRequest();
        dto.IDEditore = idEditore;
        dto.IDImmobile = idImmobile;
        dto.Inserzione = this.realEstateModel2InserzioneDTO(model);
        dto.Pubblica = pubblica;
        dto.WithInserzione = false;
        return dto;
    }

    realEstateModel2InserzioneDTO(model: RealEstateModel): InserzioneDTO {
        let dto = new InserzioneDTO();
        dto.IDEditore = 0;
        dto.IDImmobile = 0;
        dto.IDInserzione = 0;
        dto.PubblicaImmobile = true;

        dto.PubblicaIndirizzo = true;
        dto.PubblicaCivico = true;

        if (model.IsForSell)
            dto.PubblicaPrezzoVendita = model.SellInformation.Price;

        if (model.IsForRent)
            dto.PubblicaPrezzoLocazione = model.RentInformation.Price;

        if (model.IsBareOwnership)
            dto.PubblicaPrezzoNudaProprieta = model.BareOwnershipInformation.Price;

        dto.PubblicaMq = true;
        dto.PubblicaLocali = true;
        dto.PubblicaStatoLocativo = true;
        dto.PubblicaCondizioni = true;
        dto.PubblicaMappa = true;
        dto.PubblicaVirtualTour = true;
        dto.PubblicaNCamereDaLetto = true;
        dto.PubblicaNBagni = true;
        dto.PubblicaCucina = true;
        dto.PubblicaCantina = true;
        dto.PubblicaNBalconi = true;
        dto.PubblicaNTerrazzi = true;
        dto.PubblicaAscensore = true;
        dto.PubblicaNPostiAuto = true;
        dto.PubblicaNBox = true;
        dto.PubblicaRiscaldamento = true;
        dto.PubblicaPortineria = true;
        dto.PubblicaCitofono = true;
        dto.PubblicaGiardino = true;
        dto.PubblicaPiscina = true;
        dto.PubblicaPiano = true;
        dto.PubblicaSpeseCondominio = true;
        dto.PubblicaPianiStabile = true;
        dto.PubblicaAnnoCostruzione = true;
        dto.PubblicaAnnoRistrutturazione = true;
        dto.PubblicaTipoStabile = true;
        dto.PubblicaStatoStabile = true;
        dto.PubblicaEsposizione = true;
        dto.PubblicaCondizionatore = true;

        dto.Quartiere = ''; // TODO: fix
        dto.Zona = ''; // TODO: fix

        dto.PubblicaClasseEnergetica = true;
        dto.NonPubblicareFoto = true;


        return dto;
    }
}